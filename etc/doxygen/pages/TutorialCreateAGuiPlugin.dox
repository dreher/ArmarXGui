/**
\page ArmarXGui-Tutorials-CreateGuiPlugin Tutorial: Creating a new Gui widget for ArmarX

Prerequisites: None

\tableofcontents

\section ArmarXGui-Tutorial-CreateGuiPlugin-introduction Introduction
This tutorial will show you how to create a new ArmarX gui-plugin by implementing a simple timer as a ArmarX gui-plugin.

\attention This tutorial assumes that you have already installed ArmarX (at least ArmarXCore and ArmarXGui.) If you haven't done yet, 
follow the \ref ArmarXCore-Installation "Installation instructions for ArmarXCore" to install them.

\section ArmarXGui-Tutorial-CreateGuiPlugin-getting-started Getting Started
You can use the armarx-package tool to create new packages, gui-plugins, template files required for them, etc. 

\subsection ArmarXGui-Tutorial-CreateGuiPlugin-create-package Create your own package

#If you already have a package in which you would like to add a new qui-plugin, go to the next step \ref ArmarXGui-Tutorial-CreateGuiPlugin-create-gui-plugin.

In order to create a new ArmarX package called "GuiPluginTutorials", execute the following code in the directory where you want to make your package,
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init GuiPluginTutorials
\endcode
or you can use "-d" option to specify the directory.
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init GuiPluginTutorials -d ${ArmarX_DIR}/
\endcode

\subsection ArmarXGui-Tutorial-CreateGuiPlugin-create-gui-plugin Generate template files of your new gui-plugin
Execute the following command in the toplevel directory of your package, all required files relevant to a new gui-plugin called "MyGuiPlugin" will be generated.
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package add gui-plugin MyGuiPlugin
\endcode

Have a look into the directory to check if everything worked as intended. You should find the following files:
\code{.sh}
source/GuiPluginTutorials/gui-plugins/CMakeLists.txt
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin/MyGuiPluginGuiPlugin.h
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin/MyGuiPluginGuiPlugin.cpp
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin/MyGuiPluginWidgetController.h
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin/MyGuiPluginWidgetController.cpp
source/GuiPluginTutorials/gui-plugins/MyGuiPlugin/MyGuiPluginWidget.ui
source/GuiPluginTutorials/gui-plugins/CMakeLists.txt
\endcode

\section ArmarXGui-Tutorial-CreateGuiPlugin-implementing Start Implementing
Next step is implementation. 

ArmarX Gui wigets are created based on Qt. Thus, using Qt Creator is good solution to edit ui files and C++ source.
Open your project on Qt Creator by following the \ref ArmarXCore-Tutorials-sce-edit-the-source-code "Tutorial: Counting with Statecharts -> Edit the source code"; then, you find the file tree like this:

<!--\htmlonly <img src="tutorial-gp-fileTree.png" width="500px"> \endhtmlonly  -->
\image html tutorial-gp-fileTree.png
\note Before "run cmake", you have to add a dependency on the ArmarXGui package (otherwise, you fail the "run cmake".) 
Add the following line into the CMakeLists.txt at the top-level directory of your project.
\code
depends_on_armarx_package(ArmarXGui "OPTIONAL")
\endcode

\subsection ArmarXGui-Tutorial-CreateGuiPlugin-design-gui-widget Design gui widget on QT-creator
The simple timer needs two buttons to start and stop and a text box to display the time on the widget.
Open MyGuiPluginWidget.ui from the file tree on the left side of Qt Creator, add Qt UI elements by selecting from the left-side list, and set paramters like the following way:

\li Open MyGuiPluginWidget.ui
<!--\htmlonly <img src="tutorial-gp-open-ui.png" width="1000px"> \endhtmlonly-->
\image html tutorial-gp-open-ui.png

\li Add "push botton" on the widget. Layout elements help to align child UI elements. 
Visit Qt's HP (for example <a href="http://doc.qt.io/qt-5/qtexamplesandtutorials.html">Qt Examples And Tutorials</a>) to know how to design GUI by Qt and on Qt Creator.
<!--\htmlonly <img src="tutorial-gp-add-button.png" width="1000px"> \endhtmlonly-->
\image html tutorial-gp-add-button.png

\li Change the instance name and initial text of the button. Repeat the same process for "stop" button.
<!--\htmlonly <img src="tutorial-gp-set-button-property.png" width="1000px"> \endhtmlonly-->
\image html tutorial-gp-set-button-property.png

\li Add "label" to show the elapsed time, and change the instance name and initial text of the label.
<!--\htmlonly <img src="tutorial-gp-add-text.png" width="1000px"> \endhtmlonly-->
\image html tutorial-gp-add-text.png

\li The following is the sample design.
<!--\htmlonly <img src="tutorial-gp-last-design.png" width="1000px"> \endhtmlonly-->
\image html tutorial-gp-last-design.png

\subsection ArmarXGui-Tutorial-CreateGuiPlugin-edit-source Edit the source code

Next step is adding some souce code into the template files. 
Now, you have two kinds of classes for the new gui-plugin: 
\li MyGuiPluginGuiPlugin and 
\li MyGuiPluginWidgetController. 

MyGuiPluginGuiPlugin is something like an interface between your gui-plugin and ArmarXGui and MyGuiPluginWidgetController is the class to define and control the functionality of your gui-plugin. 
Therefore, you should add some code in MyGuiPluginWidgetController. 

For MyGuiPluginWidgetController.h, add QTimer for periodic task of your gui-plugin and two slots related to the Qt's PushButton operation.
\code{.h}
    public slots:
        // QT slot declarations
        void updateGui();
        void stopButtonPressed();  
        void startButtonPressed(); 

    private:
        Ui::MyGuiPluginWidget widget;
        QTimer* updateTimer;      
\endcode

For MyGuiPluginWidgetController.cpp, implement QTimer and Qt slots.

1) Create QTimer
\code{.cpp}
MyGuiPluginWidgetController::MyGuiPluginWidgetController()
{
    widget.setupUi(getWidget());
    updateTimer = new QTimer(this); // <-- add this line
}
\endcode

2) Connect Qt's signals and slots
\code{.cpp}
void MyGuiPluginWidgetController::onConnectComponent()
{
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateGui()));                  // <-- add this line
    connect(widget.buttonStart, SIGNAL(clicked()), this, SLOT(startButtonPressed()));  // <-- add this line
    connect(widget.buttonStop, SIGNAL(clicked()), this, SLOT(stopButtonPressed()));    // <-- add this line
}
\endcode

3) Implement the slots
\code{.cpp}
void MyGuiPluginWidgetController::startButtonPressed()
{
    ARMARX_INFO << getWidgetName() << ": startButtonPressed()";
    // start the qt-timer
    updateTimer->start(1000);
    tStart = IceUtil::Time::now();
    widget.labelTimer->setText("00:00:00.000");
}

void MyGuiPluginWidgetController::stopButtonPressed()
{

    ARMARX_INFO << getWidgetName() << ": stopButtonPressed()";
    // stop the qt-timer
    updateTimer->stop();
}

void MyGuiPluginWidgetController::updateGui()
{
    // update the text of widget.labelTimer
    IceUtil::Time tDiff = IceUtil::Time::now() - tStart;
    QString qTxt = QString::fromStdString(tDiff.toDuration());
    widget.labelTimer->setText(qTxt);
}
\endcode


\subsection ArmarXGui-Tutorial-CreateGuiPlugin-build Build your package
Then, you can build your package/gui-plugin. All you need to do for building is hammering the following lines into the console:

\code{.sh}
    cd ${YourPackageDirectory}/build
    cmake ..
    make
\endcode

\section ArmarXGui-Tutorial-CreateGuiPlugin-execute Load your new gui-plugin from ArmarXGui
Finally, you can use your new gui-plugin!! 

\li Start ice
\note  Ice needs to be started only once on the system. If you have already started Ice, skip this process.

ArmarX relies on Ice (the Internet communication engine) to enable communication between its components.
Before you can use ArmarX functionality, such as the ArmarXGui, you need to start Ice. 
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx start
\endcode

\li Start ArmarXGui
Then, let's pen ArmarXGui by using the following command:
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx gui
\endcode
or
\code{.sh}
${ArmarX_DIR}/Gui/build/bin/ArmarXGuiRun
\endcode

\li Load your new gui-plugin <br>
First, you have to load the library of your gui-plugin in order to open it on ArmarXGui. On ArmarXGui, "File" -> "Load Plugin" -> select your library (libMyGuiPluginGuiPlugin.so in this case).  
Then, you can select your library from "Add widget" as follows.
\image html tutorial-gp-AmarXGui.png
\note The name of your gui-plugin presented in "Add widget" tree is defined in the getWidgetName() in MyGuiPluginWidgetController.h. You can determin the hierarchy of the tree by using "." for the name.
\code{.h}
        virtual QString getWidgetName() const
        {
            return "Tutorial.MyGuiPlugin";
        }
\endcode


\li Execute your simple timer!! 
\image html tutorial-gp-simpletimer.png

*/
