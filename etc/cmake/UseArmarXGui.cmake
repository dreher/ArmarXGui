add_definitions(-DQT_DISABLE_DEPRECATED_BEFORE=0x040800)

function(armarx_check_qt4)
    find_package(Qt4 QUIET)
    if(NOT QT_FOUND)
        message(FATAL "ARMARX_USE_QT5 = ${ARMARX_USE_QT5} but QT_FOUND = ${QT_FOUND}")
    else()
        message(STATUS "ARMARX_USE_QT5 = ${ARMARX_USE_QT5} and QT_FOUND = ${QT_FOUND}")
    endif()
endfunction(armarx_check_qt4)

if(ARMARX_USE_QT5)
    #https://forum.qt.io/topic/81252/cmake-qt5_wrap_ui-issue
    # > Since QT5_WRAP_UI macro is delivered as part of Qt5Widget packacge it is necessery
    # > to make a call to find_package (Qt5Widget) before issuing QT5_WRAP_UI call.
    find_package(Qt5 COMPONENTS Widgets QUIET)
    if(NOT Qt5_FOUND)
        message(FATAL "ARMARX_USE_QT5 = ${ARMARX_USE_QT5} but Qt5_FOUND = ${Qt5_FOUND}")
    else()
        message(STATUS "ARMARX_USE_QT5 = ${ARMARX_USE_QT5} and Qt5_FOUND = ${Qt5_FOUND}")
    endif()
    find_package(Qt5LinguistTools)
else()
    armarx_check_qt4()
endif()

string(REGEX REPLACE "/[^/]+$" "" QT_EXECUTABLE_PRE "${QT_MOC_EXECUTABLE}")

set(QT_UIC_EXECUTABLE "${QT_EXECUTABLE_PRE}/uic")
set(QT_RCC_EXECUTABLE "${QT_EXECUTABLE_PRE}/rcc")

function(armarx_gui_library PLUGIN_NAME SOURCES QT_MOC_HDRS QT_UIS QT_RESOURCES COMPONENT_LIBS)
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXGui")
        armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")
        if(NOT ArmarXGui_FOUND)
            return()
        endif()
    endif()

    armarx_find_qt_base("QtCore;QtGui;QtOpenGL;QtXml;QtScript;QtDesigner" REQUIRED)
    if(NOT ARMARX_USE_QT5)
        include_directories(${QT_INCLUDE_DIRS})
    endif()

    if(${VERBOSE})
        message(STATUS "        QT_MOC_HDRS:")
        printlist("            " "${QT_MOC_HDRS}")
        message(STATUS "        QT_UIS:")
        printlist("            " "${QT_UIS}")
    endif()
    add_definitions(" -DQ_COMPILER_INITIALIZER_LISTS ")

    if(NOT "ArmarXGuiBase" STREQUAL "${PLUGIN_NAME}")
        if(COMPONENT_LIBS)
            list(APPEND COMPONENT_LIBS ArmarXCore ArmarXGuiBase ${QT_LIBRARIES})
        else()
            set(COMPONENT_LIBS ArmarXCore ArmarXGuiBase ${QT_LIBRARIES})
        endif()
    endif()

    include_directories(
        ${CMAKE_CURRENT_BINARY_DIR}
    )

    if(QT_RESOURCES)
        armarx_qt_add_resources( QT_RC_SRCS ${QT_RESOURCES} )
        if(${VERBOSE})
            Message(STATUS "        Resources: " )
            printlist("              " "${QT_RESOURCES};${QT_RC_SRCS}")
        endif()
    endif()
    list(APPEND SOURCES ${QT_RC_SRCS})

    if(ARMARX_USE_QT5)
        set(CMAKE_AUTOMOC "YES")
        set(CMAKE_AUTOUIC "YES")
        set(CMAKE_AUTORCC "YES")
        list(APPEND SOURCES ${QT_MOC_HDRS})
        set(UI_HEADER ${QT_UIS})
    else()
        if(QT_MOC_HDRS)
            armarx_qt_wrap_cpp(SOURCES ${QT_MOC_HDRS})
        endif()

        if(QT_UIS)
            armarx_qt_wrap_ui(UI_HEADER ${QT_UIS})
        endif()
    endif()

    list(APPEND HEADERS ${UI_HEADER})

    printtarget("${HEADERS}" "${SOURCES}" "${QT_RC_SRCS}" "${COMPONENT_LIBS}")

    #message(STATUS "moc_flags: " ${moc_flags})
    #message(STATUS "QT_DEFINITIONS: " ${QT_DEFINITIONS})
    #message(STATUS "QT_USE_FILE: " ${QT_USE_FILE})
    #message(STATUS "QT_LIBRARIES: " ${QT_LIBRARIES})

    armarx_add_library("${PLUGIN_NAME}" "${SOURCES}" "${HEADERS}" "${COMPONENT_LIBS}")

    if(${VERBOSE})
        message(STATUS "        Gui Library Directories:")
        printlist("              " "${ArmarXGui_LIBRARY_DIRS}")
        message(STATUS "        Include Directories: ")
        printlist("              " "${INCLUDE_DIRECTORIES}")
    endif()
endfunction()
