ArmarXGui is distributed under GPLv2.0

installation instructions can be found in

etc/doc/doxygen/installation.dox

The HTML documentation can be generated if CMake and doxygen are
available on the system by running the following commands

mkdir build
cd build
cmake ..
make doc

