<!-- ArmarX properties -->

<property name="ArmarX.ApplicationName">
  <description>Application name</description>
  <attributes>
    <attribute name="ArmarX.Default">""</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.CachePath">
  <description>Path for cache files</description>
  <attributes>
    <attribute name="ArmarX.Default">${HOME}/.armarx/mongo/.cache</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.Config">
  <description>Comma-separated list of configuration files </description>
  <attributes>
    <attribute name="ArmarX.Default">""</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DataPath">
  <description>Semicolon-separated search list for data files</description>
  <attributes>
    <attribute name="ArmarX.Default">""</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DefaultPackages">
  <description>List of ArmarX packages which are accessible by default</description>
  <attributes>
    <attribute name="ArmarX.Default">ArmarXCore, ArmarXGui, MemoryX, RobotAPI, RobotComponents, RobotSkillTemplates, ArmarXSimulation, VisionX, SpeechX, Armar3, Armar4</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DependenciesConfig">
  <description>Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.</description>
  <attributes>
    <attribute name="ArmarX.Default">./config/dependencies.cfg</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DisableLogging">
  <description>Turn logging off in whole application</description>
  <attributes>
    <attribute name="ArmarX.Default">0</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.EnableProfiling">
  <description>Enable profiling of CPU load produced by this application</description>
  <attributes>
    <attribute name="ArmarX.Default">0</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.RedirectStdout">
  <description>Redirect std::cout and std::cerr to ArmarXLog</description>
  <attributes>
    <attribute name="ArmarX.Default">1</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.RemoteHandlesDeletionTimeout">
  <description>The timeout (in ms) before a remote handle deletes the managed object after the use count reached 0. This time can be used by a client to increment the count again (may be required when transmitting remote handles)</description>
  <attributes>
    <attribute name="ArmarX.Default">3000</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.UseTimeServer">
  <description>Enable using a global Timeserver (e.g. from ArmarXSimulator)</description>
  <attributes>
    <attribute name="ArmarX.Default">0</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.Verbosity">
  <description>Global logging level for whole application</description>
  <attributes>
    <attribute name="ArmarX.Default">Verbose</attribute>
    <attribute name="ArmarX.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.Required">no</attribute>
    <values>
      <value>Debug</value>
      <value>Error</value>
      <value>Fatal</value>
      <value>Important</value>
      <value>Info</value>
      <value>Undefined</value>
      <value>Verbose</value>
      <value>Warning</value>
    </values>
  </attributes>
</property>

<!-- ArmarX.DynamicSimulationDepthImageProvider properties -->

<property name="ArmarX.DynamicSimulationDepthImageProvider.BaseLine">
  <description>The value returned from getBaseline(). It has no other effect.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">0.075000003</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DistanceZFar">
  <description>Distance of the far clipping plain. (DistanceZFar-DistanceZNear should be minimal, DistanceZFar &gt; DistanceZNear)</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">5000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.min">1e-08</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DistanceZNear">
  <description>Distance of the near clipping plain. (If set to small the agent&apos;s model&apos;s inside may be visible</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">20</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.min">1e-08</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud">
  <description>Whether the point cloud is drawn to the given DebugDrawerTopic</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">0</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipPoints">
  <description>Whether to clip the point cloud drawn to the given DebugDrawerTopic</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">1</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipXHi">
  <description>Skip points with x higher than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipXLo">
  <description>Skip points with x lower than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">-25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipYHi">
  <description>Skip points with y higher than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipYLo">
  <description>Skip points with y lower than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">-25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipZHi">
  <description>Skip points with z higher than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_ClipZLo">
  <description>Skip points with z lower than this limit.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">-25000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_DebugDrawerTopic">
  <description>Name of the DebugDrawerTopic</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">DebugDrawerUpdates</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_DrawDelay">
  <description>The time between updates of the drawn point cloud (in ms)</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">1000</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_PointSize">
  <description>The size of a point.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">4</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.DrawPointCloud_PointSkip">
  <description>Only draw every n&apos;th point in x and y direction (n=DrawPointCloud_PointSkip). Increase this whenever the ice buffer size is to small to transmitt the cloud size. (&gt;0)</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">3</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.EnableProfiling">
  <description>enable profiler which is used for logging performance events</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">0</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.FOV">
  <description>Vertical FOV in rad.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">0.785398185</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.FrameRate">
  <description>Frames per second</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">30</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.min">0</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.max">60</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Format">\d+(.\d*)?</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.ImageSize">
  <description>Target resolution of the images. Captured images will be converted to this size.</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">640x480</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>1024x768</value>
      <value>1280x960</value>
      <value>1600x1200</value>
      <value>320x240</value>
      <value>640x480</value>
      <value>768x576</value>
      <value>800x600</value>
      <value>none</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.MinimumLoggingLevel">
  <description>Local logging level only for this component</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">Undefined</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>Debug</value>
      <value>Error</value>
      <value>Fatal</value>
      <value>Important</value>
      <value>Info</value>
      <value>Undefined</value>
      <value>Verbose</value>
      <value>Warning</value>
    </values>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.ObjectName">
  <description>Name of IceGrid well-known object</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">""</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.RobotName">
  <description>The robot</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">Armar3</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.RobotNodeCamera">
  <description>The coordinate system of the used camera</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">DepthCameraSim</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.framerate">
  <description>framerate for the point clouds</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">30</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.min">0</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.max">60</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
  </attributes>
</property>

<property name="ArmarX.DynamicSimulationDepthImageProvider.isEnabled">
  <description>enable the capturing process immediately</description>
  <attributes>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Default">1</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.CaseSensitivity">no</attribute>
    <attribute name="ArmarX.DynamicSimulationDepthImageProvider.Required">no</attribute>
    <values>
      <value>0</value>
      <value>1</value>
      <value>false</value>
      <value>no</value>
      <value>true</value>
      <value>yes</value>
    </values>
  </attributes>
</property>


