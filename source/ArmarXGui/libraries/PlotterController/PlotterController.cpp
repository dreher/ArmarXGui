/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlotterController.h"

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <qwt_legend_label.h>
#else
#include <qwt/qwt_legend_item.h>
#endif

using namespace armarx;

PlotterController::PlotterController(QObject* parent) :
    QObject(parent),
    plotter(new QwtPlot())
{
    qRegisterMetaType<std::map<std::string, VariantPtr>>("std::map<std::string, VariantPtr>");
    widget = new QWidget();
    widget->setMinimumHeight(150);
    //            widget->setMaximumWidth(600);
    //            widget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
    ////////////////
    //  Setup Plotter
    ///////////////
    // panning with the left mouse button
    (void) new QwtPlotPanner(plotter->canvas());

    // zoom in/out with the wheel
    QwtPlotMagnifier* magnifier = new QwtPlotMagnifier(plotter->canvas());
    magnifier->setAxisEnabled(QwtPlot::xBottom, false);

    dynamic_cast<QwtPlotCanvas&>(*plotter->canvas()).setPaintAttribute(QwtPlotCanvas::BackingStore, false); //increases performance for incremental drawing

    QwtLegend* legend = new QwtLegend;
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    legend->setDefaultItemMode(QwtLegendData::Mode::Checkable);
#else
    legend->setItemMode(QwtLegend::CheckableItem);
#endif
    plotter->insertLegend(legend, QwtPlot::BottomLegend);


    plotter->setAxisTitle(QwtPlot::xBottom, "Time (in sec)");
    plotter->enableAxis(QwtPlot::yLeft, false);

    plotter->enableAxis(QwtPlot::yRight, true);
    plotter->setAxisAutoScale(QwtPlot::yRight, true);
    //        plotter->setAutoReplot();


    //            plotter->setCanvasBackground(* new QBrush(Qt::white));

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    connect(legend, SIGNAL(checked(const QVariant&, bool, int)),
            SLOT(legendChecked(const QVariant&, bool)));
#else
    connect(plotter, SIGNAL(legendChecked(QwtPlotItem*, bool)),
            SLOT(showCurve(QwtPlotItem*, bool)));
#endif

    connect(&timer, SIGNAL(timeout()), this, SLOT(updateGraph()));
    stackedLayout = new QStackedLayout(widget);
    stackedLayout->addWidget(plotter);

    barlayout = new QHBoxLayout;
    QWidget* barsWidget = new QWidget();
    barsWidget->setLayout(barlayout);
    barsWidget->setAccessibleName("barsWidget");
    stackedLayout->addWidget(barsWidget);
}

PlotterController::~PlotterController()
{
    ARMARX_DEBUG << "~PlotterController";
    if (!widget.isNull())
    {
        widget->deleteLater();
    }
    timer.stop();
    if (pollingTask)
    {
        pollingTask->stop();
    }
}

QWidget* PlotterController::getPlotterWidget()
{
    return widget;
}

void PlotterController::pollingExec()
{
    std::map<std::string, VariantPtr> newData = getData(selectedDatafields);


    emit newDataAvailable(TimeUtil::GetTime().toMicroSeconds(), newData);
}

void PlotterController::updateGraph()
{
    if (this->__plottingPaused)
    {
        return;
    }
    ScopedLock lock(dataMutex);

    IceUtil::Time curTime = TimeUtil::GetTime();
    // erase old markers
    for (auto& markerChannel : markers)
    {
        for (auto it = markerChannel.second.begin(); it != markerChannel.second.end();)
        {
            if ((curTime - it->first).toSecondsDouble() > shownInterval)
            {
                for (auto elem : it->second)
                {
                    elem.second->detach();
                }
                it = markerChannel.second.erase(it);
            }
            else
            {
                it++;
            }
        }
    }

    GraphDataMap::iterator it = dataMap.begin();

    auto addMarker = [this](IceUtil::Time & age, VariantPtr & var, std::string & datafieldId)
    {
        std::map < std::string, QwtPlotMarkerPtr>& markerSubMap = markers[datafieldId][age];

        std::string value = var->getString();
        auto it2 = markerSubMap.find(value);
        if (it2 != markerSubMap.end())
        {
            QwtPlotMarkerPtr marker = it2->second;
            marker->setXValue(0.001 * age.toMilliSecondsDouble());
        }
        else
        {
            QwtPlotMarkerPtr marker(new QwtPlotMarker());
            marker->setValue(0.001 * age.toMilliSecondsDouble(), 0);
            marker->setLabel(QwtText(QString::fromStdString(value)));
            marker->setLineStyle(QwtPlotMarker::VLine);
            marker->setLabelAlignment(Qt::AlignBottom | Qt::AlignLeft);
            int i = 0;
            for (auto& markerSubMap : markers)
            {
                if (markerSubMap.first == datafieldId)
                {
                    break;
                }
                i++;
            }
            marker->setLinePen(QColor(Qt::GlobalColor((i) % 5 + 13)));
            marker->attach(plotter);
            markerSubMap[value] = marker;
        }

    };

    for (; it != dataMap.end(); ++it)
    {

        QVector<QPointF> pointList;
        pointList.clear();
        auto datafieldId = it->first;
        std::vector<TimeData>& dataVec = it->second;
        //            int newSize = min(size,(int)dataVec.size());
        pointList.reserve(dataVec.size());


        try
        {
            int j = 0;

            for (int i = dataVec.size() - 1; i >= 0 ; --i)
            {
                TimeData& data = dataVec[i];
                IceUtil::Time age = (data.time - curTime);

                if (age.toSecondsDouble() <= shownInterval /*&& age.toMicroSeconds() > 0*/)
                {
                    VariantPtr var = VariantPtr::dynamicCast(data.data);

                    if (var->getInitialized())
                    {
                        float xValue = 0.001 * age.toMilliSecondsDouble();
                        auto type = var->getType();
                        double yValue = 0;
                        if (type == VariantType::Float && var->getFloat() != std::numeric_limits<float>::infinity())
                        {
                            yValue = (var->getFloat());
                        }
                        else if (type == VariantType::Double && var->getDouble() != std::numeric_limits<double>::infinity())
                        {
                            yValue = (var->getDouble());
                        }
                        else if (type == VariantType::Int && var->getInt() != std::numeric_limits<int>::infinity())
                        {
                            yValue = (var->getInt());
                        }
                        else if (type == VariantType::Bool)
                        {
                            yValue = (var->getBool() ? 1 : -1);
                        }
                        else if (type == VariantType::String)
                        {
                            addMarker(age, var, datafieldId);
                        }
                        else
                        {
                            continue;
                        }

                        pointList.push_back(QPointF(xValue, yValue));

                        j++;
                    }
                    else
                    {
                        ARMARX_WARNING << deactivateSpam(3) << "uninitialized field: " << it->first;
                    }

                }
                else
                {
                    break;    // data too old from now
                }
            }
        }
        catch (...)
        {
            handleExceptions();
        }

        QwtSeriesData<QPointF>* pointSeries = new  QwtPointSeriesData(pointList);

        if (pointList.size() > 0 && curves.find(it->first) != curves.end() && bars.find(it->first) != bars.end())
        {
            QwtPlotCurve* curve = curves[it->first];
            curve->setData(pointSeries);
            curve->setYAxis(QwtPlot::yRight);
            QwtThermo* bar = bars[it->first];
            bar->setValue(pointList.first().y());
            float range = curve->maxYValue() - curve->minYValue();
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
            bar->setLowerBound(curve->minYValue() - 0.05 * range);
            bar->setUpperBound((curve->maxYValue()) + range * 0.05);
#else
            bar->setRange(curve->minYValue() - 0.05 * range, (curve->maxYValue()) + range * 0.05); //this autoscales the graph
#endif
        }
    }

    plotter->setAxisAutoScale(QwtPlot::yRight, autoScalePlot);

    //        plotter->setAxisScale( QwtPlot::yLeft, -1, 1);

    plotter->replot();
}

void PlotterController::showCurve(QwtPlotItem* item, bool on)
{
    item->setVisible(on);
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    QwtLegend* lgd = qobject_cast<QwtLegend*>(plotter->legend());

    QList<QWidget*> legendWidgets =
        lgd->legendWidgets(plotter->itemToInfo(item));

    if (legendWidgets.size() == 1)
    {
        QwtLegendLabel* legendLabel =
            qobject_cast<QwtLegendLabel*>(legendWidgets[0]);

        if (legendLabel)
        {
            legendLabel->setChecked(on);
        }
        else
        {
            ARMARX_WARNING << "legendLabel is no QwtLegendLabel";
        }
    }
    else
    {
        ARMARX_WARNING << "found " << legendWidgets.size() << " items for the given curve";
    }
#else
    QwtLegendItem* legendItem =
        qobject_cast<QwtLegendItem*>(plotter->legend()->find(item));
    if (legendItem)
    {
        legendItem->setChecked(on);
    }
#endif
    plotter->replot();
}

void PlotterController::autoScale(bool toggled)
{
    ARMARX_VERBOSE << "clicked autoscale" << flush;

    plotter->setAxisAutoScale(QwtPlot::yRight, toggled);
    plotter->replot();
    autoScalePlot = toggled;
}

void PlotterController::plottingPaused(bool toggled)
{
    __plottingPaused = toggled;

    //            if (pollingTask)
    //            {
    //                pollingTask->stop();
    //            }

    if (__plottingPaused)
    {
        timer.stop();
    }
    else
    {
        //                pollingTask = new PeriodicTask<PlotterController>(this, &PlotterController::pollingExec, pollingInterval, false, "DataPollingTask", false);
        //                pollingTask->start();
        timer.start(updateInterval);
    }
}

std::map<std::string, VariantPtr> PlotterController::getData(const QStringList& channels)
{
    if (iceManager->isShutdown())
    {
        return std::map<std::string, VariantPtr>();
    }
    std::map< std::string, DataFieldIdentifierBaseList > channelsSplittedByObserver;
    foreach (QString channel, channels)
    {
        DataFieldIdentifierPtr identifier = new DataFieldIdentifier(channel.toStdString());
        channelsSplittedByObserver[identifier->getObserverName()].push_back(identifier);
        //            ARMARX_INFO << identifier;
    }
    std::map<std::string, VariantPtr> newData;
    {
        ScopedLock lock(dataMutex);

        // first clear to old entries
        auto now = TimeUtil::GetTime();
        GraphDataMap::iterator itmap = dataMap.begin();

        for (; itmap != dataMap.end(); ++itmap)
        {
            std::vector<TimeData>& dataVec = itmap->second;
            int stepSize = std::max<int>(1, dataVec.size() * 0.01);
            int thresholdIndex = -1;

            for (unsigned int i = 0; i < dataVec.size(); i += stepSize)
            {
                // only delete if entries are older than 2*showninterval
                // and delete then all entries that are older than showninterval.
                // otherwise it would delete items on every call, which would be very slow

                if ((now - dataVec[i].time).toSecondsDouble() > shownInterval * 2
                    || (thresholdIndex != -1 && (now - dataVec[i].time).toSecondsDouble() > shownInterval)
                   )
                {
                    thresholdIndex = i;
                }
                else
                {
                    break;
                }
            }

            if (thresholdIndex != -1)
            {
                unsigned int offset = std::min((int)dataVec.size(), thresholdIndex);

                //                ARMARX_IMPORTANT << "Erasing " << offset << " fields";
                if (offset > dataVec.size())
                {
                    dataVec.clear();
                }
                else
                {
                    dataVec.erase(dataVec.begin(), dataVec.begin() + offset);
                }
            }

            //            ARMARX_IMPORTANT << deactivateSpam(5) << "size: " << dataVec.size();
        }

    }

    // now get new data
    IceUtil::Time time = TimeUtil::GetTime();
    std::map<std::string, DataFieldIdentifierBaseList >::iterator it = channelsSplittedByObserver.begin();

    try
    {
        for (; it != channelsSplittedByObserver.end(); ++it)
        {
            std::string observerName = it->first;

            if (proxyMap.find(observerName) == proxyMap.end())
            {
                if (!iceManager)
                {
                    continue;
                }
                proxyMap[observerName] = iceManager->getProxy<ObserverInterfacePrx>(observerName);
            }

            //            QDateTime time(QDateTime::currentDateTime());
            TimedVariantBaseList variants = proxyMap[observerName]->getDataFields(it->second);

            ScopedLock lock(dataMutex);
            //            #    ARMARX_IMPORTANT << "data from observer: " << observerName;
            for (unsigned int i = 0; i < variants.size(); ++i)
            {
                //                    ARMARX_IMPORTANT << "Variant: " << VariantPtr::dynamicCast(variants[i]);
                VariantPtr var = VariantPtr::dynamicCast(variants[i]);
                std::string id = DataFieldIdentifierPtr::dynamicCast(it->second[i])->getIdentifierStr();
                if (!var->getInitialized())
                {
                    //dataMap[id] = {};
                    continue;
                }
                auto type = var->getType();
                if (type == VariantType::String)
                {
                    if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                    {
                        // only insert if changed
                        dataMap[id].push_back(TimeData(time, var));
                        newData[id] = var;
                    }
                }
                else if (VariantType::IsBasicType(type))
                {
                    dataMap[id].push_back(TimeData(time, var));
                    newData[id] = var;
                }
                else
                {
                    auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                    for (const auto& e : dict)
                    {
                        if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                        {
                            continue;
                        }
                        std::string key = id + "." + e.first;
                        //                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                        VariantPtr var = VariantPtr::dynamicCast(e.second);
                        auto type = var->getType();
                        if (type == VariantType::String)
                        {
                            // complex contain additional strings often, which cannot be selected right now -> disable strings from complex types
                            //                                if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                            //                                {
                            //                                    // only insert if changed
                            //                                    dataMap[id].push_back(TimeData(time, var));
                            //                                    newData[key] = var;
                            //                                }
                        }
                        else
                        {
                            dataMap[key].push_back(TimeData(time, var));
                            newData[key] = var;
                        }
                    }
                }

            }

        }
    }
    catch (Ice::NotRegisteredException& e)
    {
        ARMARX_WARNING << deactivateSpam(3) << "Caught Ice::NotRegisteredException: " << e.what();
    }
    catch (exceptions::local::InvalidDataFieldException& e)
    {
        ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidDataFieldException: " << e.what();
    }
    catch (exceptions::local::InvalidChannelException& e)
    {
        ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidChannelException: " << e.what();
    }
    catch (armarx::UserException& e)
    {
        ARMARX_WARNING << deactivateSpam(5) << "Caught UserException: " << e.what() << "\nReason: " << e.reason;
    }
    catch (...)
    {
        ARMARX_WARNING << deactivateSpam(5) << GetHandledExceptionString();
    }

    return newData;
}

void PlotterController::setupCurves(int samplingIntervalMs)
{
    {
        if (samplingIntervalMs < 0)
        {
            samplingIntervalMs = pollingInterval;
        }
        ScopedLock lock(dataMutex);
        clear();
        auto now = TimeUtil::GetTime();
        std::map<std::string, TimedVariantBaseList> histories;
        markers.clear();
        //                for(auto marker : markers)
        //                    marker->detach();
        plotter->detachItems();
        for (int i = 0; i < selectedDatafields.size(); i++)
        {
            try
            {
                ARMARX_VERBOSE << "Channel: " << selectedDatafields.at(i).toStdString() << flush;
                DataFieldIdentifierPtr identifier = new DataFieldIdentifier(selectedDatafields.at(i).toStdString());
                auto prx = iceManager->getProxy<ObserverInterfacePrx>(identifier->observerName);


                // get past data of that datafield
                auto id = identifier->getIdentifierStr();
                auto historiesIt = histories.find(id);

                if (historiesIt == histories.end())
                {
                    auto start = IceUtil::Time::now();
                    histories[id] = prx->getPartialDatafieldHistory(identifier->channelName, identifier->datafieldName,
                                    (now - IceUtil::Time::seconds(shownInterval)).toMicroSeconds(), now.toMicroSeconds(), samplingIntervalMs);
                    ARMARX_DEBUG << "history data polling took : " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " got " << histories[identifier->channelName].size() << " entries";
                    historiesIt = histories.find(id);
                }

                long lastTimestamp = 0;

                VariantPtr var = VariantPtr::dynamicCast(prx->getDataField(identifier));
                auto type = var->getType();
                if (type == VariantType::String)
                {
                    // do nothing for now
                }
                else if (VariantType::IsBasicType(type))
                {
                    QwtPlotCurve* curve = createCurve(selectedDatafields.at(i));
                    curves[selectedDatafields.at(i).toStdString()] = curve;

                    QwtThermo* bar = createBar(selectedDatafields.at(i));
                    bars[selectedDatafields.at(i).toStdString()] = bar;

                    for (TimedVariantBasePtr& entry : historiesIt->second)
                    {
                        if (lastTimestamp + pollingInterval < entry->getTimestamp())
                        {
                            dataMap[id].push_back(TimeData(IceUtil::Time::microSeconds(entry->getTimestamp()), VariantPtr::dynamicCast(entry)));
                            lastTimestamp = entry->getTimestamp();
                        }
                    }
                }
                else
                {
                    auto id = identifier->getIdentifierStr();
                    ARMARX_VERBOSE << id;
                    auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                    for (auto e : dict)
                    {
                        if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                        {
                            continue;
                        }
                        VariantTypeId type = e.second->getType();

                        if (type == VariantType::Double
                            || type == VariantType::Float
                            || type == VariantType::Int)
                        {
                            std::string key = id + "." + e.first;
                            ARMARX_VERBOSE << key << ": " << *VariantPtr::dynamicCast(e.second);
                            QwtPlotCurve* curve = createCurve(QString::fromStdString(key));
                            curves[key] = curve;

                            QwtThermo* bar = createBar(QString::fromStdString(key));
                            bars[key] = bar;
                        }
                    }
                    for (auto& entry : historiesIt->second)
                    {
                        if (lastTimestamp + pollingInterval < entry->getTimestamp())
                        {
                            auto dict = JSONObject::ConvertToBasicVariantMap(json, entry);
                            for (const auto& e : dict)
                            {
                                if (e.first == "timestamp") // TimedVariants always contain a timestamp field which is irrelevant
                                {
                                    continue;
                                }
                                std::string key = id + "." + e.first;
                                //                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                                VariantPtr var = VariantPtr::dynamicCast(e.second);
                                auto type = var->getType();
                                if (type == VariantType::String)
                                {
                                    // complex contain additional strings often, which cannot be selected right now -> disable strings from complex types
                                    //                                if (dataMap[id].size() == 0 || dataMap[id].rbegin()->data->getString() != var->getString())
                                    //                                {
                                    //                                    // only insert if changed
                                    //                                    dataMap[id].push_back(TimeData(time, var));
                                    //                                    newData[key] = var;
                                    //                                }
                                }
                                else
                                {
                                    dataMap[key].push_back(TimeData(IceUtil::Time::microSeconds(entry->getTimestamp()), var));
                                }
                            }
                        }
                    }


                }

            }
            catch (...)
            {
                handleExceptions();
            }
        }

        plotter->replot();

    }
    plotter->setAxisScale(QwtPlot::xBottom, shownInterval * -1, 0.f);

    timer.start(updateInterval);

    if (pollingTask)
    {
        pollingTask->stop();
    }

    pollingTask = new PeriodicTask<PlotterController>(this, & PlotterController::pollingExec, pollingInterval, false, "DataPollingTask", false);
    pollingTask->start();
}

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
void PlotterController::legendChecked(const QVariant& itemInfo, bool on)
{
    QwtPlotItem* plotItem = plotter->infoToItem(itemInfo);
    if (plotItem)
    {
        showCurve(plotItem, on);
    }
    else
    {
        ARMARX_WARNING << "nonexistent legend item toggled to " << on;
    }
}
#else
void PlotterController::legendChecked(const QVariant& itemInfo, bool on)
{
    //dummy function for qt4
    //required because moc ignores preprocessor ifs
}
#endif

QwtPlotCurve* PlotterController::createCurve(const QString& label)
{
    QwtPlotCurve* curve = new QwtPlotCurve(label);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setPen(QColor(Qt::GlobalColor((curves.size()) % 15 + 7)));
    curve->setStyle(QwtPlotCurve::Lines);

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    curve->setPaintAttribute(QwtPlotCurve::CacheSymbols, true);
#endif
    curve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);

    curve->attach(plotter);
    showCurve(curve, true);

    return curve;
}

QwtThermo* PlotterController::createBar(const QString& label)
{
    //creates a widget containing a bar chart and a label
    //with the fill color of chart same as its curve

    QwtThermo* bar = new QwtThermo(this->widget);
    bar->setAccessibleName(label);
    bar->setFillBrush(QBrush(QColor(Qt::GlobalColor((bars.size() + 7) % 15))));
    bar->setStyleSheet("* { background-color: rgb(240, 240, 240); }"); //neccessary because white is a picked fill color

    QTextEdit* lab = new QTextEdit(label, this->widget);
    lab->setStyleSheet("* { background-color: rgb(240, 240, 240); }");
    lab->setLineWrapMode(QTextEdit::WidgetWidth);
    lab->setTextInteractionFlags(0);
    lab->setFrameStyle(0);
    int linecount = lab->document()->blockCount();
    lab->setMaximumHeight(70 * linecount);
    lab->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(bar);
    layout->addWidget(lab);

    QWidget* widget = new QWidget(layout->widget());
    widget->setLayout(layout);
    barlayout->addWidget(widget);

    return bar;
}

QStringList PlotterController::getSelectedDatafields() const
{
    ScopedLock lock(dataMutex);
    return selectedDatafields;
}



std::vector<std::string> PlotterController::getSelectedDatafieldsKeys() const
{

    std::vector<std::string> keys;

    ScopedLock lock(dataMutex);

    for (int i = 0; i < selectedDatafields.size(); i++)
    {
        try
        {
            DataFieldIdentifierPtr identifier = new DataFieldIdentifier(selectedDatafields.at(i).toStdString());
            ObserverInterfacePrx observer = iceManager->getProxy<ObserverInterfacePrx>(identifier->observerName);

            VariantPtr var = VariantPtr::dynamicCast(observer->getDataField(identifier));
            VariantTypeId type = var->getType();
            if (type == VariantType::String || VariantType::IsBasicType(type))
            {
                keys.push_back(selectedDatafields.at(i).toStdString());
            }
            else
            {
                // TODO auto load libraries see ObserverItemModel.cpp:108

                auto dict = JSONObject::ConvertToBasicVariantMap(json, var);
                for (auto e : dict)
                {
                    ARMARX_LOG_S << e.first;
                    if (e.first == "timestamp")
                    {
                        continue;
                    }
                    VariantTypeId type = e.second->getType();

                    if (type == VariantType::Double
                        || type == VariantType::Float
                        || type == VariantType::Int)
                    {
                        keys.push_back(identifier->getIdentifierStr() + "." + e.first);
                    }

                }
            }
        }
        catch (...)
        {
            handleExceptions();
        }

    }
    return keys;
}


void PlotterController::setSelectedDatafields(const QStringList& value, int samplingIntervalMs)
{
    {
        ScopedLock lock(dataMutex);
        if (selectedDatafields == value)
        {
            return;
        }
        selectedDatafields = value;
    }
    setupCurves(samplingIntervalMs);
}

int PlotterController::getPollingInterval() const
{
    return pollingInterval;
}

void PlotterController::setPollingInterval(int value)
{
    pollingInterval = value;
    if (pollingTask)
    {
        pollingTask->changeInterval(value);
    }
}

int PlotterController::getUpdateInterval() const
{
    return updateInterval;
}

void PlotterController::setUpdateInterval(int value)
{
    updateInterval = value;
    timer.start(value);
}

void PlotterController::setIceManager(const IceManagerPtr& value)
{
    iceManager = value;
    json = new JSONObject(iceManager->getCommunicator());

}

void PlotterController::clearBarList()
{
    //maybe comparing new and old selected list better than doing a new one every time?
    QLayoutItem* child;
    int containedItems = barlayout->count() - 1;

    for (int i = containedItems; i >= 0 ; i--) //must be done in this order due to internal implementation of layout item numbering
    {
        child = barlayout->itemAt(i);
        barlayout->removeItem(child);
        child->widget()->deleteLater();
    }

    bars.clear();
}

void PlotterController::clear()
{
    plotter->detachItems();
    markers.clear();
    curves.clear();
    dataMap.clear();
    clearBarList();

}

int PlotterController::getShownInterval() const
{
    return shownInterval;
}

void PlotterController::setShownInterval(int value)
{
    ARMARX_VERBOSE << "Setting shown interval to " << shownInterval;
    shownInterval = value;
    plotter->setAxisScale(QwtPlot::xBottom, shownInterval * -1, 0.f);

}

bool PlotterController::getAutoScale() const
{
    return autoScalePlot;
}

void PlotterController::setAutoScale(bool value)
{
    autoScalePlot = value;
}

int PlotterController::getGraphStyle() const
{
    return graphStyle;
}

void PlotterController::setGraphStyle(int style)
{
    if (graphStyle != style)
    {
        graphStyle = style;
        stackedLayout->setCurrentIndex(graphStyle);
    }
}
