set(LIB_NAME       WidgetDescription)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

find_package(qwt QUIET)
armarx_build_if(qwt_FOUND "Qwt not available")
if(qwt_FOUND)
    include_directories(SYSTEM ${qwt_INCLUDE_DIR})
endif()

set(COMPONENT_LIBS
    ${qwt_LIBRARIES}
    ArmarXCoreInterfaces
    ArmarXCore
    ArmarXGuiInterfaces
    ArmarXGuiBase
    VariantWidget
)

set(SOURCES WidgetDescription.cpp)
set(HEADERS WidgetDescription.h)
set(GUI_MOC_HDRS WidgetDescription.h)


armarx_gui_library("${LIB_NAME}" "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")

# add unit tests
add_subdirectory(test)
