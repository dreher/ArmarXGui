#include "Storage.h"

namespace armarx
{
    namespace RemoteGui
    {

        bool buttonClicked(RemoteGui::ValueMap const& newValues, RemoteGui::ValueMap const& oldValues, std::string const& name)
        {
            auto oldIter = oldValues.find(name);
            if (oldIter == oldValues.end())
            {
                return false;
            }

            auto newIter = newValues.find(name);
            if (newIter == newValues.end())
            {
                throw LocalException("Could find value with name: ") << name;
            }

            int oldValue = getValue<int>(oldIter->second);
            int newValue = getValue<int>(newIter->second);

            return newValue > oldValue;
        }

    }
}
