#pragma once

#include "Storage.h"

#include <ArmarXGui/interface/RemoteGuiInterface.h>

namespace armarx
{
    namespace RemoteGui
    {

        namespace detail
        {
            class WidgetBuilder
            {
            public:
                WidgetBuilder& hidden(bool hidden = true)
                {
                    widget_->defaultState.hidden = hidden;
                    return *this;
                }

                WidgetBuilder& disabled(bool disabled = true)
                {
                    widget_->defaultState.disabled = disabled;
                    return *this;
                }

                operator WidgetPtr() const
                {
                    return widget_;
                }

            protected:
                WidgetPtr widget_;
            };

            template <typename WidgetT, typename Derived>
            class WidgetMixin : public WidgetBuilder
            {
            public:
                WidgetMixin(std::string const& name)
                {
                    widget_ = new WidgetT;
                    widget_->name = name;
                    widget_->defaultValue = Derived::defaultValue();
                }

            public:
                WidgetT& widget()
                {
                    return *dynamic_cast<WidgetT*>(widget_.get());
                }
            };

            template <typename WidgetT, typename Derived>
            struct NoValueMixin : WidgetMixin<WidgetT, Derived>
            {
                using WidgetMixin<WidgetT, Derived>::WidgetMixin;

                static Value defaultValue()
                {
                    return Value();
                }
            };

            template <typename WidgetT, typename ValueT, typename Derived>
            struct ValueMixin : WidgetMixin<WidgetT, Derived>
            {
                using WidgetMixin<WidgetT, Derived>::WidgetMixin;

                static Value defaultValue()
                {
                    return makeValue(ValueT());
                }

                Derived& value(ValueT const& value)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().defaultValue = makeValue(value);
                    return this_;
                }
            };

            template <typename Derived>
            struct BoolValueMixin
            {
                Derived& value(bool value)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().defaultValue = makeValue(value);
                    return this_;
                }
            };

            template <typename Derived>
            struct IntMinMaxMixin
            {
                Derived& min(int min)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().min = min;
                    return this_;
                }

                Derived& max(int max)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().max = max;
                    return this_;
                }
            };

            template <typename Derived>
            struct FloatMinMaxMixin
            {
                Derived& min(float min)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().min = min;
                    return this_;
                }

                Derived& max(float max)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().max = max;
                    return this_;
                }

                Derived& steps(int steps)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().steps = steps;
                    return this_;
                }
            };

            template <typename Derived>
            struct LabelMixin
            {
                Derived& label(std::string const& label)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().label = label;
                    return this_;
                }
            };

            template <typename Derived>
            struct SingleChildMixin
            {
                Derived& child(WidgetPtr const& child)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().children = {child};
                    return this_;
                }
            };

            template <typename Derived>
            struct ChildrenMixin
            {
                Derived& addChild(WidgetPtr const& child)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().children.push_back(child);
                    return this_;
                }

                Derived& children(std::vector<WidgetPtr> const& children)
                {
                    Derived& this_ = *static_cast<Derived*>(this);
                    this_.widget().children = children;
                    return this_;
                }
            };

            struct LabelBuilder
                    : public ValueMixin<Label, std::string, LabelBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct LineEditBuilder
                    : public ValueMixin<LineEdit, std::string, LineEditBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct ComboBoxBuilder
                    : public ValueMixin<ComboBox, std::string, ComboBoxBuilder>
            {
                using ValueMixin::ValueMixin;

                ComboBoxBuilder& options(std::vector<std::string> const& options)
                {
                    widget().options = options;
                    return *this;
                }
            };

            struct CheckBoxBuilder
                    : public ValueMixin<CheckBox, bool, CheckBoxBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct ToggleButtonBuilder
                    : public ValueMixin<ToggleButton, bool, ToggleButtonBuilder>
                    , public LabelMixin<ToggleButtonBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct IntSpinBoxBuilder
                    : public ValueMixin<IntSpinBox, int, IntSpinBoxBuilder>
                    , public IntMinMaxMixin<IntSpinBoxBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct IntSliderBuilder
                    : public ValueMixin<IntSlider, int, IntSliderBuilder>
                    , public IntMinMaxMixin<IntSliderBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct ButtonBuilder
                    : public ValueMixin<Button, int, ButtonBuilder>
                    , public LabelMixin<ButtonBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct FloatSpinBoxBuilder
                    : public ValueMixin<FloatSpinBox, float, FloatSpinBoxBuilder>
                    , public FloatMinMaxMixin<FloatSpinBoxBuilder>
            {
                using ValueMixin::ValueMixin;

                FloatSpinBoxBuilder& decimals(int decimals)
                {
                    widget().decimals = decimals;
                    return *this;
                }
            };

            struct FloatSliderBuilder
                    : public ValueMixin<FloatSlider, float, FloatSliderBuilder>
                    , public FloatMinMaxMixin<FloatSliderBuilder>
            {
                using ValueMixin::ValueMixin;
            };

            struct HBoxLayoutBuilder
                    : public NoValueMixin<HBoxLayout, HBoxLayoutBuilder>
                    , public ChildrenMixin<HBoxLayoutBuilder>
            {
                using NoValueMixin::NoValueMixin;
            };

            struct VBoxLayoutBuilder
                    : public NoValueMixin<VBoxLayout, VBoxLayoutBuilder>
                    , public ChildrenMixin<VBoxLayoutBuilder>
            {
                using NoValueMixin::NoValueMixin;
            };

            struct GroupBoxBuilder
                    : public NoValueMixin<GroupBox, GroupBoxBuilder>
                    , public SingleChildMixin<GroupBoxBuilder>
                    , public LabelMixin<GroupBoxBuilder>
            {
                using NoValueMixin::NoValueMixin;
            };

        }

        inline detail::CheckBoxBuilder makeCheckBox(std::string const& name)
        {
            return detail::CheckBoxBuilder(name);
        }

        inline detail::ToggleButtonBuilder makeToggleButton(std::string const& name)
        {
            return detail::ToggleButtonBuilder(name);
        }

        inline detail::LabelBuilder makeLabel(std::string const& name)
        {
            return detail::LabelBuilder(name);
        }

        inline detail::LabelBuilder makeTextLabel(std::string const& text)
        {
            return detail::LabelBuilder("").value(text);
        }

        inline detail::LineEditBuilder makeLineEdit(std::string const& name)
        {
            return detail::LineEditBuilder(name);
        }

        inline detail::ComboBoxBuilder makeComboBox(std::string const& name)
        {
            return detail::ComboBoxBuilder(name);
        }

        inline detail::IntSpinBoxBuilder makeIntSpinBox(std::string const& name)
        {
            return detail::IntSpinBoxBuilder(name);
        }

        inline detail::IntSliderBuilder makeIntSlider(std::string const& name)
        {
            return detail::IntSliderBuilder(name);
        }

        inline detail::ButtonBuilder makeButton(std::string const& name)
        {
            return detail::ButtonBuilder(name);
        }

        inline detail::FloatSpinBoxBuilder makeFloatSpinBox(std::string const& name)
        {
            return detail::FloatSpinBoxBuilder(name);
        }

        inline detail::FloatSliderBuilder makeFloatSlider(std::string const& name)
        {
            return detail::FloatSliderBuilder(name);
        }

        inline detail::HBoxLayoutBuilder makeHBoxLayout(std::string const& name = "")
        {
            return detail::HBoxLayoutBuilder(name);
        }

        inline detail::VBoxLayoutBuilder makeVBoxLayout(std::string const& name = "")
        {
            return detail::VBoxLayoutBuilder(name);
        }

        inline detail::GroupBoxBuilder makeGroupBox(std::string const& name = "")
        {
            return detail::GroupBoxBuilder(name);
        }

    }
}
