#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/Storage.h>

namespace armarx
{
    namespace RemoteGui
    {

        class TabProxy;

        template <typename T>
        class ValueProxy;

        class ButtonProxy;

        class TabProxy
        {
        public:
            TabProxy() = default;

            TabProxy(RemoteGuiInterfacePrx const& remoteGui, std::string const& tabId)
                : remoteGui(remoteGui)
                , tabId(tabId)
            {
                currentValues = remoteGui->getValues(tabId);
                oldValues = currentValues;
                newValues = currentValues;

                currentWidgetStates = remoteGui->getWidgetStates(tabId);
                oldWidgetStates = currentWidgetStates;
                newWidgetStates = currentWidgetStates;
            }

            void receiveUpdates()
            {
                oldValues = currentValues;
                currentValues = remoteGui->getValues(tabId);
                newValues = currentValues;
                valuesChanged = false;

                currentWidgetStates = remoteGui->getWidgetStates(tabId);
                oldWidgetStates = currentWidgetStates;
                newWidgetStates = currentWidgetStates;
                widgetChanged = false;
            }

            void sendUpdates()
            {
                if (valuesChanged)
                {
                    remoteGui->setValues(tabId, newValues);
                }
                if (widgetChanged)
                {
                    remoteGui->setWidgetStates(tabId, newWidgetStates);
                }
            }

            template <typename T>
            ValueProxy<T> getValue(std::string const& name);

            ButtonProxy getButton(std::string const& name);

            template <typename T>
            T internalGetValue(std::string const& name) const
            {
                return RemoteGui::getValue<T>(currentValues, name);
            }

            template <typename T>
            void internalSetValue(std::string const& name, T const& value)
            {
                T currentValue = internalGetValue<T>(name);
                if (currentValue != value)
                {
                    valuesChanged = true;
                }
                newValues[name] = RemoteGui::makeValue(value);
            }

            bool internalButtonClicked(std::string const& name) const
            {
                return RemoteGui::buttonClicked(newValues, oldValues, name);
            }

            void internalSetHidden(std::string const& name, bool hidden)
            {
                RemoteGui::WidgetState& state = getWidgetState(name);
                if (state.hidden != hidden)
                {
                    widgetChanged = true;
                }
                state.hidden = hidden;
            }

            void internalSetDisabled(std::string const& name, bool disabled)
            {
                RemoteGui::WidgetState& state = getWidgetState(name);
                if (state.disabled != disabled)
                {
                    widgetChanged = true;
                }
                state.disabled = disabled;
            }

            RemoteGui::WidgetState& getWidgetState(std::string const& name)
            {
                auto iter = currentWidgetStates.find(name);
                if (iter == currentWidgetStates.end())
                {
                    throw LocalException("No widget with name '") << name << "' found";
                }
                return iter->second;
            }

        private:
            RemoteGuiInterfacePrx remoteGui;
            std::string tabId;
            RemoteGui::ValueMap currentValues;
            RemoteGui::ValueMap oldValues;
            RemoteGui::ValueMap newValues;

            RemoteGui::WidgetStateMap currentWidgetStates;
            RemoteGui::WidgetStateMap oldWidgetStates;
            RemoteGui::WidgetStateMap newWidgetStates;

            bool valuesChanged = false;
            bool widgetChanged = false;
        };

        class WidgetProxy
        {
        public:
            WidgetProxy(TabProxy* tab, std::string const& name)
                : tab(tab)
                , name(name)
            {
            }

            void setHidden(bool hidden = true)
            {
                tab->internalSetHidden(name, hidden);
            }

            void setDisabled(bool disabled = true)
            {
                tab->internalSetDisabled(name, disabled);
            }

            bool isHidden() const
            {
                return tab->getWidgetState(name).hidden;
            }

            bool isDisabled() const
            {
                return tab->getWidgetState(name).disabled;
            }

        protected:
            TabProxy* tab;
            std::string name;
        };

        template <typename T>
        class ValueProxy : public WidgetProxy
        {
        public:
            using WidgetProxy::WidgetProxy;

            T get() const
            {
                return tab->internalGetValue<T>(name);
            }

            void set(T const& value)
            {
                return tab->internalSetValue<T>(name, value);
            }
        };

        class ButtonProxy : public ValueProxy<int>
        {
        public:
            using ValueProxy::ValueProxy;

            bool clicked() const
            {
                return tab->internalButtonClicked(name);
            }
        };

        template <typename T>
        ValueProxy<T> TabProxy::getValue(std::string const& name)
        {
            return ValueProxy<T>(this, name);
        }

        ButtonProxy TabProxy::getButton(std::string const& name)
        {
            return ButtonProxy(this, name);
        }

    }
}
