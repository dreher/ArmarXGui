#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <ArmarXCore/core/exceptions/Exception.h>

#include <boost/optional.hpp>

namespace armarx
{
    namespace RemoteGui
    {
        inline RemoteGui::Value makeValue(bool value)
        {
            RemoteGui::Value result;
            result.type = RemoteGui::eValue_Bool;
            result.valueInt = value ? 1 : 0;
            return result;
        }

        inline RemoteGui::Value makeValue(int value)
        {
            RemoteGui::Value result;
            result.type = RemoteGui::eValue_Int;
            result.valueInt = value;
            return result;
        }

        inline RemoteGui::Value makeValue(float value)
        {
            RemoteGui::Value result;
            result.type = RemoteGui::eValue_Float;
            result.valueFloat = value;
            return result;
        }

        inline RemoteGui::Value makeValue(std::string const& value)
        {
            RemoteGui::Value result;
            result.type = RemoteGui::eValue_String;
            result.valueString = value;
            return result;
        }

        template <typename T>
        T getValue(RemoteGui::Value const& value);

        template <>
        inline bool getValue<bool>(RemoteGui::Value const& value)
        {
            if (value.type != eValue_Bool)
            {
                throw LocalException("Expected type bool but got: ") << value.type;
            }

            return value.valueInt != 0;
        }

        template <>
        inline int getValue<int>(RemoteGui::Value const& value)
        {
            if (value.type != eValue_Int)
            {
                throw LocalException("Expected type int but got: ") << value.type;
            }

            return value.valueInt;
        }

        template <>
        inline float getValue<float>(RemoteGui::Value const& value)
        {
            if (value.type != eValue_Float)
            {
                throw LocalException("Expected type float but got: ") << value.type;
            }

            return value.valueFloat;
        }

        template <>
        inline std::string getValue<std::string>(RemoteGui::Value const& value)
        {
            if (value.type != eValue_String)
            {
                throw LocalException("Expected type string but got: ") << value.type;
            }

            return value.valueString;
        }

        template <typename T>
        T getValue(RemoteGui::ValueMap const& values, std::string const& name)
        {
            auto iter = values.find(name);
            if (iter == values.end())
            {
                throw LocalException("No value with name '") << name << "' found";
            }
            else
            {
                return getValue<T>(iter->second);
            }
        }

        bool buttonClicked(RemoteGui::ValueMap const& newValues, RemoteGui::ValueMap const& oldValues, std::string const& name);

        namespace detail
        {

            template <ValueType ValueType>
            struct Storage
            {
                using Type = std::nullptr_t;

                static Type get(RemoteGui::Value const& value)
                {
                    return Type {};
                }

                static void set(RemoteGui::Value& value, Type newValue)
                {
                    // Do nothing
                }
            };

            template <>
            struct Storage<eValue_Bool>
            {
                using Type = bool;

                static Type get(RemoteGui::Value const& value)
                {
                    return value.valueInt != 0;
                }

                static void set(RemoteGui::Value& value, Type newValue)
                {
                    value.valueInt = newValue ? 1 : 0;
                }
            };

            template <>
            struct Storage<eValue_Int>
            {
                using Type = int;

                static Type get(Value const& value)
                {
                    return value.valueInt;
                }

                static void set(Value& value, Type newValue)
                {
                    value.valueInt = newValue;
                }
            };

            template <>
            struct Storage<eValue_Float>
            {
                using Type = float;

                static Type get(Value const& value)
                {
                    return value.valueFloat;
                }

                static void set(Value& value, Type newValue)
                {
                    value.valueFloat = newValue;
                }
            };

            template <>
            struct Storage<eValue_String>
            {
                using Type = std::string;

                static Type const& get(Value const& value)
                {
                    return value.valueString;
                }

                static void set(Value& value, Type const& newValue)
                {
                    value.valueString = newValue;
                }
            };

        }
    }
}
