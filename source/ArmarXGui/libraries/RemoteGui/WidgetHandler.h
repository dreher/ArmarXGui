#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <functional>

class QWidget;
class QObject;
class QString;

namespace armarx
{
    namespace RemoteGui
    {

        std::string toUtf8(QString const& qstring);
        QString toQString(std::string const& string);

        using QWidgetPtr = QWidget* ;
        using CreateWidgetCallback = std::function<QWidgetPtr(WidgetPtr const&)>;

        struct WidgetHandler
        {
            virtual ~WidgetHandler() = default;

            virtual bool isValid(Widget const& desc) const = 0;

            virtual QWidget* createWidget(Widget const& desc,
                                          Value const& initialValue,
                                          CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver,
                                          const char* stateChangeSlot) const = 0;

            virtual void updateGui(Widget const& desc,
                                   QWidget* widget,
                                   Value const& value) const = 0;

            virtual RemoteGui::Value handleGuiChange(Widget const& desc,
                    QWidget* widget) const = 0;
        };

    }
}
