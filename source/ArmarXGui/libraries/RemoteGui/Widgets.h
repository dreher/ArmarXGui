#pragma once

#include "TypedWidget.h"

#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QSlider>
#include <QPushButton>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt_slider.h>

namespace armarx
{
    namespace RemoteGui
    {

        struct IntSpinBoxHandler : TypedWidget<IntSpinBox, QSpinBox, eValue_Int>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();

                widget->setMinimum(desc.min);
                widget->setMaximum(desc.max);

                QObject::connect(widget, SIGNAL(valueChanged(int)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setValue(value);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->value();
            }
        };

        struct IntSliderHandler : TypedWidget<IntSlider, QSlider, eValue_Int>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();

                widget->setOrientation(Qt::Horizontal);

                widget->setMinimum(desc.min);
                widget->setMaximum(desc.max);

                QObject::connect(widget, SIGNAL(valueChanged(int)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setValue(value);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->value();
            }
        };

        struct FloatSpinBoxHandler : TypedWidget<FloatSpinBox, QDoubleSpinBox, eValue_Float>
        {
            static bool isValid(RemoteWidgetT const& desc)
            {
                return desc.min <= desc.max
                       && desc.steps > 0
                       && desc.decimals >= 0;
            }

            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();

                widget->setMinimum(desc.min);
                widget->setMaximum(desc.max);
                widget->setDecimals(desc.decimals);
                widget->setSingleStep((desc.max - desc.min) / desc.steps);

                QObject::connect(widget, SIGNAL(valueChanged(double)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setValue(value);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->value();
            }
        };

        struct FloatSliderHandler : TypedWidget<FloatSlider, QwtSlider, eValue_Float>
        {
            static bool isValid(RemoteWidgetT const& desc)
            {
                return desc.min <= desc.max
                       && desc.steps > 0;
            }

            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT(nullptr);

                float singleStep = (desc.max - desc.min) / desc.steps;
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
                widget->setScale(desc.min, desc.max, singleStep);
#else
                widget->setRange(desc.min, desc.max, singleStep);
#endif

                QObject::connect(widget, SIGNAL(valueChanged(double)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setValue(value);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->value();
            }
        };

        struct LabelHandler : TypedWidget<Label, QLabel, eValue_String>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();
                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setText(toQString(value));
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return toUtf8(widget->text());
            }
        };

        struct LineEditHandler : TypedWidget<LineEdit, QLineEdit, eValue_String>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();

                QObject::connect(widget, SIGNAL(editingFinished()), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setText(toQString(value));
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return toUtf8(widget->text());
            }
        };

        struct ComboBoxHandler : TypedWidget<ComboBox, QComboBox, eValue_String>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();

                for (std::string const& option : desc.options)
                {
                    widget->addItem(toQString(option));
                }

                QObject::connect(widget, SIGNAL(currentIndexChanged(int)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                int index = widget->findText(toQString(value));
                if (index >= 0)
                {
                    widget->setCurrentIndex(index);
                }
                else
                {
                    throw LocalException() << "Invalid value set for ComboBox '"
                                           << desc.name << "': " << value;
                }
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return toUtf8(widget->currentText());
            }
        };

        struct CheckBoxHandler : TypedWidget<CheckBox, QCheckBox, eValue_Bool>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();
                widget->setText(toQString(desc.label));

                QObject::connect(widget, SIGNAL(stateChanged(int)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setCheckState(value ? Qt::Checked : Qt::Unchecked);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->checkState() == Qt::Checked;
            }
        };

        struct ToggleButtonHandler : TypedWidget<ToggleButton, QPushButton, eValue_Bool>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();
                widget->setText(toQString(desc.label));
                widget->setCheckable(true);

                QObject::connect(widget, SIGNAL(toggled(bool)), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setChecked(value);
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                return widget->isChecked();
            }
        };

        struct ButtonHandler : TypedWidget<Button, QPushButton, eValue_Int>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidgetT();
                widget->setText(toQString(desc.label));

                QObject::connect(widget, SIGNAL(clicked()), stateChangeReceiver, stateChangeSlot);

                return widget;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                widget->setProperty("ClickCount", QVariant(value));
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                int newValue = widget->property("ClickCount").toInt() + 1;
                widget->setProperty("ClickCount", QVariant(newValue));
                return newValue;
            }
        };


        struct GroupBoxHandler : TypedWidget<GroupBox, QGroupBox>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QGroupBox* widget = new QGroupBox;
                widget->setTitle(toQString(desc.label));

                ARMARX_CHECK_EXPRESSION(desc.children.size() <= 1);
                if (desc.children.size() == 1)
                {
                    QWidget* child = createChild(desc.children[0]);

                    QHBoxLayout* layout = new QHBoxLayout;
                    widget->setLayout(layout);
                    layout->setContentsMargins(0, 9, 0, 0);
                    layout->addWidget(child);
                }

                return widget;
            }
        };

        struct HSpacerHandler : TypedWidget<HSpacer, QWidget>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidget();

                widget->setLayout(new QVBoxLayout);
                widget->layout()->setContentsMargins(0, 0, 0, 0);
                widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

                return widget;
            }
        };

        struct VSpacerHandler : TypedWidget<VSpacer, QWidget>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidget();

                widget->setLayout(new QVBoxLayout);
                widget->layout()->setContentsMargins(0, 0, 0, 0);
                widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

                return widget;
            }
        };

        struct HLineHandler : TypedWidget<HLine, QFrame>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QFrame();

                widget->setFrameShape(QFrame::HLine);
                widget->setFrameShadow(QFrame::Sunken);

                return widget;
            }
        };
        struct VLineHandler : TypedWidget<VLine, QFrame>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QFrame();

                widget->setFrameShape(QFrame::VLine);
                widget->setFrameShadow(QFrame::Sunken);

                return widget;
            }
        };

        struct VBoxLayoutHandler : TypedWidget<VBoxLayout, QWidget>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidget();

                QVBoxLayout* layout = new QVBoxLayout(widget);
                widget->setLayout(layout);
                layout->setContentsMargins(0, 0, 0, 0);
                for (const RemoteGui::WidgetPtr& child : desc.children)
                {
                    QWidget* childWidget = createChild(child);
                    layout->addWidget(childWidget);
                }

                return widget;
            }
        };

        struct HBoxLayoutHandler : TypedWidget<HBoxLayout, QWidget>
        {
            static QWidgetT* createWidget(RemoteWidgetT const& desc, CreateWidgetCallback const& createChild,
                                          const QObject* stateChangeReceiver, const char* stateChangeSlot)
            {
                QWidgetT* widget = new QWidget();

                QHBoxLayout* layout = new QHBoxLayout(widget);
                widget->setLayout(layout);
                layout->setContentsMargins(0, 0, 0, 0);
                for (const RemoteGui::WidgetPtr& child : desc.children)
                {
                    QWidget* childWidget = createChild(child);
                    layout->addWidget(childWidget);
                }

                return widget;
            }
        };

    }
}
