#include "WidgetRegister.h"

#include "Widgets.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <memory>
#include <map>
#include <typeindex>

namespace armarx
{
    namespace RemoteGui
    {

        typedef std::map<std::type_index, WidgetHandler*> WidgetRegister;

        template <typename HandlerT>
        void registerHandler(WidgetRegister& register_)
        {
            using WidgetHandler = TypedWidgetHandler<HandlerT>;
            static WidgetHandler handler;
            register_.emplace(typeid(typename WidgetHandler::RemoteWidgetT), &handler);
        }

        WidgetRegister createWidgetRegister()
        {
            WidgetRegister result;

            registerHandler<CheckBoxHandler>(result);
            registerHandler<ToggleButtonHandler>(result);
            registerHandler<ButtonHandler>(result);

            registerHandler<IntSpinBoxHandler>(result);
            registerHandler<IntSliderHandler>(result);
            registerHandler<FloatSpinBoxHandler>(result);
            registerHandler<FloatSliderHandler>(result);

            registerHandler<LabelHandler>(result);
            registerHandler<LineEditHandler>(result);
            registerHandler<ComboBoxHandler>(result);

            registerHandler<GroupBoxHandler>(result);
            registerHandler<VBoxLayoutHandler>(result);
            registerHandler<HBoxLayoutHandler>(result);

            registerHandler<HSpacerHandler>(result);
            registerHandler<VSpacerHandler>(result);
            registerHandler<HLineHandler>(result);
            registerHandler<VLineHandler>(result);

            return result;
        }

        const WidgetHandler& getWidgetHandler(WidgetPtr const& desc)
        {
            static const WidgetRegister widgetRegister = createWidgetRegister();

            auto descPtr = desc.get();
            ARMARX_CHECK_EXPRESSION(descPtr != nullptr);

            std::type_info const& typeInfo = typeid(*descPtr);

            auto iter = widgetRegister.find(typeInfo);
            if (iter == widgetRegister.end())
            {
                throw armarx::LocalException() << "Unexpected widget type: " << typeInfo.name();
            }

            auto& widgetHandler = iter->second;
            ARMARX_CHECK_EXPRESSION(widgetHandler);

            return *widgetHandler;
        }

    }
}
