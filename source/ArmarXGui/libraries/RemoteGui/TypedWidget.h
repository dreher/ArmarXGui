#pragma once

#include "WidgetHandler.h"
#include "Storage.h"

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QWidget>

namespace armarx
{
    namespace RemoteGui
    {

        template <typename HandlerT>
        struct TypedWidgetHandler : WidgetHandler
        {
            using RemoteWidgetT = typename HandlerT::RemoteWidgetT;
            using QWidgetT = typename HandlerT::QWidgetT;
            static const RemoteGui::ValueType ValueType = HandlerT::ValueType;
            using StorageT = detail::Storage<ValueType>;
            using ValueT = typename StorageT::Type;

            bool isValid(Widget const& desc) const override
            {
                if (desc.defaultValue.type != ValueType)
                {
                    return false;
                }

                auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
                ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

                return HandlerT::isValid(*concreteDesc);
            }

            QWidget* createWidget(Widget const& desc,
                                  Value const& initialValue,
                                  CreateWidgetCallback const& createChild,
                                  const QObject* stateChangeReceiver,
                                  const char* stateChangeSlot) const override
            {
                auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
                ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

                ARMARX_CHECK_EXPRESSION(initialValue.type == ValueType);
                ValueT concreteInitialValue = StorageT::get(initialValue);

                auto* widget = HandlerT::createWidget(*concreteDesc, createChild,
                                                      stateChangeReceiver, stateChangeSlot);

                HandlerT::updateGui(*concreteDesc, widget, concreteInitialValue);

                return widget;
            }

            virtual void updateGui(Widget const& desc,
                                   QWidget* widget,
                                   Value const& value) const override
            {
                auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
                ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

                QWidgetT* concreteWidget = qobject_cast<QWidgetT*>(widget);
                ARMARX_CHECK_EXPRESSION(concreteWidget != nullptr);

                ARMARX_CHECK_EXPRESSION(value.type == ValueType);
                ValueT concreteValue = StorageT::get(value);

                HandlerT::updateGui(*concreteDesc, concreteWidget, concreteValue);
            }

            virtual Value handleGuiChange(Widget const& desc,
                                          QWidget* widget) const override
            {
                auto concreteDesc = dynamic_cast<RemoteWidgetT const*>(&desc);
                ARMARX_CHECK_EXPRESSION(concreteDesc != nullptr);

                QWidgetT* concreteWidget = qobject_cast<QWidgetT*>(widget);
                ARMARX_CHECK_EXPRESSION(concreteWidget != nullptr);

                ValueT value = HandlerT::handleGuiChange(*concreteDesc, concreteWidget);

                Value currentState;
                currentState.type = ValueType;
                StorageT::set(currentState, value);

                return currentState;
            }
        };


        template <typename RemoteWidgetT_,
                  typename QWidgetT_,
                  ValueType ValueType_ = eValue_Unknown>
        struct TypedWidget
        {
            using RemoteWidgetT = RemoteWidgetT_;
            using QWidgetT = QWidgetT_;
            static const RemoteGui::ValueType ValueType = ValueType_;
            using ValueT = typename detail::Storage<ValueType>::Type;

            static bool isValid(RemoteWidgetT const& desc)
            {
                return true;
            }
        };

        template <typename RemoteWidgetT_,
                  typename QWidgetT_>
        struct TypedWidget<RemoteWidgetT_, QWidgetT_, RemoteGui::eValue_Unknown>
        {
            using RemoteWidgetT = RemoteWidgetT_;
            using QWidgetT = QWidgetT_;
            static const RemoteGui::ValueType ValueType = RemoteGui::eValue_Unknown;
            using ValueT = std::nullptr_t;

            static bool isValid(RemoteWidgetT const& desc)
            {
                return true;
            }

            static void updateGui(RemoteWidgetT const& desc, QWidgetT* widget, ValueT const& value)
            {
                // Do nothing
            }

            static ValueT handleGuiChange(RemoteWidgetT const& desc, QWidgetT* widget)
            {
                // Do nothing
                return ValueT {};
            }
        };

    }
}
