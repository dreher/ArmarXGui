/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::SimpleConfigDialog
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SimpleConfigDialog.h"

#include "ui_SimpleConfigDialog.h"

armarx::SimpleConfigDialog::SimpleConfigDialog(QWidget* parent):
    QDialog(parent),
    ui(new Ui::SimpleConfigDialog),
    uuid {IceUtil::generateUUID()}
{
    ui->setupUi(this);
}

armarx::SimpleConfigDialog::~SimpleConfigDialog()
{
    delete ui;
}

void armarx::SimpleConfigDialog::onInitComponent()
{
    auto manager = getIceManager();
    for (auto& entry : entries)
    {
        entry.second.setIceManager(manager);
    }
}

std::string armarx::SimpleConfigDialog::getDefaultName() const
{
    return "SimpleConfigDialog" + uuid;
}


QVBoxLayout* armarx::SimpleConfigDialog::getLayout()
{
    return ui->verticalLayout;
}
