/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>
#include <QtPlugin>
#include "ArmarXWidgetController.h"

class ArmarXWidgetInfo;
typedef boost::shared_ptr<ArmarXWidgetInfo> ArmarXWidgetInfoPtr;

typedef armarx::ArmarXWidgetControllerPtr(*WidgetCreatorFunction)();
typedef std::map<QString, ArmarXWidgetInfoPtr > WidgetCreatorMap;

/**
 * @brief The ArmarXWidgetInfo class
 *
 * \ingroup ArmarXGuiBase
 */
class ArmarXWidgetInfo
{
public:
    ArmarXWidgetInfo(WidgetCreatorFunction creatorFunction, QIcon icon, QIcon categoryIcon)
        : creatorFunction(creatorFunction), icon(icon), categoryIcon(categoryIcon)
    { }

    armarx::ArmarXWidgetControllerPtr createInstance() const
    {
        return creatorFunction();
    }
    QIcon getIcon() const
    {
        return icon;
    }
    QIcon getCategoryIcon() const
    {
        return categoryIcon;
    }

private:
    WidgetCreatorFunction creatorFunction;
    QIcon icon;
    QIcon categoryIcon;
};

/**
  \class ArmarXGuiInterface
  \brief The main gui interface.
  \see ArmarXGuiPlugin

 */
class ArmarXGuiInterface
{
public:
    virtual ~ArmarXGuiInterface()
    {
    }

    virtual QString getPluginName()
    {
        return "ArmarXGuiInterface";
    }

    virtual WidgetCreatorMap getProvidedWidgets() = 0;
};


Q_DECLARE_INTERFACE(ArmarXGuiInterface, "ArmarXGuiInterface/1.00")


