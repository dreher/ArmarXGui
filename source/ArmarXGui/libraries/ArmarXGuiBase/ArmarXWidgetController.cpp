/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXWidgetController.h"

#include <ArmarXCore/core/Component.h>

#include <Inventor/nodes/SoNode.h>

#include <QMessageBox>
#include <QDialog>
#include <QToolBar>

using namespace armarx;

ArmarXWidgetController::ArmarXWidgetController()
{
    __appMainWindow = NULL;
}

ArmarXWidgetController::~ArmarXWidgetController()
{
    if (__widget && !__widget->parent())
    {
        delete __widget;
    }

    //    ARMARX_VERBOSE << "Deleting WidgetController " ;
}

QPointer<QWidget> ArmarXWidgetController::getWidget()
{
    if (!__widget)
    {
        __widget = new QWidget();
    }

    return __widget;
}

QPointer<QDialog> ArmarXWidgetController::getConfigDialog(QWidget* parent)
{
    return 0;
}

QPointer<QWidget> ArmarXWidgetController::getCustomTitlebarWidget(QWidget* parent)
{
    return 0;
}

bool ArmarXWidgetController::setInstanceName(QString instanceName)
{
    if (instanceName.length() == 0)
    {
        throw LocalException("The instance name of the widget must not be empty");
    }

    if (__instanceName.length() > 0)
    {
        return false;
    }

    __instanceName = instanceName;
    return true;
}

QString ArmarXWidgetController::getInstanceName()
{
    return __instanceName;
}

void ArmarXWidgetController::setMainWindow(QMainWindow* mainWindow)
{
    __appMainWindow = mainWindow;
}

QMainWindow* ArmarXWidgetController::getMainWindow()
{
    return __appMainWindow;
}

QPointer<TipDialog> ArmarXWidgetController::getTipDialog() const
{
    return tipDialog;
}

void armarx::ArmarXWidgetController::setTipDialog(QPointer<TipDialog> tipDialog)
{
    this->tipDialog = tipDialog;
}


void ArmarXWidgetController::setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D)
{
    //ARMARX_INFO << "Widget controller " << getInstanceName() << ": set mutex " << mutex3D.get();
    this->mutex3D = mutex3D;
}

void ArmarXWidgetController::configAccepted()
{
    //    ARMARX_VERBOSE << "config was accepted";
    configured();
    emit configAccepted(this);
}

void ArmarXWidgetController::configRejected()
{
    emit configRejected(this);
}

void ArmarXWidgetController::enableMainWidget(bool enable)
{
    getWidget()->setEnabled(enable);
}


int ArmarXWidgetController::showMessageBox(const QString& msg)
{
    QMessageBox msgBox;
    ARMARX_WARNING_S << "MessageBox says: " << msg.toStdString() << flush;
    msgBox.setText(msg);
    return msgBox.exec();
}

void ArmarXWidgetController::enableMainWidgetAsync(bool enable)
{
    if (!QMetaObject::invokeMethod(this, "enableMainWidget", Q_ARG(bool, enable)))
    {
        ARMARX_WARNING << "Failed to invoke enable";
    }
}


std::ostream& std::operator<< (std::ostream& stream, const QString& string)
{
    stream << string.toStdString();

    return stream;
}


std::ostream& std::operator<< (std::ostream& stream, const QPointF& point)
{
    stream << point.x() << ", " << point.y();

    return stream;
}

std::ostream& std::operator<<(std::ostream& stream, const QRectF& rect)
{
    stream << "topleft: " << rect.topLeft() << ", bottomright:"  << rect.bottomRight();

    return stream;
}

std::ostream& std::operator<<(std::ostream& stream, const QSizeF& rect)
{
    stream << "SizeF: " << rect.width() << "x"  << rect.height();

    return stream;
}




