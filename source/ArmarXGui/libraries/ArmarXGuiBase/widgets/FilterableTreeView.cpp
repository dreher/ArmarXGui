/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "FilterableTreeView.h"

#include <QLineEdit>
#include <QMouseEvent>
#include <QStandardItem>

#include <ArmarXCore/core/exceptions/Exception.h>

#define DEFAULT_FILTER_CONTENT  "Click&type to filter"
const QString FilterableTreeView::DefaultFilterStr = DEFAULT_FILTER_CONTENT;
#define FILTER_HEIGHT 22
FilterableTreeView::FilterableTreeView(QWidget* parent, bool hideChildren) :
    QTreeView(parent),
    hideChildren(hideChildren)
{
    setStyleSheet("QTreeView{margin-top: " + QString::number(FILTER_HEIGHT) + "px}");
    filterLineEdit = new QLineEdit(this);
    filterLineEdit->setPlaceholderText(DefaultFilterStr);
    filterLineEdit->resize(this->width(), FILTER_HEIGHT);
    connect(filterLineEdit, SIGNAL(textChanged(QString)), this, SLOT(applyFilter(QString)));
}

void FilterableTreeView::applyFilter(QString filterStr)
{
    QStandardItemModel* stateModel = qobject_cast<QStandardItemModel*>(model());

    if (!stateModel)
    {
        throw armarx::LocalException("casted model is null");
    }

    QStandardItem* root = stateModel->invisibleRootItem();

    if (filterStr == DefaultFilterStr)
    {
        return;
    }

    if (hideChildren)
    {
        applyFilterBottomUp(root, filterStr);
    }
    else
    {
        applyFilterTopDown(root, filterStr, false);
    }

    //    setRowHidden(0, root->index(), false);
    if (filterStr.length() > 0)
    {
        expandAll();
    }
}

void FilterableTreeView::resizeEvent(QResizeEvent* event)
{
    filterLineEdit->resize(event->size().width(), FILTER_HEIGHT);
    QTreeView::resizeEvent(event);
}

bool FilterableTreeView::applyFilterBottomUp(const QStandardItem* parent, QString searchString)
{
    bool show = false;

    for (int row = 0; row < parent->rowCount(); ++row)
    {
        QStandardItem* child = parent->child(row);

        if (!child)
        {
            continue;
        }

        bool showThisRow = applyFilterBottomUp(child, searchString);
        QRegExp rex(searchString, Qt::CaseInsensitive, QRegExp::Wildcard);

        if (child->data(Qt::DisplayRole).toString().contains(rex))
        {
            showThisRow = true;
        }

        if (showThisRow)
        {
            show = showThisRow;
        }

        setRowHidden(row, parent->index(), !showThisRow);

    }

    return show;

}

bool FilterableTreeView::applyFilterTopDown(const QStandardItem* parent, QString searchString, bool showAnyway)
{
    bool show = false;

    for (int row = 0; row < parent->rowCount(); ++row)
    {
        QStandardItem* child = parent->child(row);

        if (!child)
        {
            continue;
        }

        QRegExp rex(searchString, Qt::CaseInsensitive, QRegExp::Wildcard);
        bool rowMatches = child->data(Qt::DisplayRole).toString().contains(rex);
        bool showThisRow = applyFilterTopDown(child, searchString, rowMatches || showAnyway);

        if (rowMatches)
        {
            showThisRow = true;
        }

        if (showThisRow)
        {
            show = showThisRow;
        }

        setRowHidden(row, parent->index(), !(showThisRow || showAnyway));


    }

    return show;
}

