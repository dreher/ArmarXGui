/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Nikolaus Vahrenkamp
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include "CoinViewer.h"
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx
{


    CoinViewer::CoinViewer(QWidget* parent,
                           const char* name,
                           SbBool embed,
                           SoQtFullViewer::BuildFlag flag,
                           SoQtViewer::Type type)
        : SoQtExaminerViewer(parent, name, embed, flag, type)
    {
        mutex.reset(new boost::recursive_mutex);
        //ARMARX_IMPORTANT_S << "** Coin viewer init mutex:" << mutex.get();
    }

    CoinViewer::~CoinViewer()
    {
    }

    void CoinViewer::setMutex(boost::shared_ptr<boost::recursive_mutex> m)
    {
        //ARMARX_IMPORTANT_S << "set coinviewer mutex3d:" << m.get();
        mutex = m;
    }

    ScopedRecursiveLockPtr CoinViewer::getScopedLock()
    {
        ScopedRecursiveLockPtr l;

        if (mutex)
        {
            l.reset(new ScopedRecursiveLock(*mutex));
        }

        return l;
    }

    void CoinViewer::actualRedraw(void)
    {
        // require lock
        clock_t start = clock();
        ScopedRecursiveLockPtr l = getScopedLock();
        clock_t end = clock();
        long timeMS = (long)(((float)(end - start) / (float)CLOCKS_PER_SEC) * 1000.0);

        if (timeMS > 50)
        {
            ARMARX_IMPORTANT_S << " Redraw lock time:" << timeMS;
        }

        // Render normal scenegraph.
        SoQtExaminerViewer::actualRedraw();
    }
}
