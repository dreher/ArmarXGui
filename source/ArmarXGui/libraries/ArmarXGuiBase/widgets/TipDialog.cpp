/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TipDialog.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/ui_TipDialog.h>
#include <ArmarXCore/core/logging/Logging.h>



namespace armarx
{

    TipDialog::TipDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::TipDialog)
    {
        ui->setupUi(this);
    }

    TipDialog::~TipDialog()
    {
        delete ui;
    }

    void TipDialog::showMessage(const QString& tipText, const QString& windowTitle, const QString& stringIdentifier)
    {
        if (tipText.isEmpty())
        {
            return;
        }

        if (isBlackListed(stringIdentifier.isEmpty() ? tipText : stringIdentifier))
        {
            this->setWindowTitle(windowTitle);
            ui->editTip->setPlainText(tipText);
            show();
            lastId = stringIdentifier.isEmpty() ? tipText : stringIdentifier;
        }
        else
        {
            ARMARX_DEBUG_S << "Not showing because of blacklist";
        }
    }

    QStringList TipDialog::getBlackListedStrings() const
    {
        return blackList;
    }

    void TipDialog::setBlackListedStrings(const QStringList& strings)
    {
        blackList = strings;
    }

    bool TipDialog::isBlackListed(const QString& stringId) const
    {
        return !blackList.contains(stringId);
    }

    void TipDialog::hideEvent(QHideEvent*)
    {
        if (ui->cbDoNotShowAgain->isChecked())
        {
            ARMARX_DEBUG_S << "Storing in blacklist";

            if (!blackList.contains(lastId))
            {
                blackList.append(lastId);
            }
        }
    }

} // namespace armarx
