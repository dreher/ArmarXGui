#pragma once

#include <QCompleter>
#include <QObject>

class QStringListModel;

namespace armarx
{
    class InfixFilterModel;

    /**
     * @brief This class changes the standard QCompleter to an infix match completer.
     */
    class InfixCompleter : public QCompleter
    {
        Q_OBJECT

        // QCompleter interface
    public:
        InfixCompleter(const QStringList& completionList, QObject* parent = 0);
        void setCompletionList(const QStringList& completionList);
        InfixFilterModel* getProxyModel() const;
        QStringList splitPath(const QString& path) const override;
    public slots:
        void setCompletionInfix(const QString& infix);
    protected:
        InfixFilterModel* proxyModel;
        QStringListModel* completionModel;
    };


}

