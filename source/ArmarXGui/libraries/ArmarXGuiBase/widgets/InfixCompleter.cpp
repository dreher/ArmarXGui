#include "InfixCompleter.h"
#include "InfixFilterModel.h"

#include <QListView>
#include <QStringListModel>

namespace armarx
{

    InfixCompleter::InfixCompleter(const QStringList& completionList, QObject* parent) : QCompleter(parent)
    {
        QListView* popup = new QListView();
        completionModel = new QStringListModel(completionList, this);
        proxyModel = new InfixFilterModel();
        proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
        proxyModel->setSourceModel(completionModel);
        setModel(proxyModel);
        setPopup(popup);
        setCaseSensitivity(Qt::CaseInsensitive);
    }

    void InfixCompleter::setCompletionList(const QStringList& completionList)
    {
        completionModel->setStringList(completionList);
    }

    InfixFilterModel* InfixCompleter::getProxyModel() const
    {
        return proxyModel;
    }

    QStringList InfixCompleter::splitPath(const QString& path) const
    {
        return QStringList("");
    }

    void InfixCompleter::setCompletionInfix(const QString& infix)
    {
        proxyModel->setFilterFixedString(infix);
    }



}
