/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QDialog>
class QLineEdit;

namespace armarx
{
    /**
     * @class RegExpValidatedInputDialog
     * @brief This class is similar to the QInputDialog, but offers the possibility to specify a regex as input validation.
     */
    class RegExpValidatedInputDialog : public QDialog
    {
        Q_OBJECT
    public:
        explicit RegExpValidatedInputDialog(const QString& windowTitle, const QString& labelName, const QRegExp& regExp, QWidget* parent = 0);
        QString getTextValue() const;
    signals:

    public slots:
    private:
        QLineEdit* edit;
    };

}

