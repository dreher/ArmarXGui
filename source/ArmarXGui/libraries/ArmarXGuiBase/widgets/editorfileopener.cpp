/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "editorfileopener.h"

#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

using namespace armarx;



EditorFileOpener::EditorFileOpener(std::string configFile)
{
    setTag("EditorFileOpener");
    using namespace boost::property_tree;


    static CMakePackageFinder cmake("ArmarXGui");

    if (configFile.empty())
    {
        configFile = cmake.getDataDir()  + "/" + std::string(EDITORFILEOPENER_CONFIGFILE);
    }

    if (!ArmarXDataPath::getAbsolutePath(configFile, configFile))
    {
        ARMARX_WARNING << "Cannot find file '" << configFile << "'";
        return;
    }

    try
    {
        xml_parser::read_xml(configFile, pt);
    }
    catch (xml_parser::xml_parser_error& e)
    {

        ARMARX_WARNING << "Could not load state-configfile '"
                       << configFile << "'. Required parameters may be unset.\nReason at "
                       << e.filename() << ":" << e.line() << ":\n" << e.message() << flush;
        return;
    }

    BOOST_FOREACH(ptree::value_type const & v, pt.get_child("editors"))
    {
        if (v.first == "editor")
        {
            std::string editorName = v.second.get<std::string>("name");
            std::string openCommandLine = v.second.get<std::string>("opencommandline");
            editors[editorName] = openCommandLine;
        }
    }
}

void EditorFileOpener::openFile(const std::string& editorName, const std::string& filepath, int lineNumber)
{
    std::string openCommandLine  = editors[editorName];

    if (openCommandLine.empty())
    {
        ARMARX_WARNING << "Cannot find editor named '" << editorName << "'";
        return;
    }

    std::string::size_type lenFile = std::string(FILE_VARIABLE).size();
    std::string::size_type posFile = openCommandLine.find(FILE_VARIABLE);

    if (posFile != std::string::npos)
    {
        openCommandLine.replace(posFile, lenFile, filepath);
    }

    std::string::size_type lenLine = std::string(LINE_VARIABLE).size();
    std::string::size_type posLine = openCommandLine.find(LINE_VARIABLE);
    std::stringstream lineStr;
    lineStr << lineNumber;

    if (posLine != std::string::npos)
    {
        openCommandLine.replace(posLine, lenLine, lineStr.str());
    }

    boost::algorithm::trim(openCommandLine);

    if (*openCommandLine.rbegin() != '&')
    {
        openCommandLine += " &";
    }

    ARMARX_VERBOSE << "OpenCommand: " << openCommandLine;

    if (std::system(openCommandLine.c_str())) {}
}
