/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MarkdownEditor.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/ui_MarkdownEditor.h>

#include "cpp-markdown/markdown.h"

#include <QDesktopServices>
#include <QMenu>
#include <sstream>

namespace armarx
{

    MarkdownEditor::MarkdownEditor(QWidget* parent) :
        QWidget(parent),
        ui(new Ui::MarkdownEditor)
    {
        ui->setupUi(this);
        connect(ui->btnEdit, SIGNAL(toggled(bool)), this, SLOT(toggleEditor(bool)));
        connect(ui->btnSyntax, SIGNAL(clicked()), this, SLOT(openSyntaxUrl()));
        connect(ui->htmlView, SIGNAL(textChanged()), this, SLOT(__forwardTextChanged()));


    }

    MarkdownEditor::~MarkdownEditor()
    {
        delete ui;
    }

    QString MarkdownEditor::toPlainText() const
    {
        if (ui->btnEdit->isChecked())
        {
            return ui->htmlView->toPlainText();
        }
        else
        {
            return plainText;
        }
    }

    void MarkdownEditor::setPlainText(const QString& plainText)
    {
        this->plainText = plainText;

        if (ui->btnEdit->isChecked())
        {
            ui->htmlView->setPlainText(plainText);
        }
        else
        {
            showMarkdown(plainText);
        }
    }

    void MarkdownEditor::showMarkdown(const QString& rawString)
    {
        markdown::Document doc;

        if (rawString.length() > 0)
        {
            doc.read(rawString.toStdString());
        }
        else
        {
            doc.read(plainText.toStdString());
        }

        std::stringstream html;
        doc.write(html);
        ui->htmlView->setHtml(html.str().c_str());
    }

    void MarkdownEditor::toggleEditor(bool toggled)
    {
        if (toggled)
        {
            ui->htmlView->setPlainText(plainText);
            ui->htmlView->setReadOnly(false);
        }
        else
        {
            plainText = ui->htmlView->toPlainText();
            showMarkdown();
            ui->htmlView->setReadOnly(true);
        }

        //        ui->plainView->setVisible(toggled);
        //        ui->htmlView->setVisible(!toggled);
    }

    void MarkdownEditor::openSyntaxUrl()
    {
        QDesktopServices::openUrl(QUrl("https://help.github.com/articles/markdown-basics/"));
    }

    void MarkdownEditor::__forwardTextChanged()
    {
        if (ui->btnEdit->isChecked())
        {
            emit textChanged();
        }
    }

}
