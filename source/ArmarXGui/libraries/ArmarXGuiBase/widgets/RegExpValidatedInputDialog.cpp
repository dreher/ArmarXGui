/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reservethis->
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RegExpValidatedInputDialog.h"

#include <QDialogButtonBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QValidator>

namespace armarx
{

    RegExpValidatedInputDialog::RegExpValidatedInputDialog(QString const& windowTitle,
            QString const& labelName, const QRegExp& regExp, QWidget* parent) : QDialog(parent)
    {

        this->setWindowTitle(windowTitle);
        QDialogButtonBox* buttonBox = new QDialogButtonBox(this);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        edit = new QLineEdit();
        edit->setValidator(new QRegExpValidator(regExp));
        QGridLayout* layout = new QGridLayout(this);
        this->setLayout(layout);
        layout->addWidget(new QLabel(labelName), 0, 0);
        layout->addWidget(edit, 0, 1);
        layout->addWidget(buttonBox, 1, 0, 1, 2);
        connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
        connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
        resize(250, height());
        edit->setFocus();
    }

    QString RegExpValidatedInputDialog::getTextValue() const
    {
        return edit->text();
    }

} // namespace armarx
