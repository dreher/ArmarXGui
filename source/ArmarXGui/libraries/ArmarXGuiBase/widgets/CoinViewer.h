/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Nikolaus Vahrenkamp
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <boost/thread/recursive_mutex.hpp>
#include <ArmarXCore/core/system/Synchronization.h>

// project
namespace armarx
{

    /*!
     * Basically this is a SoQtExaminerViewer, with the extension that you can specify a mutex that protects the drawing.
     */
    class CoinViewer :
        public SoQtExaminerViewer
    {

    public:
        CoinViewer(QWidget* parent,
                   const char* name = NULL,
                   SbBool embed = TRUE,
                   SoQtFullViewer::BuildFlag flag = BUILD_ALL,
                   SoQtViewer::Type type = BROWSER);
        ~CoinViewer() override;

        /*!
         * If set, the drawing is protected by this mutex. This overwrites the default mutex.
         */
        void setMutex(boost::shared_ptr<boost::recursive_mutex> m);


        /*!
         * \return This lock allows to safely access the viewer's scene graph.
         */
        ScopedRecursiveLockPtr getScopedLock();

    protected:

        /*!
        * \brief actualRedraw Reimplement the redraw method in order to lock engine mutex
        */
        void actualRedraw(void) override;

        boost::shared_ptr<boost::recursive_mutex> mutex;
    };

    typedef boost::shared_ptr<CoinViewer> CoinViewerPtr;
}

