/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>
#include <IceUtil/Handle.h>
#include <ArmarXCore/core/IceGridAdmin.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ui_IceProxyFinder.h>
#include <IceStorm/IceStorm.h>


#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

namespace Ui
{
    class IceProxyFinder;
}

namespace armarx
{
    class IceManager;
    typedef IceUtil::Handle<IceManager> IceManagerPtr;

    /**
     * \class IceProxyFinderBase
     * \brief The IceProxyFinderBase class provides a convenient way to query online proxies in the
     * ice network, i.e. registered with IceGrid.
     *
     * This is just the non-template base class. For the real implementation refer to
     * \ref armarx::IceProxyFinder and \ref armarx::IceTopicFinder.
     *
     * The selected proxy name can be queried with getSelectedProxyName().
     */
    class IceProxyFinderBase :
        public QWidget,
        public Logging
    {
        Q_OBJECT

    public:
        explicit IceProxyFinderBase(QWidget* parent = 0);
        ~IceProxyFinderBase() override;
        void setIceManager(IceManagerPtr icemanager, bool fetchProxies = true);
        virtual QStringList getProxyNameList(QString searchMask) const = 0;
        QString getSelectedProxyName() const;
        void setDefaultSelectedProxy(const QString& proxyName);
        void setSearchMask(const QString& searchMask);
        void showSearchMaskField(bool show);
        void showLabels(bool show);
        Ui::IceProxyFinder* getUi();

    signals:
        void updateProxyListComboBoxSignal(const QStringList& proxyList);
        void triggerProxyListComboBoxUpdateSignal();
        void validProxySelected(const QString& proxyName);

    public slots:
        void triggerProxyListUpdate();
        void comboBoxTextChanged(QString currentText);
        void updateProxyListComboBox(const QStringList& proxyList);
        void evaluateValidProxySelectedSignal(const QString& proxyName);

    protected:
        void updateProxyList();
        void setupUi();
        IceManagerPtr icemanager;
        Ui::IceProxyFinder* ui;
        RunningTask<IceProxyFinderBase>::pointer_type refreshProxyListTask;
    };

    /**
     * @class IceProxyFinder
     * Widget to conveniently retrieve a proxy instance name of a specific interface type (the template parameter).
     * The search mask supports wild cards (only one, \*Unit\* is not possible) and seperators (e.g. Unit|Result).
     */
    template <typename ProxyType>
    class IceProxyFinder :
        public IceProxyFinderBase
    {
    public:
        IceProxyFinder(QWidget* parent = 0) :
            IceProxyFinderBase(parent)
        {}

        QStringList getProxyNameList(QString searchMask) const override
        {
            if (searchMask.length() == 0)
            {
                searchMask = "*";
            }

            Ice::StringSeq proxyList;
            QStringList qProxyList;
            if (!icemanager)
            {
                throw LocalException("IceManager must not be NULL");
            }
            auto searchMasks = Split(searchMask.toStdString(), "|");
            for (auto& subSearchMasks : searchMasks)
            {
                if (icemanager && icemanager->getIceGridSession())
                {

                    IceGridAdminPtr iceGridSession = icemanager->getIceGridSession();
                    IceGrid::AdminPrx admin = iceGridSession->getAdmin();
                    Ice::AsyncResultPtr localBegin_getAllObjectInfos = admin->begin_getAllObjectInfos(subSearchMasks);
                    while (!localBegin_getAllObjectInfos->isCompleted())
                    {
                        if (refreshProxyListTask->isStopped())
                        {
                            return qProxyList;
                        }
                        usleep(10000);
                    }
                    IceGrid::ObjectInfoSeq objects = admin->end_getAllObjectInfos(localBegin_getAllObjectInfos);
                    IceGrid::ObjectInfoSeq::iterator iter = objects.begin();
                    Ice::StringSeq tempProxyList;

                    while (iter != objects.end())
                    {
                        Ice::ObjectPrx current = iter->proxy;

                        ProxyType object;

                        // if objects are hangig we might get connection refused
                        try
                        {
                            if (refreshProxyListTask->isStopped())
                            {
                                return qProxyList;
                            }
                            object = ProxyType::checkedCast(current->ice_timeout(60));
                        }
                        catch (...)
                        {
                        }

                        if (object)
                        {
                            tempProxyList.push_back(iter->proxy->ice_getIdentity().name);
                        }

                        ++iter;
                    }


                    proxyList.insert(proxyList.end(), tempProxyList.begin(), tempProxyList.end());
                }
            }



            for (unsigned int i = 0; i < proxyList.size(); ++i)
            {
                qProxyList << QString::fromStdString(proxyList.at(i));
            }
            qProxyList.removeDuplicates();
            return qProxyList;
        }
    };

    /**
     * @class IceTopicFinder
     * @brief The IceTopicFinder class queries and show all available topic registered with IceStorm.
     */
    class IceTopicFinder :
        public IceProxyFinderBase
    {
    public:
        IceTopicFinder(QWidget* parent = 0) :
            IceProxyFinderBase(parent)
        {
            ui->labelProxy->setText("Topic");
        }

        QStringList getProxyNameList(QString searchMask) const override
        {
            if (searchMask.length() == 0)
            {
                searchMask = "*";
            }

            IceStorm::TopicDict topicList;

            if (!icemanager)
            {
                throw LocalException("IceManager must not be NULL");
            }

            if (icemanager && icemanager->getIceGridSession())
            {
                topicList = icemanager->getTopicManager()->retrieveAll();
            }

            QStringList qProxyList;

            for (auto& e : topicList)
            {
                auto name =  QString::fromStdString(e.first);
                QRegExp rex(searchMask, Qt::CaseInsensitive, QRegExp::Wildcard);

                if (name.contains(rex))
                {
                    qProxyList << name;
                }
            }

            return qProxyList;
        }
    };

}

