/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QStandardItem>

#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx
{

    class VariantItem : public QStandardItem
    {
    public:
        explicit VariantItem(const QString& name, const VariantPtr& value);

    signals:

    public slots:
    private:
        VariantPtr value;

        // QStandardItem interface
    public:
        int type() const override;
        static const int TYPE = QStandardItem::UserType + 1337;
    };

} // namespace armarx

