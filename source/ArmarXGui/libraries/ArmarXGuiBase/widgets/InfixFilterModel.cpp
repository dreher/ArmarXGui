/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "InfixFilterModel.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <QTreeView>

namespace armarx
{


    InfixFilterModel::InfixFilterModel(QObject* parent) : QSortFilterProxyModel(parent)
    {

    }

    bool InfixFilterModel::containsAll(const QString& text, const QStringList& searchParts)
    {
        for (const QString& s : searchParts)
        {
            if (!text.contains(s, Qt::CaseInsensitive))
            {
                return false;
            }
        }
        return true;
    }

    void InfixFilterModel::ExpandFilterResults(QTreeView* treeView)
    {
        if (!treeView)
        {
            return;
        }
        treeView->collapseAll();
        QSortFilterProxyModel* proxyModel = qobject_cast<QSortFilterProxyModel*>(treeView->model());
        if (!proxyModel)
        {
            ARMARX_WARNING_S << "Could not cast treeview model to proxymodel!";
            return;
        }
        QList<QModelIndex> indexList;
        for (int i = 0; i <  proxyModel->rowCount(); ++i)
        {
            indexList << proxyModel->index(i, 0);
        }
        QStringList searchParts = proxyModel->filterRegExp().pattern().split(" ", QString::SkipEmptyParts);
        while (indexList.size() > 0)
        {
            QModelIndex& index = indexList.front();
            if (containsAll(proxyModel->data(index).toString(), searchParts))
            {
                QModelIndex current = index.parent();
                while (current.isValid())
                {
                    treeView->expand(current);
                    current = current.parent();
                }
            }
            int i = 0;

            while (index.child(i, 0).isValid())
            {
                indexList << index.child(i, 0);
                i++;
            }
            indexList.pop_front();
        }
    }

    bool InfixFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
    {

        QModelIndex index0 = sourceModel()->index(source_row, 0, source_parent);
        QList<QModelIndex> indexList;
        indexList << index0;
        /*auto checkIndex = [this](QModelIndex index)
        {
            ARMARX_IMPORTANT << filterRegExp().pattern().toStdString();
            QString text = sourceModel()->data(index).toString();
            for (const QString& s : filterRegExp().pattern().split(" ", QString::SkipEmptyParts))
            {
                ARMARX_IMPORTANT << s.toStdString();
                if (!text.contains(s, Qt::CaseInsensitive))
                {
                    return false;
                }
            }
            return true;
            //return sourceModel()->data(index).toString().contains(filterRegExp());
        };*/

        QStringList searchParts = filterRegExp().pattern().split(" ", QString::SkipEmptyParts);
        while (indexList.size() > 0)
        {
            QModelIndex& index = indexList.front();
            if (containsAll(sourceModel()->data(index).toString(), searchParts))
            {
                return true;
            }
            int i = 0;

            while (index.child(i, 0).isValid())
            {
                indexList << index.child(i, 0);
                i++;
            }
            indexList.pop_front();
        }
        QModelIndex parent = source_parent;
        while (parent.isValid())
        {
            if (sourceModel()->data(parent).toString().contains(filterRegExp()))
            {
                return true;
            }
            parent = parent.parent();

        }
        return false;
    }


} // namespace armarx
