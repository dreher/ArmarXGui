/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "IceProxyFinder.h"

#include <QLineEdit>
#include <QtGui>

using namespace armarx;
IceProxyFinderBase::IceProxyFinderBase(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::IceProxyFinder)
{

    setupUi();
}

IceProxyFinderBase::~IceProxyFinderBase()
{
    if (refreshProxyListTask)
    {
        refreshProxyListTask->stop(true);
    }
    delete ui;
}

void IceProxyFinderBase::setIceManager(IceManagerPtr icemanager, bool fetchProxies)
{
    this->icemanager = icemanager;
    if (fetchProxies)
    {
        emit triggerProxyListComboBoxUpdateSignal();
    }
}

QString IceProxyFinderBase::getSelectedProxyName() const
{
    return ui->cbProxyName->currentText();
}

void IceProxyFinderBase::setDefaultSelectedProxy(const QString& proxyName)
{
    ui->cbProxyName->setEditText(proxyName);
}

void IceProxyFinderBase::setSearchMask(const QString& searchMask)
{
    ui->edtSearchMask->setText(searchMask);
}

void IceProxyFinderBase::showSearchMaskField(bool show)
{
    ui->edtSearchMask->setVisible(show);
    ui->labelMask->setVisible(show);
}

void IceProxyFinderBase::showLabels(bool show)
{
    ui->labelMask->setVisible(show);
    ui->labelProxy->setVisible(show);
}

Ui::IceProxyFinder* IceProxyFinderBase::getUi()
{
    return ui;
}

void IceProxyFinderBase::triggerProxyListUpdate()
{
    if (refreshProxyListTask && refreshProxyListTask->isRunning())
    {
        return;
    }

    ui->btnRefresh->setEnabled(false);
    ui->btnRefresh->setToolTip("Refreshing proxy list with mask '" + ui->edtSearchMask->text() + "'. Please wait...");
    refreshProxyListTask = new RunningTask<IceProxyFinderBase>(this, &IceProxyFinderBase::updateProxyList);
    refreshProxyListTask->start();
}

void IceProxyFinderBase::comboBoxTextChanged(QString currentText)
{
    triggerProxyListUpdate();
}

void IceProxyFinderBase::updateProxyListComboBox(const QStringList& proxyList)
{
    ui->cbProxyName->clear();
    int i = 0;

    for (; i < proxyList.size(); ++i)
    {
        ui->cbProxyName->addItem(proxyList.at(i));
    }

    ui->btnRefresh->setEnabled(true);
    ui->btnRefresh->setToolTip("Press to refetch all available Kinematic Units with the given mask");
}

void IceProxyFinderBase::evaluateValidProxySelectedSignal(const QString& proxyName)
{
    try
    {
        // Try to get the object proxy "objPrx" represented by "proxyName"
        IceGrid::AdminPrx admin = this->icemanager->getIceGridSession()->getAdmin();
        Ice::Identity objectIceId = this->icemanager->getCommunicator()->stringToIdentity(proxyName.toStdString());
        Ice::ObjectPrx objPrx = admin->getObjectInfo(objectIceId).proxy;

        // Try to ping the proxy (it could still be a stale entry in IceGrid)
        objPrx->ice_ping();

        // If this point is reached, the object represented by "proxyName" is valid and reachable via Ice - that is, the signal can be emitted
        emit validProxySelected(proxyName);
    }
    catch (...)
    {
        // pass: Any exception triggered in the try-block indicates that "proxyName" is not a valid indicator for an object known to Ice
    }
}

void IceProxyFinderBase::updateProxyList()
{
    auto result = getProxyNameList(ui->edtSearchMask->text());
    if (!refreshProxyListTask->isStopped())
    {
        emit updateProxyListComboBoxSignal(result);
    }
}

void IceProxyFinderBase::setupUi()
{
    this->ui->setupUi(this);

    this->connect(this, SIGNAL(updateProxyListComboBoxSignal(QStringList)), this, SLOT(updateProxyListComboBox(QStringList)));

    // These signals should trigger an update of the proxy list
    this->connect(this->ui->btnRefresh, SIGNAL(clicked()), this, SLOT(triggerProxyListUpdate()));
    this->connect(this->ui->edtSearchMask, SIGNAL(textEdited(QString)), this, SLOT(triggerProxyListUpdate()));
    this->connect(this, SIGNAL(triggerProxyListComboBoxUpdateSignal()), this, SLOT(triggerProxyListUpdate()));

    // If the combobox changes in any way (text manually updated or new item selected) it should be evaluated whether
    // the "validProxySelected" signal should be emitted
    this->connect(this->ui->cbProxyName, SIGNAL(editTextChanged(QString)), this, SLOT(evaluateValidProxySelectedSignal(QString)));
    this->connect(this->ui->cbProxyName, SIGNAL(currentIndexChanged(QString)), this, SLOT(evaluateValidProxySelectedSignal(QString)));
}
