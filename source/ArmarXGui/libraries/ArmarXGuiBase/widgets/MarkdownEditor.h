/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>

namespace armarx
{

    namespace Ui
    {
        class MarkdownEditor;
    }

    /**
     * @class MarkdownEditor
     * @brief The MarkdownEditor is a widget that provides editing of raw text and viewing
     * of processed markdown text to html.
     * The widget has a view mode (default and read only) which displays to html converted markdown text
     * like a internet browser and an edit mode that show the raw markdown text, which can be edited.
     * The mode can be toggled with the Edit button.
     * Raw markdown text can be inserted with the the setPlainText() function and retrieved
     * with the toPlainText() function.
     */
    class MarkdownEditor : public QWidget
    {
        Q_OBJECT

    public:
        explicit MarkdownEditor(QWidget* parent = 0);
        ~MarkdownEditor() override;
        QString toPlainText() const;
        void setPlainText(const QString& plainText);
    public slots:
        void showMarkdown(const QString& rawString = "");
        void toggleEditor(bool toggled);
        void openSyntaxUrl();
    private slots:
        void __forwardTextChanged();
    signals:
        void textChanged();
    private:
        Ui::MarkdownEditor* ui;
        QString plainText;
    };


}

