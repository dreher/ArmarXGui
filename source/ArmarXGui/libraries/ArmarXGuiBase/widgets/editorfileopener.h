/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/logging/Logging.h>

#include <map>

#include <boost/property_tree/ptree.hpp>

namespace armarx
{

#define EDITORFILEOPENER_CONFIGFILE "ArmarXGui/EditorFileOpenerConfig.xml"
#define FILE_VARIABLE "$file"
#define LINE_VARIABLE "$line"

    /**
     * \brief The EditorFileOpener class
     */
    class EditorFileOpener : public Logging
    {
    public:
        EditorFileOpener(std::string configFile = "");
        void openFile(const std::string& editorName, const std::string& filepath, int lineNumber = 0);
        void setOpencommand(const std::string& editorName, const std::string& openCommandLine);
    private:
        std::map<std::string, std::string> editors;
        boost::property_tree::ptree pt;
    };
}

