#include "PluginCache.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiInterface.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ArmarXManager.h>

#include <QDateTime>
#include <QDirIterator>
#include <QFile>
#include <QResource>
#include <QCryptographicHash>

namespace armarx
{

    PluginCache::PluginCache(ArmarXManagerPtr manager) :
        cachePath(GetIconCachePath()),
        manager(manager),
        s(settingsOrganization, settingsApplicationName)
    {
        ARMARX_CHECK_EXPRESSION(manager);
        QDir dir(cachePath);
        if (!dir.exists())
        {
            dir.mkpath(".");
        }


        auto map = s.value("widgets").toMap();
        for (QString key : map.keys())
        {
            QStringList w = map[key].toString().split(":");
            QStringList keyparts = key.split(":");
            if (w.size() == 3)
            {
                auto& elem = pluginData[keyparts[0]];
                elem.pluginPath = keyparts[0];
                elem.lastModified = QDateTime::fromMSecsSinceEpoch(w[0].toLongLong());
                elem.hash = w[1].toLatin1();
                ARMARX_INFO_S << "Widget: " << w[2];
                elem.widgets[w[2]] = ArmarXWidgetInfoPtr();
            }
        }
    }

    PluginCache::~PluginCache()
    {
        shutdown = true;
        if (preloadFuture.valid())
        {
            preloadFuture.wait();
        }
    }

    bool PluginCache::cachePlugin(const QString& pluginPath)
    {
        if (pluginPath.isEmpty())
        {
            return false;
        }
        bool found = false;
        QFileInfo info(pluginPath) ;
        {
            ScopedRecursiveLock lock(cacheMutex);
            for (auto& elem : pluginData)
            {
                //            ARMARX_INFO_S << info.lastModified().toString() << elem.pluginPath;
                if (elem.pluginPath == pluginPath)
                {
                    if (info.lastModified() == elem.lastModified)
                    {
                        ARMARX_DEBUG_S << "Same timestamp";
                        found = true;
                    }
                    else if (getHash(pluginPath) == elem.hash)
                    {
                        ARMARX_DEBUG_S << "Same hash - timestamps " << info.lastModified().toString() << " <> " << elem.lastModified.toString() ;
                        updateLastModifiedTimestamp(pluginPath);
                        found = true;
                    }
                    else
                    {
                        ARMARX_VERBOSE_S << "Plugin filestamp and hash is different - loading plugin again.";
                    }
                    break;
                }
            }
        }
        if (!found)
        {
            writeToCache(pluginPath);
            return false;
        }
        ARMARX_INFO_S << "Found plugin " << pluginPath << " in cache";
        return true;
    }

    bool PluginCache::cacheWidget(QString widgetName, ArmarXWidgetInfoPtr widgetCreator)
    {
        ScopedRecursiveLock lock(cacheMutex);
        pluginData[""].widgets[widgetName] = widgetCreator;
        return true;
    }

    ArmarXWidgetInfoPtr PluginCache::getWidgetCreator(const QString& widgetName)
    {
        ScopedRecursiveLock lock(cacheMutex);
        for (const PluginData& data : pluginData)
        {
            WidgetCreatorMap::const_iterator it = data.widgets.find(widgetName);
            if (it != data.widgets.end())
            {
                if (it->second)
                {
                    return it->second;
                }
            }
        }
        PluginData data = loadFromCache(widgetName);
        pluginData[data.pluginPath] = data;
        WidgetCreatorMap::const_iterator it = data.widgets.find(widgetName);
        if (it != data.widgets.end())
        {
            return it->second;
        }
        // widget does not exist (anymore) -> remove from cache file
        removeWidgetFromCache(data.pluginPath,   widgetName);
        return ArmarXWidgetInfoPtr();
    }

    QStringList PluginCache::getAvailableWidgetNames() const
    {
        ScopedRecursiveLock lock(cacheMutex);
        QStringList result;
        for (const PluginData& data : pluginData)
        {
            for (const auto& elem : data.widgets)
            {
                result << elem.first;
            }
        }
        return result;

    }

    WidgetCreatorMap PluginCache::getAvailableWidgets() const
    {
        ScopedRecursiveLock lock(cacheMutex);
        WidgetCreatorMap result;
        for (const PluginData& data : pluginData)
        {
            result.insert(data.widgets.begin(), data.widgets.end());
        }
        return result;
    }

    void PluginCache::copyResourcesToCache()
    {
        QDirIterator it(":", QDirIterator::Subdirectories);
        while (it.hasNext())
        {
            QString path = it.next().remove(0, 1);
            auto newPath = cachePath + "/resources/" + path;
            QDir dir(newPath);
            if (!dir.exists())
            {
                dir.mkpath(".");
            }
            if (!path.isEmpty() && !QFile::exists(newPath))
            {
                QFile::copy(it.next(), newPath);
            }
        }
    }

    QString PluginCache::GetIconCachePath()
    {
        return QString(armarx::ArmarXDataPath::GetCachePath().c_str()) + "/icons/";
    }

    QString PluginCache::GetIconPath(const QString& widgetName)
    {
        return GetIconCachePath() + widgetName + ".png";
    }

    QString PluginCache::GetCategoryIconPath(const QString& widgetName)
    {
        return GetIconCachePath() + widgetName + "-category.png";
    }

    bool PluginCache::ContainsAny(const QString& str, const QStringList& items)
    {
        for (const QString& item : items)
        {
            if (str.contains(item, Qt::CaseInsensitive))
            {
                return true;
            }
        }
        return false;
    }

    void PluginCache::preloadAsync(QStringList widgetNames, int delayMS)
    {
        widgetNames.removeDuplicates();
        auto preload = [this](QStringList widgetNames, int delayMS)
        {
            QStringList blacklist = {"libPointCloudViewerGuiPlugin"};
            usleep(delayMS * 1000);
            for (QString widgetName : widgetNames)
            {
                {
                    ScopedRecursiveLock lock(cacheMutex);
                    QString pluginPath;
                    for (PluginData& data : pluginData)
                    {
                        if (ContainsAny(data.pluginPath, blacklist))
                        {
                            ARMARX_VERBOSE << deactivateSpam(10, data.pluginPath.toStdString()) << "skipping blacklisted plugin for preload: " << data.pluginPath;
                            continue;
                        }
                        for (auto& elem : data.widgets)
                        {
                            if (shutdown)
                            {
                                return;
                            }
                            if (widgetName == elem.first)
                            {
                                pluginPath = data.pluginPath;
                                if (!data.pluginLoader && !pluginPath.isEmpty())
                                {
                                    auto pluginLoader = QSharedPointer<QPluginLoader>(new QPluginLoader(pluginPath));
                                    ARMARX_INFO_S << "Loading plugin " << pluginPath;
                                    pluginLoader->load();
                                    //                                data.pluginLoader->load();
                                    break;
                                }
                            }
                        }
                    }
                }
                usleep(1000); // give others chance to get mutex
            }
        };
        preloadFuture = std::async(std::launch::async, preload, widgetNames, delayMS);
    }

    void PluginCache::clearCacheFile()
    {
        ScopedRecursiveLock lock(cacheMutex);
        ARMARX_INFO << "Clearing plugin cache";
        s.clear();
    }

    QByteArray PluginCache::getHash(const QString& pluginPath)
    {
        QFile file(pluginPath);
        QByteArray hashData;
        if (file.open(QIODevice::ReadOnly))
        {
            QByteArray fileData = file.readAll();
            hashData = QCryptographicHash::hash(fileData, QCryptographicHash::Md5);
        }
        return hashData.toHex();
    }

    void PluginCache::removeWidgetFromCache(QString pluginPath, QString widgetName)
    {
        ScopedRecursiveLock lock(cacheMutex);
        QMap<QString, QVariant> widgetMap = s.value("widgets").toMap();
        widgetMap.remove(pluginPath + ":" + widgetName);
        s.setValue("widgets", widgetMap);

        QMap<QString, PluginData> newPluginDataMap;
        for (auto& key : pluginData.keys())
        {
            if (pluginData[key].pluginPath != pluginPath && pluginData[key].widgets.count(widgetName) == 0)
            {
                newPluginDataMap[key] = pluginData[key];
            }
        }
        pluginData.swap(newPluginDataMap);
    }

    void PluginCache::removePluginFromCache(QString pluginPath)
    {
        ScopedRecursiveLock lock(cacheMutex);
        ARMARX_INFO << "Removing plugin " << pluginPath << " from cache";
        QMap<QString, QVariant> widgetMap = s.value("widgets").toMap();
        QMap<QString, QVariant> newWidgetMap;
        for (auto key : widgetMap.keys())
        {
            if (!key.contains(pluginPath))
            {
                newWidgetMap[key] = widgetMap[key];
            }
        }
        QMap<QString, PluginData> newPluginDataMap;
        for (auto& key : pluginData.keys())
        {
            if (pluginData[key].pluginPath != pluginPath)
            {
                newPluginDataMap[key] = pluginData[key];
            }
        }
        pluginData.swap(newPluginDataMap);
        s.setValue("widgets", widgetMap);
    }

    void PluginCache::updateLastModifiedTimestamp(const QString& pluginPath)
    {
        QDateTime time = QFileInfo(pluginPath).lastModified();
        ScopedRecursiveLock lock(cacheMutex);
        QMap<QString, QVariant> widgetMap = s.value("widgets").toMap();
        ARMARX_DEBUG_S << "Updating last modified timestamp of plugin " << pluginPath << " to " << time.toString();
        auto& pluginDataElement = pluginData[pluginPath];
        for (auto& widget : pluginDataElement.widgets)
        {
            widgetMap[pluginPath + ":" + widget.first] = QString::number(time.toMSecsSinceEpoch()) + ":" + pluginDataElement.hash + ":" + widget.first;
        }

        assert(QString::number(time.toMSecsSinceEpoch()).toLongLong() == time.toMSecsSinceEpoch());

        pluginDataElement.lastModified = time;
        s.setValue("widgets", widgetMap);
    }

    void PluginCache::writeToCache(const QString& pluginPath)
    {
        auto start = IceUtil::Time::now();
        QByteArray hashData = getHash(pluginPath);
        ARMARX_DEBUG_S << "Hashing took " << (IceUtil::Time::now() - start).toMicroSecondsDouble();
        QSharedPointer<QPluginLoader> loader(new QPluginLoader(pluginPath));
        auto widgets = loadPlugin(loader);
        copyResourcesToCache();
        ARMARX_INFO_S << "Writing plugin " << pluginPath << " to cache";
        ScopedRecursiveLock lock(cacheMutex);
        QMap<QString, QVariant> widgetMap = s.value("widgets").toMap();
        QDateTime time = QFileInfo(pluginPath).lastModified();
        for (auto& elem : widgets)
        {
            widgetMap[pluginPath + ":" + elem.first] = QString::number(time.toMSecsSinceEpoch()) + ":" + hashData + ":" + elem.first;
        }

        s.setValue("widgets", widgetMap);


        pluginData[pluginPath] = {loader, pluginPath, hashData, time, widgets};

    }

    PluginData PluginCache::loadFromCache(const QString& widgetName)
    {
        ARMARX_INFO_S << "Loading widget " << widgetName << " from cache ";

        QString pluginPath;
        QSharedPointer<QPluginLoader> loader;
        WidgetCreatorMap widgets;
        QDateTime time;
        QByteArray hash;
        QMap<QString, QVariant> map;
        {
            ScopedRecursiveLock lock(cacheMutex);
            map = s.value("widgets").toMap();
        }
        for (auto key : map.keys())
        {
            QStringList w = map[key].toString().split(":");
            QStringList keyparts = key.split(":");
            if (w.size() == 3)
            {
                if (w[2] == widgetName)
                {
                    pluginPath = keyparts[0];
                    if (QFile::exists(pluginPath))
                    {
                        hash = w[1].toLatin1();
                        loader = QSharedPointer<QPluginLoader>(new QPluginLoader(pluginPath));
                        widgets = loadPlugin(loader);
                        time = QDateTime::fromMSecsSinceEpoch(w[0].toLongLong());
                        break;
                    }
                    else
                    {
                        ARMARX_INFO_S << pluginPath << " does not exist";
                    }

                }
            }

        }
        if (!loader)
        {
            ARMARX_WARNING_S << "Could not find " << widgetName << " in cache";
        }

        return PluginData {loader, pluginPath, hash, time, widgets};
    }

    WidgetCreatorMap PluginCache::loadPlugin(QSharedPointer<QPluginLoader> loader)
    {
        WidgetCreatorMap result;
        ARMARX_INFO_S << "Loading plugin " << loader->fileName();
        QObject* plugin = loader->instance(); // calls load

        if (plugin)
        {
            ArmarXGuiInterface* newPlugin = qobject_cast<ArmarXGuiInterface*>(plugin);
            if (newPlugin)
            {
                //ARMARX_INFO << "cast successful" << armarx::flush;
                WidgetCreatorMap newWidgets = newPlugin->getProvidedWidgets();
                for (auto elem : newWidgets)
                {
                    ArmarXWidgetInfoPtr info = elem.second;
                    QIcon icon = info->getIcon();
                    if (!icon.isNull())
                    {
                        icon.pixmap(64).save(GetIconPath(elem.first));
                    }
                    icon = info->getCategoryIcon();
                    if (!icon.isNull())
                    {
                        icon.pixmap(64).save(GetCategoryIconPath(elem.first));
                    }
                }
                result.insert(newWidgets.begin(), newWidgets.end());
                if (manager)
                {
                    manager->registerKnownObjectFactoriesWithIce();
                }
                else
                {
                    ARMARX_WARNING << "ARMARX Manager is NULL";
                }

            }
            else
            {
                ARMARX_WARNING << "plugin instance is NULL";
            }
        }
        else
        {
            ARMARX_WARNING_S << "Instantiating plugin failed: " << loader->fileName()
                             << "\nError string:\n" << loader->errorString().toStdString();
        }
        return result;
    }

} // namespace armarx
