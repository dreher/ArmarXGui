
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore DefaultWidgetDescriptions)
 
armarx_add_test(DefaultWidgetDescriptionsTest DefaultWidgetDescriptionsTest.cpp "${LIBS}")