/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <stack>
#include <sstream>

namespace armarx
{
    class JsonWriter;
    typedef boost::shared_ptr<JsonWriter> JsonWriterPtr;

    class JsonWriter
    {
    private:
        enum StackType { eEmptyObject, eObjectKey, eObject, eEmptyArray, eArray, eFinal };
        /*
         * stack empty:
         *     writer is empty
         *     valid calls: startObject, startArray, writeRawValue
         * eFinal:
         *     writer contains single value or complete object/array
         *     valid calls: none
         * eEmptyObject:
         *     after startObject()
         *     valid calls: writeKey(...)
         * eObjectKey:
         *     after writeKey(...)
         *     valid calls: startObject, startArray, writeRawValue
         * eObject:
         *     inside object, after complete value
         *     valid calls: writeKey(...)
         * eEmptyArray:
         *     after startArray()
         *     valid calls: startObject, startArray, writeRawValue
         * eArray:
         *     inside array, after complete value
         *     valid calls: startObject, startArray, writeRawValue
         *
         *
         * stack examples:
         * <empty> => empty stack
         * <value> => eFinal
         * [       => eEmptyArray
         * [5      => eArray
         * [[      => eEmptyArray, eEmptyArray
         * {       => eEmptyObject
         * {"a":   => eObjectKey
         * {"a":5  => eObject
         * {"a":[  => eObjectKey, eEmptyArray
         * */


    public:
        JsonWriter(int indenting = 0, const std::string& indentChars = "  ");
        void startObject();
        void endObject();
        void writeKey(const std::string& key);
        void startArray();
        void endArray();
        void writeRawValue(const std::string& value);
        std::string toString();

        static std::string EscapeQuote(const std::string& str);
        static std::string Escape(const std::string& str);
        static inline bool IsControlChar(char c)
        {
            return c > 0 && c <= 0x1F;
        }

    private:
        std::stringstream ss;
        std::stack<StackType> stack;
        int indenting;
        int nesting;
        void writeNewLine();
        void beginValue();
        void beginKey();
        void endKey();
        void endValue();
        void endArrayOrObject();
        std::string indentChars;
    };
}

