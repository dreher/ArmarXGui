/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "JsonWriter.h"
#include "LexerInfo.h"

#include <boost/shared_ptr.hpp>

namespace armarx
{
    class JsonData;
    typedef boost::shared_ptr<JsonData> JsonDataPtr;

    class JsonData
    {
    public:
        JsonData();
        virtual void writeJson(const JsonWriterPtr& writer) = 0;
        std::string toJsonString(int indenting = 0, const std::string indentChars = "  ");

        void setLexerStartOffset(LexerInfo lexerStartOffset);
        void setLexerEndOffset(LexerInfo lexerEndOffset);
        LexerInfo getLexerStartOffset();
        LexerInfo getLexerEndOffset();
        virtual JsonDataPtr clone() = 0;

    private:
        LexerInfo lexerStartOffset;
        LexerInfo lexerEndOffset;
    };
}

