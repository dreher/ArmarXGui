/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "JsonArray.h"

#include <ArmarXCore/core/exceptions/Exception.h>

using namespace armarx;

JsonArray::JsonArray()
{
}

JsonArray::JsonArray(const std::vector<JsonValue>& values)
{
    for (const JsonValue& v : values)
    {
        add(v);
    }
}

void JsonArray::writeJson(const JsonWriterPtr& writer)
{
    writer->startArray();

    for (JsonDataPtr e : elements)
    {
        e->writeJson(writer);
    }

    writer->endArray();
}


void armarx::JsonArray::add(const armarx::JsonDataPtr& value)
{
    elements.push_back(value);
}

void JsonArray::add(const JsonValue& value)
{
    add(value.toSharedPtr());
}

void JsonArray::add(const JsonArray& value)
{
    add(value.toSharedPtr());
}

void JsonArray::set(uint index, const JsonDataPtr& value)
{
    elements.at(index) = value;
}

void JsonArray::set(uint index, const JsonValue& value)
{
    JsonDataPtr valuePtr(new JsonValue(value));
    set(index, valuePtr);
}

void JsonArray::remove(uint index)
{
    if (index >= elements.size())
    {
        throw LocalException("Out of range.");
    }

    elements.erase(elements.begin() + index);
}

uint JsonArray::size()
{
    return elements.size();
}

void JsonArray::clear()
{
    elements.clear();
}

JsonArrayPtr JsonArray::toSharedPtr() const
{
    JsonArrayPtr ptr(new JsonArray(*this));
    return ptr;
}

JsonDataPtr JsonArray::clone()
{
    JsonArrayPtr a(new JsonArray);
    for (const JsonDataPtr& e : elements)
    {
        a->add(e->clone());
    }
    return a;
}
