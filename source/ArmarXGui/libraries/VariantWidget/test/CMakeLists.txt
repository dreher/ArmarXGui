
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore VariantWidget)
 
armarx_add_test(VariantWidgetTest VariantWidgetTest.cpp "${LIBS}")