/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <QStandardItem>
#include <QFileSystemModel>

#include <Ice/Connection.h>

#include "ArmarXManagerModel.h"
#include "ManagedIceObjectItem.h"

using namespace armarx;

ArmarXManagerModel::ArmarXManagerModel():
    iceManager(0)
{
    connect(this, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(updateItem(QStandardItem*)));
}


ArmarXManagerModel::~ArmarXManagerModel()
{

}


ArmarXManagerItem* ArmarXManagerModel::getItem(int row, int column) const
{
    return dynamic_cast<ArmarXManagerItem*>(QStandardItemModel::item(row, column));
}


ArmarXManagerItem* ArmarXManagerModel::getManagerItemByName(const QString& name)
{
    ArmarXManagerItem* item;

    for (int i = 0; i < this->rowCount(); i++)
    {
        item = this->getItem(i);

        if (item->getName().compare(name) == 0)
        {
            return item;
        }
    }

    return NULL;
}

ManagerPrxMap ArmarXManagerModel::getManagerProxyMap() const
{
    ManagerPrxMap result;
    ArmarXManagerItem* item;
    ARMARX_IMPORTANT << this->rowCount();
    for (int i = 0; i < this->rowCount(); i++)
    {
        item = this->getItem(i);
        if (!item->getManagerProxy())
        {
            try
            {
                auto proxy = getIceManager()->getProxy<ArmarXManagerInterfacePrx>(item->getName().toStdString());
                item->setManagerProxy(ArmarXManagerInterfacePrx::uncheckedCast(proxy));

            }
            catch (...)
            {
                armarx::handleExceptions();
            }
        }
        result[item->getName()] = item->getManagerProxy();
    }
    return result;
}


void ArmarXManagerModel::updateManagerDetails(const StateUpdateMap& stateMap)
{
    IceUtil::Time start = IceUtil::Time::now();
    std::set<std::string> offeredTopics;
    std::map<std::string, std::vector<QString> > offererMap;
    std::map<std::string, std::vector<QString> > listenerMap;
    // iterate over all manager items
    for (int i = 0; i < this->rowCount(); i++)
    {
        ArmarXManagerItem* item = this->getItem(i);
        Ice::EndpointSeq endpoints = item->getManagerProxy()->ice_getEndpoints();
        ARMARX_IMPORTANT << item->getManagerProxy()->ice_getAdapterId();
        for (Ice::EndpointPtr endpoint : endpoints)
        {
            ARMARX_IMPORTANT << endpoint->toString();
        }
        auto it = stateMap.find(item->getName());
        if (it == stateMap.end())
        {
            item->setOnline(false);
            continue;
        }
        else
        {
            item->setOnline(it->second.first);
        }
        const ArmarXManagerItem::ObjectMap& updates = it->second.second;
        ArmarXManagerItem::ObjectMap& objects = item->getObjects();
        for (int j = 0; j < updates.size(); j++)
        {
            QString objName = updates.keys().at(j);
            const ManagedIceObjectItem& updateItem = updates[objName];
            ManagedIceObjectItem& objItem = objects[objName];
            objItem.connectivity = updateItem.connectivity;
            objItem.state = updateItem.state;
            for (const auto& topic : objItem.connectivity.offeredTopics)
            {
                offererMap[topic].push_back(objName);
                offeredTopics.insert(topic);
            }
            for (const auto& topic : objItem.connectivity.usedTopics)
            {
                listenerMap[topic].push_back(objName);
            }

        }
    }

    // iterate over all manager items
    for (int i = 0; i < this->rowCount(); i++)
    {
        ArmarXManagerItem* item = this->getItem(i);
        item->setEditable(false);

        QBrush managerBrush;

        if (!item->isOnline())
        {
            managerBrush.setStyle(Qt::SolidPattern);
            managerBrush.setColor(Qt::lightGray);
            item->clear(false);
        }
        else
        {
            managerBrush.setColor(Qt::transparent);
        }

        item->setBackground(managerBrush);

        auto addPropertiesToItem = [](ObjectPropertyInfos properties, QStandardItem * item)
        {
            if (!properties.empty())
            {
                QStandardItem* objectPropertiesItem = new QStandardItem("Properties");
                objectPropertiesItem->setEditable(false);

                QList<QStandardItem*> objectPropertiesRow;

                for (auto property : properties)
                {
                    QStandardItem* objectPropertyItem = new QStandardItem(QString("%1 %2: %3").arg(property.first.c_str())
                            .arg(property.second.constant ? "(constant)" : "")
                            .arg(property.second.value.c_str()));
                    objectPropertyItem->setEditable(!property.second.constant);
                    if (!property.second.constant)
                    {
                        objectPropertyItem->setToolTip("Double-click to edit this property and to send the updated property to the component.");
                    }
                    objectPropertiesRow.append(objectPropertyItem);
                }

                objectPropertiesItem->appendRows(objectPropertiesRow);
                item->appendRow(objectPropertiesItem);
            }
        };

        if (item->rowCount() == 0)
        {
            ArmarXManagerInterfacePrx prx = item->getManagerProxy();
            /*Ice::EndpointSeq endpoints = prx->ice_getEndpoints();
            ARMARX_IMPORTANT << "endpoints: " << endpoints.size();
            for (Ice::EndpointPtr endpoint : endpoints)
            {
                ARMARX_IMPORTANT << "endpoint: " << endpoint->toString();
            }*/
            try
            {
                Ice::ConnectionPtr connection = prx->ice_getConnection();
                //ARMARX_IMPORTANT << "connection: " << connection->toString();
                //ARMARX_IMPORTANT << "getEndpoint: " << ;
                QStandardItem* endpointItem = new QStandardItem(QString::fromStdString("Endpoint: " + connection->getEndpoint()->toString()));
                item->appendRow(endpointItem);
            }
            catch (const IceUtil::Exception& ex)
            {
                item->appendRow(new QStandardItem(QString::fromStdString("Endpoint: ? (" + ex.ice_name() + ")")));
                //std::stringstream ss;
                //ex.ice_print(ss);
                //ARMARX_ERROR << ss.str();
            }

            auto applicationItem = new QStandardItem("Application Properties");
            applicationItem->setEditable(false);
            try
            {
                auto properties = item->getManagerProxy()->ice_timeout(1000)->getApplicationPropertyInfos();
                addPropertiesToItem(properties, applicationItem);
                item->appendRow(applicationItem);
            }
            catch (...) {}
        }


        ArmarXManagerItem::ObjectMap& objects = item->getObjects();
        // iterate over all ManagedIceObjectItems controlled by the manager
        for (int j = 0; j < objects.size(); j++)
        {
            QString objName = objects.keys().at(j);


            ManagedIceObjectItem& objItem = objects[objName];
            if (!objItem.item)
            {
                objItem.item = new QStandardItem(objName);
                objItem.item->setEditable(false);

                item->appendRow(objItem.item);

                auto properties = item->getManagerProxy()->getObjectPropertyInfos(objName.toStdString());

                // iterate over all properties and add them as a separate subentry

                addPropertiesToItem(properties, objItem.item);

                if (objItem.connectivity.offeredTopics.size() > 0)
                {
                    QStandardItem* offeredTopicsItem = new QStandardItem("Offered Topics");
                    offeredTopicsItem->setEditable(false);

                    QList<QStandardItem*> objectPropertiesRow;

                    for (const auto& topic : objItem.connectivity.offeredTopics)
                    {
                        QStandardItem* offeredTopicItem = new QStandardItem(QString::fromStdString(topic));
                        offeredTopicItem->setEditable(false);

                        auto& listeners = listenerMap[topic];
                        for (auto& listener : listeners)
                        {
                            QStandardItem* listenerItem = new QStandardItem(("Listener: " + listener));
                            listenerItem->setEditable(false);
                            offeredTopicItem->appendRow(listenerItem);
                        }

                        objectPropertiesRow.append(offeredTopicItem);

                    }

                    offeredTopicsItem->appendRows(objectPropertiesRow);
                    objItem.item->appendRow(offeredTopicsItem);
                }

                if (objItem.connectivity.usedTopics.size() > 0)
                {
                    QStandardItem* topicsItem = new QStandardItem("Used Topics");
                    topicsItem->setEditable(false);

                    QList<QStandardItem*> objectPropertiesRow;

                    for (const auto& topic : objItem.connectivity.usedTopics)
                    {
                        QStandardItem* topicItem = new QStandardItem(QString::fromStdString(topic));
                        if (offeredTopics.count(topic) == 0)
                        {
                            topicItem->setBackground(QBrush(QColor(255, 163, 33)));
                            topicItem->setToolTip("This topic not offered by any listed component - though not all components might be listed");
                        }
                        else
                        {
                            auto& offerers = offererMap[topic];
                            for (auto& offerer : offerers)
                            {
                                QStandardItem* offererItem = new QStandardItem(("Offerer: " + offerer));
                                offererItem->setEditable(false);
                                topicItem->appendRow(offererItem);
                            }

                        }
                        topicItem->setEditable(false);
                        objectPropertiesRow.append(topicItem);
                    }

                    topicsItem->appendRows(objectPropertiesRow);
                    objItem.item->appendRow(topicsItem);
                }

                DependencyMap depMap = objItem.connectivity.dependencies;
                DependencyMap::iterator it = depMap.begin();

                // iterate over proxy dependencies
                while (it != depMap.end())
                {
                    QStandardItem* depObjItem = new QStandardItem(it->second->getName().c_str());
                    depObjItem->setEditable(false);

                    if (it->second->getResolved())
                    {
                        depObjItem->setBackground(QBrush(QColor(50, 205, 50)));
                    }
                    else
                    {
                        depObjItem->setBackground(QBrush(QColor(255, 106, 106)));
                    }
                    objItem.item->appendRow(depObjItem);

                    ++it;
                }

            }
            else
            {
                DependencyMap depMap = objItem.connectivity.dependencies;
                DependencyMap::iterator it = depMap.begin();

                //                // iterate over proxy dependencies
                //                for (int i = 0; i < objItem.item->rowCount(); ++i) {
                //                    QStandardItem* curItem = objItem.item->child(i);
                //                    if(curItem)
                //                    {
                //                        auto it = depMap.find(curItem->text().toStdString());
                //                        if(it != depMap.end())
                //                        {

                //                        }
                //                    }
                //                }
                // iterate over proxy dependencies
                while (it != depMap.end())
                {
                    int itemRow = -1;
                    for (int i = 0; i < objItem.item->rowCount(); ++i)
                    {
                        QStandardItem* curItem = objItem.item->child(i);
                        if (curItem->text() == QString::fromStdString(it->second->getName()))
                        {
                            itemRow = i;
                        }
                    }

                    if (itemRow >= 0)
                    {
                        QStandardItem* depObjItem = objItem.item->child(itemRow);
                        if (depObjItem)
                        {
                            depObjItem->setEditable(false);

                            if (it->second->getResolved())
                            {
                                depObjItem->setBackground(QBrush(QColor(50, 205, 50)));
                            }
                            else
                            {
                                depObjItem->setBackground(QBrush(QColor(255, 106, 106)));
                            }
                        }

                    }

                    ++it;
                }
            }




            objItem.item->setBackground(getBrush(objItem.state));
        }
    }

    ARMARX_DEBUG_S << "Updating SystemState model took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
}


void ArmarXManagerModel::moveSelectionTo(QItemSelectionModel* selectionModel,
        ArmarXManagerModel* destinationModel)
{
    swapSelection(this, destinationModel, selectionModel);
}


void ArmarXManagerModel::takeSelectionFrom(QItemSelectionModel* selectionModel,
        ArmarXManagerModel* sourceModel)
{
    swapSelection(sourceModel, this, selectionModel);
}


void ArmarXManagerModel::swapSelection(ArmarXManagerModel* sourceModel,
                                       ArmarXManagerModel* destinationModel,
                                       QItemSelectionModel* selectionModel)
{
    QList<QPersistentModelIndex> persistentSelectionModelList =
        getPersistentModelIndex(selectionModel);

    // swap selections
    QList<QPersistentModelIndex>::Iterator persistentModelIndexIter;
    persistentModelIndexIter = persistentSelectionModelList.begin();

    while (persistentModelIndexIter != persistentSelectionModelList.end())
    {
        destinationModel->insertRow(destinationModel->rowCount(),
                                    sourceModel->takeRow(
                                        persistentModelIndexIter->row()));

        ++persistentModelIndexIter;
    }
}


void ArmarXManagerModel::deleteSelection(QItemSelectionModel* selectionModel)
{
    QList<QPersistentModelIndex> persistentSelectionModelList =
        getPersistentModelIndex(selectionModel);

    QList<QPersistentModelIndex>::Iterator persistentModelIndexIter;
    persistentModelIndexIter = persistentSelectionModelList.begin();

    while (persistentModelIndexIter != persistentSelectionModelList.end())
    {
        takeRow(persistentModelIndexIter->row());

        ++persistentModelIndexIter;
    }
}


QList<QPersistentModelIndex> ArmarXManagerModel::getPersistentModelIndex(
    QItemSelectionModel* selectionModel)
{
    QModelIndexList selection = selectionModel->selectedRows();

    // populate persistent selection model list
    QList<QPersistentModelIndex> persistentModelIndex;
    QModelIndexList::Iterator modelIndexIter;
    modelIndexIter = selection.begin();

    while (modelIndexIter != selection.end())
    {
        persistentModelIndex.append(*modelIndexIter);

        ++modelIndexIter;
    }

    return persistentModelIndex;
}


QVariant ArmarXManagerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
                case 0:
                    return "Name";

                case 1:
                    return "Object state";
            }
        }
    }

    return QVariant();
}

Mutex& ArmarXManagerModel::getMutex()
{
    return mutex;
}


void ArmarXManagerModel::updateItem(QStandardItem* item)
{
    ArmarXManagerItem* managerItem = dynamic_cast<ArmarXManagerItem*>(item);

    if (managerItem)
    {
        // ScopedLock lock(getMutex());

        managerItem->setName(item->text());
    }
}


QStringList ArmarXManagerModel::toStringList()
{
    QStringList managerList;

    for (int i = 0; i < this->rowCount(); i++)
    {
        managerList.append(this->getItem(i)->getName());
    }

    return managerList;
}


void ArmarXManagerModel::populate(const QStringList& managerList)
{
    // ScopedLock lock(getMutex());

    this->clear();

    for (int i = 0; i < managerList.size(); i++)
    {
        this->appendRow(new ArmarXManagerItem(managerList.at(i)));
    }
}


ArmarXManagerModel* ArmarXManagerModel::clone()
{
    ArmarXManagerModel* modelCopy = new ArmarXManagerModel();

    ScopedLock lock(getMutex());

    for (int i = 0;  i < rowCount(); i++)
    {
        ArmarXManagerItem* itemClone = new ArmarXManagerItem(item(i)->text());

        modelCopy->appendRow(itemClone);
    }

    return modelCopy;
}


void ArmarXManagerModel::copyFrom(ArmarXManagerModel* source)
{
    // ScopedLock lock(getMutex());
    // ScopedLock lockSource(source->getMutex());

    this->clear();

    for (int i = 0;  i < source->rowCount(); i++)
    {
        ArmarXManagerItem* itemClone = new ArmarXManagerItem(source->item(i)->text());

        appendRow(itemClone);
    }
}


void ArmarXManagerModel::setIceManager(IceManagerPtr iceManager)
{
    this->iceManager = iceManager;
}


IceManagerPtr ArmarXManagerModel::getIceManager() const
{
    return iceManager;
}


QBrush ArmarXManagerModel::getBrush(armarx::ManagedIceObjectState state)
{
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);

    switch (state)
    {
        case armarx::eManagedIceObjectCreated:
            brush.setColor(QColor(255, 106, 106));
            break;

        case armarx::eManagedIceObjectInitializing:
        case armarx::eManagedIceObjectInitialized:
            brush.setColor(QColor(255, 215, 0));
            break;

        case armarx::eManagedIceObjectStarting:
        case armarx::eManagedIceObjectStarted:
            brush.setColor(QColor(50, 205, 50));
            break;

        default:
            brush.setColor(Qt::lightGray);
    }

    return brush;
}

