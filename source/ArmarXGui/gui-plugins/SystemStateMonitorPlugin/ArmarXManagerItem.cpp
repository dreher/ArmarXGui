/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ArmarXManagerItem.h"

using namespace armarx;


ArmarXManagerItem::ArmarXManagerItem(const QString& name):
    QStandardItem(name),
    name(name),
    online(false),
    proxy(0)
{

}


void ArmarXManagerItem::clear(bool clearProxy)
{
    // ScopedLock lock(getMutex());

    while (this->rowCount() > 0)
    {
        this->removeRow(0);
    }

    objects.clear();

    if (clearProxy)
    {
        proxy = 0;
    }

    online = false;
}


QString ArmarXManagerItem::getName()
{
    return name;
}


bool ArmarXManagerItem::isOnline()
{
    return online;
}


ArmarXManagerInterfacePrx ArmarXManagerItem::getManagerProxy()
{
    return proxy;
}


ArmarXManagerItem::ObjectMap& ArmarXManagerItem::getObjects()
{
    return objects;
}


void ArmarXManagerItem::setName(QString name)
{
    // reset item if manager name has been changed
    if (getName().compare(name) != 0)
    {
        clear();

        // ScopedLock lock(getMutex());
        this->name = name;
    }
}


void ArmarXManagerItem::setOnline(bool online)
{
    // ScopedLock lock(getMutex());

    this->online = online;
}


void ArmarXManagerItem::setManagerProxy(ArmarXManagerInterfacePrx proxy)
{
    // ScopedLock lock(getMutex());

    this->proxy = proxy;
}


Mutex& ArmarXManagerItem::getMutex()
{
    return mutex;
}


