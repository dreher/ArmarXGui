/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXManagerRepositoryDialog.h"
#include "ui_ArmarXManagerRepositoryDialog.h"

#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>

#include <string>
#include <iostream>
#include <vector>

#include <QList>
#include <QInputDialog>
#include <QStandardItem>
#include <QItemSelectionModel>
#include <QPersistentModelIndex>
#include <QMessageBox>

using namespace armarx;

ArmarXManagerRepositoryDialog::ArmarXManagerRepositoryDialog(
    ArmarXManagerModel* managerRepositoryModel,
    ArmarXManagerModel* monitoredManagerModel,
    QWidget* parent):
    QDialog(parent),
    ui(new Ui::ArmarXManagerRepositoryDialog),
    parent(parent)
{
    this->managerRepositoryModel = managerRepositoryModel->clone();
    this->monitoredManagerModel = monitoredManagerModel->clone();

    setupView();
}


ArmarXManagerRepositoryDialog::~ArmarXManagerRepositoryDialog()
{
    delete ui;
}


void ArmarXManagerRepositoryDialog::setupView()
{
    ui->setupUi(this);

    ui->managerRepository->setModel(managerRepositoryModel);
    ui->monitoredManagers->setModel(monitoredManagerModel);

    connect(ui->addNewManager, SIGNAL(clicked()), this, SLOT(addNewManager()));
    connect(ui->cancelOkBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->cancelOkBox, SIGNAL(rejected()), this, SLOT(reject()));

    connect(ui->moveToMonitoredManagersButton, SIGNAL(clicked()), this, SLOT(moveToMonitoredManagers()));
    connect(ui->moveToManagerRepositoryButton, SIGNAL(clicked()), this, SLOT(moveToManagerRepository()));
    connect(ui->removeSelectedManagersButton, SIGNAL(clicked()), this, SLOT(removeSelectedManagers()));

    connect(ui->scanButton, SIGNAL(clicked()), this, SLOT(scanForManagers()));

    connect(ui->clearButton, SIGNAL(clicked()), this, SLOT(clearRepository()));
}


void ArmarXManagerRepositoryDialog::addOnlineManagers(const QStringList& managers)
{
    QStringList::ConstIterator it = managers.begin();

    QBrush brush;
    brush.setColor(QColor(78, 238, 148));
    brush.setStyle(Qt::SolidPattern);

    while (it != managers.end())
    {
        ArmarXManagerItem* item = managerRepositoryModel->getManagerItemByName(*it);
        if (!item)
        {
            item = monitoredManagerModel->getManagerItemByName(*it);
        }

        if (!item)
        {
            item = new ArmarXManagerItem(*it);

            // ScopedLock lock(managerRepositoryModel->getMutex());
            managerRepositoryModel->appendRow(item);
        }

        // ScopedLock lock(item->getMutex());
        item->setBackground(brush);

        ++it;
    }
}


void ArmarXManagerRepositoryDialog::addNewManager()
{
    bool ok;
    QString managerName = QInputDialog::getText(parent,
                          tr("New ArmarXManager"),
                          tr("Manager name"),
                          QLineEdit::Normal,
                          tr("SampleApplicationManager"),
                          &ok);

    if (ok)
    {
        ArmarXManagerItem* item = managerRepositoryModel->getManagerItemByName(managerName);

        if (!item)
        {
            //ScopedLock lock(managerRepositoryModel->getMutex());
            managerRepositoryModel->appendRow(new ArmarXManagerItem(managerName));
        }
        else
        {
            QMessageBox::information(parent,
                                     "Add new manager",
                                     managerName + " manager already in repository",
                                     QMessageBox::Ok);
        }
    }
}


void ArmarXManagerRepositoryDialog::moveToMonitoredManagers()
{
    monitoredManagerModel->takeSelectionFrom(
        ui->managerRepository->selectionModel(),
        managerRepositoryModel);
}


void ArmarXManagerRepositoryDialog::moveToManagerRepository()
{
    monitoredManagerModel->moveSelectionTo(
        ui->monitoredManagers->selectionModel(),
        managerRepositoryModel);
}


void ArmarXManagerRepositoryDialog::scanForManagers()
{
    emit requestedManagerScan();
}


void ArmarXManagerRepositoryDialog::clearRepository()
{
    managerRepositoryModel->clear();
}


ArmarXManagerModel* ArmarXManagerRepositoryDialog::getManagerRepositoryModel()
{
    return managerRepositoryModel;
}


ArmarXManagerModel* ArmarXManagerRepositoryDialog::getMonitoredManagersModel()
{
    return monitoredManagerModel;
}


void ArmarXManagerRepositoryDialog::removeSelectedManagers()
{
    managerRepositoryModel->deleteSelection(ui->managerRepository->selectionModel());
}



