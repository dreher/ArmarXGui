/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ManagedIceObjectItem.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <QObject>
#include <QMutex>
#include <QStandardItem>

namespace armarx
{
    class ArmarXManagerItem : public QStandardItem
    {
    public:
        typedef QMap<QString, ManagedIceObjectItem> ObjectMap;

    public:
        explicit ArmarXManagerItem(const QString& name);
        ~ArmarXManagerItem() override { }

        QString getName();
        bool isOnline();
        ArmarXManagerInterfacePrx getManagerProxy();
        ObjectMap& getObjects();

        void setName(QString name);
        void setOnline(bool online);
        void setManagerProxy(ArmarXManagerInterfacePrx proxy);

        /**
         * Clears the item data.
         */
        void clear(bool clearProxy = true);

        /**
         * Returns the access mutex
         *
         * @return Access mutex
         */
        Mutex& getMutex();

    private:
        QString name;
        bool online;
        ArmarXManagerInterfacePrx proxy;
        ObjectMap objects;
        Mutex mutex;
    };
}

