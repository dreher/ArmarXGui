/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ArmarXManagerModel.h"

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <QDialog>
#include <QModelIndex>
#include <QStandardItemModel>
#include <QItemSelectionModel>
#include <QListView>

namespace Ui
{
    class ArmarXManagerRepositoryDialog;
}

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT ArmarXManagerRepositoryDialog :
        public QDialog
    {
        Q_OBJECT

    public:
        explicit ArmarXManagerRepositoryDialog(
            ArmarXManagerModel* managerRepositoryModel,
            ArmarXManagerModel* monitoredManagerModel,
            QWidget* parent = 0);

        ~ArmarXManagerRepositoryDialog() override;

        void addOnlineManagers(const QStringList& managers);

    public slots:
        void addNewManager();
        void removeSelectedManagers();
        void moveToMonitoredManagers();
        void moveToManagerRepository();
        void scanForManagers();
        void clearRepository();

        ArmarXManagerModel* getManagerRepositoryModel();
        ArmarXManagerModel* getMonitoredManagersModel();

    signals:
        void requestedManagerScan();

    protected:
        void setupView();

    private:
        Ui::ArmarXManagerRepositoryDialog* ui;
        QWidget* parent;
        ArmarXManagerModel* managerRepositoryModel;
        ArmarXManagerModel* monitoredManagerModel;

    };
}
