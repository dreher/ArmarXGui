/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Manfred Kroehnert ( manfred.kroehnert at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QAbstractTableModel>
#include <IceGrid/Observer.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <QBrush>


class ServerInfoModel :
    public QAbstractTableModel
{
    Q_OBJECT
public:
    ServerInfoModel()
    {
    }


    ServerInfoModel(IceGrid::ServerDynamicInfoSeq serverInfo)
    {
        setData(serverInfo);
    }

    enum COLUMN_INDEX
    {
        eServerInfo_Enabled = 0,
        eServerInfo_Id,
        eServerInfo_Status,
        eServerInfo_Pid,
    };


    int rowCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return serverInfo.size();
    }


    int columnCount(const QModelIndex& parent = QModelIndex()) const override
    {
        return 4;
    }


    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
    {
        if (!index.isValid())
        {
            return QVariant();
        }

        if (index.row() >= (int)serverInfo.size())
        {
            return QVariant();
        }

        if (role == Qt::DisplayRole)
        {
            armarx::ScopedLock guard(serverInfoMutex);

            switch (index.column())
            {
                case eServerInfo_Id:
                    return QString::fromStdString(serverInfo.at(index.row()).id);
                    break;

                case eServerInfo_Status:
                {
                    switch (serverInfo.at(index.row()).state)
                    {
                        case IceGrid::Inactive:
                            return QString("Inactive");
                            break;

                        case IceGrid::Activating:
                            return QString("Activating");
                            break;

                        case IceGrid::ActivationTimedOut:
                            return QString("ActivationTimedOut");
                            break;

                        case IceGrid::Active:
                            return QString("Active");
                            break;

                        case IceGrid::Deactivating:
                            return QString("Deactivating");
                            break;

                        case IceGrid::Destroying:
                            return QString("Destroying");
                            break;

                        case IceGrid::Destroyed:
                            return QString("Destroyed");
                            break;
                    }
                }

                case eServerInfo_Pid:
                    return QString::number(serverInfo.at(index.row()).pid);
                    break;

                default:
                    return QVariant();
            }
        }
        else if (role == Qt::CheckStateRole)
        {
            if (index.column() == eServerInfo_Enabled)
            {
                return (serverInfo.at(index.row()).enabled) ? Qt::Checked : Qt::Unchecked;
            }
        }
        else if (role == Qt::BackgroundRole)
        {
            return (serverInfo.at(index.row()).enabled) ? QBrush(Qt::green) : QBrush(Qt::lightGray);
        }
        else if (role == Qt::EditRole)
        {
            return (index.column() == 0);
        }

        return QVariant();
    }


    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override
    {
        if (role != Qt::DisplayRole)
        {
            return QVariant();
        }

        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
                case eServerInfo_Enabled:
                    return QString("Enabled");
                    break;

                case eServerInfo_Id:
                    return QString("ID");
                    break;

                case eServerInfo_Status:
                    return QString("Status");
                    break;

                case eServerInfo_Pid:
                    return QString("PID");
                    break;

                default:
                    return QString("");
                    break;
            }
        }

        return QVariant();
    }


    Qt::ItemFlags flags(const QModelIndex& index) const override
    {
        return (index.column() == eServerInfo_Enabled) ? Qt::ItemFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable) : Qt::ItemFlags(Qt::ItemIsEnabled);
    }


    bool setData(const QModelIndex& index, const QVariant& value, int role) override
    {
        if ((role != Qt::CheckStateRole) || (index.column() != eServerInfo_Enabled))
        {
            return false;
        }

        serverInfo.at(index.row()).enabled = ((Qt::CheckState)value.toInt() == Qt::Checked);
        emit dataChanged(index, index);
        emit serverInfoChanged(serverInfo.at(index.row()));
        return true;
    }


    bool setData(IceGrid::ServerDynamicInfoSeq newInfo)
    {
        armarx::ScopedLock guard(serverInfoMutex);
        beginResetModel();
        serverInfo = newInfo;
        endResetModel();
        emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
        return true;
    }

signals:
    void serverInfoChanged(IceGrid::ServerDynamicInfo serverInfo);

protected:
    IceGrid::ServerDynamicInfoSeq serverInfo;
    mutable armarx::Mutex serverInfoMutex;
};

