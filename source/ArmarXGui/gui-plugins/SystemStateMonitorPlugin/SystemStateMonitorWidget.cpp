/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Gui
 * @author     Jan Issac ( jan.issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <stdio.h>
#include <string>
#include <vector>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <Ice/Ice.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>

#include <QLineEdit>
#include <QStyledItemDelegate>
#include <QVector>

#include "SystemStateMonitorWidget.h"
#include <ArmarXGui/gui-plugins/SystemStateMonitorPlugin/ui_ArmarXManagerRepositoryDialog.h>
#include <QApplication>
#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>
#include <ArmarXCore/core/CoreObjectFactories.h>

class PropertyEditingDelegate : public QStyledItemDelegate
{
public:
    PropertyEditingDelegate(armarx::ManagerPrxMap* managerPrxMap) : managerPrxMap(managerPrxMap)
    {
    }

    // QAbstractItemDelegate interface
    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override
    {
        return new QLineEdit(parent);
    }
    void setEditorData(QWidget* editor, const QModelIndex& index) const override
    {
        QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
        if (lineEdit)
        {
            QString propValue = index.data().toString();
            propValue.remove(0, propValue.indexOf(":") + 2);
            QString propName = index.data().toString();

            propName.truncate(propName.indexOf(":") - 1);
            lineEdit->setText(propValue);
            lineEdit->setProperty("propertyName", propName);
        }
    }
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override
    {
        QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
        if (lineEdit)
        {
            model->setData(index, lineEdit->property("propertyName").toString() +  " : " + lineEdit->text());
            auto it = managerPrxMap->find(index.parent().parent().parent().data().toString());
            if (it != managerPrxMap->end())
            {
                armarx::ArmarXManagerInterfacePrx prx = it->second;
                prx->getPropertiesAdmin()->ice_collocationOptimized(false)->setProperties({{
                        lineEdit->property("propertyName").toString().toStdString(),
                        lineEdit->text().toStdString()
                    }
                });
            }
        }
    }
private:
    armarx::ManagerPrxMap* managerPrxMap;
};


using namespace armarx;

SystemStateMonitorWidget::SystemStateMonitorWidget()
{
    setupModel();
    setupView();
    qRegisterMetaType<StateUpdateMap>("StateUpdateMap");
    filterExpansionTimer.setSingleShot(true);
    connect(&filterExpansionTimer, SIGNAL(timeout()), this, SLOT(delayedFilterExpansion()));
}


SystemStateMonitorWidget::~SystemStateMonitorWidget()
{
    //    stateUpdateTimer->stop();
    if (stateUpdateTask)
    {
        stateUpdateTask->stop();
    }

    delete managerRepositoryModel;
    delete monitoredManagerModel;
    //    delete stateUpdateTimer;
}

void SystemStateMonitorWidget::setupModel()
{
    managerRepositoryModel = new ArmarXManagerModel();
    monitoredManagerModel = new ArmarXManagerModel();
}


void SystemStateMonitorWidget::setupView()
{
    ui.setupUi(getWidget());

    managerRepositoryDialog = new ArmarXManagerRepositoryDialog(
        managerRepositoryModel,
        monitoredManagerModel,
        getWidget());
    managerRepositoryDialog->setModal(true);
    connect(managerRepositoryDialog, SIGNAL(accepted()), this, SLOT(acceptConfig()));
    connect(managerRepositoryDialog, SIGNAL(requestedManagerScan()), this, SLOT(retrieveOnlineManagers()));
    ui.monitoredManagersTree->setItemDelegate(new PropertyEditingDelegate(&currentManagerPrxMap));
    auto proxyModel = new InfixFilterModel(this);
    proxyModel->setSourceModel(monitoredManagerModel);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    ui.monitoredManagersTree->setModel(proxyModel);

    connect(ui.lineEditFilter, SIGNAL(textChanged(QString)), proxyModel, SLOT(setFilterFixedString(QString)));
    connect(ui.lineEditFilter, SIGNAL(textChanged(QString)), this, SLOT(expandFilterSelection(QString)), Qt::QueuedConnection);

    connect(ui.configureButton, SIGNAL(clicked()), this, SLOT(openManagerRepositoryDialog()));

    connect(this, SIGNAL(updateManagerStatesSignal(StateUpdateMap)), this, SLOT(updateManagerStates(StateUpdateMap)));

    //    stateUpdateTimer->start();
}


void SystemStateMonitorWidget::loadSettings(QSettings* settings)
{
    managerRepositoryModel->populate(
        settings->value("ManagerRepository").toStringList());

    monitoredManagerModel->populate(
        settings->value("MonitoredManagers").toStringList());
    ui.monitoredManagersTree->expandToDepth(1);
}


void SystemStateMonitorWidget::saveSettings(QSettings* settings)
{
    settings->setValue("ManagerRepository",
                       managerRepositoryModel->toStringList());

    settings->setValue("MonitoredManagers",
                       monitoredManagerModel->toStringList());
}


void SystemStateMonitorWidget::onInitComponent()
{
    monitoredManagerModel->setIceManager(getIceManager());
    managerRepositoryModel->setIceManager(getIceManager());
}


void SystemStateMonitorWidget::onConnectComponent()
{

    stateUpdateTask = new PeriodicTask<SystemStateMonitorWidget>(this, &SystemStateMonitorWidget::retrieveManagerObjectsState, 500, false, "SystemStateMonitorUpdate");
    stateUpdateTask->setDelayWarningTolerance(5000);
    stateUpdateTask->start();
    if (monitoredManagerModel->toStringList().isEmpty()) // only if fresh connect without preconfig
    {
        prefillView();
    }
    else
    {
        currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap();
    }
}

void SystemStateMonitorWidget::onDisconnectComponent()
{
    if (stateUpdateTask)
    {
        stateUpdateTask->stop();
    }

}

void SystemStateMonitorWidget::onExitComponent()
{
    if (stateUpdateTask)
    {
        stateUpdateTask->stop();
    }
}


void SystemStateMonitorWidget::updateManagerStates(const StateUpdateMap& stateMap)
{
    ScopedLock lock(monitoredManagerModel->getMutex());

    monitoredManagerModel->updateManagerDetails(stateMap);
}


bool SystemStateMonitorWidget::retrieveManagerObjectsState(ArmarXManagerInterfacePrx prx, ArmarXManagerItem::ObjectMap& objectStates)
{
    // get proxy if not set
    // update existence state
    try
    {
        prx->ice_timeout(60)->ice_ping();
        //        item->setOnline(true);
    }
    catch (...)
    {
        return false;
    }

    // actual retrieval
    Ice::StringSeq objectNames = prx->getManagedObjectNames();

    for (auto& objectName : objectNames)
    {
        ManagedIceObjectItem objEntry;
        objEntry.name = objectName.c_str();
        objEntry.state = prx->getObjectState(objectName);
        objEntry.connectivity = prx->getObjectConnectivity(objectName);
        objectStates[objEntry.name] = (objEntry);
    }

    return true;
}

void SystemStateMonitorWidget::retrieveManagerObjectsState()
{
    //    ScopedLock lock(monitoredManagerModel->getMutex());
    StateUpdateMap stateMap;
    {
        ScopedLock lock(managerPrxMapMutex);
        //    IceUtil::Time start = IceUtil::Time::now();
        for (auto it = currentManagerPrxMap.begin(); it != currentManagerPrxMap.end(); it++)
        {
            stateMap[it->first].first = retrieveManagerObjectsState(it->second, stateMap[it->first].second);
        }
    }

    //    ARMARX_INFO << "update duration: " << (IceUtil::Time::now() - start).toMilliSecondsDouble();
    emit updateManagerStatesSignal(stateMap);
}

void SystemStateMonitorWidget::acceptConfig()
{
    this->managerRepositoryModel->copyFrom(
        managerRepositoryDialog->getManagerRepositoryModel());

    this->monitoredManagerModel->copyFrom(
        managerRepositoryDialog->getMonitoredManagersModel());
    ui.monitoredManagersTree->expandToDepth(1);
    ScopedLock lock(managerPrxMapMutex);
    currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap();
}

QStringList SystemStateMonitorWidget::fetchOnlineManagers()
{
    getWidget()->setEnabled(false);
    auto admin = getIceManager()->getIceGridSession()->getAdmin();
    ARMARX_INFO << "Getting managers";
    IceGrid::ObjectInfoSeq objects = admin->getAllObjectInfos("*Manager");
    ARMARX_INFO << "Got new managers";
    IceGrid::ObjectInfoSeq::iterator iter = objects.begin();
    IceGrid::ObjectInfoSeq result;

    while (iter != objects.end())
    {
        Ice::ObjectPrx current = iter->proxy;

        ArmarXManagerInterfacePrx object;

        // if objects are hanging we might get connection refused
        try
        {
            object = ArmarXManagerInterfacePrx::checkedCast(current->ice_timeout(60));
        }
        catch (...)
        {
            ARMARX_INFO << current->ice_getIdentity().name << ": " << GetHandledExceptionString();
            // remove dead objects
            admin->removeObject(current->ice_getIdentity());
        }
        qApp->processEvents();

        if (object)
        {
            result.push_back(*iter);
        }

        ++iter;
    }

    getWidget()->setEnabled(true);

    QStringList managers;
    for (const IceGrid::ObjectInfo& info : result)
    {
        managers.append(info.proxy->ice_getIdentity().name.c_str());
    }

    return managers;
}


void SystemStateMonitorWidget::retrieveOnlineManagers()
{
    managerRepositoryDialog->addOnlineManagers(fetchOnlineManagers());
}


void SystemStateMonitorWidget::openManagerRepositoryDialog()
{
    managerRepositoryDialog->getManagerRepositoryModel()->copyFrom(
        managerRepositoryModel);

    managerRepositoryDialog->getMonitoredManagersModel()->copyFrom(
        monitoredManagerModel);

    retrieveOnlineManagers();

    managerRepositoryDialog->show();
}


void SystemStateMonitorWidget::prefillView()
{
    managerRepositoryModel->clear();
    monitoredManagerModel->clear();

    QStringList managers = fetchOnlineManagers();

    QMetaObject::invokeMethod(this, "watchArmarXManagers", Q_ARG(QStringList, managers));
}

void SystemStateMonitorWidget::watchArmarXManagers(QStringList managers)
{
    QStringList::ConstIterator it = managers.begin();

    while (it != managers.end())
    {
        ArmarXManagerItem* item = new ArmarXManagerItem(*it);

        monitoredManagerModel->appendRow(item);

        ++it;
    }
    ScopedLock lock(managerPrxMapMutex);
    currentManagerPrxMap = monitoredManagerModel->getManagerProxyMap();

    ui.monitoredManagersTree->expandToDepth(1);
}

void SystemStateMonitorWidget::expandFilterSelection(QString filterStr)
{
    ARMARX_INFO_S << VAROUT(filterStr);
    if (filterStr.length() == 0)
    {
        ui.monitoredManagersTree->collapseAll();
        //        ui.monitoredManagersTree->expandToDepth(1);
    }
    else
    {
        filterExpansionTimer.start(500);
    }
}

void SystemStateMonitorWidget::delayedFilterExpansion()
{
    InfixFilterModel::ExpandFilterResults(ui.monitoredManagersTree);
}
