/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ProgressbarDelegate.h"
#include <QApplication>
#include <QPainter>
#include <QProgressBar>

using namespace armarx;

void ProgressbarDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() == 3)
    {
        int progress = index.data().toInt();

        QStyleOptionProgressBarV2 progressBarOption;
        progressBarOption.rect = option.rect;
        progressBarOption.minimum = 0;
        progressBarOption.maximum = 100;
        progressBarOption.progress = progress;
        progressBarOption.text = QString::number(progress) + "%";
        progressBarOption.textVisible = true;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar,
                                           &progressBarOption, painter);

        //        QProgressBar renderer;
        //        int progressPercentage = index.model()->data(index, Qt::DisplayRole).toInt();

        //        // Customize style using style-sheet..

        //        QString style = "";
        //        style += "QProgressBar::chunk { background-color: #"+QString::number((int)(255*0.01*progressPercentage), 16,2)+"0000; }";

        //        renderer.resize(option.rect.size());
        //        renderer.setMinimum(0);
        //        renderer.setMaximum(100);
        //        renderer.setValue(progressPercentage);

        //        renderer.setStyleSheet(style);
        //        painter->save();
        //        painter->translate(option.rect.topLeft());
        //        renderer.render(painter);
        //        painter->restore();
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}
