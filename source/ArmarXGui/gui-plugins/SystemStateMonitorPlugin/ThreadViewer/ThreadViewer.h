/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/core/services/tasks/ThreadList.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <QWidget>

namespace Ui
{
    class ThreadViewer;
}
namespace armarx
{

    class ThreadViewerModel;
    class RunningTaskModel;

    /**
     * \page ArmarXGui-GuiPlugins-ThreadViewer ThreadViewer
     * \brief The ThreadViewer displays all threads of an ArmarX application.
     * \image html ThreadViewer.png
     * The ThreadViewer allows you to select a running application from the drop-down-menu on top and list all
     * of its threads.
     * ArmarXGui Documentation \ref ThreadViewer
     * \see SystemStateMonitorPlugin
     */

    /**
     * \class ThreadViewer
     * \brief The ThreadViewer displays all threads of an ArmarX application.
     */
    class ThreadViewer : public ArmarXComponentWidgetControllerTemplate<ThreadViewer>
    {
        Q_OBJECT

    public:
        explicit ThreadViewer();
        ~ThreadViewer() override;

        /**
         * Load stored manager models
         */
        void loadSettings(QSettings* settings) override;

        /**
         * Saves the manager models
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this.
         */
        static QString GetWidgetName()
        {
            return "Meta.ThreadViewer";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/thread_viewer.svg"};
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onExitComponent() override;

    signals:
        void threadManagerListUpdated(QStringList list);
        void threadManagerUpdateRequired();
        void cpuUsageValueUpdated(int value);

    public slots:
        void triggerThreadManagerListUpdate();
        void updateThreadManagerList(QStringList list);
        void managedSelectionChanged(QString selectedString);
        void retrieveThreadManagerList();
        void cpuUsageValueSet(int value);

    private:
        void runThreadManagerUpdate();
        mutable RecursiveMutex proxyMutex;
        ThreadListInterfacePrx threadManagerProxy;
        PeriodicTask<ThreadViewer>::pointer_type periodicTask;
        RunningTask<ThreadViewer>::pointer_type managerUpdateTask;
        ThreadViewerModel* periodicTaskModel;
        RunningTaskModel* runningTaskModel;
        Ui::ThreadViewer* ui;
    };
}

