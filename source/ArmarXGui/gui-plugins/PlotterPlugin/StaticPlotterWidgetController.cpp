#include "StaticPlotterWidgetController.h"


#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QStackedLayout>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt_legend.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>
#include <qwt_series_data.h>
#include <qwt_plot_canvas.h>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <qwt_legend_label.h>
#else
#include <qwt/qwt_legend_item.h>
#endif
#pragma GCC diagnostic pop
#include <QToolBar>

namespace armarx
{

    StaticPlotterWidgetController::StaticPlotterWidgetController() :
        customToolbar(nullptr)

    {
        ui.setupUi(getWidget());
        plotter = new QwtPlot(ui.plotWidget);
        QStackedLayout* stackedLayout = new QStackedLayout(ui.plotWidget);
        stackedLayout->addWidget(plotter);
        ////////////////
        //  Setup Plotter
        ///////////////
        // panning with the left mouse button
        (void) new QwtPlotPanner(plotter->canvas());

        // zoom in/out with the wheel
        /*QwtPlotMagnifier* magnifier = */new QwtPlotMagnifier(plotter->canvas());

        //        plotter->canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false); //increases performance for incremental drawing

        QwtLegend* legend = new QwtLegend;
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
        legend->setDefaultItemMode(QwtLegendData::Mode::Checkable);
#else
        legend->setItemMode(QwtLegend::CheckableItem);
#endif
        plotter->insertLegend(legend, QwtPlot::BottomLegend);


        plotter->setAxisTitle(QwtPlot::xBottom, "Time (in sec)");
        plotter->enableAxis(QwtPlot::yLeft, true);

        //        plotter->enableAxis(QwtPlot::yRight, true);
        plotter->setAxisAutoScale(QwtPlot::yLeft, true);
        plotter->setAxisAutoScale(QwtPlot::xBottom, true);
        plotter->setAutoReplot();

        //            plotter->setCanvasBackground(* new QBrush(Qt::white));

        connect(plotter, SIGNAL(legendChecked(QwtPlotItem*, bool)),
                SLOT(showCurve(QwtPlotItem*, bool)));

        connect(this, SIGNAL(plotAdded(QString)),
                this,
                SLOT(addToPlotList(QString)));

        connect(ui.listWidgetPlots, SIGNAL(currentTextChanged(QString)),
                this,
                SLOT(changePlot(QString)));

        //        connect(&timer, SIGNAL(timeout()), this, SLOT(updateGraph()));
        //        stackedLayout = new QStackedLayout(widget);
        //        stackedLayout->addWidget(plotter);

    }

    void StaticPlotterWidgetController::onInitComponent()
    {
        usingTopic("StaticPlotter");
        //        offeringTopic("StaticPlotter");
    }

    void StaticPlotterWidgetController::onConnectComponent()
    {
        //        topicPrx = getTopic<StaticPlotterInterfacePrx>("StaticPlotter");
        //        task = new SimpleRunningTask<>([&]
        //        {
        //            sleep(1);
        //            ARMARX_INFO << "Sending";

        //            Vector2fSeq data {{1, 0},
        //                {200, 300},
        //                {300, 100}
        //            };
        //            Vector2fSeq data2 {{1, 0},
        //                {200, 500},
        //                {340, 600}
        //            };
        //            topicPrx->addPlot("TestCurve", {{"curve", data}, {"curve2", data2}});
        //            topicPrx->addPlotWithTimestampVector("TestCurve2", {0, 1, 2}, {{"curve", {1, 45, 8}}});
        //        });
        //        task->start();
    }

    void StaticPlotterWidgetController::showCurve(QwtPlotItem* item, bool on)
    {
        item->setVisible(on);
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
        QwtLegend* lgd = qobject_cast<QwtLegend*>(plotter->legend());

        QList<QWidget*> legendWidgets =
            lgd->legendWidgets(plotter->itemToInfo(item));

        if (legendWidgets.size() == 1)
        {
            QwtLegendLabel* legendLabel =
                qobject_cast<QwtLegendLabel*>(legendWidgets[0]);

            if (legendLabel)
            {
                legendLabel->setChecked(on);
            }
        }
#else
        QwtLegendItem* legendItem =
            qobject_cast<QwtLegendItem*>(plotter->legend()->find(item));
        if (legendItem)
        {
            legendItem->setChecked(on);
        }
#endif
        plotter->replot();
    }

    void StaticPlotterWidgetController::clearPlots()
    {
        ui.listWidgetPlots->clear();
        plotter->detachItems();
        plotter->replot();
        ScopedLock lock(dataMutex);
        plotsMap.clear();
    }

    void StaticPlotterWidgetController::addToPlotList(QString plotName)
    {
        if (ui.listWidgetPlots->findItems(plotName, Qt::MatchExactly).isEmpty())
        {
            ui.listWidgetPlots->addItem(plotName);
        }
        else if (ui.listWidgetPlots->currentItem() != NULL && ui.listWidgetPlots->currentItem()->text() == plotName)
        {
            changePlot(plotName);
        }
    }

    void StaticPlotterWidgetController::changePlot(QString plotName)
    {
        if (plotName.isEmpty())
        {
            return;
        }
        ScopedLock lock(dataMutex);
        auto it = plotsMap.find(plotName);
        if (it == plotsMap.end())
        {
            ARMARX_INFO << "Did not find plot with name " << plotName;
            return;
        }
        ARMARX_INFO << "Changing plot";
        plotter->detachItems();
        int i = 0;
        for (auto& elem  : it->second)
        {
            Vector2fSeq points = elem.second;
            QVector<QPointF> pointList;
            pointList.reserve(points.size());

            for (Vector2f& point :  points)
            {
                pointList.push_back({point.e0, point.e1});
            }
            QwtSeriesData<QPointF>* pointSeries = new  QwtPointSeriesData(pointList);
            QwtPlotCurve* curve = createCurve(QString::fromStdString(elem.first), QColor(Qt::GlobalColor(i % 15 + 7)));
            curve->setData(pointSeries);
            curve->attach(plotter);
            showCurve(curve, true);
            i++;
        }
        plotter->replot();
    }





    void StaticPlotterWidgetController::loadSettings(QSettings* settings)
    {
    }

    void StaticPlotterWidgetController::saveSettings(QSettings* settings)
    {
    }

    void StaticPlotterWidgetController::addPlot(const std::string& plotName, const StringVector2fSeqDict& plotsData, const Ice::Current&)
    {
        ARMARX_CHECK_EXPRESSION(!plotName.empty());
        ARMARX_CHECK_EXPRESSION(!plotsData.empty());
        QString qplotName = QString::fromStdString(plotName);
        {
            ScopedLock lock(dataMutex);
            plotsMap[qplotName] = plotsData;
        }
        emit plotAdded(qplotName);

    }


    void StaticPlotterWidgetController::addPlotWithTimestampVector(const std::string& plotName, const Ice::FloatSeq& timestamps, const StringFloatSeqDict& plotsData, const Ice::Current& c)
    {
        StringVector2fSeqDict plotsDataMap;
        for (auto& elem : plotsData)
        {
            Vector2fSeq data;
            ARMARX_CHECK_EXPRESSION(timestamps.size() == elem.second.size());
            for (size_t i = 0; i < timestamps.size(); ++i)
            {
                data.push_back({timestamps.at(i), elem.second.at(i)});
            }
            plotsDataMap[elem.first] = data;
        }
        addPlot(plotName, plotsDataMap, c);
    }


    QwtPlotCurve* StaticPlotterWidgetController::createCurve(const QString& label, QColor color)
    {
        QwtPlotCurve* curve = new QwtPlotCurve(label);
        curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        curve->setPen(color);
        curve->setStyle(QwtPlotCurve::Lines);

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        curve->setPaintAttribute(QwtPlotCurve::CacheSymbols, true);
#endif
        curve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);

        //        showCurve(curve, true);
        return curve;
    }


    QPointer<QWidget> StaticPlotterWidgetController::getCustomTitlebarWidget(QWidget* parent)
    {
        if (customToolbar)
        {
            if (parent != customToolbar->parent())
            {
                customToolbar->setParent(parent);
            }

            return customToolbar;
        }

        customToolbar = new QToolBar(parent);
        customToolbar->setIconSize(QSize(16, 16));
        customToolbar->addAction(QIcon(":/icons/Trash.svg"), "Delete Plots", this, SLOT(clearPlots()));

        return customToolbar;
    }

} // namespace armarx
