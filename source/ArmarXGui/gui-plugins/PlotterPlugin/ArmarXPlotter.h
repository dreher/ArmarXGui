/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

//ignore errors about extra ; from qwt
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <ArmarXGui/gui-plugins/PlotterPlugin/ui_ArmarXPlotter.h>
#pragma GCC diagnostic pop

// ArmarX
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/gui-plugins/ObserverPropertiesPlugin/ObserverItemModel.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/components/TopicRecorderInterface.h>
// QT
#include <QWidget>
#include <QDialog>
#include <QDateTime>

#include <vector>
#include <fstream>
#include <mutex>

#include <ArmarXGui/libraries/PlotterController/PlotterController.h>

//forward declarations
class QwtPlotCurve;
class QwtThermo;
class QStackedLayout;
class QwtPlotMarker;

namespace armarx
{
    class ArmarXPlotterDialog;
    /*!
     * \page ArmarXGui-GuiPlugins-PlotterPlugin Live Plotter
     * \brief The plotter widget allows the user to plot any sensor channel.
     * \image html PlotterGUI.png
     * A sensor channel can be selected for plotting by clicking the wrench button at the bottom.
      */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ArmarXPlotter:
        public ArmarXComponentWidgetControllerTemplate<ArmarXPlotter>,
        public TopicReplayerListenerInterface

    {
        Q_OBJECT
    public:
        void onStartReplay(const std::string& filename, const Ice::Current& c = Ice::Current()) override
        {
            if (!syncDataLogging)
            {
                return;
            }

            if (!ui.btnLogToFile->isChecked())
            {
                this->filename = filename;
                ui.btnLogToFile->setChecked(true);
            }
            else
            {
                ARMARX_WARNING << "already logging to file";
            }
        }

        void onStopReply(const Ice::Current& c = Ice::Current()) override
        {
            if (!syncDataLogging)
            {
                return;
            }

            if (ui.btnLogToFile->isChecked())
            {
                ui.btnLogToFile->setChecked(false);
            }
            else
            {
                ARMARX_WARNING << "not logging to file.";
            }
        }

        Ui::ArmarXPlotter ui;
        QPointer<ArmarXPlotterDialog> dialog;
        QTimer* timer;
        ConditionHandlerInterfacePrx handler;
        //        QStringList selectedChannels;
        QString loggingDir;

        explicit ArmarXPlotter();
        ~ArmarXPlotter() override;
        //inherited from ArmarXWidget
        static QString GetWidgetName()
        {
            return "Observers.LivePlotter";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/combo_chart.svg");
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        //for AbstractFactoryMethod class

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        /**
        * emits the closeRequest signal
        */
        void onCloseWidget(QCloseEvent* event);
        QwtPlotCurve* createCurve(const QString& label);
        QwtThermo*  createBar(const QString& label);
    signals:

    public slots:
        void ButtonAddSensorChannelClicked();
        void configDialog();
        void configDone();
        void toggleLogging(bool toggled);
        void onGraphStyleChanged(int idx);
        void logToFile(long timestamp, const std::map<std::string, VariantPtr>& dataMaptoAppend);

    protected:
        boost::shared_ptr<QSettings> settings;
    private:

        void pollingExec();
        QDateTime startUpTime;
        GraphDataMap dataMap;
        std::map< std::string, ObserverInterfacePrx> proxyMap;
        bool __plottingPaused;
        JSONObjectPtr json;

        Mutex dataMutex;
        bool syncDataLogging;
        std::vector<std::string> csvHeader;
        std::ofstream logstream;
        IceUtil::Time logStartTime;
        std::string filename = "";

        std::mutex mutex;

        std::mutex fileMutex;

        PlotterController* plotterController;
    };
}
