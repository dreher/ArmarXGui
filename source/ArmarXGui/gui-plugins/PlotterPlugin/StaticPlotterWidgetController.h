#pragma once

#include <functional>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <ArmarXGui/gui-plugins/PlotterPlugin/ui_StaticPlotterWidget.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <qwt/qwt_plot.h>
#include <qwt_plot_curve.h>
#pragma GCC diagnostic pop
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

namespace armarx
{

    /*!
         * \page ArmarXGui-GuiPlugins-StaticPlotterPlugin Static Plotter
         * \brief The plotter widget allows the user to send data to a the StaticPlotterInterface topic, which can be plotted with this widget.
         * To send plotting data from your code use code similar to this:
           \verbatim
           #include <ArmarXGui/interface/StaticPlotterInterface.h>
           topicPrx = getTopic<StaticPlotterInterfacePrx>("StaticPlotter");
           Vector2fSeq data {{1, 0},
               {200, 300},
               {300, 100}
           };
           Vector2fSeq data2 {{1, 0},
               {200, 500},
               {340, 600}
           };
           topicPrx->addPlot("TestCurve", {{"curve", data}, {"curve2", data2}});
           \endverbatim
           The GUI needs to be running before the plot is sent.
          */
    class StaticPlotterWidgetController :
        public ArmarXComponentWidgetControllerTemplate<StaticPlotterWidgetController>,
        public StaticPlotterInterface
    {
        Q_OBJECT
    public:
        StaticPlotterWidgetController();

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

        // ArmarXWidgetController interface
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
    signals:
        void plotAdded(QString plotName);
    public slots:
        void addToPlotList(QString plotName);
        void changePlot(QString plotName);
        void showCurve(QwtPlotItem* item, bool on);
        void clearPlots();
    public:
        static QString GetWidgetName()
        {
            return "Util.Plotter";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void addPlot(const std::string& plotName, const StringVector2fSeqDict& plotsData, const Ice::Current&) override;

    private:
        QwtPlotCurve* createCurve(const QString& label, QColor color);

        Ui::StaticPlotterWidget ui;
        QPointer<QwtPlot> plotter;
        std::map<QString, StringVector2fSeqDict > plotsMap;
        Mutex dataMutex;
        QToolBar* customToolbar;

        //        StaticPlotterInterfacePrx topicPrx ;
        //        armarx::SimpleRunningTask<>::pointer_type task;

        // StaticPlotterInterface interface
    public:
        void addPlotWithTimestampVector(const std::string& plotName, const Ice::FloatSeq& timestamps, const StringFloatSeqDict& plotsData, const Ice::Current&) override;
    };

} // namespace armarx

