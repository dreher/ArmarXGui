/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::ClockWidgetController
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ui_ClockWidget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/core/TimeServerInterface.h>

#include <QTimer>

namespace armarx
{
    /**
    * \page ArmarXGui-GuiPlugins-Clock Clock
    * \brief The Clock allows viewing and manipulating the time.
    *
    * \image html Clock.png
    * When synchronized time is available, the user can start/stop/step the
    * progression of time. The current timeserver time is shown.
    *
    * Othewise the current system time is shown.
    *
    * \see \ref ArmarXSimulation-HowTos-Timeserver
    * \see ClockGuiPlugin
    */

    /**
     * \class ClockWidgetController
     * \brief ClockWidgetController allows showing and manipulating the time
     *
     * \see ClockGuiPlugin
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ClockWidgetController:
        public ArmarXComponentWidgetControllerTemplate<ClockWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit ClockWidgetController();

        /**
         * Controller destructor
         */
        ~ClockWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Clock";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onDisconnectComponent() override;

    public slots:
        /* QT slot declarations */
        void updateGui();
        void startButtonPressed();
        void stepButtonPressed();
        void stopButtonPressed();
        void speedChanged(double newSpeed);

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::ClockWidget widget;
        QTimer* updateTimer;
        TimeServerInterfacePtr timeServerPtr;

        IceUtil::Time lastChangeSystemTime;
        IceUtil::Time lastChangeTime;

        /**
         * number of iterations of the update function
         */
        size_t iteration;

        // ArmarXWidgetController interface
    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/Time-And-Date-Clock-icon.png");
        }
    };
}

