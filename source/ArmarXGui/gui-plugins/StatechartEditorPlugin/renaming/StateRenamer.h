/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QVector>
#include <QPair>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/State.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StateTreeNode.h>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/model/StatechartGroup.h>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

namespace armarx
{

    class StateRenamer
    {

    public:
        struct InstanceRenameInfo
        {
            StatechartGroupPtr group;
            StateTreeNodePtr parentState;
            QString fromName;
            QString toName;
        };

        static bool RenameState(const QVector<InstanceRenameInfo>& instanceRenameInfos,
                                const StateTreeNodePtr& state,
                                const QString& newStateName,
                                const StatechartGroupPtr& group);

    private:
        static void RenameStateInstances(const QVector<InstanceRenameInfo>& instanceRename);
        static bool RenameStateClass(const StateTreeNodePtr& state, const QString& newStateName);
        static bool AdjustStatechartGroupFile(const StatechartGroupPtr& group, const StateTreeNodePtr& state, const QString& newName);

        static bool AttemptMoveTo(const QString& source, const QString& target);
    };
}

