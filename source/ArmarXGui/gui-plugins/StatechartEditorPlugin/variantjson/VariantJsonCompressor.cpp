/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VariantJsonCompressor.h"
#include "VariantJsonException.h"

#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

#include <ArmarXCore/util/json/JSONObject.h>

#include <Ice/ObjectFactory.h>

using namespace armarx;

VariantJsonCompressor::VariantJsonCompressor()
{
}

ParseResult VariantJsonCompressor::CheckUserInput(const std::string& json, const std::string& variantBaseTypeName, const Ice::CommunicatorPtr& communicator)
{
    StructuralJsonParser parser(json, false, true);
    parser.parse();
    if (parser.iserr())
    {
        return ParseResult("JSON parse error: " + parser.geterr(), ParseResult::eSytaxError);
    }

    ContainerTypePtr containerInfo = ::armarx::VariantContainerType::FromString(variantBaseTypeName);
    ParseResult result = CheckUserInput(parser.parsedJson, containerInfo, communicator);
    if (result.iserr())
    {
        JPathNavigator nav(parser.parsedJson);
        nav = nav.selectSingleNode("type");
        if (nav.isString())
        {
            std::string type = nav.asString();
            if (type == SingleTypeVariantList::getTypePrefix() || type == StringValueMap::getTypePrefix() || type == SingleVariant::getTypePrefix())
            {
                return ParseResult("Input might be in old format. Try importing.\n" + result.geterr(), result.getErrorType());
            }
        }
    }
    return result;
}

ParseResult VariantJsonCompressor::FormatUserInput(std::string& json, int indenting)
{
    StructuralJsonParser parser(json, false, true);
    parser.parse();
    if (parser.iserr())
    {
        return ParseResult("JSON parse error: " + parser.geterr(), ParseResult::eSytaxError);
    }
    json = parser.parsedJson->toJsonString(indenting);
    return ParseResult::Success();
}

ParseResult VariantJsonCompressor::CheckUserInput(const JsonDataPtr& json, const ContainerTypePtr& containerInfo, const Ice::CommunicatorPtr& communicator)
{
    JPathNavigator nav(json);
    if (containerInfo->subType)
    {
        if (containerInfo->typeId == SingleTypeVariantList::getTypePrefix())
        {
            if (!nav.isArray())
            {
                return ParseResult::Error("Unexpected JSON data type. Expected array at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eContainerMismatch);
            }
            for (JPathNavigator child : nav.select("*"))
            {
                ParseResult r = CheckUserInput(child.getData(), containerInfo->subType, communicator);
                if (r.iserr())
                {
                    return r;
                }
            }
            return ParseResult::Success();
        }
        else if (containerInfo->typeId == StringValueMap::getTypePrefix())
        {
            if (!nav.isObject())
            {
                return ParseResult::Error("Unexpected JSON data type. Expected object at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eContainerMismatch);
            }
            for (JPathNavigator child : nav.select("*"))
            {
                ParseResult r = CheckUserInput(child.getData(), containerInfo->subType, communicator);
                if (r.iserr())
                {
                    return r;
                }
            }
            return ParseResult::Success();
        }
        else
        {
            return ParseResult::Error("Unknown container type: " + containerInfo->typeId + ". At " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eContainerMismatch);
        }
    }
    else if (VariantType::IsBasicType(Variant::hashTypeName(containerInfo->typeId)))
    {
        VariantTypeId type = Variant::hashTypeName(containerInfo->typeId);
        if (type == VariantType::Bool && !nav.isBool())
        {
            return ParseResult::Error("Cannot parse Variant: Expected bool at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        else if (type == VariantType::String && !nav.isString())
        {
            return ParseResult::Error("Cannot parse Variant: Expected string at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        else if (type == VariantType::Float && !nav.isNumber())
        {
            return ParseResult::Error("Cannot parse Variant: Expected number at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        else if (type == VariantType::Double && !nav.isNumber())
        {
            return ParseResult::Error("Cannot parse Variant: Expected number at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        else if (type == VariantType::Int && !nav.isInt())
        {
            return ParseResult::Error("Cannot parse Variant: Expected int at " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }

        return ParseResult::Success();
    }
    else
    {
        Ice::ObjectFactoryPtr factory = communicator->findObjectFactory(containerInfo->typeId);
        SerializablePtr obj = SerializablePtr::dynamicCast(factory->create(containerInfo->typeId));
        static Ice::Current c;

        JSONObjectPtr jsonObject = new JSONObject();
        jsonObject->fromString(json->toJsonString(0));
        try
        {
            obj->deserialize(jsonObject, c);
        }
        catch (const UserException& ex)
        {
            return ParseResult::Error(std::string("Cannot parse Variant: ") + ex.ice_name() + ": " + ex.reason + ". At " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        catch (LocalException& ex)
        {
            return ParseResult::Error(std::string("Cannot parse Variant: ") + ex.name() + ": " + ex.getReason() + ". At " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }

        VariantDataPtr variant = VariantDataPtr::dynamicCast(obj);
        if (variant == 0)
        {
            return ParseResult::Error("Cannot cast Variant. Expected: " + containerInfo->typeId + ". Is: " + obj->ice_id() + ". At " + nav.getData()->getLexerStartOffset().toLongString(), ParseResult::eVariantParseError);
        }
        return ParseResult::Success();
    }
}

std::string VariantJsonCompressor::Compress(const std::string& json, const std::string& variantBaseTypeName, int indenting)
{
    JsonDataPtr compressed = CompressToJson(json, variantBaseTypeName);
    return compressed->toJsonString(indenting);
}

JsonDataPtr VariantJsonCompressor::CompressToJson(const std::string& json, const std::string& variantBaseTypeName)
{
    StructuralJsonParser parser(json, false, true);
    parser.parse();
    if (parser.iserr())
    {
        throw VariantJsonException("Invalid json: ") << parser.geterr();
    }

    ContainerTypePtr containerInfo = ::armarx::VariantContainerType::FromString(variantBaseTypeName);
    return Compress(parser.parsedJson, containerInfo);
}

std::string VariantJsonCompressor::Decompress(const std::string& json, const std::string& variantBaseTypeName)
{
    StructuralJsonParser parser(json, false, true);
    parser.parse();
    if (parser.iserr())
    {
        throw VariantJsonException("Invalid json: ") << parser.geterr();
    }

    ContainerTypePtr containerInfo = ::armarx::VariantContainerType::FromString(variantBaseTypeName);
    JsonDataPtr decompressed = Decompress(parser.parsedJson, containerInfo);
    return decompressed->toJsonString(0);
}

std::string VariantJsonCompressor::DecompressBasicVariant(const std::string& value, const std::string& variantBaseTypeName)
{
    int typeId = Variant::hashTypeName(variantBaseTypeName);

    JsonObjectPtr variant(new JsonObject);
    variant->add("typeName", variantBaseTypeName);

    if (typeId == VariantType::Int)
    {
        variant->add("value", JsonValue::CreateRaw(JsonValue::eNumber, value));
    }
    else if (typeId == VariantType::Bool)
    {
        variant->add("value", JsonValue::CreateRaw(JsonValue::eBool, value));
    }
    else if (typeId == VariantType::Float)
    {
        variant->add("value", JsonValue::CreateRaw(JsonValue::eNumber, value));
    }
    else if (typeId == VariantType::Double)
    {
        variant->add("value", JsonValue::CreateRaw(JsonValue::eNumber, value));
    }
    else if (typeId == VariantType::String)
    {
        variant->add("value", JsonValue::CreateRaw(JsonValue::eString, value));
    }
    else
    {
        throw VariantJsonException("Unsupported Basic Type: ") << variantBaseTypeName;
    }

    JsonObjectPtr o(new JsonObject);
    o->add("type", JsonValue(SingleVariant::getTypePrefix()));
    o->add("variant", variant);
    return o->toJsonString(0);
}

JsonDataPtr VariantJsonCompressor::Decompress(const JsonDataPtr& json, const ContainerTypePtr& containerInfo)
{
    if (containerInfo->subType)
    {
        JPathNavigator nav(json);
        if (containerInfo->typeId == SingleTypeVariantList::getTypePrefix())
        {
            JsonArrayPtr array(new JsonArray);
            if (!nav.isArray())
            {
                throw VariantJsonException("Wrong JSON data type. Expected array at ") << nav.getData()->getLexerStartOffset().toLongString();
            }
            for (JPathNavigator child : nav.select("*"))
            {
                array->add(Decompress(child.getData(), containerInfo->subType));
            }
            JsonObjectPtr o(new JsonObject);
            o->add("array", array);
            o->add("type", JsonValue(containerInfo->typeId));
            return o;
        }
        else if (containerInfo->typeId == StringValueMap::getTypePrefix())
        {
            JsonObjectPtr map(new JsonObject);
            if (!nav.isObject())
            {
                throw VariantJsonException("Wrong JSON data type. Expected object at ") << nav.getData()->getLexerStartOffset().toLongString();
            }
            for (JPathNavigator child : nav.select("*"))
            {
                map->add(child.getKey(), Decompress(child.getData(), containerInfo->subType));
            }
            JsonObjectPtr o(new JsonObject);
            o->add("map", map);
            o->add("type", JsonValue(containerInfo->typeId));
            return o;
        }
        else
        {
            throw VariantJsonException("Unknown container type: ") << containerInfo->typeId;
        }
    }
    else
    {
        JsonObjectPtr variant(new JsonObject);
        variant->add("typeName", containerInfo->typeId);
        variant->add("value", json->clone());
        JsonObjectPtr o(new JsonObject);
        o->add("type", JsonValue(SingleVariant::getTypePrefix()));
        o->add("variant", variant);
        return o;
    }
}

JsonDataPtr VariantJsonCompressor::Compress(const JPathNavigator& nav, const ContainerTypePtr& containerInfo)
{
    if (containerInfo->subType)
    {
        if (containerInfo->typeId == SingleTypeVariantList::getTypePrefix())
        {
            JsonArrayPtr a(new JsonArray);
            for (JPathNavigator child : nav.select("array/*"))
            {
                a->add(Compress(child.getData(), containerInfo->subType));
            }
            return a;
        }
        else if (containerInfo->typeId == StringValueMap::getTypePrefix())
        {
            JsonObjectPtr o(new JsonObject);
            for (JPathNavigator child : nav.select("map/*"))
            {
                o->add(child.getKey(), Compress(child.getData(), containerInfo->subType));
            }
            return o;
        }
        else
        {
            throw VariantJsonException("Unknown container type: ") << containerInfo->typeId;
        }
    }
    else
    {
        std::string typeName = nav.selectSingleNode("variant/typeName").asString();
        if (typeName != containerInfo->typeId)
        {
            throw VariantJsonException("typeName mismatch. expected: ") << containerInfo->typeId << ". is: " << typeName;
        }

        return nav.selectSingleNode("variant/value").getData();
    }
}


