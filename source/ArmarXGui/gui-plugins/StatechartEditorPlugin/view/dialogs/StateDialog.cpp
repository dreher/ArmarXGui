/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateDialog.h"
#include "ui_StateDialog.h"

#include "../../../StatechartViewerPlugin/model/stateinstance/RemoteState.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/TipDialog.h>
#include <QFileDialog>
#include <QInputDialog>
#include <QtGui>
#include <QDesktopWidget>
#include <QMessageBox>

#include <iterator>

#include <ArmarXCore/core/ArmarXManager.h>

#include <ArmarXCore/observers/variant/VariantContainer.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/RegExpValidatedInputDialog.h>

namespace armarx
{

    QMap<QString, DynamicLibraryPtr> StateDialog::Libraries = QMap<QString, DynamicLibraryPtr>();

    StateDialog::StateDialog(statechartmodel::StateInstancePtr stateInstance, Ice::CommunicatorPtr communicator, VariantInfoPtr variantInfo, StatechartProfilePtr currentProfile, bool locked, bool readOnly, QPointer<TipDialog> tipDialog, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::StateDialog),
        stateInstance(stateInstance),
        communicator(communicator),
        variantInfo(variantInfo),
        currentProfile(currentProfile),
        tipDialog(tipDialog),
        readOnly(readOnly)
    {
        ui->setupUi(this);
        setWindowTitle("State " + stateInstance->getInstanceName() + " Dialog");
        ui->tableInput->setCommunicator(communicator);
        ui->tableLocal->setCommunicator(communicator);
        ui->tableOutput->setCommunicator(communicator);
        ui->tableInput->setCurrentProfile(currentProfile);
        ui->tableLocal->setCurrentProfile(currentProfile);
        ui->tableOutput->setCurrentProfile(currentProfile);
        ui->tableLocal->hideColumn(StatechartEditorParameterEditor::eOptional);
        ui->tableLocal->hideValueColumns();
        ui->tableOutput->hideValueColumns();
        //        ui->tableLocal->hideColumn(StatechartEditorParameterEditor::eProvideDefaultValue);
        //        ui->tableLocal->hideColumn(StatechartEditorParameterEditor::eEditableValue);
        //        ui->tableOutput->hideColumn(StatechartEditorParameterEditor::eProvideDefaultValue);
        //        ui->tableOutput->hideColumn(StatechartEditorParameterEditor::eEditableValue);
        ui->tableInput->setVariantInfo(variantInfo);
        ui->tableLocal->setVariantInfo(variantInfo);
        ui->tableOutput->setVariantInfo(variantInfo);

        if (stateInstance)
        {
            statechartmodel::StatePtr state = stateInstance->getStateClass();

            if (!stateInstance->getParent())
            {
                // toplevel state-> instance name is not real anyway
                ui->editInstanceName->setEnabled(false);
            }

            ui->editInstanceName->setText(stateInstance->getInstanceName());
            ui->editInstanceName->setValidator(new QRegExpValidator(QRegExp("([a-zA-Z][a-zA-Z0-9_]*)"), this));

            if (state)
            {
                ui->editStateName->setText(state->getStateName());
                ui->stateDocuEditor->setPlainText(state->getDescription());
                statechartmodel::EventList outgoingEvents = state->getOutgoingEvents();
                foreach (statechartmodel::EventPtr e, outgoingEvents)
                {
                    ui->cbOutgoingEvents->addItem(e->name);
                    eventDescriptionMap[e->name] = e->description;
                }

                if (!outgoingEvents.isEmpty())
                {
                    eventNameComboboxChanged(ui->cbOutgoingEvents->currentText());
                }

                ui->tableInput->setStateInstance(stateInstance);
                ui->tableInput->buildFromMap(state->getInputParameters());
                ui->tableLocal->setStateInstance(stateInstance);
                ui->tableLocal->buildFromMap(state->getLocalParameters());
                ui->tableOutput->setStateInstance(stateInstance);
                ui->tableOutput->buildFromMap(state->getOutputParameters());

                connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(setBlackLists()));
                setBlackLists();



            }
            else
            {
                ui->editStateName->setEnabled(false);
                ui->stateDocuEditor->setEnabled(false);
                ui->cbOutgoingEvents->setEnabled(false);
                ui->btnAddEvent->setEnabled(false);
                ui->tabWidget->setTabEnabled(1, false);
                ui->tabWidget->setTabEnabled(2, false);
                ui->tabWidget->setTabEnabled(3, false);
            }

            statechartmodel::RemoteStatePtr remoteState = boost::dynamic_pointer_cast<statechartmodel::RemoteState>(stateInstance);

            if (remoteState)
            {
                ui->editProxyName->setText(remoteState->proxyName);
            }
            else
            {
                ui->editProxyName->setEnabled(false);
            }

            statechartmodel::EndStatePtr endstate = boost::dynamic_pointer_cast<statechartmodel::EndState>(stateInstance);

            if (endstate)
            {
                ui->labelInstanceName->setText("State/Event name");
            }
            else
            {
                ui->labelInstanceName->setText("State Instance name");
            }
        }

        fileDialog = new QFileDialog(this);
        fileDialog->setModal(true);
        fileDialog->setFileMode(QFileDialog::ExistingFiles);
        fileDialog->setOption(QFileDialog::HideNameFilterDetails, false);
        QList<QUrl> urls;
        urls << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
             << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation));

        if (!ArmarXDataPath::getHomePath().empty())
        {
            urls << QUrl::fromLocalFile(QString::fromStdString(ArmarXDataPath::getHomePath()));
        }

        fileDialog->setSidebarUrls(urls);
        QStringList fileTypes;
        fileTypes << tr("Libraries (*.so)")
                  << tr("All Files (*.*)");
        fileDialog->setNameFilters(fileTypes);
        //        connect(ui->btnLoadLib, SIGNAL(clicked()), fileDialog, SLOT(show()));
        connect(fileDialog, SIGNAL(accepted()), this, SLOT(loadLibrary()));

        connect(ui->cbOutgoingEvents, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(eventNameComboboxChanged(const QString&)));
        connect(ui->btnAddEvent, SIGNAL(clicked()), this, SLOT(eventButtonAdd()));
        connect(ui->btnDeleteEvent, SIGNAL(clicked()), this, SLOT(eventButtonDelete()));
        connect(ui->editEventDescription, SIGNAL(textChanged()), this, SLOT(eventDescriptionChanged()));
        connect(ui->btnLock, SIGNAL(toggled(bool)), this, SLOT(setLockStatus(bool)));
        connect(ui->tableInput, SIGNAL(typeChanged(int, QString)), this, SLOT(updateLinkHint(int, QString)));
        connect(ui->tableLocal, SIGNAL(typeChanged(int, QString)), this, SLOT(updateLinkHint(int, QString)));
        connect(ui->tableOutput, SIGNAL(typeChanged(int, QString)), this, SLOT(updateLinkHint(int, QString)));

        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!readOnly);


        if (locked)
        {
            setLockStatus(false);
        }

        if (readOnly || !locked)
        {
            ui->btnLock->hide();
        }

        QRect rec = QApplication::desktop()->screenGeometry();
        this->resize(rec.width() * 0.9, this->height());

        updateLinkHint(0, 0);

    }

    StateDialog::~StateDialog()
    {
        delete ui;
    }

    statechartmodel::StateParameterMap StateDialog::getInputParams() const
    {
        statechartmodel::StatePtr state;

        if (stateInstance && (state = stateInstance->getStateClass()))
        {
            return getUpdatedParameters(state->getInputParameters(), *ui->tableInput);
        }

        throw new LocalException("Could not get state parameters; invalid state?");
        return statechartmodel::StateParameterMap();
    }

    statechartmodel::StateParameterMap StateDialog::getLocalParams() const
    {
        statechartmodel::StatePtr state;

        if (stateInstance && (state = stateInstance->getStateClass()))
        {
            return getUpdatedParameters(state->getLocalParameters(), *ui->tableLocal);
        }

        throw new LocalException("Could not get state parameters; invalid state?");
        return statechartmodel::StateParameterMap();
    }

    statechartmodel::StateParameterMap StateDialog::getOutputParams() const
    {
        statechartmodel::StatePtr state;

        if (stateInstance && (state = stateInstance->getStateClass()))
        {
            return getUpdatedParameters(state->getOutputParameters(), *ui->tableOutput);
        }

        throw new LocalException("Could not get state parameters; invalid state?");
        return statechartmodel::StateParameterMap();
    }

    QString StateDialog::getStateInstanceName() const
    {
        return ui->editInstanceName->text();
    }

    QString StateDialog::getStateName() const
    {
        return ui->editStateName->text();
    }

    QString StateDialog::getProxyName() const
    {
        return ui->editProxyName->text();
    }

    QString StateDialog::getDescription() const
    {
        return ui->stateDocuEditor->toPlainText();
    }

    statechartmodel::EventList StateDialog::getOutgoingEvents() const
    {
        statechartmodel::EventList eventList;

        for (int i = 0; i < ui->cbOutgoingEvents->count(); ++i)
        {
            statechartmodel::EventPtr event(new statechartmodel::Event());
            event->name = ui->cbOutgoingEvents->itemText(i);
            event->description = eventDescriptionMap[event->name];
            eventList.append(event);
        }

        return eventList;
    }

    void StateDialog::eventNameComboboxChanged(const QString& eventName)
    {
        ui->editEventDescription->setEnabled(true);
        ui->editEventDescription->setPlainText(eventDescriptionMap[eventName]);
        ui->btnDeleteEvent->setEnabled(true);
    }

    void StateDialog::eventButtonAdd()
    {
        RegExpValidatedInputDialog d("New Event Name", "Event name",
                                     QRegExp("^([a-zA-Z_]{1})([a-zA-Z_0-9]+)$"), this);

        if (d.exec() == QDialog::Accepted)
        {
            auto eventName = d.getTextValue();
            if (!eventName.isEmpty())
            {
                ui->cbOutgoingEvents->addItem(eventName);
            }
            // No need to create entry in event description map (automatically done by operator[] on access)
        }
    }

    void StateDialog::eventButtonDelete()
    {
        if (ui->cbOutgoingEvents->currentText() != "Failure")
        {
            ui->cbOutgoingEvents->removeItem(ui->cbOutgoingEvents->currentIndex());
        }
        else
        {
            QMessageBox::warning(0, "Deletion failed", "You cannot delete the failure event. This event is needed to handle internal statechart failures.");
        }
    }

    void StateDialog::eventDescriptionChanged()
    {
        eventDescriptionMap[ui->cbOutgoingEvents->currentText()] = ui->editEventDescription->toPlainText();
    }

    void StateDialog::loadLibrary()
    {
        ARMARX_INFO_S << "loading libs";
        QStringList files = fileDialog->selectedFiles();
        foreach (QString file, files)
        {
            ARMARX_INFO_S << "loading " << file.toStdString();

            if (Libraries.find(file) != Libraries.end())
            {
                continue;
            }

            DynamicLibraryPtr lib(new DynamicLibrary());
            lib->load(file.toStdString());
            Libraries[file] = lib;
        }
        ArmarXManager::RegisterKnownObjectFactoriesWithIce(communicator);
        ui->tableInput->refreshVariantTypes();
        ui->tableLocal->refreshVariantTypes();
        ui->tableOutput->refreshVariantTypes();
    }

    void StateDialog::setBlackLists()
    {
        ui->tableInput->setKeyBlackList(ui->tableLocal->getKeys());
        ui->tableLocal->setKeyBlackList(ui->tableInput->getKeys());
    }

    void StateDialog::setLockStatus(bool unlock)
    {
        if (unlock && tipDialog)
        {
            tipDialog->setModal(true);
            tipDialog->showMessage("You are unlocking the editing of the state class. Editing a state class will change all occurrences of this state and not only this instance. To use custom parameters for this instance use the transition mapping.", "State class unlocking");
            tipDialog->resize(500, 150);
            tipDialog->activateWindow();
            tipDialog->raise();
        }

        ui->tableInput->setEnabled(unlock);
        ui->tableOutput->setEnabled(unlock);
        ui->tableLocal->setEnabled(unlock);
        ui->cbOutgoingEvents->setEnabled(unlock);
        ui->stateDocuEditor->setEnabled(unlock);
        ui->editEventDescription->setEnabled(unlock);
    }

    void StateDialog::updateLinkHint(int, QString)
    {
        if (!variantInfo)
        {
            return;
        }

        QSet<QString> vars;
        vars.unite(ui->tableInput->getTypes())
        .unite(ui->tableLocal->getTypes())
        .unite(ui->tableOutput->getTypes());
        Ice::StringSeq types;
        for (auto& var : vars)
        {
            auto type = VariantContainerType::GetInnerType(var.toStdString());
            if (!type.empty())
            {
                types.push_back(type);
            }
        }
        QStringList libs;
        for (std::string& lib : variantInfo->findLibNames(types))
        {
            libs << QString::fromStdString(lib);
        }
        ui->editDependencies->setText(libs.join(" "));
    }


    statechartmodel::StateParameterMap StateDialog::getUpdatedParameters(const statechartmodel::StateParameterMap& source, const StatechartEditorParameterEditor& parameterEditor) const
    {
        statechartmodel::StateParameterMap newValues = parameterEditor.getStateParameters();

        QSet<QString> profiles;
        StatechartProfilePtr p = currentProfile;

        while (p)
        {
            profiles.insert(QString::fromUtf8(p->getName().data()));
            p = p->getParent();
        }

        for (const std::pair<QString, statechartmodel::StateParameterPtr>& oldParamEntry : source.toStdMap())
        {
            QString key = oldParamEntry.first;

            // copy default values for other profiles if the parameter still exists && the type is unchanged:
            if (newValues.contains(key) && newValues[key]->type == oldParamEntry.second->type)
            {
                for (const std::pair<QString, QPair<VariantContainerBasePtr, QString>>& pdvEntry : oldParamEntry.second->profileDefaultValues.toStdMap())
                {
                    if (!profiles.contains(pdvEntry.first))
                    {
                        newValues[key]->profileDefaultValues.insert(pdvEntry.first, pdvEntry.second);
                    }
                }
            }
        }

        return newValues;
    }

    void StateDialog::showEvent(QShowEvent*)
    {
        if (readOnly && tipDialog)
        {

            tipDialog->showMessage("This state is read-only. Thus, changes cannot be saved.", "Read-only state");
            tipDialog->raise();
            tipDialog->activateWindow();
            tipDialog->exec();

        }
    }
}
