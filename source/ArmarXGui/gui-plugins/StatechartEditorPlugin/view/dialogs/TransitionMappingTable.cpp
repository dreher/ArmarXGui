/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TransitionMappingTable.h"

using namespace armarx;

#include "../../../StatechartEventSenderPlugin/TreeBox.h"
#include "../../../ObserverPropertiesPlugin/ObserverItemModel.h"

#include <QHeaderView>
#include <QLineEdit>
#include <QComboBox>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/widgets/MultiProfileDefaultValueEditWidget.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/widgets/ProfileDefaultValueEditWidget.h>

#include <ArmarXCore/util/json/JSONObject.h>

class NoWheelComboBox : public QComboBox
{
public:
    NoWheelComboBox(QWidget* parent = NULL) : QComboBox(parent) {}

    // QWidget interface
protected:
    void wheelEvent(QWheelEvent* e) override
    {
        e->ignore();
    }
};

TransitionMappingTable::TransitionMappingTable(QWidget* parent) :
    QTableWidget(parent)
{
}

void TransitionMappingTable::setup(const statechartmodel::StateParameterMap& targetDict, const statechartmodel::ParameterMappingList& sourceMapping, statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QStringList profileNames, Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo)
{
    this->ic = ic;
    this->variantInfo = variantInfo;
    destDict = targetDict;
    this->transition = transition;
    parentState = state;
    oldSourceMapping = sourceMapping;
    this->profileNames = profileNames;
    setColumnCount(5);

    if (!transition->destinationState->getStateClass() && transition->destinationState->getType() != eFinalState)
    {
        return;
    }

    setRowCount(destDict.size());
    setHorizontalHeaderLabels(QString("Target parameter;Value Type;Mapping Required;Source Type;Source Parameter;").split(";"));
    horizontalHeaderItem(eMappingRequired)->setToolTip("Checked if this parameter has no default value and is not optional, so a mapping is required.");
    setColumnWidth(eDestKey, 180);
    setColumnWidth(eDestType, 200);
    setColumnWidth(eSource, 100);
    setColumnWidth(eMappingRequired, 25);
    hideColumn(eMappingRequired);
    horizontalHeader()->setStretchLastSection(true);

    QStringList mappingSources;
    mappingSources << "Parent input";
    mappingSources << "State output";
    mappingSources << "Datafield" << "Event";
    mappingSources << "Value";

    int row = 0;
    statechartmodel::StateParameterMap::const_iterator it = destDict.begin();

    for (; it != destDict.end(); it++)  //for every input
    {
        QString keyInput = it.key();

        statechartmodel::ParameterMappingPtr mapping = transition->findMapping(keyInput, sourceMapping);

        if (!mapping)
        {
            ARMARX_VERBOSE_S << "No mapping found for " << keyInput;
        }

        QString typeInput = it.value()->type;
        auto destKeyItem = new QTableWidgetItem(keyInput);
        setItem(row, eDestKey, destKeyItem);
        destKeyItem->setToolTip((it.value()->description.isEmpty() ? "" : "Description:\n") + it.value()->description);
        item(row, eDestKey)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

        setItem(row, eMappingRequired, new QTableWidgetItem());
        item(row, eMappingRequired)->setCheckState(!it.value()->optional && !it.value()->getDefaultValue() ? Qt::Checked : Qt::Unchecked);
        item(row, eMappingRequired)->setFlags(Qt::ItemIsEnabled);




        QComboBox* comboBoxMappingSource = new NoWheelComboBox(this);

        setCellWidget(row, eSource, comboBoxMappingSource);
        comboBoxMappingSource->addItems(mappingSources);


        if (mapping)
        {
            //            ARMARX_VERBOSE_S << "Setting source type to " << (int)mapping->source << " and output to " << mapping->sourceKey;
            comboBoxMappingSource->setCurrentIndex(mapping->source);
            setSourceSpecificMappingParameters((int)mapping->source, row, mapping->sourceKey);
        }
        else
        {
            int mappingSource = transition->sourceState ? 1 : 0;
            comboBoxMappingSource->setCurrentIndex(mappingSource);
            setSourceSpecificMappingParameters(mappingSource, row);
        }

        connect(comboBoxMappingSource, SIGNAL(currentIndexChanged(int)), this, SLOT(setSourceSpecificMappingParameters(int)));


        // this disables the combobox entries
        if (!transition->sourceState)
        {

            comboBoxMappingSource->setItemData(1, QVariant(0), Qt::UserRole - 1);
        }
        comboBoxMappingSource->setItemData(2, QVariant(0), Qt::UserRole - 1);
        comboBoxMappingSource->setItemData(3, QVariant(0), Qt::UserRole - 1);


        auto humanType = QString::fromUtf8(variantInfo->getNestedHumanNameFromBaseName(typeInput.toUtf8().data()).c_str());
        setItem(row, eDestType, new QTableWidgetItem(humanType));
        item(row, eDestType)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item(row, eDestType)->setToolTip(typeInput);
        row++;

    }

}



statechartmodel::ParameterMappingList TransitionMappingTable::getMapping() const
{
    statechartmodel::ParameterMappingList result;

    for (int row = 0; row < rowCount(); row++)
    {
        statechartmodel::ParameterMappingPtr mapping(new statechartmodel::ParameterMapping());
        mapping->destinationKey = item(row, eDestKey)->text();
        QComboBox* combo = qobject_cast<QComboBox*>(cellWidget(row, eSource));

        if (combo)
        {
            mapping->source = (MappingSource)combo->currentIndex();
        }

        QComboBox* sourceCombo = qobject_cast<QComboBox*>(cellWidget(row, eSourceKey));

        if (sourceCombo)
        {
            mapping->sourceKey = sourceCombo->currentText();
        }
        if (!mapping->sourceKey.isEmpty() && mapping->sourceKey != "none")
        {
            result.append(mapping);
        }



        if (MultiProfileDefaultValueEditWidget* valueEdit = qobject_cast<MultiProfileDefaultValueEditWidget*>(cellWidget(row, eSourceKey)))
        {
            mapping->profileValues = valueEdit->getJsonMap();
            result.append(mapping);
        }


    }

    return result;
}

QList<std::pair<QString, QString>> TransitionMappingTable::mapByCriteria(std::function<bool(QString, QString)> compare)
{
    QList<std::pair<QString, QString>> mappedRows;
    for (int row = 0; row < rowCount(); row++)
    {
        QComboBox* comboSourceType = qobject_cast<QComboBox*>(cellWidget(row, eSource));
        int oldIndex = comboSourceType->currentIndex();
        bool found = false;
        for (int type = 1; type >= 0; type--)
        {
            //            if (comboSourceType->itemData(type, Qt::UserRole - 1).toInt() == 0)
            //            {
            //                continue;
            //            }
            if (found)
            {
                break;
            }

            QComboBox* combo = qobject_cast<QComboBox*>(cellWidget(row, eSourceKey));

            if (combo && combo->currentIndex() > 0) // only map values that are not set already
            {
                break;
            }

            comboSourceType->setCurrentIndex(type);
            if (comboSourceType->currentIndex() != type)
            {
                continue;
            }
            auto destinationKey = item(row, eDestKey)->text();


            combo = qobject_cast<QComboBox*>(cellWidget(row, eSourceKey));

            if (combo && combo->currentIndex() <= 0) // only map values that are not set already
            {

                for (int i = 0; i < combo->count(); i++)
                {
                    ARMARX_DEBUG_S << combo->itemText(i) << destinationKey;
                    if (compare(combo->itemText(i), destinationKey))
                    {
                        combo->setCurrentIndex(i);
                        ARMARX_DEBUG_S << " setting to " << i << combo->itemText(i);
                        found = true;
                        mappedRows.push_back(std::make_pair(destinationKey, comboSourceType->currentText() + "." + combo->itemText(i)));
                        break;
                    }
                }
            }
        }
        if (!found)
        {
            comboSourceType->setCurrentIndex(oldIndex);
        }
    }
    return mappedRows;
}

void TransitionMappingTable::setSourceSpecificMappingParameters(int index, int tableRow, QString initialValue)
{
    //    const statechartmodel::StateParameterMap& destDict = transition->destinationState->getStateClass()->getInputParameters();
    statechartmodel::StateParameterMap::const_iterator it = destDict.begin();

    if (tableRow == -1)
    {
        QWidget* wid = qobject_cast<QWidget*>(sender());

        if (!wid)
        {
            throw LocalException("Could not get row of table");
        }

        QModelIndex mIndex = indexAt(wid->pos());
        tableRow = mIndex.row();
    }

    for (int i = 0; i < tableRow; ++i, it++)
    {
    }

    ARMARX_DEBUG_S << VAROUT(index);
    if (index == 0)
    {
        QComboBox* parentInput = new NoWheelComboBox();

        auto selection = getFilteredParams(parentState->getInputAndLocalParameters(), *it.value());
        int i = 0;
        for (auto it = selection.begin(); it != selection.end(); ++it, i++)
        {
            parentInput->addItem(it->first);
            parentInput->setItemData(i, it->second.isEmpty() ? "" : "Description:\n" + it->second, Qt::ToolTipRole);
        }
        if (initialValue.length() > 0)
        {
            parentInput->setCurrentIndex(parentInput->findText(initialValue));
        }

        setCellWidget(tableRow, eSourceKey, parentInput);
    }
    else if (index == 1)
    {
        if (transition->sourceState)
        {
            QComboBox* box = getFilteredOutputItems(it.key(), transition);

            if (initialValue.length() > 0)
            {
                box->setCurrentIndex(box->findText(initialValue));
            }

            setCellWidget(tableRow, eSourceKey, box);
        }
        else
        {
            setCellWidget(tableRow, eSourceKey, new QWidget());
        }
    }
    else if (index == 4 && ic)
    {
        auto destType = getInputTypeString(tableRow);
        ARMARX_INFO_S << VAROUT(destType);

        int i = 0;
        for (statechartmodel::ParameterMappingPtr m : oldSourceMapping)
        {
            if (m->destinationKey == it.key())
            {
                break;
            }
            i++;
        }
        QMap<QString, QString> values;
        if (i < oldSourceMapping.size())
        {
            statechartmodel::ParameterMappingPtr mappingEntry = oldSourceMapping[i];
            if (mappingEntry)
            {
                values = mappingEntry->profileValues;
            }

        }
        MultiProfileDefaultValueEditWidget* w = new MultiProfileDefaultValueEditWidget(destType, profileNames, values, ic, this);
        setCellWidget(tableRow, eSourceKey, w);


        //        connect(datafieldInput,SIGNAL(itemSelected(QModelIndex)), this, SLOT(verifySelectedField(QModelIndex)));
    }
    else
    {
        //            setCellWidget(tableRow, eSourceKey , NULL);
        setCellWidget(tableRow, eSourceKey, new QLineEdit(initialValue));
        //            setItem(tableRow, eSourceKey, new QTableWidgetItem(initialValue));
    }
}

QList<QPair<QString, QString>> TransitionMappingTable::getFilteredParams(const statechartmodel::StateParameterMap& source, const statechartmodel::StateParameter&  argumentToMatch) const
{
    QList<QPair<QString, QString>> items;

    statechartmodel::StateParameterMap::const_iterator it = source.begin();

    for (; it != source.end(); it++)
    {
        if (it.value()->type == argumentToMatch.type)
        {
            items.append(qMakePair(it.key(), it.value()->description));
        }
    }


    //    if (argumentToMatch.optional)
    {
        items.insert(0, qMakePair(tr("none"), QString("")));
    }
    return items;
}

QString TransitionMappingTable::getInputTypeString(int row)
{
    statechartmodel::StateParameterMap::const_iterator it = destDict.begin();

    if (row == -1)
    {
        QWidget* wid = qobject_cast<QWidget*>(sender());

        if (!wid)
        {
            throw LocalException("Could not get row of table");
        }

        QModelIndex mIndex = indexAt(wid->pos());
        row = mIndex.row();
    }

    for (int i = 0; i < row; ++i, it++)
    {
    }
    if (it != destDict.end())
    {
        return it.value()->type;
    }
    return "";
}




QComboBox* TransitionMappingTable::getFilteredOutputItems(QString key, statechartmodel::TransitionCPtr transition)
{
    QList<QString> items;

    QComboBox* cbo = new NoWheelComboBox(this);

    if (!transition->sourceState)
    {
        ARMARX_ERROR_S << ("The sourceState of the transition with event ") << transition->eventName << " is NULL - output parameters are not loaded" ;
        return cbo;
    }

    if (!transition->sourceState->getStateClass())
    {
        ARMARX_ERROR_S << ("The stateClass pointer of the sourceState named '") << transition->sourceState->getInstanceName() << "' of the transition is NULL - output parameters of state '" << transition->sourceState->getInstanceName() << "' are not loaded";
        return cbo;
    }

    const statechartmodel::StateParameterMap& source = transition->sourceState->getStateClass()->getOutputParameters();
    //    const statechartmodel::StateParameterMap& destDict = transition->destinationState->getStateClass()->getInputParameters();
    statechartmodel::StateParameterPtr param = destDict[key];
    statechartmodel::StateParameterMap::const_iterator it = source.begin();
    ARMARX_DEBUG_S << "key: " << key;

    if (!param)
    {
        return cbo;
    }

    //    if (param && param->optional)
    {
        items.insert(0, tr("none"));
    }

    for (; it != source.end(); it++)
    {
        if (it.value()->type == param->type)
        {
            items.append(it.key());
        }
    }

    for (auto s : items)
    {
        ARMARX_DEBUG_S << s;
    }

    cbo->addItems(items);

    return cbo;
}

