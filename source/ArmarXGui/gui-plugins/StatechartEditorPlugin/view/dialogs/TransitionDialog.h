/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "../../../StatechartViewerPlugin/model/State.h"
#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <QDialog>

class QComboBox;


namespace armarx
{

    namespace Ui
    {
        class TransitionDialog;
    }

    class TransitionDialog :
        public QDialog
    {
        Q_OBJECT

    public:
        explicit TransitionDialog(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr parentState, QStringList profileNames, Ice::CommunicatorPtr ic = NULL, VariantInfoPtr variantInfo = VariantInfoPtr(), QWidget* parent = 0);
        ~TransitionDialog() override;

        statechartmodel::ParameterMappingList getMappingToNextStateInput() const;
        statechartmodel::ParameterMappingList getMappingToParentStateLocal() const;
        statechartmodel::ParameterMappingList getMappingToParentStateOutput() const;
    private slots:
        void on_btnAutoMap_clicked();

    private:
        void setup();
        Ui::TransitionDialog* ui;
        statechartmodel::TransitionCPtr transition;
        statechartmodel::StatePtr parentState;
        Ice::CommunicatorPtr ic;
        VariantInfoPtr variantInfo;
        QStringList profileNames;
        QComboBox* getFilteredOutputItems(QString key, statechartmodel::TransitionCPtr transition);
        QStringList getFilteredParams(const statechartmodel::StateParameterMap& source, const statechartmodel::StateParameter& argumentToMatch) const;
    };

}

