/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QListWidgetItem>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

class Ui_StatechartEditorConfigDialog;
namespace armarx
{
    class StatechartEditorConfigDialog :
        public QDialog
    {
        Q_OBJECT

    public:
        explicit StatechartEditorConfigDialog(StatechartProfilesPtr profiles, QWidget* parent = 0);
        ~StatechartEditorConfigDialog() override;

        StatechartProfilePtr getSelectedProfile();
        bool openAllStatesIsSelected();


    private slots:
        void itemSelectionChanged();
        void on_listWidgetProfiles_doubleClicked(const QModelIndex& index);

    private:
        Ui_StatechartEditorConfigDialog* ui;
        std::map<QListWidgetItem*, StatechartProfilePtr> profileMap;
    };
}

