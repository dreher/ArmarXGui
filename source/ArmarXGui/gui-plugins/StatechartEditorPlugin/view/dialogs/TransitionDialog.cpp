/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TransitionDialog.h"
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/ui_TransitionDialog.h>

#include "../../../ObserverPropertiesPlugin/ObserverItemModel.h"

#include <QComboBox>
#include <QFileDialog>
#include <QLineEdit>
#include <QMessageBox>


namespace armarx
{

    TransitionDialog::TransitionDialog(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QStringList profileNames, Ice::CommunicatorPtr ic, VariantInfoPtr variantInfo, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::TransitionDialog),
        transition(transition),
        parentState(state),
        ic(ic),
        variantInfo(variantInfo),
        profileNames(profileNames)
    {
        ui->setupUi(this);
        setup();
    }

    TransitionDialog::~TransitionDialog()
    {
        delete ui;
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToNextStateInput() const
    {
        return ui->inputTableMapping->getMapping();
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToParentStateLocal() const
    {
        return ui->parentLocalMapping->getMapping();
    }

    statechartmodel::ParameterMappingList TransitionDialog::getMappingToParentStateOutput() const
    {
        return ui->parentOutputMapping->getMapping();
    }


    void TransitionDialog::setup()
    {
        //    ui->editEventName->setText(transition->eventName);
        //    ui->editEventName->setFocus();
        ui->labelEventName->setText(transition->eventName);

        if (!transition->destinationState)
        {
            ui->labelDestinationState->setText("Transition is detached");
            return;
        }


        if (transition->sourceState)
        {
            ui->labelSourceState->setText(transition->sourceState->getInstanceName());
            setWindowTitle("Edit Transition " + transition->eventName + " [" + transition->sourceState->getInstanceName() + " -> " + transition->destinationState->getInstanceName() + "]");
        }
        else
        {
            ui->labelSourceState->setText("This is the initial transition to the start state");
            setWindowTitle("Edit Initial Transition  -> " + transition->destinationState->getInstanceName());
        }

        ui->labelDestinationState->setText(transition->destinationState->getInstanceName());


        statechartmodel::StateParameterMap destDict;

        if (transition->destinationState->getType() != eFinalState)
        {
            if (transition->destinationState->getStateClass())
            {
                destDict = transition->destinationState->getStateClass()->getInputParameters();
            }
            else
            {
                QMessageBox::warning(this, "Transition Editing Error", "Transition editing not possible because destination state is not loaded - load statechart group of destination state.");
                return;
            }
        }
        else
        {
            destDict = transition->destinationState->getParent()->getOutputParameters();
        }

        ui->inputTableMapping->setup(destDict, transition->mappingToNextStatesInput, transition, parentState, profileNames, ic, variantInfo);

        if (transition->sourceState)
        {
            ui->tabWidget->setTabEnabled(1, true);
            ui->tabWidget->setTabEnabled(2, true);
            ui->parentLocalMapping->hideColumn(TransitionMappingTable::eMappingRequired);
            ui->parentOutputMapping->hideColumn(TransitionMappingTable::eMappingRequired);
            ui->parentLocalMapping->setup(parentState->getLocalParameters(),  transition->mappingToParentStatesLocal, transition, parentState, profileNames,  ic, variantInfo);
            ui->parentOutputMapping->setup(parentState->getOutputParameters(),  transition->mappingToParentStatesOutput, transition, parentState, profileNames, ic, variantInfo);
        }
        else
        {
            ui->tabWidget->setTabEnabled(1, false);
            ui->tabWidget->setTabEnabled(2, false);
        }

    }





}

void armarx::TransitionDialog::on_btnAutoMap_clicked()
{
    TransitionMappingTable* mapping;
    if (ui->tabWidget->currentIndex() == 0)
    {
        mapping = ui->inputTableMapping;
    }
    else if (ui->tabWidget->currentIndex() == 1)
    {
        mapping = ui->parentLocalMapping;
    }
    else
    {
        mapping = ui->parentOutputMapping;
    }


    auto caseSensitive = [](QString s1, QString s2)
    {
        return s1 == s2;
    };
    auto caseInsensitive = [](QString s1, QString s2)
    {
        return s1.compare(s1, s2, Qt::CaseInsensitive) == 0;
    };


    auto mappedRows = mapping->mapByCriteria(caseSensitive);
    mappedRows.append(mapping->mapByCriteria(caseInsensitive));
    QString foundMappingsString;
    for (auto& elem : mappedRows)
    {
        foundMappingsString += elem.first + " from " + elem.second + "\n";
    }
    if (!foundMappingsString.isEmpty())
    {
        foundMappingsString = ":\n" + foundMappingsString;
    }
    QMessageBox::information(this, "Auto Mapping Results", "Mapped " + QString::number(mappedRows.size()) + (mappedRows.size() == 1 ? " entry" : " entries") + foundMappingsString);
}
