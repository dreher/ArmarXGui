/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Valerij Wittenbeck (valerij.wittenbeck at student dot kit dot edu
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RenameStateDialog.h"
#include "ui_RenameStateDialog.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <QFileDialog>
#include <QTimer>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/cloning/GroupCloner.h>
#include <numeric>

#include <QtGui>

namespace armarx
{

    RenameStateDialog::RenameStateDialog(const StateTreeModelPtr& treeModel, const StatechartGroupPtr& sourceGroup, const statechartmodel::StatePtr& sourceState, QWidget* parent) :
        QDialog(parent),
        ui(new Ui::RenameStateDialog),
        sourceGroup(sourceGroup),
        sourceState(sourceState),
        validStateNameRegExp("[a-zA-Z][a-zA-Z0-9]*"),
        validInstanceNameRegExp("[a-zA-Z][a-zA-Z0-9_]*"),
        validNewName(false),
        validInstanceNames(true),
        colorGreen(QColor::fromRgb(120, 255, 120)),
        colorRed(QColor::fromRgb(255, 120, 120)),
        colorYellow(QColor::fromRgb(255, 200, 0)),
        saveAll(false),
        alreadyChecking(false)
    {
        ui->setupUi(this);
        setOkButtonsEnabled(false);
        ui->editOldName->setText(sourceState->getStateName());
        ui->editOldName->setReadOnly(true);
        //        ui->editOldName->setEnabled(false);

        ui->editNewName->setValidator(new QRegExpValidator(validStateNameRegExp, this));

        connect(ui->btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
        connect(ui->btnSaveAndProceed, SIGNAL(clicked()), this, SLOT(saveAllProceedButtonClicked()));
        connect(ui->btnDontSaveAndProceed, SIGNAL(clicked()), this, SLOT(accept()));
        connect(ui->editNewName, SIGNAL(textChanged(QString)), this, SLOT(verifyNewName(QString)));
        connect(ui->statesWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(verifyInstanceName(QTableWidgetItem*)));

        localStates = sourceGroup->getAllStates(true);

        for (const StatechartGroupPtr& group : treeModel->getGroups())
        {
            if (group->getWriteAccess() == StatechartGroup::eReadOnly)
            {
                continue;
            }
            for (const StateTreeNodePtr& node : group->getAllNodes())
            {
                if (!node->isState() || !node->getState())
                {
                    continue;
                }

                auto state = node->getState();

                for (const statechartmodel::StateInstancePtr& instance : state->getSubstates())
                {
                    if (!instance->getStateClass())
                    {
                        continue;
                    }

                    if (instance->getStateClass()->getUUID() == sourceState->getUUID())
                    {
                        instanceRenameInfos.push_back({group, node, instance->getInstanceName(), instance->getInstanceName()});
                    }
                }
            }
        }

        std::sort(instanceRenameInfos.begin(), instanceRenameInfos.end(), [](const StateRenamer::InstanceRenameInfo & info1, const StateRenamer::InstanceRenameInfo & info2)
        {
            if (info1.group->getName() == info2.group->getName())
            {
                if (info1.parentState->getState()->getStateName() == info2.parentState->getState()->getStateName())
                {
                    return info1.fromName.compare(info2.toName);
                }

                return info1.parentState->getState()->getStateName().compare(info2.parentState->getState()->getStateName());
            }

            return info1.group->getName().compare(info2.group->getName());
        });

        ui->statesWidget->clear();
        ui->statesWidget->setRowCount(instanceRenameInfos.size());
        ui->statesWidget->setColumnCount(3);
        ui->statesWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
        ui->statesWidget->setHorizontalHeaderLabels({"Statechart Group", "Parent State", "Instance Name"});

        int i = 0;

        for (const auto& info : instanceRenameInfos)
        {
            QTableWidgetItem* groupItem = new QTableWidgetItem(info.group->getName());
            groupItem->setFlags(groupItem->flags() ^ Qt::ItemIsEditable);
            ui->statesWidget->setItem(i, 0, groupItem);

            QTableWidgetItem* stateItem = new QTableWidgetItem(info.parentState->getState()->getStateName());
            stateItem->setFlags(stateItem->flags() ^ Qt::ItemIsEditable);
            ui->statesWidget->setItem(i, 1, stateItem);

            QTableWidgetItem* newInstanceItem = new QTableWidgetItem(info.toName);
            newInstanceItem->setBackgroundColor(colorGreen);
            ui->statesWidget->setItem(i, 2, newInstanceItem);

            ++i;
        }
    }

    RenameStateDialog::~RenameStateDialog()
    {
        delete ui;
    }

    void RenameStateDialog::setOkButtonsEnabled(bool enabled)
    {
        ui->btnDontSaveAndProceed->setEnabled(enabled);
        ui->btnSaveAndProceed->setEnabled(enabled);
    }

    StatechartGroupPtr RenameStateDialog::getGroup() const
    {
        return sourceGroup;
    }

    statechartmodel::StatePtr RenameStateDialog::getState() const
    {
        return sourceState;
    }

    QVector<StateRenamer::InstanceRenameInfo> RenameStateDialog::getInstanceRenameInfos() const
    {
        return instanceRenameInfos;
    }

    bool RenameStateDialog::isSaveAllRequested() const
    {
        return saveAll;
    }

    QString RenameStateDialog::getNewStateName() const
    {
        return ui->editNewName->text();
    }

    void RenameStateDialog::saveAllProceedButtonClicked()
    {
        saveAll = true;
        accept();
    }

    void RenameStateDialog::verifyInstanceName(QTableWidgetItem* item)
    {
        const int r = item->row();
        const int c = item->column();

        //setting the background color triggers the "itemchanged" signal, which is what called this method in the first place
        //this elegant solution ensures that no recursive calling is taking place
        if (c != 2 || ui->statesWidget->columnCount() < 3 || ui->statesWidget->rowCount() == 0 || alreadyChecking)
        {
            return;
        }

        alreadyChecking = true;

        StateRenamer::InstanceRenameInfo& info = instanceRenameInfos[r];

        bool hasNameCollisions = false;

        for (int i = 0; i < ui->statesWidget->rowCount(); ++i)
        {
            const auto& itemCheck = ui->statesWidget->item(i, 2);

            if (!itemCheck)
            {
                continue;
            }

            const StateRenamer::InstanceRenameInfo& infoCheck = instanceRenameInfos[i];

            bool collision = false;

            for (int j = 0; j < ui->statesWidget->rowCount() && !collision; ++j)
            {
                const auto& itemOther = ui->statesWidget->item(j, 2);

                if (i == j || !itemOther)
                {
                    continue;
                }

                const StateRenamer::InstanceRenameInfo& infoR = instanceRenameInfos[j];

                if (infoCheck.parentState->getState()->getUUID() == infoR.parentState->getState()->getUUID())
                {
                    ARMARX_INFO_S << " checking: " << itemCheck->text() << " " << itemOther->text();
                    collision = itemCheck->text() == itemOther->text();
                }
            }

            hasNameCollisions |= collision;
            itemCheck->setBackgroundColor(collision ? colorRed : colorGreen);
        }

        bool validName = validInstanceNameRegExp.exactMatch(item->text());

        if (validName)
        {
            info.toName = item->text();
        }

        item->setBackgroundColor(!validName ? colorRed : item->backgroundColor());
        validInstanceNames = validName && !hasNameCollisions;
        setOkButtonsEnabled(validInstanceNames && validNewName);
        alreadyChecking = false;
    }

    void RenameStateDialog::verifyNewName(QString newName)
    {
        bool inUse = false;

        for (const auto& s : localStates)
        {
            if (newName == s->getStateName())
            {
                inUse = true;
                break;
            }
        }

        QColor colorToUse;

        if (inUse)
        {
            ui->labelNewNameError->setText("Name already in use");
            colorToUse = colorRed;
        }
        else
        {
            ui->labelNewNameError->setText("Valid name");
            colorToUse = colorGreen;
        }

        validNewName = (!inUse && newName.size() != 0);
        setOkButtonsEnabled(validNewName && validInstanceNames);

        QPalette p(ui->labelNewNameError->palette());
        p.setColor(ui->labelNewNameError->backgroundRole(), colorToUse);
        ui->labelNewNameError->setPalette(p);
    }

} // namespace armarx
