/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartEditorMainWindow.h"
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateTabWidget.h>
#include "CppHighlighter.h"
#include "dialogs/StateDialog.h"
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/layout/LayoutController.h>

#include "../../StatechartViewerPlugin/view/StatechartView.h"
#include "../../StatechartViewerPlugin/view/StateItem.h"

#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>
#include <QToolBar>

#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/view/dialogs/TransitionDialog.h>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/TransitionItem.h>

namespace armarx
{

    StatechartEditorMainWindow::StatechartEditorMainWindow(QWidget* parent) :
        QMainWindow(parent),
        ui(new Ui::StatechartEditorMainWindow)
    {
        ui->setupUi(this);
        setupStatechartGroupsToolbar();
        addAction(ui->actionSave_State);
        addAction(ui->actionDelete_State);

    }

    StatechartEditorMainWindow::~StatechartEditorMainWindow()
    {
        delete ui;
    }

    StateTabWidget* StatechartEditorMainWindow::getStateTabWidget() const
    {
        return ui->stateTabWidget;
    }

    void StatechartEditorMainWindow::setTipDialog(QPointer<TipDialog> tipDialog)
    {
        this->tipDialog = tipDialog;
    }

    void StatechartEditorMainWindow::originalZoomCurrentState()
    {
        if (ui->stateTabWidget->currentStateview())
        {
            ui->stateTabWidget->currentStateview()->setOriginalZoom();
        }
    }

    void StatechartEditorMainWindow::deleteSelectedStates()
    {
        if (QMessageBox::question(this, "Delete state?",
                                  "Do you really want to delete the selected states?",
                                  QMessageBox::Yes | QMessageBox::No,
                                  QMessageBox::Yes)
            == QMessageBox::Yes)
        {
            if (ui->stateTabWidget->currentStateview())
            {
                ui->stateTabWidget->currentStateview()->deleteSelectedStates();
            }
        }
    }


    void StatechartEditorMainWindow::connectOverviewToTab(int tabIndex)
    {
        StatechartView* view = ui->stateTabWidget->currentStateview();

        if (view && view->getScene())
        {
            ui->graphicsOverview->setScene(view->getScene());
            ui->graphicsOverview->fitInView(view->getScene()->itemsBoundingRect(), Qt::KeepAspectRatio);
            ui->graphicsOverview->setZoomer(view->getGraphicsViewZoomer());
        }
        else
        {
            ui->graphicsOverview->setZoomer(NULL);
        }
    }

    void StatechartEditorMainWindow::layoutState(statechartmodel::StateInstancePtr instance)
    {
        if (!instance)
        {
            if (ui->stateTabWidget->currentStateview()
                && ui->stateTabWidget->currentStateview()->getScene()
                && ui->stateTabWidget->currentStateview()->getScene()->selectedItems().size() > 0)
            {
                StateItem* item = dynamic_cast<StateItem*>(*ui->stateTabWidget->currentStateview()->getScene()->selectedItems().begin());
                if (item)
                {
                    instance = item->getStateInstance();
                }
            }
        }

        StatechartView* view = ui->stateTabWidget->currentStateview();
        if (instance && view)
        {
            view->getLayoutController().layoutNow(view->getLayoutController().getStateId(instance->getStateClass()), true);
        }


    }





    /*void StatechartEditorMainWindow::setStatechartGroupModel(StatechartGroupTreeModel* model)
    {
        ui->treeViewGroups->setModel(model);
    }*/

    void StatechartEditorMainWindow::addEndState()
    {
        if (ui->stateTabWidget->currentStateview()
            && ui->stateTabWidget->currentStateview()->getScene()
            && ui->stateTabWidget->currentStateview()->getScene()->selectedItems().size() > 0)
        {
            StateItem* item = dynamic_cast<StateItem*>(*ui->stateTabWidget->currentStateview()->getScene()->selectedItems().begin());

            if (item && item->getStateInstance() && item->getStateInstance()->getStateClass())
            {
                ARMARX_INFO_S << "Adding new endstate";
                statechartmodel::StatePtr state = item->getStateInstance()->getStateClass();
                int i = 2;
                const QString newStateNameBase = "MyEndState";
                QString newStateName = newStateNameBase;

                while (!state->addEndSubstate(newStateName, newStateName))
                {
                    newStateName = newStateNameBase + "_" + QString::number(i);
                    i++;
                }
            }
            else
            {
                ARMARX_INFO_S << "Cannot add endstate";
            }


        }

    }


    void StatechartEditorMainWindow::zoomToViewAll()
    {
        if (ui->stateTabWidget->currentStateview())
        {
            ui->stateTabWidget->currentStateview()->viewAll();
        }
    }

    void StatechartEditorMainWindow::setupStatechartGroupsToolbar()
    {
        stGroupToolBar = new QToolBar(this);
        stGroupToolBar->setIconSize(QSize(16, 16));
        ui->gridLayout_3->addWidget(stGroupToolBar, 0, 0);
        stGroupToolBar->addAction(ui->actionNew_Statechart_Group);
        stGroupToolBar->addAction(ui->actionOpenStatechartGroup);
        stGroupToolBar->addSeparator();
        stGroupToolBar->addAction(ui->actionNew_State_Definition);
        stGroupToolBar->addAction(ui->actionDelete_State_Definition);
        //        stGroupToolBar->addAction(ui->actionRefresh_Statechart_Groups);

        connect(ui->actionZoom_Original, SIGNAL(triggered()), this, SLOT(originalZoomCurrentState()));
        connect(ui->actionDelete_State, SIGNAL(triggered()), this, SLOT(deleteSelectedStates()));
        connect(ui->actionView_All, SIGNAL(triggered()), this, SLOT(zoomToViewAll()));
        connect(ui->actionInsert_Endstate, SIGNAL(triggered()), this, SLOT(addEndState()));
        connect(ui->actionLayout_state, SIGNAL(triggered()), this, SLOT(layoutState()));

        connect(ui->stateTabWidget, SIGNAL(currentChanged(int)), this, SLOT(connectOverviewToTab(int)), Qt::UniqueConnection);
        highlighter = new CppHighlighter(ui->textEditCppCode->document());


    }


}

void armarx::StatechartEditorMainWindow::on_actionSave_as_Image_file_triggered()
{
    if (ui->stateTabWidget->currentStateview())
    {
        QFileDialog d(0, "Save statechart to file", "", "Images (*.png *.jpg *.jpeg)");
        d.setDefaultSuffix("png");
        d.setFileMode(QFileDialog::QFileDialog::AnyFile);
        d.setAcceptMode(QFileDialog::AcceptSave);
        d.layout()->addWidget(new QLabel("Image width in pixel"));
        QLineEdit* localQLineEdit = new QLineEdit("4000");
        localQLineEdit->setValidator(new QIntValidator(1, 20000));
        d.layout()->addWidget(localQLineEdit);
        if (d.exec() == QDialog::Accepted)
        {
            ui->stateTabWidget->currentStateview()->getScene()->saveSceneToSVG(d.selectedFiles().first(), localQLineEdit->text().toInt());
        }
    }

}
