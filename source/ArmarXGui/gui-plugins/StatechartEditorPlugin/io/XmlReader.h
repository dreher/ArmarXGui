/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../../StatechartViewerPlugin/model/State.h"

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/observers/variant/VariantInfo.h>


namespace armarx
{
    namespace statechartio
    {
        /**
         * @class XmlReader
         * @brief XML reader class used to build State objects from XML representations.
         */
        class XmlReader
        {
        public:
            /**
             * Creates a new XmlReader. The ICE communicator is required for deserialization of parameter default values from JSON.
             * @param iceCommunicator Ice communicator.
             */
            XmlReader(Ice::CommunicatorPtr iceCommunicator, VariantInfoPtr info);

            /**
             * Parses the given XML document and builds a State object (that can be retrieved using getRootState()).
             * The State object will not contain references to other states until addReferences() has been called.
             * @param xmlString XML document to parse.
             */
            void parseXml(const QString& xmlString);

            /**
            * Returns the State object created by this XML reader.
            * @return State object.
            */
            armarx::statechartmodel::StatePtr getLoadedState() const
            {
                return loadedState;
            }

        private:
            QString readDescription(rapidxml::xml_node<>* rootNode) const;
            armarx::statechartmodel::EventList readEventList(rapidxml::xml_node<>* eventListNode) const;
            armarx::statechartmodel::StateParameterMap readParameterList(rapidxml::xml_node<>* parameterListNode) const;
            armarx::statechartmodel::ParameterMappingList readParameterMappingList(rapidxml::xml_node<>* parameterMappingListNode) const;
            void readStartState(rapidxml::xml_node<>* startStateNode) const;
            armarx::statechartmodel::StateInstanceMap readSubstateList(rapidxml::xml_node<>* substateListNode) const;
            armarx::statechartmodel::TransitionList readTransitionList(rapidxml::xml_node<>* transitionListNode) const;

            armarx::statechartmodel::StateInstancePtr getSubstateByInstanceName(const QString& name) const;

            QString readAttribute(rapidxml::xml_node<>* node, const QString& attributeName, bool required = true) const;

            Ice::CommunicatorPtr iceCommunicator;
            VariantInfoPtr info;
            armarx::statechartmodel::StatePtr loadedState;
            bool hasSubstateByInstanceName(const QString& name) const;
        };

        /**
         * @class XmlNodeIterator
         * @brief Iterate over all child nodes with a certain name of a given XML node.
         */
        class XmlNodeIterator
        {
        public:
            XmlNodeIterator(rapidxml::xml_node<>* parentNode, const QString& nodeName, bool required);

            rapidxml::xml_node<>* getNext();

        private:
            rapidxml::xml_node<>* parentNode, *currentChildNode;
            const QString& nodeName;
            const QByteArray nodeNameUtf8;
            bool required;
        };
    }
}

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            class XmlReaderException : public LocalException
            {
            public:
                XmlReaderException(const std::string& errorText) : LocalException("Parsing the XML file failed: " + errorText) {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::XmlReaderException";
                }
            };
        }
    }
}

