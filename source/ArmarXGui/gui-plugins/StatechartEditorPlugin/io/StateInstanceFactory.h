/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../../StatechartViewerPlugin/model/stateinstance/StateInstance.h"

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>


namespace armarx
{
    namespace statechartio
    {
        typedef std::pair<rapidxml::xml_node<>*, armarx::statechartmodel::StatePtr> XmlParentPair;

        class StateInstanceFactoryBase : public AbstractFactoryMethod<StateInstanceFactoryBase, XmlParentPair>
        {
        public:
            StateInstanceFactoryBase(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState);
            virtual ~StateInstanceFactoryBase() {}

            virtual armarx::statechartmodel::StateInstancePtr getStateInstance() = 0;

        protected:
            rapidxml::xml_node<>* xmlNode;
            armarx::statechartmodel::StatePtr parentState;
        };

        typedef boost::shared_ptr<StateInstanceFactoryBase> StateInstanceFactoryBasePtr;

        class EndStateInstanceFactory : public StateInstanceFactoryBase
        {
        public:
            EndStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState);

            armarx::statechartmodel::StateInstancePtr getStateInstance() override;

            static StateInstanceFactoryBasePtr createInstance(XmlParentPair);
            static std::string getName()
            {
                return "EndState";
            }

        private:
            static SubClassRegistry registry;
        };

        class LocalStateInstanceFactory : public StateInstanceFactoryBase
        {
        public:
            LocalStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState);

            armarx::statechartmodel::StateInstancePtr getStateInstance() override;

            static StateInstanceFactoryBasePtr createInstance(XmlParentPair);
            static std::string getName()
            {
                return "LocalState";
            }

        private:
            static SubClassRegistry registry;
        };

        class RemoteStateInstanceFactory : public StateInstanceFactoryBase
        {
        public:
            RemoteStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState);

            armarx::statechartmodel::StateInstancePtr getStateInstance() override;

            static StateInstanceFactoryBasePtr createInstance(XmlParentPair);
            static std::string getName()
            {
                return "RemoteState";
            }

        private:
            static SubClassRegistry registry;
        };

        class DynamicRemoteStateInstanceFactory : public StateInstanceFactoryBase
        {
        public:
            DynamicRemoteStateInstanceFactory(rapidxml::xml_node<>* xmlNode, armarx::statechartmodel::StatePtr parentState);

            armarx::statechartmodel::StateInstancePtr getStateInstance() override;

            static StateInstanceFactoryBasePtr createInstance(XmlParentPair);
            static std::string getName()
            {
                return "DynamicRemoteState";
            }

        private:
            static SubClassRegistry registry;
        };
    }
}

