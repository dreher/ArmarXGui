/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::Core::StructuralJsonParser
#define ARMARX_BOOST_TEST
#include <ArmarXGui/Test.h>
#include <ArmarXGui/gui-plugins/StatechartEditorPlugin/variantjson/VariantJsonCompressor.h>

using namespace armarx;


void CompressTestStep(const std::string& variantBaseTypeName, const std::string& compressedJson, const std::string& expandedJson, int indenting = 0)
{
    std::string actual = VariantJsonCompressor::Compress(expandedJson, variantBaseTypeName, indenting);
    BOOST_CHECK_EQUAL(compressedJson, actual);
}

void DecompressTestStep(const std::string& variantBaseTypeName, const std::string& compressedJson, const std::string& expandedJson, int indenting = 0)
{
    std::string actual = VariantJsonCompressor::Decompress(compressedJson, variantBaseTypeName);
    BOOST_CHECK_EQUAL(expandedJson, actual);
}

void TestStep(std::string variantBaseTypeName, const std::string& compressedJson, const std::string& expandedJson)
{
    CompressTestStep(variantBaseTypeName, compressedJson, expandedJson);
    DecompressTestStep(variantBaseTypeName, compressedJson, expandedJson);
}

BOOST_AUTO_TEST_CASE(testCompressVariantJson)
{
    TestStep("::armarx::StringValueMapBase(::armarx::FloatVariantData)", R"({"Elbow L":1.1,"Elbow R":1.2,"Hand Palm 2 L":1.3})", R"({"map":{"Elbow L":{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::FloatVariantData","value":1.1}},"Elbow R":{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::FloatVariantData","value":1.2}},"Hand Palm 2 L":{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::FloatVariantData","value":1.3}}},"type":"::armarx::StringValueMapBase"})");
}




