/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Gui::XmlWriterTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>

#include "../io/XmlWriter.h"
#include "../../StatechartViewerPlugin/model/State.h"

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <iostream>
#include <QTextStream>

using namespace armarx::statechartio;
using namespace armarx::statechartmodel;

BOOST_AUTO_TEST_CASE(testXmlWriter)
{
    QString rootProfileName = QString::fromUtf8(armarx::StatechartProfiles::GetRootName().data());
    // Build state object
    StatePtr state(new State("09F54A1A-3A29-4560-B20C-299069288823"));
    state->setStateName("RootStateName");
    state->setDescription("Test description\nLine 2");
    state->setSize(QSizeF(500, 456));

    // Add parameters to state
    StateParameterPtr inputParameter1(new StateParameter());
    inputParameter1->type = "string";

    armarx::VariantContainerBasePtr inputParameter1Variant(new armarx::SingleVariant(armarx::Variant("DefaultValue")));
    inputParameter1->profileDefaultValues[rootProfileName] = {inputParameter1Variant, R"({"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::StringVariantData","value":"DefaultValue"}})"};
    inputParameter1->optional = false;
    inputParameter1->description = "Input Parameter 1 Description";

    StateParameterPtr inputParameter2(new StateParameter());
    inputParameter2->type = "float";
    armarx::VariantContainerBasePtr inputParameter2Variant(new armarx::SingleVariant(armarx::Variant(13.25f)));
    inputParameter2->profileDefaultValues["TestProfileA"] = QPair<armarx::VariantContainerBasePtr, QString>(inputParameter2Variant, R"({"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::FloatVariantData","value":13.25}})");
    inputParameter2->optional = true;

    StateParameterMap inputParameters;
    inputParameters["InputParameter1Name"] = inputParameter1;
    inputParameters["InputParameter2Name"] = inputParameter2;
    state->setInputParameters(inputParameters);

    StateParameterPtr localParameter1(new StateParameter());
    localParameter1->type = "int";
    armarx::VariantContainerBasePtr localParameter1Variant1(new armarx::SingleVariant(armarx::Variant(1)));
    armarx::VariantContainerBasePtr localParameter1Variant2(new armarx::SingleVariant(armarx::Variant(2)));
    localParameter1->profileDefaultValues[rootProfileName] = {localParameter1Variant1, R"({"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::IntVariantData","value":1}})"};
    localParameter1->profileDefaultValues["TestProfileA"] = QPair<armarx::VariantContainerBasePtr, QString>(localParameter1Variant2, R"({"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::IntVariantData","value":2}})");
    localParameter1->profileDefaultValues["TestProfileB"] = QPair<armarx::VariantContainerBasePtr, QString>(0, "JSON-encoded string");
    localParameter1->optional = false;

    StateParameterMap localParameters;
    localParameters["LocalParameter1Name"] = localParameter1;
    state->setLocalParameters(localParameters);

    StateParameterPtr outputParameter1(new StateParameter());
    outputParameter1->type = "string";
    outputParameter1->optional = true;

    StateParameterMap outputParameters;
    outputParameters["OutputParameter1Name"] = outputParameter1;
    state->setOutputParameters(outputParameters);

    // Add substates to state
    StatePtr localSubstate(new State("CF3662A8-9234-46BD-82C1-856344108823"));
    localSubstate->setStateName("LocalSubstateName");
    state->addSubstate(localSubstate);
    StateInstancePtr localSubstateInst = state->getSubstates()["LocalSubstateName"];
    localSubstateInst->setPosition(QPointF(160, 150));
    localSubstateInst->setBoundingBox(50.0);
    state->setStartState(localSubstateInst);

    StatePtr remoteSubState(new State("148C7D1D-1DB4-4A67-893F-D22D36A88823"));
    remoteSubState->setStateName("RemoteSubstateName");
    state->addRemoteSubstate(remoteSubState, "RemoteSubstateProxyName");
    StateInstancePtr remoteSubstateInst = state->getSubstates()["RemoteSubstateName"];
    remoteSubstateInst->setPosition(QPointF(160, 150));
    remoteSubstateInst->setBoundingBox(50.0);

    state->addEndSubstate("EndSubstateName", "Success");
    StateInstancePtr endSubstateInst = state->getSubstates()["EndSubstateName"];
    endSubstateInst->setPosition(QPointF(160, 150));
    endSubstateInst->setBoundingBox(50.0);

    // Add events to state
    EventList eventList;
    EventPtr e1(new Event());
    e1->name = "Success";
    e1->description = "Event Description";
    eventList.append(e1);
    EventPtr e2(new Event());
    e2->name = "Failure";
    eventList.append(e2);
    state->setOutgoingEvents(eventList);

    // Add start state parameter mapping
    ParameterMappingPtr startParameterMapping1(new ParameterMapping());
    startParameterMapping1->source = armarx::eParent;
    startParameterMapping1->sourceKey = "StartParameterMapping1SourceKey";
    startParameterMapping1->destinationKey = "StartParameterMapping1DestinationKey";

    ParameterMappingPtr startParameterMapping2(new ParameterMapping());
    startParameterMapping2->source = armarx::eOutput;
    startParameterMapping2->sourceKey = "StartParameterMapping2SourceKey";
    startParameterMapping2->destinationKey = "StartParameterMapping2DestinationKey";

    ParameterMappingList startParameterMappingList;
    startParameterMappingList.append(startParameterMapping1);
    startParameterMappingList.append(startParameterMapping2);
    state->setStartStateInputMapping(startParameterMappingList);

    // Add transitions
    TransitionPtr transition1(new Transition());
    transition1->eventName = "Transition1EventName";
    transition1->sourceState = localSubstateInst;
    transition1->destinationState = remoteSubstateInst;
    ParameterMappingPtr transition1ParameterMapping(new ParameterMapping());
    transition1ParameterMapping->source = armarx::eEvent;
    transition1ParameterMapping->sourceKey = "Transition1ParameterMappingSourceKey";
    transition1ParameterMapping->destinationKey = "Transition1ParameterMappingDestinationKey";
    transition1->mappingToNextStatesInput.append(transition1ParameterMapping);
    transition1->supportPoints.append(QPointF(12, 34));
    transition1->supportPoints.append(QPointF(56, 78));
    state->addTransition(transition1);

    TransitionPtr transition2(new Transition());
    transition2->eventName = "Transition2EventName";
    transition2->sourceState = remoteSubstateInst;
    transition2->destinationState = endSubstateInst;
    state->addTransition(transition2);


    // Serialize it
    armarx::VariantInfoPtr varInfo = armarx::VariantInfo::ReadInfoFiles({"ArmarXCore", "ArmarXGui"});
    armarx::statechartio::XmlWriter writer(varInfo);
    writer.serialize(state);
    QString actualResult = writer.getXmlString(true);
    std::cout << actualResult << "\"" << std::endl;

    QString correctResult = R"xml(<?xml version="1.0" encoding="utf-8"?>
<State version="1.2" name="RootStateName" uuid="09F54A1A-3A29-4560-B20C-299069288823" width="500" height="456" type="Normal State">
    <Description>Test description
Line 2</Description>
    <InputParameters>
        <Parameter name="InputParameter1Name" type="string" docType="" optional="no">
            <Description>Input Parameter 1 Description</Description>
            <DefaultValue value='{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::StringVariantData","value":"DefaultValue"}}' docValue="DefaultValue"/>
        </Parameter>
        <Parameter name="InputParameter2Name" type="float" docType="" optional="yes">
            <DefaultValue profile="TestProfileA" value='{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::FloatVariantData","value":13.25}}' docValue="13.25"/>
        </Parameter>
    </InputParameters>
    <OutputParameters>
        <Parameter name="OutputParameter1Name" type="string" docType="" optional="yes"/>
    </OutputParameters>
    <LocalParameters>
        <Parameter name="LocalParameter1Name" type="int" docType="" optional="no">
            <DefaultValue value='{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::IntVariantData","value":1}}' docValue="1"/>
            <DefaultValue profile="TestProfileA" value='{"type":"::armarx::SingleVariantBase","variant":{"typeName":"::armarx::IntVariantData","value":2}}' docValue="2"/>
            <DefaultValue profile="TestProfileB" value="JSON-encoded string"/>
        </Parameter>
    </LocalParameters>
    <Substates>
        <EndState name="EndSubstateName" event="Success" left="160" top="150" boundingSquareSize="50"/>
        <LocalState name="LocalSubstateName" refuuid="CF3662A8-9234-46BD-82C1-856344108823" left="160" top="150" boundingSquareSize="50"/>
        <RemoteState name="RemoteSubstateName" refuuid="148C7D1D-1DB4-4A67-893F-D22D36A88823" proxyName="RemoteSubstateProxyName" left="160" top="150" boundingSquareSize="50"/>
    </Substates>
    <Events>
        <Event name="Success">
            <Description>Event Description</Description>
        </Event>
        <Event name="Failure"/>
    </Events>
    <StartState substateName="LocalSubstateName">
        <ParameterMappings>
            <ParameterMapping sourceType="Parent" from="StartParameterMapping1SourceKey" to="StartParameterMapping1DestinationKey"/>
            <ParameterMapping sourceType="Output" from="StartParameterMapping2SourceKey" to="StartParameterMapping2DestinationKey"/>
        </ParameterMappings>
        <SupportPoints/>
        </StartState>
    <Transitions>
        <Transition from="LocalSubstateName" to="RemoteSubstateName" eventName="Transition1EventName">
            <ParameterMappings>
                <ParameterMapping sourceType="Event" from="Transition1ParameterMappingSourceKey" to="Transition1ParameterMappingDestinationKey"/>
            </ParameterMappings>
            <ParameterMappingsToParentsLocal/>
            <ParameterMappingsToParentsOutput/>
            <SupportPoints>
                <SupportPoint posX="12" posY="34"/>
                <SupportPoint posX="56" posY="78"/>
            </SupportPoints>
        </Transition>
        <Transition from="RemoteSubstateName" to="EndSubstateName" eventName="Transition2EventName">
            <ParameterMappings/>
            <ParameterMappingsToParentsLocal/>
            <ParameterMappingsToParentsOutput/>
            <SupportPoints/>
        </Transition>
    </Transitions>
</State>

)xml";

    QTextStream actualResultStream(&actualResult);
    QTextStream correctResultStream(&correctResult);
    while (true)
    {
        QString actual = actualResultStream.readLine().trimmed();
        QString correct = correctResultStream.readLine().trimmed();
        BOOST_CHECK_EQUAL(actual.isNull(), correct.isNull());
        if (actual.isNull() || correct.isNull())
            break;
        BOOST_CHECK_EQUAL(actual, correct);
    }

}
