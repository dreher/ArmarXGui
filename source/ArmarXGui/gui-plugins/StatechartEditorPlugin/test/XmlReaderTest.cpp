/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Christian Mandery (christian.mandery at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Gui::XmlReaderTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include "../io/XmlReader.h"
#include "../../StatechartViewerPlugin/model/State.h"
#include "../../StatechartViewerPlugin/model/stateinstance/LocalState.h"
#include "../../StatechartViewerPlugin/model/stateinstance/RemoteState.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>


using namespace armarx::exceptions::local;
using namespace armarx::statechartio;
using namespace armarx::statechartmodel;

BOOST_AUTO_TEST_CASE(testParseXml)
{
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    armarx::CMakePackageFinder finder("ArmarXGui");
    armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
    QString xmlInput = armarx::RapidXmlReader::ReadFileContents("ArmarXGui/Teststate.xml").c_str();


    XmlReader xmlReader(ic, armarx::VariantInfoPtr());
    xmlReader.parseXml(xmlInput);

    StatePtr state = xmlReader.getLoadedState();
    StateParameterPtr stateParameter;
    StateInstancePtr stateInstance;
    EndStatePtr endStateInstance;
    LocalStatePtr localStateInstance;
    RemoteStatePtr remoteStateInstance;
    EventList eventList;
    EventPtr event;
    ParameterMappingList parameterMappingList;
    ParameterMappingPtr parameterMapping;
    CTransitionList transitionList;
    TransitionCPtr transition;
    QString rootProfileName = QString::fromUtf8(armarx::StatechartProfiles::GetRootName().c_str());

    BOOST_CHECK(state);

    BOOST_CHECK_EQUAL(state->getUUID(), "09F54A1A-3A29-4560-B20C-299069288823");
    BOOST_CHECK_EQUAL(state->getStateName(), "RootStateName");
    BOOST_CHECK_EQUAL(state->getDescription(), "Test description\nLine 2");
    BOOST_CHECK_EQUAL(state->getSize().width(), 500);
    BOOST_CHECK_EQUAL(state->getSize().height(), 456);

    StateParameterMap inputParameters = state->getInputParameters();
    BOOST_CHECK_EQUAL(inputParameters.size(), 2);

    BOOST_CHECK(inputParameters.contains("InputParameter1Name"));
    stateParameter = inputParameters["InputParameter1Name"];
    auto defaultValue = stateParameter->profileDefaultValues[rootProfileName];
    BOOST_CHECK(stateParameter);
    BOOST_CHECK_EQUAL(stateParameter->type, "string");
    BOOST_CHECK_EQUAL(armarx::SingleVariantPtr::dynamicCast(defaultValue.first)->get()->getString(), "DefaultValue");
    BOOST_CHECK_EQUAL(defaultValue.second, "{\"type\":\"::armarx::SingleVariantBase\",\"variant\":{\"typeName\":\"::armarx::StringVariantData\",\"value\":\"DefaultValue\"}}");
    BOOST_CHECK_EQUAL(stateParameter->optional, false);
    BOOST_CHECK_EQUAL(stateParameter->description, "Input Parameter 1 Description");

    BOOST_CHECK(inputParameters.contains("InputParameter2Name"));
    stateParameter = inputParameters["InputParameter2Name"];
    defaultValue = stateParameter->profileDefaultValues[rootProfileName];
    BOOST_CHECK(stateParameter);
    BOOST_CHECK_EQUAL(inputParameters["InputParameter2Name"]->type, "float");
    BOOST_CHECK_CLOSE(armarx::SingleVariantPtr::dynamicCast(defaultValue.first)->get()->getFloat(), 13.37, 0.00001);
    BOOST_CHECK_EQUAL(defaultValue.second, "{\"type\":\"::armarx::SingleVariantBase\",\"variant\":{\"typeName\":\"::armarx::FloatVariantData\",\"value\":13.36999988555908}}");
    BOOST_CHECK_EQUAL(inputParameters["InputParameter2Name"]->optional, true);
    BOOST_CHECK(inputParameters["InputParameter2Name"]->description.isEmpty());

    StateParameterMap localParameters = state->getLocalParameters();
    BOOST_CHECK_EQUAL(localParameters.size(), 1);

    BOOST_CHECK(localParameters.contains("LocalParameter1Name"));
    stateParameter = localParameters["LocalParameter1Name"];
    defaultValue = stateParameter->profileDefaultValues[rootProfileName];
    BOOST_CHECK(stateParameter);
    BOOST_CHECK_EQUAL(stateParameter->type, "int");
    BOOST_CHECK_EQUAL(armarx::SingleVariantPtr::dynamicCast(defaultValue.first)->get()->getInt(), 1);
    BOOST_CHECK_EQUAL(defaultValue.second, "{\"type\":\"::armarx::SingleVariantBase\",\"variant\":{\"typeName\":\"::armarx::IntVariantData\",\"value\":1}}");
    BOOST_CHECK_EQUAL(stateParameter->profileDefaultValues.size(), 2);
    BOOST_CHECK_EQUAL(stateParameter->profileDefaultValues.contains("TestProfileA"), true);
    BOOST_CHECK_EQUAL(armarx::SingleVariantPtr::dynamicCast(stateParameter->profileDefaultValues["TestProfileA"].first)->get()->getInt(), 2);
    BOOST_CHECK_EQUAL(stateParameter->profileDefaultValues["TestProfileA"].second, "{\"type\":\"::armarx::SingleVariantBase\",\"variant\":{\"typeName\":\"::armarx::IntVariantData\",\"value\":2}}");
    BOOST_CHECK_EQUAL(stateParameter->optional, false);
    BOOST_CHECK(stateParameter->description.isEmpty());

    StateParameterMap outputParameters = state->getOutputParameters();
    BOOST_CHECK_EQUAL(outputParameters.size(), 1);

    BOOST_CHECK(outputParameters.contains("OutputParameter1Name"));
    stateParameter = outputParameters["OutputParameter1Name"];
    defaultValue = stateParameter->profileDefaultValues[rootProfileName];
    BOOST_CHECK(stateParameter);
    BOOST_CHECK_EQUAL(stateParameter->type, "string");
    BOOST_CHECK(!defaultValue.first);
    BOOST_CHECK(defaultValue.second.isEmpty());
    BOOST_CHECK_EQUAL(stateParameter->optional, true);
    BOOST_CHECK(stateParameter->description.isEmpty());

    StateInstanceMap substates = state->getSubstates();
    BOOST_CHECK_EQUAL(substates.size(), 3);

    BOOST_CHECK(substates.contains("LocalSubstateName"));
    stateInstance = substates["LocalSubstateName"];
    BOOST_CHECK(stateInstance);
    BOOST_CHECK_EQUAL(stateInstance->getParent(), state);
    BOOST_CHECK_EQUAL(stateInstance->getInstanceName(), "LocalSubstateName");
    BOOST_CHECK_EQUAL(stateInstance->getTopLeft(), QPointF(160, 150));
    BOOST_CHECK_EQUAL(stateInstance->getBoundingSquareSize(), 50.0);
    localStateInstance = boost::dynamic_pointer_cast<LocalState>(stateInstance);
    BOOST_CHECK(localStateInstance);
    BOOST_CHECK_EQUAL(localStateInstance->getClassUUID(), "CF3662A8-9234-46BD-82C1-856344108823");
    BOOST_CHECK(!localStateInstance->getStateClass());

    BOOST_CHECK(substates.contains("RemoteSubstateName"));
    stateInstance = substates["RemoteSubstateName"];
    BOOST_CHECK(stateInstance);
    BOOST_CHECK_EQUAL(stateInstance->getParent(), state);
    BOOST_CHECK_EQUAL(stateInstance->getInstanceName(), "RemoteSubstateName");
    BOOST_CHECK_EQUAL(stateInstance->getTopLeft(), QPointF(160, 150));
    BOOST_CHECK_EQUAL(stateInstance->getBoundingSquareSize(), 50.0);
    remoteStateInstance = boost::dynamic_pointer_cast<RemoteState>(stateInstance);
    BOOST_CHECK(remoteStateInstance);
    BOOST_CHECK_EQUAL(remoteStateInstance->getClassUUID(), "148C7D1D-1DB4-4A67-893F-D22D36A88823");
    BOOST_CHECK(!remoteStateInstance->getStateClass());
    BOOST_CHECK_EQUAL(remoteStateInstance->proxyName, "RemoteSubstateProxyName");

    BOOST_CHECK(substates.contains("EndSubstateName"));
    stateInstance = substates["EndSubstateName"];
    BOOST_CHECK(stateInstance);
    BOOST_CHECK_EQUAL(stateInstance->getParent(), state);
    BOOST_CHECK_EQUAL(stateInstance->getInstanceName(), "EndSubstateName");
    BOOST_CHECK_EQUAL(stateInstance->getTopLeft(), QPointF(160, 150));
    BOOST_CHECK_EQUAL(stateInstance->getBoundingSquareSize(), 50.0);
    endStateInstance = boost::dynamic_pointer_cast<EndState>(stateInstance);
    BOOST_CHECK(endStateInstance);
    BOOST_CHECK_EQUAL(endStateInstance->getEventName(), "Success");

    eventList = state->getOutgoingEvents();
    BOOST_CHECK_EQUAL(eventList.size(), 2);
    event = eventList[0];
    BOOST_CHECK(event);
    BOOST_CHECK_EQUAL(event->name, "Success");
    BOOST_CHECK_EQUAL(event->description, "Event Description");
    event = eventList[1];
    BOOST_CHECK(event);
    BOOST_CHECK_EQUAL(event->name, "Failure");
    BOOST_CHECK_EQUAL(event->description, "");

    parameterMappingList = state->getStartStateInputMapping();
    BOOST_CHECK_EQUAL(parameterMappingList.size(), 2);
    parameterMapping = parameterMappingList[0];
    BOOST_CHECK(parameterMapping);
    BOOST_CHECK_EQUAL(parameterMapping->source, armarx::eParent);
    BOOST_CHECK_EQUAL(parameterMapping->sourceKey, "StartParameterMapping1SourceKey");
    BOOST_CHECK_EQUAL(parameterMapping->destinationKey, "StartParameterMapping1DestinationKey");
    parameterMapping = parameterMappingList[1];
    BOOST_CHECK(parameterMapping);
    BOOST_CHECK_EQUAL(parameterMapping->source, armarx::eOutput);
    BOOST_CHECK_EQUAL(parameterMapping->sourceKey, "StartParameterMapping2SourceKey");
    BOOST_CHECK_EQUAL(parameterMapping->destinationKey, "StartParameterMapping2DestinationKey");

    transitionList = state->getTransitions();
    BOOST_CHECK_EQUAL(transitionList.size(), 2);

    transition = transitionList[0];
    BOOST_CHECK(transition);
    BOOST_CHECK_EQUAL(transition->eventName, "Transition1EventName");
    BOOST_CHECK_EQUAL(transition->sourceState, localStateInstance);
    BOOST_CHECK_EQUAL(transition->destinationState, remoteStateInstance);
    BOOST_CHECK_EQUAL(transition->mappingToNextStatesInput.size(), 1);
    parameterMapping = transition->mappingToNextStatesInput[0];
    BOOST_CHECK(parameterMapping);
    BOOST_CHECK_EQUAL(parameterMapping->source, armarx::eEvent);
    BOOST_CHECK_EQUAL(parameterMapping->sourceKey, "Transition1ParameterMappingSourceKey");
    BOOST_CHECK_EQUAL(parameterMapping->destinationKey, "Transition1ParameterMappingDestinationKey");
    BOOST_CHECK_EQUAL(transition->supportPoints.toPointList().size(), 2);
    BOOST_CHECK_EQUAL(transition->supportPoints.toPointList()[0], QPointF(12, 34));
    BOOST_CHECK_EQUAL(transition->supportPoints.toPointList()[1], QPointF(56, 78));

    transition = transitionList[1];
    BOOST_CHECK(transition);
    BOOST_CHECK_EQUAL(transition->eventName, "Transition2EventName");
    BOOST_CHECK_EQUAL(transition->sourceState, remoteStateInstance);
    BOOST_CHECK_EQUAL(transition->destinationState, endStateInstance);
    BOOST_CHECK(transition->mappingToNextStatesInput.isEmpty());
    BOOST_CHECK(transition->supportPoints.toPointList().isEmpty());
}

BOOST_AUTO_TEST_CASE(testAddReferences)
{
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    // Once we have C++0x support, we should use a raw string and turn on indentation
    QString rootStateXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"RootStateName\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"123\" height=\"456\"><Substates>"
                           "<LocalState name=\"LocalSubstate1\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"12\" top=\"34\" boundingSquareSize=\"50\"/>"
                           "<LocalState name=\"LocalSubstate2\" refuuid=\"148C7D1D-1DB4-4A67-893F-D22D36A88823\" left=\"12\" top=\"34\" boundingSquareSize=\"50\"/>"
                           "</Substates></State>";
    QString subStateXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"RootStateName\" uuid=\"CF3662A8-9234-46BD-82C1-856344108823\" width=\"123\" height=\"456\"/>";

    XmlReader xmlReaderRootState(ic, armarx::VariantInfoPtr());
    xmlReaderRootState.parseXml(rootStateXml);

    XmlReader xmlReaderSubState(ic, armarx::VariantInfoPtr());
    xmlReaderSubState.parseXml(subStateXml);

    StatePtr rootState = xmlReaderRootState.getLoadedState();
    BOOST_CHECK(rootState);

    StatePtr subState = xmlReaderSubState.getLoadedState();
    BOOST_CHECK(subState);

    BOOST_CHECK_EQUAL(rootState->getSubstates().size(), 2);
    BOOST_CHECK(!rootState->getSubstates()["LocalSubstate1"]->getStateClass());
    BOOST_CHECK(!rootState->getSubstates()["LocalSubstate2"]->getStateClass());

    QMap<QString, StatePtr> uuidStateMap;
    uuidStateMap[rootState->getUUID()] = rootState;
    uuidStateMap[subState->getUUID()] = subState;
    rootState->addReferences(uuidStateMap);

    BOOST_CHECK_EQUAL(rootState->getSubstates().size(), 2);
    BOOST_CHECK_EQUAL(rootState->getSubstates()["LocalSubstate1"]->getStateClass(), subState);
    BOOST_CHECK(!rootState->getSubstates()["LocalSubstate2"]->getStateClass());
}

BOOST_AUTO_TEST_CASE(testInvalidXml)  // Of course, we cannot test all possible causes of error here, just some very simple examples
{
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    armarx::ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    // Completely malformed
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("This is not XML!"), XmlReaderException);

    // Wrong version number
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"0.1\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\"/>"), XmlReaderException);

    // Missing attribute (here: name)
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\"/>"), XmlReaderException);

    // Invalid value for parameter default
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<InputParameters><Parameter name=\"ParameterName\" type=\"string\" default=\"no JSON\" optional=\"no\"/></InputParameters>"
                      "</State>"), XmlReaderException);

    // Invalid value for optional attribute of parameter
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<InputParameters><Parameter name=\"ParameterName\" type=\"string\" optional=\"maybe\"/></InputParameters>"
                      "</State>"), XmlReaderException);

    // Duplicate parameter name
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<InputParameters><Parameter name=\"ParameterName\" type=\"string\" optional=\"yes\"/><Parameter name=\"ParameterName\" type=\"string\" optional=\"no\"/></InputParameters>"
                      "</State>"), XmlReaderException);

    // Invalid substate type
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Substates><BogusState name=\"SubstateName\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                      "</State>"), XmlReaderException);

    // Duplicate substate name
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Substates><EndState name=\"SubstateName\" event=\"foo\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/><EndState name=\"SubstateName\" event=\"bar\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                      "</State>"), XmlReaderException);

    // Duplicate event name
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Events><Event name=\"EventName\"/><Event name=\"EventName\"/></Events>"
                      "</State>"), XmlReaderException);

    // Invalid start state name
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Substates><LocalState name=\"LocalSubstateName\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                      "<StartState substateName=\"NotExistingSubstateName\"/>"
                      "</State>"), XmlReaderException);

    // Invalid from state name for transition
    XmlReader xmlReaderForInvalidFromState(ic, armarx::VariantInfoPtr());
    xmlReaderForInvalidFromState.parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                                          "<Substates><LocalState name=\"LocalSubstateName\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/><RemoteState name=\"RemoteSubstateName\" refuuid=\"148C7D1D-1DB4-4A67-893F-D22D36A88823\" proxyName=\"RemoteSubstateProxyName\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                                          "<Transitions><Transition from=\"NotExistingSubstateName\" to=\"RemoteSubstateName\" eventName=\"Transition1EventName\"/></Transitions>"
                                          "</State>");
    BOOST_CHECK_EQUAL(xmlReaderForInvalidFromState.getLoadedState()->getTransitions().size(), 0);

    // Invalid to state name for transition
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Substates><LocalState name=\"LocalSubstateName\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/><RemoteState name=\"RemoteSubstateName\" refuuid=\"148C7D1D-1DB4-4A67-893F-D22D36A88823\" proxyName=\"RemoteSubstateProxyName\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                      "<Transitions><Transition from=\"LocalSubstateName\" to=\"NotExistingSubstateName\" eventName=\"Transition1EventName\"/></Transitions>"
                      "</State>"), XmlReaderException);

    // Invalid sourceType name for parameter mapping
    BOOST_CHECK_THROW(XmlReader(ic, armarx::VariantInfoPtr()).parseXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"Name\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"0\" height=\"0\">"
                      "<Substates><LocalState name=\"LocalSubstateName\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/><RemoteState name=\"RemoteSubstateName\" refuuid=\"148C7D1D-1DB4-4A67-893F-D22D36A88823\" proxyName=\"RemoteSubstateProxyName\" left=\"0\" top=\"0\" boundingSquareSize=\"0\"/></Substates>"
                      "<Transitions><Transition from=\"LocalSubstateName\" to=\"RemoteSubstateName\" eventName=\"Transition1EventName\"><ParameterMappings><ParameterMapping sourceType=\"invalidSourceType\" from=\"Transition1ParameterMappingSourceKey\" to=\"Transition1ParameterMappingDestinationKey\"/></ParameterMappings></Transition></Transitions>"
                      "</State>"), XmlReaderException);
}
