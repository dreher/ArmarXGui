/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApplicationDatabaseController.h"

#include "../gui/applicationdatabaseitem.h"

#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <QMessageBox>

using namespace ScenarioManager;
using namespace Controller;
using namespace Data_Structure;
using namespace Exec;
using namespace std;
using namespace armarx;

ApplicationDatabaseController::ApplicationDatabaseController(
    PackageVectorPtr packages,
    ExecutorPtr executor, QObject* parent)
    : QObject(parent),
      treemodel(),
      model(new FilterableTreeModelSortFilterProxyModel()),
      packages(packages),
      executor(executor)
{
    //Q_DECLARE_METATYPE(FilterableTreeModelSortFilterProxyModelPtr);
    //qRegisterMetaType<FilterableTreeModelSortFilterProxyModelPtr>();
    qRegisterMetaType<FilterableTreeModelSortFilterProxyModelPtr>("FilterableTreeModelSortFilterProxyModelPtr");
    model->setSourceModel(&treemodel);
}

ApplicationDatabaseController::~ApplicationDatabaseController()
{

}

FilterableTreeModelSortFilterProxyModelPtr ApplicationDatabaseController::getModel()
{
    return model;
}

void ApplicationDatabaseController::updatePackages()
{
    treemodel.clear();

    ApplicationDatabaseItem* rootItem = treemodel.getRootItem();

    for (auto package : *packages)
    {
        ApplicationDatabaseItem* packageItem = new ApplicationDatabaseItem(package);

        for (auto application : *package->getApplications())
        {
            ApplicationDatabaseItem* applicationItem = new ApplicationDatabaseItem(application);

            if (!application->getFound())
            {
                applicationItem->setEnabled(false);
            }

            packageItem->appendChild(applicationItem);
        }

        rootItem->appendChild(packageItem);
    }

    treemodel.update();
    model->setSourceModel(&treemodel);

    emit modelUpdated(model);
}

void ApplicationDatabaseController::on_itemClicked(const QModelIndex& index)
{
    //emit showApplication mit app pointer;
    ApplicationDatabaseItem* appItem = model->data(index, APPLICATIONITEMSOURCE).value<ApplicationDatabaseItem*>();

    if (appItem == nullptr)
    {
        return;
    }

    if (!appItem->isEnabled())
    {
        QMessageBox message;
        message.setText("Could not find Application " + appItem->data(0).toString() + " at " + QString::fromStdString(appItem->getApplication()->getPathToExecutable()) + ".");
        message.exec();
        return;
    }

    //ApplicationInstance is clicked
    if (index.parent().isValid())
    {
        //appItem = reinterpret_cast<ApplicationDatabaseItem*>(rootItem->child(index.parent().row())->child(index.row()));
        std::string cacheDir = Parser::IceParser::getCacheDir();

        executor->loadAndSetCachedProperties(appItem->getApplication(), cacheDir);
        emit applicationClicked(appItem->getApplication());
    }
    else   //scenario clicked
    {
        //appItem = reinterpret_cast<ApplicationDatabaseItem*>(rootItem->child(index.row()));
        emit packageClicked(appItem->getPackage());
    }
}
