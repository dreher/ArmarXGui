/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DetailedApplicationController.h"

#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>
#include <string>
#include <stdlib.h>

using namespace ScenarioManager;
using namespace Controller;
using namespace Exec;
using namespace Data_Structure;
using namespace std;

DetailedApplicationController::DetailedApplicationController(ExecutorPtr executor, QObject* parent) : QObject(parent), executor(executor), showingStartable(false)
{
    QObject::connect(&propertyAdderView, SIGNAL(create(std::string, std::string)),
                     this, SLOT(setProperty(std::string, std::string)));
}

DetailedApplicationController::~DetailedApplicationController()
{
}

void DetailedApplicationController::setDetailedApplicationView(DetailedApplicationView* ptr)
{
    view = ptr;
}

void DetailedApplicationController::showApplicationInstance(ApplicationInstancePtr application, ScenarioItem* item)
{
    if (application.get() == nullptr)
    {
        return;
    }
    showingStartable = true;
    currentApplication = application;
    currentScenario = ScenarioPtr();

    executor->loadAndSetCachedProperties(application, Parser::IceParser::getCacheDir(), false, false);
    application->load();
    view->setVisible(true);
    view->showApplicationInstance(application, item);
}

void DetailedApplicationController::showApplication(ApplicationPtr application)
{
    if (application.get() == nullptr)
    {
        return;
    }
    showingStartable = false;
    currentApplication = ApplicationInstancePtr();
    currentScenario = ScenarioPtr();
    ApplicationPtr app(new Application(*application));
    executor->loadAndSetCachedProperties(app, Parser::IceParser::getCacheDir());

    view->setVisible(true);
    view->showApplication(app);
}

void DetailedApplicationController::showScenario(ScenarioPtr scenario)
{
    if (scenario.get() == nullptr)
    {
        return;
    }
    showingStartable = true;
    currentApplication = ApplicationInstancePtr();
    currentScenario = scenario;

    view->setVisible(true);
    view->showScenario(scenario);
}

void DetailedApplicationController::showPackage(PackagePtr package)
{
    if (package.get() == nullptr)
    {
        return;
    }
    showingStartable = false;
    currentApplication = ApplicationInstancePtr();
    currentScenario = ScenarioPtr();
    view->showPackage(package);
}

void DetailedApplicationController::showInStandardEditor()
{
    if (showingStartable)
    {
        std::string url = "file://";
        if (currentApplication.get() != nullptr)
        {
            url.append(currentApplication->getConfigPath());
        }
        else if (currentScenario.get() != nullptr)
        {
            url.append(currentScenario->getGlobalConfigPath());
        }
        QDesktopServices::openUrl(QUrl(url.c_str()));
    }
}

void DetailedApplicationController::setProperty(std::string name, std::string value)
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            currentApplication->addProperty(name, value);
            currentApplication->save();
            showApplicationInstance(currentApplication, nullptr);
        }
        else if (currentScenario.get() != nullptr)
        {
            currentScenario->getGlobalConfig()->defineOptionalProperty<std::string>(name, "::NOT_DEFINED::", "Custom Property");
            currentScenario->getGlobalConfig()->getProperties()->setProperty(name, value);
            currentScenario->save();
            showScenario(currentScenario);
        }
    }
}

void DetailedApplicationController::start()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            executor->startApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            executor->startScenario(currentScenario);
        }
    }
}

void DetailedApplicationController::stop()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            executor->stopApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            executor->stopScenario(currentScenario);
        }
    }
}

void DetailedApplicationController::save()
{
    if (showingStartable)
    {
        if (currentApplication.get() != nullptr)
        {
            currentApplication->save();
            executor->restartApplication(currentApplication);
        }
        else if (currentScenario.get() != nullptr)
        {
            currentScenario->save();
            executor->restartScenario(currentScenario);
        }
    }
}

void DetailedApplicationController::deleteProperty(std::string name)
{

}

void DetailedApplicationController::showPropertyAddView()
{
    propertyAdderView.exec();
}
