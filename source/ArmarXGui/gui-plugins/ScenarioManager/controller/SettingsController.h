/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../gui/treemodel.h"
#include "../gui/settingsview.h"
#include "../gui/settingsmodel.h"
#include "../gui/settingsitem.h"
#include "../gui/packageadderview.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactoryLinux.h>
#include <QObject>
#include <string>
#include <vector>

namespace ScenarioManager
{
    namespace Controller
    {
        /**
        * @class SettingsController
        * @ingroup controller
        * @brief This controller manages the signals and model of the SettingsView.
        * Any signals involving the settings need to be connected to the corresponding slots in this controller.
        * This controller also manages settings that get saved between sessions, as well as the Executor configuration.
        */
        class SettingsController : public QObject
        {
            Q_OBJECT
        public:
            /**
            * Constructor that sets the data structure this controller operates on, the executor which gets configured by this controller
            * and optionally the parent object.
            * @param packages list of packages. Need to contain the Packages displayed in the SettingsView.
            * @param executor executor which gets configured by the settings
            * @param parent standard QT option to specify a parent
            */
            SettingsController(Data_Structure::PackageVectorPtr packages, Exec::ExecutorPtr executor, QObject* parent = 0);

            void init();

            /**
            * Destructor.
            */
            ~SettingsController() override;

        signals:
            /**
            * Emitted when a package gets added in the settings. Allows other controllers to update their models
            * with the new package.
            * @param name name of the new package
            */
            void packageAdded(std::string name);

            /**
            * Emitted when a package gets removed. Other packages need to update their models.
            */
            void packageRemoved();

            void setExecutorState(int killMethodIndex, int delay, int stopStrategyIndex);


        public slots:
            /**
            * Shows the settings dialog.
            */
            void showSettings();

            /**
            * Shows a view that allows the user to add new packages.
            */
            void showPackageAdderView();

            /**
            * Adds a new package to the settings.
            */
            void addPackage(std::string name);

            /**
            * Removes a package from the settings.
            * @param row row of the package
            * @param column column of the package
            * @param parent index of the package to be removed
            */
            void removePackage(int row, int column, QModelIndex parent);

            /**
            * Configures the Executor.
            * @param killMethod "StopAndKill" or "Stop"
            * @param delay length of the delay in the StopAndKill stop-method
            * @param stopMethod "By Name" or "By Pid"
            */
            void setExecutorStopStrategy(std::string killMethod, int delay, std::string stopMethod);

            /**
            * Updates the packages displayed in the settingsview by reloading all packages into the model.
            */
            void updateModel();

            void clearXmlCache();
            void clearPidCache();

        private:

            ScenarioManager::Exec::StopStrategyFactory stopStrategyFactory;
            ScenarioManager::Exec::StopperFactoryPtr stopperFactory;
            ScenarioManager::Exec::StarterFactoryPtr starterFactory;

            SettingsModelPtr treemodel;
            FilterableTreeModelSortFilterProxyModelPtr model;
            Data_Structure::PackageVectorPtr packages;
            Exec::ExecutorPtr executor;
            SettingsView settingsDialog;
            PackageAdderView packageAdderView;
        };
    }
}

