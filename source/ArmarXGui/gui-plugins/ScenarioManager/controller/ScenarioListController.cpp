/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ScenarioListController.h"

#include "../gui/namelocationview.h"
#include "../gui/scenarioitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/DependenciesGenerator.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QTimer>
#include <QSettings>
#include <QMessageBox>
#include <sstream>
#include <QApplication>

using namespace ScenarioManager;
using namespace Controller;
using namespace Data_Structure;
using namespace Parser;
using namespace Exec;
using namespace std;

#define UPDATE_TIMER 500 //ms

ScenarioListController::ScenarioListController(PackageVectorPtr packages, ExecutorPtr executor, QObject* parent)
    : QObject(parent),
      treemodel(),
      model(new FilterableTreeModelSortFilterProxyModel()),
      packages(packages), executor(executor)
{
    qRegisterMetaType<FilterableTreeModelSortFilterProxyModelPtr>("FilterableTreeModelSortFilterProxyModelPtr");

    QTimer* timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateStati()));
    timer->start(UPDATE_TIMER);

    model->setDynamicSortFilter(true);
    model->setSourceModel(&treemodel);

    QObject::connect(&createScenarioView, SIGNAL(created(std::string, std::string)),
                     this, SLOT(createdScenario(std::string, std::string)));

    QObject::connect(&treemodel, SIGNAL(applicationsDrop(QList<QPair<QString, ScenarioManager::Data_Structure::Application*>>, int, QModelIndex)),
                     this, SLOT(addApplicationsToScenario(QList<QPair<QString, ScenarioManager::Data_Structure::Application*>>, int, QModelIndex)));
}

ScenarioListController::~ScenarioListController()
{

}

FilterableTreeModelSortFilterProxyModelPtr ScenarioListController::getTreeModel()
{
    return model;
}

void ScenarioListController::stop(int row, int column, QModelIndex parent)
{
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (item == nullptr)
    {
        return;
    }
    if (parent.isValid())
    {
        if (parent.parent().isValid())  //stop application
        {
            if (!item->isEnabled())
            {
                return;
            }
            ApplicationInstancePtr instance = item->getApplicationInstance();
            executor->stopApplication(instance);
        }
        else     //stop packageSubpackage
        {
            if (!item->isEnabled())
            {
                return;
            }
            for (auto app : *item->getApplicationInstances())
            {
                executor->stopApplication(app);
            }
        }

    }
    else   //stop scenario
    {
        ScenarioPtr scenario = item->getScenario();
        executor->stopScenario(scenario);
    }
}

void ScenarioListController::start(int row, int column, QModelIndex parent)
{
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (item == nullptr)
    {
        return;
    }
    if (parent.isValid())
    {
        if (parent.parent().isValid())  //start application
        {
            if (!item->isEnabled())
            {
                return;
            }
            ApplicationInstancePtr instance = item->getApplicationInstance();
            executor->startApplication(instance);
        }
        else     // start packagesubtree
        {
            if (!item->isEnabled())
            {
                return;
            }

            for (auto app : *item->getApplicationInstances())
            {
                executor->startApplication(app);
            }
        }
    }
    else   //start scenario
    {
        ScenarioPtr scenario = item->getScenario();
        executor->startScenario(scenario);
    }
}

void ScenarioListController::restart(int row, int column, QModelIndex parent)
{
    //hole entsprechende application aus dem package und uebergebe sie dem executor zum restarten & update model entsprechend
    ScenarioItem* item = model->data(model->index(row, column, parent), SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (item == nullptr)
    {
        return;
    }
    if (parent.isValid())
    {
        if (parent.parent().isValid())
        {
            //start/stop application
            if (item->isEnabled())
            {
                executor->restartApplication(item->getApplicationInstance());
            }
            else
            {
                return;
            }
        }
        else
        {
            //start/stop packageSubtree
            if (item->isEnabled())
            {
                for (auto app : *item->getApplicationInstances())
                {
                    executor->restartApplication(app);
                }
            }
            else
            {
                return;
            }
        }

    }
    else   //start/stop scenario
    {
        executor->restartScenario(item->getScenario());
    }
}

void ScenarioListController::addApplicationsToScenario(QList<QPair<QString, Application*>> applications,
        int row, const QModelIndex& parent)
{
    ScenarioItem* item;//reinterpret_cast<ScenarioItem*>(model->data(parent, SCENARIOITEM).data());
    QModelIndex scenarioIndex;

    if (parent.parent().isValid() && parent.parent().parent().isValid())
    {
        item = model->data(parent.parent().parent(), SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent.parent().parent();
    }
    else if (parent.parent().isValid())
    {
        item = model->data(parent.parent(), SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent.parent();
    }
    else
    {
        item = model->data(parent, SCENARIOITEMPROXY).value<ScenarioItem*>();
        scenarioIndex = parent;
    }

    if (item == nullptr)
    {
        return;
    }

    ScenarioPtr scenario = item->getScenario();
    if (!scenario->isScenarioFileWriteable())
    {
        QMessageBox::critical(0, "Adding application failed!", "The scenario '" + QString::fromStdString(scenario->getName()) + "' is read only. You cannot add an application to this scenario!");
        return;
    }
    Parser::DependenciesGenerator generator;
    std::vector<string> acceptedApps = generator.getDependencieTree(scenario->getPackage()->getName());


    for (auto pair : applications)
    {
        ScenarioManager::Data_Structure::Application* app = pair.second;
        PackagePtr appPackage;
        for (auto package : *packages)
        {
            for (auto pApp : *package->getApplications())
            {
                if (pApp->getName().compare(app->getName()) == 0)
                {
                    appPackage = package;
                }
            }
        }

        if (appPackage.get() == nullptr)
        {
            return;
        }
        if (std::find(acceptedApps.begin(), acceptedApps.end(), appPackage->getName()) == acceptedApps.end())
        {
            QMessageBox box;
            QString message("You cannot drop an App from ");
            message.append(QString::fromStdString(appPackage->getName()));
            message.append(" into a Scenario from ");
            message.append(QString::fromStdString(scenario->getPackage()->getName()));
            box.setText(message);
            box.exec();
            return;
        }


        string appName = app->getName();
        size_t runIndex = appName.rfind("Run");
        if (runIndex != std::string::npos)
        {
            appName = appName.substr(0, runIndex);
        }

        int instanceCounter = 0;
        std::string instanceName = pair.first.toStdString();
        if (instanceName.empty())
        {
            for (ApplicationInstancePtr instances : *scenario->getApplications())
            {
                if (instances->getName().compare(appName) == 0)
                {
                    instanceCounter++;
                }
            }
            if (instanceCounter == 0)
            {
                instanceName = "";
            }
            else
            {
                instanceName = "instance" + to_string(instanceCounter);
            }

        }
        else
        {
            for (ApplicationInstancePtr instances : *scenario->getApplications())
            {
                if (instances->getInstanceName().compare(instanceName) == 0)
                {
                    instanceCounter++;
                }
            }
            if (instanceCounter > 0)
            {
                instanceName += to_string(instanceCounter);
            }
        }


        std::string configPath = scenario->getPath();
        configPath = configPath.substr(0, configPath.find_last_of('/'));
        configPath.append("/config/");


        configPath.append(app->getName());
        if (!instanceName.empty())
        {
            configPath.append(".");
            configPath.append(instanceName);
        }
        configPath.append(".cfg");

        std::ofstream out(configPath);
        out.close();

        ApplicationInstancePtr appInstance(new ApplicationInstance(appName, app->getPathToExecutable(), instanceName, configPath, appPackage->getName(), scenario, true));

        std::string cacheDir = Parser::IceParser::getCacheDir();

        executor->loadAndSetCachedProperties(appInstance, cacheDir, true);
        appInstance->load(true);

        for (auto elem : app->getProperties()->getProperties()->getPropertiesForPrefix(""))
        {
            appInstance->modifyProperty(elem.first, elem.second);
            appInstance->setDefaultPropertyEnabled(elem.first, true);
        }

        appInstance->save();

        scenario->addApplication(appInstance);

        QModelIndex subScenarioIndex = findSubScenarioModelIndexByScenarioIndex(scenarioIndex, appPackage->getName());;

        //if this is unvalid than the user dropped an app out of an package that is not currently used inside the scenario
        //so create subscenario item
        //this behaviour was kind of buggy be careful while optimising
        if (!subScenarioIndex.isValid())
        {
            std::vector<ApplicationInstancePtr> list;

            ScenarioItem* subScenarioItem = new ScenarioItem(appPackage->getName(), list);

            treemodel.insertRow(0, subScenarioItem, scenarioIndex);

            subScenarioIndex = findSubScenarioModelIndexByScenarioIndex(scenarioIndex, appPackage->getName());

            treemodel.insertRow(0, new ScenarioItem(appInstance), subScenarioIndex);
        }
        else
        {
            treemodel.insertRow(0, new ScenarioItem(appInstance), subScenarioIndex);
        }
    }
    scenario->save();

}

void ScenarioListController::removeItem(QModelIndex item)
{
    ScenarioItem* scenItem;
    if (item.parent().isValid() && item.parent().parent().isValid())
    {
        scenItem = model->data(item.parent().parent(), SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioItem* appItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        ScenarioPtr scenario = scenItem->getScenario();
        scenario->removeApplication(appItem->getApplicationInstance());

        scenario->save();
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
    }
    else if (item.parent().isValid())
    {
        scenItem = model->data(item.parent(), SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioItem* packageItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        ScenarioPtr scenario = scenItem->getScenario();

        for (auto app : *packageItem->getApplicationInstances())
        {
            scenario->removeApplication(app);
        }
        scenario->save();
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
    }
    else
    {
        scenItem = model->data(item, SCENARIOITEMSOURCE).value<ScenarioItem*>();

        if (scenItem == nullptr)
        {
            return;
        }

        ScenarioPtr scen = scenItem->getScenario();

        QSettings settings("KIT", "ScenarioManager");
        QStringList scenarios = settings.value("scenarios").toStringList();
        QString toFind(QString::fromStdString(scen->getName()));
        toFind.append("::Package::");
        toFind.append(QString::fromStdString(scen->getPackage()->getName()));
        scenarios.removeAt(scenarios.indexOf(toFind));
        settings.setValue("scenarios", scenarios);

        for (auto package : *packages)
        {
            for (auto it = package->getScenarios()->begin(); it != package->getScenarios()->end(); ++it)
            {
                if (scen.get() == it->get())
                {
                    package->getScenarios()->erase(it);
                    break;
                }
            }
        }
        treemodel.removeRow(model->mapToSource(item).row(), model->mapToSource(item.parent()));
        emit updated();
    }
}

void ScenarioListController::createScenario()
{
    if (packages->size() != 0)
    {
        QVector<QPair<QString, bool> > packageNames;
        for (auto package : *packages)
        {
            packageNames << qMakePair(QString::fromStdString(package->getName()), package->isScenarioPathWritable());
        }
        createScenarioView.setPackages(packageNames);
        createScenarioView.exec();
    }
    else
    {
        QMessageBox messageBox;
        messageBox.setText("You have to have at least one Package open to create an Scenario");
        messageBox.exec();
    }
}

void ScenarioListController::createdScenario(string name, string packageStr)
{
    XMLScenarioParser parser;

    PackagePtr package;
    for (unsigned int i = 0; i < packages->size(); i++)
    {
        if (packages->at(i)->getName().compare(packageStr) == 0)
        {
            package = packages->at(i);
            break;
        }
    }
    if (package.get() == nullptr)
    {
        return;
    }

    if (parser.isScenarioExistend(name, package))
    {
        QMessageBox box;
        QString message("The Scenario " + QString::fromStdString(name) + " already exists in Package " + QString::fromStdString(packageStr));
        box.setText(message);
        box.exec();
        return;
    }

    ScenarioPtr ptr = parser.createNewScenario(name, package);
    if (ptr.get() == nullptr)
    {
        return;
    }

    for (auto package : *packages)
    {
        if (package->getName().compare(packageStr) == 0)
        {
            package->addScenario(ptr);
            treemodel.insertRow(0, new ScenarioItem(ptr));
            break;
        }
    }

    QSettings settings("KIT", "ScenarioManager");
    QStringList scenarios = settings.value("scenarios").toStringList();
    QString toAdd;
    toAdd.append(QString::fromStdString(ptr->getName()));
    toAdd.append("::Package::");
    toAdd.append(QString::fromStdString(package->getName()));
    scenarios.append(toAdd);
    settings.setValue("scenarios", scenarios);
    emit updated();
}

void ScenarioListController::showApplication(const QModelIndex& index)
{

    ScenarioItem* item = model->data(index, SCENARIOITEMSOURCE).value<ScenarioItem*>();
    if (index.column() > 0 || item == nullptr)
    {
        return;
    }

    if (index.parent().isValid())
    {
        if (index.parent().parent().isValid())
        {
            ApplicationInstancePtr appInstance = item->getApplicationInstance();
            appInstance->updateFound();
            if (!appInstance->getFound())
            {
                QMessageBox message;
                message.setText("Could not find Application " + item->data(0).toString() + " at " + QString::fromStdString(appInstance->getPathToExecutable()) + ".");
                message.exec();
                if (!appInstance->getFound())
                {
                    updateModel();
                    emit updated();
                }
                return;
            }

            emit applicationInstanceClicked(appInstance, item);
        }
        else
        {
            return;
        }
    }
    else
    {
        emit scenarioClicked(item->getScenario());
    }
}

void ScenarioListController::updateModel() //Scenarios don't have a setStatus-method, therefore UI should calculate Scenario status based on other
{
    ScenarioItem* rootItem = treemodel.getRootItem();

    for (auto package : *packages)
    {
        for (auto scenario : *package->getScenarios())
        {
            std::map<std::string, std::vector<ApplicationInstancePtr>> packageSubtrees;
            for (auto app : *scenario->getApplications())
            {
                std::string packageName = app->getPackageName();

                if (packageSubtrees.count(packageName) == 0)
                {
                    packageSubtrees[packageName] = std::vector<ApplicationInstancePtr>();
                }
                packageSubtrees[packageName].push_back(app);
            }

            ScenarioItem* scenarioItem;
            int index = findScenario(rootItem, scenario->getName(), scenario->getPackage()->getName());
            if (index == -1)
            {
                scenarioItem = new ScenarioItem(scenario);

                for (auto it : packageSubtrees)
                {
                    ScenarioItem* currentItem = new ScenarioItem(it.first, it.second);
                    scenarioItem->appendChild(currentItem);
                }
                treemodel.insertRow(0, scenarioItem);
            }
            else
            {
                scenarioItem = static_cast<ScenarioItem*>(rootItem->child(index));

                for (int i = 0; i < scenarioItem->childCount(); i++)
                {
                    ScenarioItem* subPackageItem = static_cast<ScenarioItem*>(scenarioItem->child(i));
                    std::string name = subPackageItem->data(0).toString().toStdString();

                    std::vector<ApplicationInstancePtr> currentBuildSubtree = packageSubtrees[name];
                    ApplicationInstanceVectorPtr currentTreeSubtree = subPackageItem->getApplicationInstances();
                    for (size_t k = 0; k < currentTreeSubtree->size(); k++)
                    {
                        bool found = false;
                        for (size_t j = 0; j < currentBuildSubtree.size(); j++)
                        {
                            if (currentTreeSubtree->at(k)->getName() == currentBuildSubtree[j]->getName()
                                && currentTreeSubtree->at(k)->getInstanceName() == currentBuildSubtree[j]->getInstanceName())
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            treemodel.insertRow(0, new ScenarioItem(currentTreeSubtree->at(k)), findSubScenarioModelIndex(scenario->getName(), name));
                        }
                    }
                }
            }
        }
    }
}
QModelIndex ScenarioListController::findApplicationModelIndex(ScenarioManager::Data_Structure::ApplicationInstancePtr application)
{
    QModelIndex subScenarioModelIndex = findSubScenarioModelIndex(application->getScenario()->getName(), application->getPackageName());
    if (!subScenarioModelIndex.isValid())
    {
        return QModelIndex();
    }
    int count = 0;
    QModelIndex appIndex = subScenarioModelIndex.child(0, 0);
    while (true)
    {
        appIndex = appIndex.sibling(count, 0);
        if (!appIndex.isValid())
        {
            return QModelIndex();
        }
        if (application->getInstanceName().empty())
        {
            if (application->getName() == appIndex.data().toString().toStdString())
            {
                return appIndex;
            }
        }
        else
        {
            if (application->getInstanceName() + "." + application->getName() == appIndex.data().toString().toStdString())
            {
                return appIndex;
            }
        }

        count++;
    }
}

QModelIndex ScenarioListController::findSubScenarioModelIndex(std::string scenarioName, std::string packageName)
{
    QModelIndex scenarioIndex = treemodel.index(0, 0);

    bool scenarioFound = false;
    int count = 0;
    while (!scenarioFound)
    {
        scenarioIndex = scenarioIndex.sibling(count, 0);

        if (!scenarioIndex.isValid())
        {
            return QModelIndex();
        }
        if (scenarioName == scenarioIndex.data().toString().toStdString())
        {
            scenarioFound = true;
            break;
        }

        count++;
    }

    count = 0;
    while (true)
    {
        QModelIndex currentSubPackageIndex = scenarioIndex.child(count, 0);
        if (!currentSubPackageIndex.isValid())
        {
            return QModelIndex();
        }
        if (packageName == currentSubPackageIndex.data().toString().toStdString())
        {
            return currentSubPackageIndex;
        }
        count++;
    }

    return QModelIndex();
}

QModelIndex ScenarioListController::findSubScenarioModelIndexByScenarioIndex(QModelIndex scenarioIndex, std::string packageName)
{
    int count = 0;
    while (true)
    {
        QModelIndex currentSubPackageIndex = scenarioIndex.child(count, 0);
        if (packageName == currentSubPackageIndex.data().toString().toStdString())
        {
            return currentSubPackageIndex;
        }
        if (!currentSubPackageIndex.isValid())
        {
            return QModelIndex();
        }
        count++;
    }

    return QModelIndex();
}

//looks if the current scenario is already added
int ScenarioListController::findScenario(ScenarioItem* rootItem, std::string name, std::string packageName)
{
    for (int i = 0; i < rootItem->childCount(); i++)
    {
        if (static_cast<ScenarioItem*>(rootItem->child(i))->getScenario()->getName() == name
            && static_cast<ScenarioItem*>(rootItem->child(i))->getScenario()->getPackage()->getName() == packageName)
        {
            return i;
        }
    }

    return -1;
}

void ScenarioListController::updateStati()
{
    IceUtil::Time start = IceUtil::Time ::now();
    bool changed = false;
    //update all app instances and scenarios
    for (ScenarioManager::Data_Structure::PackagePtr& package : *packages)
    {
        ScenarioVectorPtr scenarios = package->getScenarios();
        for (ScenarioPtr scenario : *scenarios)
        {
            ApplicationInstanceVectorPtr appInsts = scenario->getApplications();
            for (ApplicationInstancePtr  appInst : *appInsts)
            {
                std::string status = executor->getApplicationStatus(appInst);
                bool statusChanged = appInst->setStatus(status);
                changed |= statusChanged;
            }
        }
    }
    ARMARX_DEBUG << deactivateSpam(5) << "Updating scenarios took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    if (changed)
    {
        start = IceUtil::Time ::now();
        treemodel.update();
        ARMARX_DEBUG << deactivateSpam(5) << "Updating scenario Model took " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    }

    emit statusUpdated();
}

void ScenarioListController::saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr application)
{
    for (auto package : *packages)
    {
        for (auto scenario : *package->getScenarios())
        {
            bool saveScenario = false;
            for (auto app : *scenario->getApplications())
            {
                if (!saveScenario &&
                    app->getName().compare(application->getName()) == 0 &&
                    app->getInstanceName().compare(application->getInstanceName()) == 0)
                {
                    saveScenario = true;
                }

                if (saveScenario)
                {
                    std::string cacheDir = Parser::IceParser::getCacheDir();
                    executor->loadAndSetCachedProperties(app, cacheDir, false, false);
                    qApp->processEvents();
                }
            }

            if (saveScenario)
            {
                scenario->save();
                return;
            }
        }
    }
}
