/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ComponentManagementTool::ControllerTest
#define ARMARX_BOOST_TEST

#include <ComponentManagementTool/Test.h>
#include "../executor/Executor.h"
#include <iostream>

#include "../data_structure/Application.h"
#include "../data_structure/ApplicationInstance.h"
#include "../data_structure/Package.h"
#include "../data_structure/Scenario.h"

#include "../executor/StopperFactoryLinux.h"
#include "../executor/StopStrategyFactory.h"
#include "../executor/StarterFactoryLinux.h"

#include "../controller/ApplicationDatabaseController.h"
#include "../controller/DetailedApplicationController.h"
#include "../controller/openscenariocontroller.h"
#include "../controller/ScenarioListController.h"
#include "../controller/SettingsController.h"

#include "../parser/PackageBuilder.h"
#include "../parser/XMLScenarioParser.h"

using namespace ComponentManagementTool;
using namespace Exec;
using namespace Data_Structure;
using namespace Controller;
using namespace Parser;

PackageVectorPtr packages = PackageVectorPtr();

StopperFactoryLinux stopF;
StopStrategyFactory stopStratF;
StopStrategyPtr ssStopAndKillByName = stopStratF.getStopAndKillStrategy(stopF.getByNameStopper(), 1);
StarterFactoryLinux sfl;
ApplicationStarterPtr asp = sfl.getStarter();
ExecutorPtr exe = ExecutorPtr(new Executor(ssStopAndKillByName, asp));

PackageBuilder parser = PackageBuilder();

PackagePtr coreP = parser.parsePackage("ArmarXCore");
PackagePtr guiP = parser.parsePackage("ArmarXGui");

ApplicationDatabaseController* adc;
DetailedApplicationController* dac;
OpenScenarioController* osc;
ScenarioListController* slc;
SettingsController* sc;

BOOST_AUTO_TEST_CASE(init)
{
    packages->push_back(coreP);
    packages->push_back(guiP);
    adc = new ApplicationDatabaseController(packages, exe, NULL);
    dac = new DetailedApplicationController(exe, NULL);
    osc = new OpenScenarioController(packages, NULL);
    slc = new ScenarioListController(packages, exe, NULL);
    sc = new SettingsController(packages, exe, NULL);

}

BOOST_AUTO_TEST_CASE(ApplicationDatabase)
{
}

BOOST_AUTO_TEST_CASE(DetailedApplication)
{
}

BOOST_AUTO_TEST_CASE(OpenScenario)
{
}

BOOST_AUTO_TEST_CASE(ScenarioList)
{
}

BOOST_AUTO_TEST_CASE(Settings)
{
}



