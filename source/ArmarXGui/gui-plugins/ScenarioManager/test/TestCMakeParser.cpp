/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ScenarioManager::TestPackageBuilder
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <boost/test/output_test_stream.hpp>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>



using namespace ScenarioManager;
using namespace Parser;
using namespace Data_Structure;
using namespace armarx;

using boost::test_tools::output_test_stream;

PackageBuilder parser = PackageBuilder();

BOOST_AUTO_TEST_CASE(parsePackage)
{
    PackagePtr coreP = parser.parsePackage("ArmarXCore");
    PackagePtr guiP = parser.parsePackage("ArmarXGui");

    BOOST_CHECK_EQUAL(coreP->getName(), "ArmarXCore");
    BOOST_CHECK_EQUAL(guiP->getName(), "ArmarXGui");
}

BOOST_AUTO_TEST_CASE(nonexistant_package)
{
    PackagePtr badP = parser.parsePackage("asjhdbajasda");
    if (badP != NULL)
    {
        BOOST_ERROR("Parser returned nonexistant package");
    }
}


/*
    if(coreP.packageFound()) {
        BOOST_CHECK_EQUAL(pFinder2.getName(), "ArmarXCore");
    } else BOOST_WARNING("Couldn't find ArmarXCore");

    if(guiP.packageFound()) {
        BOOST_CHECK_EQUAL(guiP.getName(), "ArmarXGui");
    } else BOOST_WARNING("Couldn't find ArmarXGui");
}

BOOST_AUTO_TEST_CASE(nonexistant_package) {
    PackageBuilder badParser("asdfkjasldfjh");
    if(coreP.packageFound()) {
        BOOST_ERROR("Found nonexistant package");
    }
}
*/


