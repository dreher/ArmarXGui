/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ScenarioManager::ExecutorTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>

#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>
#include <iostream>

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Application.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>

#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopAndKill.h>

#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <boost/test/output_test_stream.hpp>
#include <boost/shared_ptr.hpp>

#include "ExecutorTestEnvironment.h"

/*  In seconds.
    The smaller the value, the less likely these tests succeed. Used as a buffer between
    Application/Scenario starts and stops, since these use system calls that can be slow and
    are asynced from the main thread.
    Current total sleep() calls in this test: 19
*/
#define SLEEP_TIME 1

using namespace ScenarioManager;
using namespace Exec;
using namespace Data_Structure;


ExecutorPtr exe;
ApplicationInstancePtr ai;
ScenarioPtr s;
//ExecutorTestEnvironmentPtr environment = ExecutorTestEnvironmentPtr(new ExecutorTestEnvironment());

//------ All kinds of stop-strategies -----
StopStrategyPtr ssStopByPid;
StopStrategyPtr ssStopByName;
StopStrategyPtr ssStopAndKillByPid;
StopStrategyPtr ssStopAndKillByName;
//-----
BOOST_AUTO_TEST_CASE(ExecutorInitialization)
{
}

////This makes sure the application is actually running, before testing restarting or stopping.
//void makeSureApplicationIsRunning() {
//    if(exe->getApplicationStatus(ai).get() == "Stopped") {
//        ARMARX_INFO_S << "Application is stopped. Starting it. \n";
//        exe->startApplication(ai).wait();
//        sleep(SLEEP_TIME);
//    } else if(exe->getApplicationStatus(ai).get() == "Zombie") {
//        ARMARX_INFO_S << "Application is a zombie. Stopping and starting it. \n";
//        exe->setStopStrategy(ssStopAndKillByName); //This is the safest stopstrategy.
//        exe->stopApplication(ai).wait(); //Restart is unreliable, stopping and safing is safer
//        sleep(SLEEP_TIME);
//        exe->startApplication(ai).wait();
//        sleep(SLEEP_TIME);
//    }
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");
//}

////Directly copied from ScenarioListController, this updates the stati in the scenario.
//void updateScenarioStati() {
//    ApplicationInstanceVectorPtr appInsts = s->getApplications();
//    for (auto aIt = appInsts->begin(); aIt != appInsts->end(); aIt++) {
//        (*aIt)->setStatus(exe->getApplicationStatus(*aIt).get());
//    }
//}

//BOOST_AUTO_TEST_CASE(ExecutorInitialization)
//{
//    //environment = ExecutorTestEnvironmentPtr(new ExecutorTestEnvironment());

//    StopperFactoryPtr stopF = StopperFactory::getFactory();
//    StopStrategyFactory stopStratF = StopStrategyFactory();

//    ssStopByPid = stopStratF.getStopStrategy(stopF->getPidStopper());
//    ssStopByName = stopStratF.getStopStrategy(stopF->getByNameStopper());
//    ssStopAndKillByPid = stopStratF.getStopAndKillStrategy(stopF->getPidStopper(), 1);
//    ssStopAndKillByName = stopStratF.getStopAndKillStrategy(stopF->getByNameStopper(), 1);

//    StarterFactoryPtr sfl = StarterFactory::getFactory();
//    ApplicationStarterPtr asp = sfl->getStarter();

//    exe = ExecutorPtr(new Executor(ssStopAndKillByName, asp));
//}

//BOOST_AUTO_TEST_CASE(ApplicationStart)
//{
//    Parser::PackageBuilder cMParser = Parser::PackageBuilder();
//    Data_Structure::PackagePtr p = cMParser.parsePackage("ArmarXCore");

//    Data_Structure::ApplicationVectorPtr applications = p->getApplications();
//    Data_Structure::ApplicationPtr app = p->getApplicationByName("SystemObserverRun");
//    Data_Structure::ApplicationPtr app2 = p->getApplicationByName("ExampleUnitAppRun");
//    Data_Structure::ApplicationPtr app3 = p->getApplicationByName("ExampleUnitObserverRun");

////    if(app == NULL || app2 == NULL || app3 == NULL )  BOOST_ERROR("One of the Applications does not exist in package ArmarXCore");

//    ai = ApplicationInstancePtr(new Data_Structure::ApplicationInstance((*app), "testInstance", ""));
////    ApplicationInstancePtr ai2 = ApplicationInstancePtr(new Data_Structure::ApplicationInstance((*app2), "testInstance2", ""));
////    ApplicationInstancePtr ai3 = ApplicationInstancePtr(new Data_Structure::ApplicationInstance((*app3), "testInstance3", ""));

//    s = ScenarioPtr(new Data_Structure::Scenario("testScenario", "0", "0", p.get()));
//    s->addApplication(ai);
////    s->addApplication(ai2);
////    s->addApplication(ai3);
//    exe->getStarter()->commandLineParameters = "--Ice.Default.Locator=\"IceGrid/Locator:tcp -p 12000 -h localhost\" ";
//    if (exe->getApplicationStatus(ai).get() != "Stopped") exe->stopApplication(ai).wait();
//    sleep(SLEEP_TIME);

//    exe->startApplication(ai).wait();
//    sleep(SLEEP_TIME);

//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");
//}

///*  The following four tests set one of the four possible stop-strategies and then first restart
//*   and then stop the application.
//*/

//BOOST_AUTO_TEST_CASE(Application_StopBy_Pid)
//{
//    makeSureApplicationIsRunning();

//    exe->setStopStrategy(ssStopByPid);

//    exe->restartApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");

//    exe->stopApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Stopped"); //Stop sometimes doesn't work and that's okay.
//}

//BOOST_AUTO_TEST_CASE(Application_Stop_ByName)
//{
//    makeSureApplicationIsRunning();

//    exe->setStopStrategy(ssStopByName);

//    exe->restartApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");

//    exe->stopApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Stopped"); //Stop sometimes doesn't work and that's okay
//}

//BOOST_AUTO_TEST_CASE(Application_StopAndKill_ByPid)
//{
//    makeSureApplicationIsRunning();

//    exe->setStopStrategy(ssStopAndKillByPid);

//    exe->restartApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");

//    exe->stopApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Stopped");
//}

//BOOST_AUTO_TEST_CASE(Application_StopAndKill_ByName)
//{
//    makeSureApplicationIsRunning();

//    exe->setStopStrategy(ssStopAndKillByName);

//    exe->restartApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Running");

//    exe->stopApplication(ai).wait();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Stopped");
//}

//BOOST_AUTO_TEST_CASE(Scenario_Status) {
//    BOOST_CHECK_EQUAL(exe->getApplicationStatus(ai).get(), "Stopped");
//    updateScenarioStati();
//    BOOST_CHECK_EQUAL(s->getStatus(), "Stopped");

//    exe->startScenario(s).get();
//    sleep(SLEEP_TIME);
//    updateScenarioStati();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(s->getStatus(), "Running");

//    exe->restartScenario(s).get();
//    sleep(SLEEP_TIME);
//    updateScenarioStati();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(s->getStatus(), "Running");

//    exe->stopScenario(s).get();
//    sleep(SLEEP_TIME);
//    updateScenarioStati();
//    sleep(SLEEP_TIME);
//    BOOST_CHECK_EQUAL(s->getStatus(), "Stopped");
//}

//BOOST_AUTO_TEST_CASE(Application_ColdStart)
//{
//    //Just test if no exceptions occur.
//    exe->coldStartApplication(ai, "");
//}
