/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::ScenarioManager::XMLScenarioParserTest
#define ARMARX_BOOST_TEST

#include <ArmarXGui/Test.h>

#include <iostream>

#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h>

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Application.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>

#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>

#include "ExecutorTestEnvironment.h"

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>

#include <algorithm>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace Parser;
using namespace Exec;
using namespace rapidxml;
using namespace std;
using namespace armarx;

PackageVectorPtr packages = PackageVectorPtr(new std::vector<PackagePtr>());//boost::shared_ptr<std::vector<PackagePtr>>
ScenarioPtr s;
PackagePtr coreP;
XMLScenarioParser xmlParser = XMLScenarioParser();
PackageBuilder parser = PackageBuilder();
ApplicationPtr app1;
ApplicationInstancePtr api1;

BOOST_AUTO_TEST_CASE(InitDataPath)
{
    //init dataPath
    std::string packageName("ArmarXGui");
    armarx::CMakePackageFinder finder(packageName);
    if (!finder.packageFound())
    {
        BOOST_ERROR("Cannot find ArmarXGui Package");
    }

    std::string packageDataDir = finder.getDataDir();
    ArmarXDataPath::addDataPaths(packageDataDir);
}


BOOST_AUTO_TEST_CASE(IceParserLoadFromXml_DebugObserverTest)
{
    //init app for test
    app1 = ApplicationPtr(new Application("DebugObserver", "not relevant", "ArmarXCore"));
    api1 = ApplicationInstancePtr(new ApplicationInstance((*app1), "", "not relevant", s, false));

    //find DebugObserver.xml file
    std::string relativeFilename("ArmarXGui/ScenarioManagerTestData/xmlCache/ArmarXCore.DebugObserver.xml");
    std::string absoluteFilename;
    if (!ArmarXDataPath::getAbsolutePath(relativeFilename, absoluteFilename))
    {
        BOOST_ERROR("Cannot find file ArmarXGui/ScenarioManagerTestData/xmlCache/ArmarXCore.DebugObserver.xml in ArmarXGui DataPath");
    }

    //parse the file
    IceParser parser;
    armarx::PropertyDefinitionsPtr definitions = parser.loadFromXml(absoluteFilename, api1);

    //check properties required parsing
    Ice::PropertyDict properties = definitions->getProperties()->getPropertiesForPrefix("");

    for (auto it : properties)
    {
        if (it.first.compare("Ice.Config") != 0)
        {
            BOOST_CHECK_EQUAL(api1->isDefaultProperty(it.first), true);
            BOOST_CHECK_EQUAL(api1->isDefaultPropertyEnabled(it.first), false);
        }
        if (it.second.compare("::_NOT_SET_::") == 0)
        {
            BOOST_ERROR("Got Required property in " + absoluteFilename + " which should not contain Required Properties");
        }
    }
}

BOOST_AUTO_TEST_CASE(IceParserLoadFromXml_HeadIKUnitTest)
{
    //init app for test
    app1 = ApplicationPtr(new Application("HeadIKUnit", "not relevant", "RobotApi"));
    api1 = ApplicationInstancePtr(new ApplicationInstance((*app1), "", "not relevant", s, false));

    //find DebugObserver.xml file
    std::string relativeFilename("ArmarXGui/ScenarioManagerTestData/xmlCache/RobotAPI.HeadIKUnit.xml");
    std::string absoluteFilename;
    if (!ArmarXDataPath::getAbsolutePath(relativeFilename, absoluteFilename))
    {
        BOOST_ERROR("Cannot find file ArmarXGui/ScenarioManagerTestData/xmlCache/RobotAPI.HeadIKUnit.xml in ArmarXGui DataPath");
    }

    //parse the file
    IceParser parser;
    armarx::PropertyDefinitionsPtr definitions = parser.loadFromXml(absoluteFilename, api1);

    //check properties required parsing
    Ice::PropertyDict properties = definitions->getProperties()->getPropertiesForPrefix("");
    int reqCount = 0;
    for (auto it : properties)
    {
        if (it.first.compare("Ice.Config") != 0)
        {
            BOOST_CHECK_EQUAL(api1->isDefaultProperty(it.first), true);
            BOOST_CHECK_EQUAL(api1->isDefaultPropertyEnabled(it.first), false);
        }
        if (it.second.compare("::_NOT_SET_::") == 0)
        {
            reqCount ++;
        }
    }
    if (reqCount != 1)
    {
        BOOST_ERROR("Got " + to_string(reqCount) + " Required properties in " + absoluteFilename + " which should only contain one Required Property");
    }
}


BOOST_AUTO_TEST_CASE(IceParserLoadFromXml_DepthImageProviderDynamicSimulationAppTest)
{
    //init app for test
    app1 = ApplicationPtr(new Application("DepthImageProviderDynamicSimulationApp", "not relevant", "ArmarXSimulation"));
    api1 = ApplicationInstancePtr(new ApplicationInstance((*app1), "", "not relevant", s, false));

    //find DebugObserver.xml file
    std::string relativeFilename("ArmarXGui/ScenarioManagerTestData/xmlCache/ArmarXSimulation.DepthImageProviderDynamicSimulationApp.xml");
    std::string absoluteFilename;
    if (!ArmarXDataPath::getAbsolutePath(relativeFilename, absoluteFilename))
    {
        BOOST_ERROR("Cannot find file ArmarXGui/ScenarioManagerTestData/xmlCache/ArmarXSimulation.DepthImageProviderDynamicSimulationApp.xml in ArmarXGui DataPath");
    }

    //parse the file
    IceParser parser;
    armarx::PropertyDefinitionsPtr definitions = parser.loadFromXml(absoluteFilename, api1);

    //check properties required parsing
    Ice::PropertyDict properties = definitions->getProperties()->getPropertiesForPrefix("");
    for (auto it : properties)
    {
        if (it.first.compare("Ice.Config") != 0)
        {
            BOOST_CHECK_EQUAL(api1->isDefaultProperty(it.first), true);
            BOOST_CHECK_EQUAL(api1->isDefaultPropertyEnabled(it.first), false);
        }
        if (it.second.compare("::_NOT_SET_::") == 0)
        {
            BOOST_ERROR("Got Required property in " + absoluteFilename + " which should not contain Required Properties");
        }
    }
}

//BOOST_AUTO_TEST_CASE(IceParserParseCfg_DebugObserverTest)
//{
//    //init app for test
//    app1 = ApplicationPtr(new Application("DebugObserver", "not relevant", "ArmarXCore"));
//    api1 = ApplicationInstancePtr(new ApplicationInstance((*app1), "", "not relevant", s));

//    //find DebugObserver.xml file
//    std::string relativeFilename("ArmarXGui/ScenarioManagerTestData/cfgs/DebugObserver.untouched.cfg");
//    std::string absoluteFilename;
//    api1->setConfigPath(absoluteFilename);
//    if (!ArmarXDataPath::getAbsolutePath(relativeFilename, absoluteFilename))
//    {
//        BOOST_ERROR("Cannot find file ArmarXGui/ScenarioManagerTestData/xmlCache/ArmarXCore.DebugObserver.xml in ArmarXGui DataPath");
//    }

//    //parse the file
//    IceParser parser;
//    armarx::PropertyDefinitionsPtr definitions = parser.mergeXmlAndCfg(api1.get());

//    //TODO
//    //    //check properties
//    //    Ice::PropertyDict properties = definitions->getProperties()->getPropertiesForPrefix("");

//    //    for (auto it : properties)
//    //    {
//    //        if (it.first.compare("Ice.Config") != 0)
//    //        {
//    //            BOOST_CHECK_EQUAL(api1->isDefaultProperty(it.first), true);
//    //            BOOST_CHECK_EQUAL(api1->isDefaultPropertyEnabled(it.first), false);
//    //        }
//    //    }
//}


//ExecutorTestEnvironmentPtr environment = ExecutorTestEnvironmentPtr(new ExecutorTestEnvironment());
//BOOST_AUTO_TEST_CASE(initialize_packages)
//{
//    coreP = parser.parsePackage("ArmarXCore");
//    PackagePtr guiP = parser.parsePackage("ArmarXGui");

//    packages->push_back(coreP);
//    packages->push_back(guiP);
//}

//BOOST_AUTO_TEST_CASE(create_new_scenario_in_package)
//{
//    s = xmlParser.createNewScenario("testScenario", coreP);
//    if (s == NULL)
//    {
//        BOOST_ERROR("SCENARIO IS NULL");
//    }

//    app1 = coreP->getApplicationByName("SystemObserverRun");
//    ApplicationPtr app2 = coreP->getApplicationByName("TimeoutExampleRun");
//    api1 = ApplicationInstancePtr(new ApplicationInstance((*app1), "", coreP->getScenarioPath() + "/testScenario/config", s));
//    ApplicationInstancePtr api2 = ApplicationInstancePtr(new ApplicationInstance((*app2), "", s->getPath() + "/testScenario/config", s));

//    s->addApplication(api1);
//    s->addApplication(api2);

//    xmlParser.saveScenario(s.get());
//}

//BOOST_AUTO_TEST_CASE(parse_scenario_file)
//{
//    s = xmlParser.parseScenario(s);

//    if (s == NULL)
//    {
//        BOOST_ERROR("NO SCENARIO CREATED");
//    }

//    BOOST_CHECK_EQUAL(s->getName(), "testScenario");
//}

//BOOST_AUTO_TEST_CASE(application_data)
//{
//    if (s == NULL)
//    {
//        BOOST_ERROR("SCENARIO IS NULL");
//    }

//    ApplicationInstanceVectorPtr apps = s->getApplications();

//    BOOST_CHECK_EQUAL((*apps).size(), 2);

//    BOOST_CHECK_EQUAL((*apps)[0]->getName(), "SystemObserverRun");
//    BOOST_CHECK_EQUAL((*apps)[1]->getName(), "TimeoutExampleRun");
//}

//BOOST_AUTO_TEST_CASE(get_package)
//{
//    PackagePtr p = xmlParser.getScenarioPackage(s, packages);
//    BOOST_CHECK_EQUAL(p, coreP);
//}

////This is hard to test, since the closed-scenario-paths value always changes, so we only test if no exceptions occur.
//BOOST_AUTO_TEST_CASE(closed_scenario_paths)
//{
//    QStringList scenariopaths = parser.getAllClosedScenarios();
//}
