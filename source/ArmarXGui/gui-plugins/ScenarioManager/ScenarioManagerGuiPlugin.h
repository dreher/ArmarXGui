/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ScenarioManager::gui-plugins::ScenarioManager
 * @author     [Cedric Seehausen] ( [usdnr@student.kit.edu] )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{

    /**
      \page ArmarXGui-GuiPlugins-ScenarioManagerGuiPlugin ScenarioManager


    A typical robot application setup in ArmarX consists of dozens of applications, which offers the possibility to
    distribute the applications over multiple hosts to comply with hardware requirements or to balance the work-
    load. Additionally, ArmarX applications are highly configurable and require therefore robot specific properties
    to run successfully.
    To this end, we developed a graphical ScenarioManager tool to manage these application setups, called scenarios.
    With this tool, you can create a scenario, configure all its applications conveniently, and
    trigger and monitor the execution of all involved components.

     \image html ScenarioManager.png

     \section ScenarioManagerGuiPlugin-Intro Demonstration of the ScenarioManager
     This video shows briefly how to use the ScenarioManager for creating, managing and starting scenarios.
      \htmlonly
    <video width="700" controls>
    <source src="images/ScenarioManager-2016-06-01_20.15.42.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
    </video>
      \endhtmlonly
    */

    /**
     * @class ScenarioManagerGuiPlugin
     * @ingroup ArmarXGuiPlugins
     * @brief ScenarioManagerGuiPlugin brief description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT ScenarioManagerGuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
#endif
    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        ScenarioManagerGuiPlugin();
    };
}

