/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ScenarioManager::gui-plugins::ScenarioManagerWidgetController
 * @author     [Cedric Seehausen] ( [usdnr@student.kit.edu] )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <string>

#include "ui_ScenarioManagerWidget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include "gui/applicationdatabaseview.h"
#include "gui/scenariolistview.h"
#include <ArmarXGui/libraries/qtpropertybrowser/src/qttreepropertybrowser.h>

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>

#include "controller/ApplicationDatabaseController.h"
#include "controller/DetailedApplicationController.h"
#include "controller/ScenarioListController.h"
#include "controller/SettingsController.h"
#include "controller/openscenariocontroller.h"
#include <boost/shared_ptr.hpp>

#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace armarx
{
    /**
     * @class ScenarioManagerWidgetController
     * @ingroup ArmarXGuiPlugins
     * @brief ScenarioManagerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ScenarioManagerWidgetController:
        public ArmarXComponentWidgetControllerTemplate<ScenarioManagerWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit ScenarioManagerWidgetController();

        /**
         * Controller destructor
         */
        ~ScenarioManagerWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Meta.ScenarioManager";
        }

        /**
         * @see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

    public slots:
        /* QT slot declarations */
        void reparsePackages();
        void reparsePackage(std::string name);
        void updateModels();
        void editMode(bool edit);

    signals:
        /* QT signal declarations */

    private slots:
        //used to force Qt main thread to init this plugin
        void init();
    private:
        /**
         * Widget Form
         */
        Ui::ScenarioManagerWidget widget;


        ScenarioManager::Exec::StopStrategyFactory stopStrategyFactory;
        ScenarioManager::Exec::StopperFactoryPtr stopperFactory;
        ScenarioManager::Exec::StarterFactoryPtr starterFactory;


        ScenarioManager::Data_Structure::PackageVectorPtr packages;
        ScenarioManager::Exec::ExecutorPtr executor;

        ScenarioManager::Controller::ApplicationDatabaseController applicationController;
        ScenarioManager::Controller::DetailedApplicationController detailedApplicationController;
        ScenarioManager::Controller::ScenarioListController scenarioListController;
        ScenarioManager::Controller::SettingsController settingsController;
        ScenarioManager::Controller::OpenScenarioController openScenarioController;
        QAction* editModeAction;
        QSettings settings;
        QPointer<QToolBar> customToolbar;
        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
        // ArmarXWidgetController interface
    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/ArmarX_Play_Store.svg");
        }
    };
}

