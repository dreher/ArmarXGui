/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ScenarioManager::gui-plugins::ScenarioManagerGuiPlugin
 * @author     [Author Name] ( [Author Email] )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ScenarioManagerGuiPlugin.h"

#include "ScenarioManagerWidgetController.h"

using namespace armarx;

ScenarioManagerGuiPlugin::ScenarioManagerGuiPlugin()
{
    addWidget < ScenarioManagerWidgetController > ();
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
Q_EXPORT_PLUGIN2(armarx_gui_ScenarioManagerGuiPlugin, ScenarioManagerGuiPlugin)
#endif
