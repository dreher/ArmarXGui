/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <QDialog>

namespace Ui
{
    class PackageAdderView;
}

/**
* @class PackageAdderView
* @brief View that allows the user to find and add new packages.
*/
class PackageAdderView : public QDialog
{
    Q_OBJECT

public:
    /**
    * Constructor that sets up the ui.
    * @param parent parent widget
    */
    explicit PackageAdderView(QWidget* parent = 0);

    /**
    * Destructor.
    */
    ~PackageAdderView() override;

signals:
    void created(std::string name);

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();

    void on_packageName_textChanged(const QString& text);

private:
    Ui::PackageAdderView* ui;
    bool found;
};

