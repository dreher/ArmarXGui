/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "scenarioitem.h"

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;
using namespace ScenarioManager;
using namespace Data_Structure;

ScenarioItem::ScenarioItem(QList<QVariant> rootData)
{
    m_itemData = rootData.toVector();
}

ScenarioItem::ScenarioItem(ScenarioPtr scenario)
{
    this->scenario = scenario;

    update();
}

ScenarioItem::ScenarioItem(ApplicationInstancePtr application)
{
    this->application = application;
    setEnabled(application->getEnabled());

    update();
}

ScenarioItem::ScenarioItem(std::string name, std::vector<ApplicationInstancePtr> applications)
    : applications(new std::vector<ApplicationInstancePtr>(applications))
{
    this->packageName = name;

    for (auto application : applications)
    {
        ScenarioItem* currentItem = new ScenarioItem(application);
        currentItem->setEnabled(application->getEnabled());

        this->appendChild(currentItem);
    }

    update();
}

void ScenarioItem::update()
{
    if (m_itemData.size() < 5)
    {
        m_itemData.resize(5);
        m_itemData[1] = "Start";
        m_itemData[2] = "Stop";
        m_itemData[3] = "Restart";
    }
    if (scenario.get() != nullptr)
    {
        m_itemData[0] = scenario->getName().c_str();
        m_itemData[4] = QString::fromStdString(scenario->getStatus());
    }
    else if (!packageName.empty())
    {
        m_itemData[0] = QString::fromStdString(packageName);

        std::string status = ApplicationStatus::Unknown;
        for (int i = 0; i < childCount(); i++)
        {
            static_cast<ScenarioItem*>(child(i))->update();
            if (!child(i)->isEnabled())
            {
                continue;
            }
            if (status == ApplicationStatus::Unknown && child(i)->data(4).toString().toStdString() == ApplicationStatus::Running)
            {
                status = ApplicationStatus::Running;
            }
            else if (status == ApplicationStatus::Unknown && child(i)->data(4).toString().toStdString() == ApplicationStatus::Stopped)
            {
                status = ApplicationStatus::Stopped;
            }
            else if (status == ApplicationStatus::Running && child(i)->data(4).toString().toStdString() == ApplicationStatus::Running)
            {
                status = ApplicationStatus::Running;
            }
            else if (status == ApplicationStatus::Stopped && child(i)->data(4).toString().toStdString() == ApplicationStatus::Stopped)
            {
                status = ApplicationStatus::Stopped;
            }
            else if (status == ApplicationStatus::Running && child(i)->data(4).toString().toStdString() == ApplicationStatus::Stopped)
            {
                status = ApplicationStatus::Mixed;
                break;
            }
            else if (status == ApplicationStatus::Stopped && child(i)->data(4).toString().toStdString() == ApplicationStatus::Running)
            {
                status = ApplicationStatus::Mixed;
                break;
            }
            else if (child(i)->data(4).toString().toStdString() == ApplicationStatus::Waiting)
            {
                status = ApplicationStatus::Waiting;
                break;
            }
            else
            {
                status = ApplicationStatus::Mixed;
                break;
            }
        }

        m_itemData[4] = QString::fromStdString(status);
    }
    else
    {
        if (application->getInstanceName().empty())
        {
            m_itemData [0] = application->getName().c_str();
        }
        else
        {
            m_itemData[0] = (application->getInstanceName() + "." + application->getName()).c_str();
        }

        std::string status = application->getStatus();
        if (!application->getFound())
        {
            m_itemData[4] = "Not Found";
        }
        else if (status == ApplicationStatus::Running)
        {
            m_itemData[4] = QVariant(QString::fromStdString(ApplicationStatus::Running));
        }
        else if (status == ApplicationStatus::Stopped)
        {
            m_itemData[4] = QVariant(QString::fromStdString(ApplicationStatus::Stopped));
        }
        else if (status == ApplicationStatus::Waiting)
        {
            m_itemData[4] = QVariant(QString::fromStdString(ApplicationStatus::Waiting));
        }
        else
        {
            m_itemData[4] = QVariant(QString::fromStdString(ApplicationStatus::Unknown));
        }

        setEnabled(application->getEnabled());
    }
}

std::string ScenarioItem::getPackageName()
{
    return packageName;
}

ScenarioPtr ScenarioItem::getScenario()
{
    return scenario;
}

ApplicationInstancePtr ScenarioItem::getApplicationInstance()
{
    return application;
}

ApplicationInstanceVectorPtr ScenarioItem::getApplicationInstances()
{
    return applications;
}

