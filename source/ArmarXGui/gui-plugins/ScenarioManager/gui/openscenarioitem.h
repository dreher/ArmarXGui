/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include "treeitem.h"
#include <string>

/**
* @class OpenScenarioItem
* @brief Item in the TreeItemView. Every item represents a Scenario.
*/
class OpenScenarioItem : public TreeItem
{
public:
    /**
    * Constructor that sets up the data based on the given data.
    * @param rootItem that replaces the current data
    */
    OpenScenarioItem(QList<QVariant> rootItem);

    /**
    * Sets up this item based on the given data.
    * @param scenarioPath contains the path of the scenario to be displayed
    */
    OpenScenarioItem(QVariant scenarioName, QVariant scenarioPackage);

    /**
    * Returns the Name of the scenario package represented by this item.
    * @return path of the scenario
    */
    std::string getScenarioPackage();


    /**
    * Returns the Name of the scenario represented by this item.
    * @return path of the scenario
    */
    std::string getScenarioName();

private:
    QVariant scenarioName;
    QVariant scenarioPackage;
};

Q_DECLARE_METATYPE(OpenScenarioItem*)
