/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "detailedapplicationview.h"
#include "ui_detailedapplicationview.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionHelpFormatter.h>

#define UPDATE_TIMER_INTERVAL 500

using namespace ScenarioManager;
using namespace Data_Structure;

DetailedApplicationView::DetailedApplicationView(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::DetailedApplicationView),
    variantManager(nullptr),
    variantFactory(nullptr),
    statusUpdateRelevant(false),
    lastScenarioItem(nullptr),
    neadsUpdate(false)
{
    updateTimer.setSingleShot(true);
    QObject::connect(&updateTimer, SIGNAL(timeout()), this, SLOT(on_fileUpdate()));
}

DetailedApplicationView::~DetailedApplicationView()
{
    delete ui;
}

void DetailedApplicationView::init()
{
    ui->setupUi(this);
    variantManager = boost::shared_ptr<OptionalVariantManager>(new OptionalVariantManager());
    variantFactory = boost::shared_ptr<OptionalVariantFactory>(new OptionalVariantFactory());

    ui->propertyBrowser->setFactoryForManager(variantManager.get(), variantFactory.get());

    ui->propertyBrowser->setAlternatingRowColors(true);
    ui->propertyBrowser->setResizeMode(QtTreePropertyBrowser::ResizeMode::Interactive);
    ui->propertyBrowser->setSplitterPosition(200);

    QObject::connect(variantManager.get(), SIGNAL(valueChanged(QtProperty*, const QVariant&)),
                     this, SLOT(itemChanged(QtProperty*, const QVariant&)));

    QObject::connect(variantManager.get(), SIGNAL(attributeChanged(QtProperty*, const QString&, const QVariant&)),
                     this, SLOT(itemAttributeChanged(QtProperty*, const QString&, const QVariant&)));


    void attributeChanged(QtProperty * property,
                          const QString & attribute, const QVariant & val);
    //QObject::connect(variantManager.get(), SIGNAL())
}

void DetailedApplicationView::showApplication(ApplicationPtr application)
{
    if (application.get() == nullptr)
    {
        return;
    }
    if (variantManager.get() == nullptr || variantFactory.get() == nullptr)
    {
        init();
    }

    ui->dataLabel->setText(QString::fromStdString(application->getName()));

    lastAppInstance = ApplicationInstancePtr();
    lastScenario = ScenarioPtr();
    lastScenarioItem = nullptr;
    statusUpdateRelevant = false;

    ui->stateLabel->setStyleSheet("QLabel { color : black; }");
    ui->stateLabel->setText("No Status");

    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
    ui->restartButton->setEnabled(false);
    ui->addParameterButton->setEnabled(false);
    ui->toolButton->setEnabled(false);

    //build propertybrowser items
    variantManager->clear();
    static_cast<OptionalVariantFactory*>(variantFactory.get())->setElementName(QString::fromStdString(application->getName()));
    ui->propertyBrowser->clear();

    QtProperty* appTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                             QLatin1String("Application"));

    appTopItem->setEnabled(false);

    QtVariantProperty* nameItem = variantManager->addProperty(QVariant::String, QString("Name"));
    nameItem->setValue(QString::fromStdString(application->getName()));
    appTopItem->addSubProperty(nameItem);

    QtVariantProperty* execPathItem = variantManager->addProperty(QVariant::String, QString("Executable Path"));
    execPathItem->setValue(QString::fromStdString(application->getPathToExecutable()));
    appTopItem->addSubProperty(execPathItem);

    QtProperty* appPropertyTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                     QLatin1String("Default Properties"));
    appTopItem->addSubProperty(appPropertyTopItem);

    armarx::PropertyDefinitionsPtr props = application->getProperties();

    Ice::PropertyDict dict = props->getProperties()->getPropertiesForPrefix("");

    for (auto const& property : dict)
    {
        QtVariantProperty* appPropertyItem = variantManager->addProperty(QVariant::String, QString::fromStdString(property.first));
        appPropertyItem->setValue(QString::fromStdString(property.second));
        appPropertyItem->setStatusTip(QString::fromStdString(props->getDescription()));

        if (property.second.compare("<set value!>") == 0 || property.second.compare("::_NOT_SET_::") == 0)
        {
            //There is currently no way to change colors of any sort in qtproperty browser but if you later want to add this do it here
        }
        appPropertyTopItem->addSubProperty(appPropertyItem);
    }

    ui->propertyBrowser->addProperty(appTopItem);
    ui->propertyBrowser->setRootIsDecorated(false);
}

void DetailedApplicationView::showApplicationInstance(ApplicationInstancePtr appInstance, ScenarioItem* item)
{
    if (appInstance.get() == nullptr)
    {
        showPackage(PackagePtr());
        return;
    }

    if (variantManager.get() == nullptr || variantFactory.get() == nullptr)
    {
        init();
    }

    if (appInstance->getStatus().compare("Running") == 0)
    {
        ui->stateLabel->setStyleSheet("QLabel { color : green; }");
        //stateLabelPallet.setColor(QPalette::WindowText, Qt::blue);
    }
    else if (appInstance->getStatus().compare("Stopped") == 0)
    {
        ui->stateLabel->setStyleSheet("QLabel { color : red; }");
    }
    else
    {
        ui->stateLabel->setStyleSheet("QLabel { color : black; }");
    }

    ui->dataLabel->setText(QString::fromStdString(appInstance->getName()));
    ui->stateLabel->setText(QString::fromStdString(appInstance->getStatus()));
    statusUpdateRelevant = true;
    lastAppInstance = appInstance;
    lastScenario = ScenarioPtr(nullptr);
    if (item != nullptr)
    {
        lastScenarioItem = item;
    }

    if (lastAppInstance->getEnabled())
    {
        ui->startButton->setEnabled(true);
        ui->stopButton->setEnabled(true);
        ui->restartButton->setEnabled(true);
    }
    else
    {
        ui->startButton->setEnabled(false);
        ui->stopButton->setEnabled(false);
        ui->restartButton->setEnabled(false);
    }

    ui->addParameterButton->setEnabled(true);
    ui->toolButton->setEnabled(true);
    //build propertybrowser items
    variantManager->clear();
    static_cast<OptionalVariantFactory*>(variantFactory.get())->setElementName(QString::fromStdString(appInstance->getName()));
    ui->propertyBrowser->clear();

    QtProperty* appInstanceTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                     QLatin1String("ApplicationInstance"));

    if (!appInstance->isConfigWritable())
    {
        appInstanceTopItem->setEnabled(false);
    }

    QtVariantProperty* nameItem = variantManager->addProperty(QVariant::String, QString("Name"));

    nameItem->setValue(QString::fromStdString(appInstance->getName()));
    appInstanceTopItem->addSubProperty(nameItem);

    QtVariantProperty* execPathItem = variantManager->addProperty(QVariant::String, QString("Executable Path"));
    execPathItem->setValue(QString::fromStdString(appInstance->getPathToExecutable()));
    appInstanceTopItem->addSubProperty(execPathItem);

    QtVariantProperty* instanceNameItem = variantManager->addProperty(QVariant::String, QString("Instance Name"));
    instanceNameItem->setValue(QString::fromStdString(appInstance->getInstanceName()));
    appInstanceTopItem->addSubProperty(instanceNameItem);

    QtVariantProperty* configPathItem = variantManager->addProperty(QVariant::String, QString("Config Path"));
    configPathItem->setValue(QString::fromStdString(appInstance->getConfigPath()));
    appInstanceTopItem->addSubProperty(configPathItem);

    QtVariantProperty* pidItem = variantManager->addProperty(QVariant::Int, QString("Pid (Stopped = -1)"));
    pidItem->setValue(appInstance->getPid());
    pidItem->setEnabled(false);
    appInstanceTopItem->addSubProperty(pidItem);

    QtVariantProperty* enabledItem = variantManager->addProperty(QVariant::Bool, QString("Enabled"));
    enabledItem->setValue(appInstance->getEnabled());
    appInstanceTopItem->addSubProperty(enabledItem);

    QtProperty* appPropertyTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                     QLatin1String("Properties"));
    appInstanceTopItem->addSubProperty(appPropertyTopItem);

    armarx::PropertyDefinitionsPtr props = appInstance->getProperties();

    Ice::PropertyDict dict = props->getProperties()->getPropertiesForPrefix("");

    for (auto const& property : dict)
    {
        if (property.first != "Ice.Config")
        {
            QtVariantProperty* appPropertyItem;
            if (appInstance->isDefaultProperty(property.first))
            {
                appPropertyItem = variantManager->addProperty(OptionalVariantManager::optionalProprtyTypeId(), QString::fromStdString(property.first));

                appPropertyItem->setAttribute(QLatin1String("enabled"), QVariant(appInstance->isDefaultPropertyEnabled(property.first)));
            }
            else
            {
                appPropertyItem = variantManager->addProperty(QVariant::String, QString::fromStdString(property.first));
            }
            appPropertyItem->setValue(QString::fromStdString(property.second));

            try
            {
                armarx::PropertyDefinitionHelpFormatter formatter;
                QString result = QString::fromStdString("Current value: " + property.second + "\n\n" + props->getDefintion<std::string>(property.first).toString(formatter, props->getValue(property.first)));
                appPropertyItem->setToolTip(result);
            }
            catch (armarx::LocalException&)
            {
                //nothing to do
            }
            appPropertyTopItem->addSubProperty(appPropertyItem);
        }
    }

    ui->propertyBrowser->addProperty(appInstanceTopItem);
    ui->propertyBrowser->setRootIsDecorated(false);
}

void DetailedApplicationView::showScenario(ScenarioPtr scenario)
{
    if (scenario.get() == nullptr)
    {
        showPackage(PackagePtr());
        return;
    }

    if (variantManager.get() == nullptr || variantFactory.get() == nullptr)
    {
        init();
    }


    ui->dataLabel->setText(QString::fromStdString(scenario->getName()));
    statusUpdateRelevant = true;
    lastScenario = scenario;
    lastAppInstance = ApplicationInstancePtr(nullptr);
    lastScenarioItem = nullptr;


    if (scenario->getStatus().compare("Running") == 0)
    {
        ui->stateLabel->setStyleSheet("QLabel { color : green; }");
        //stateLabelPallet.setColor(QPalette::WindowText, Qt::blue);
    }
    else if (scenario->getStatus().compare("Stopped") == 0)
    {
        ui->stateLabel->setStyleSheet("QLabel { color : red; }");
    }
    else if (scenario->getStatus().compare("Mixed") == 0)
    {
        ui->stateLabel->setStyleSheet("QLabel { color : darkYellow; }");
    }
    else
    {
        ui->stateLabel->setStyleSheet("QLabel { color : black; }");
    }


    ui->stateLabel->setText(QString::fromStdString(scenario->getStatus()));

    ui->startButton->setEnabled(true);
    ui->restartButton->setEnabled(true);
    ui->stopButton->setEnabled(true);
    ui->addParameterButton->setEnabled(true);
    ui->toolButton->setEnabled(true);

    //build propertybrowser items
    variantManager->clear();
    static_cast<OptionalVariantFactory*>(variantFactory.get())->setElementName(QString::fromStdString(scenario->getName()));
    ui->propertyBrowser->clear();

    QtProperty* scenarioTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                  QLatin1String("Scenario"));
    if (!scenario->isScenarioFileWriteable())
    {
        scenarioTopItem->setEnabled(false);
    }

    QtVariantProperty* nameItem = variantManager->addProperty(QVariant::String, QString("Name"));
    nameItem->setValue(QString::fromStdString(scenario->getName()));
    scenarioTopItem->addSubProperty(nameItem);

    QtVariantProperty* scenarioPathItem = variantManager->addProperty(QVariant::String, QString("Scenario Path"));
    scenarioPathItem->setValue(QString::fromStdString(scenario->getPath()));
    scenarioTopItem->addSubProperty(scenarioPathItem);

    QtVariantProperty* createdAtItem = variantManager->addProperty(QVariant::String, QString("Created At"));
    createdAtItem->setValue(QString::fromStdString(scenario->getCreationTime()));
    scenarioTopItem->addSubProperty(createdAtItem);

    QtVariantProperty* lastChangeItem = variantManager->addProperty(QVariant::String, QString("Last Change At"));
    lastChangeItem->setValue(QString::fromStdString(scenario->getLastChangedTime()));
    scenarioTopItem->addSubProperty(lastChangeItem);

    QtProperty* scenarioPropertyTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                          QLatin1String("Scenario Properties"));
    scenarioTopItem->addSubProperty(scenarioPropertyTopItem);
    if (!scenario->isGlobalConfigWritable())
    {
        scenarioPropertyTopItem->setEnabled(false);
    }

    armarx::PropertyDefinitionsPtr props = scenario->getGlobalConfig();

    Ice::PropertyDict dict = props->getProperties()->getPropertiesForPrefix("");

    for (auto const& property : dict)
    {
        if (property.first != "Ice.Config")
        {
            QtVariantProperty* scenarioPropertyItem = variantManager->addProperty(QVariant::String, QString::fromStdString(property.first));
            scenarioPropertyItem->setValue(QString::fromStdString(property.second));
            try
            {
                scenarioPropertyItem->setToolTip(QString::fromStdString(props->getDefintion<std::string>(property.first).getDescription()));
            }
            catch (armarx::LocalException&)
            {
                //nothing to do
            }
            scenarioPropertyTopItem->addSubProperty(scenarioPropertyItem);
        }
    }

    ui->propertyBrowser->addProperty(scenarioTopItem);
    ui->propertyBrowser->setRootIsDecorated(false);
}

void DetailedApplicationView::showPackage(PackagePtr package)
{
    if (variantManager.get() == nullptr || variantFactory.get() == nullptr)
    {
        init();
    }
    if (package == nullptr)
    {
        //build propertybrowser items
        variantManager->clear();
        ui->propertyBrowser->clear();

        ui->startButton->setEnabled(false);
        ui->stopButton->setEnabled(false);
        ui->restartButton->setEnabled(false);
        ui->addParameterButton->setEnabled(false);
        ui->toolButton->setEnabled(false);

        lastAppInstance = ApplicationInstancePtr(nullptr);
        lastScenario = ScenarioPtr(nullptr);
        statusUpdateRelevant = false;

        return;
    }


    ui->dataLabel->setText(QString::fromStdString(package->getName()));

    lastAppInstance = ApplicationInstancePtr();
    lastScenario = ScenarioPtr();
    lastScenarioItem = nullptr;

    statusUpdateRelevant = false;

    ui->stateLabel->setStyleSheet("QLabel { color : black; }");
    ui->stateLabel->setText("No Status");

    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
    ui->restartButton->setEnabled(false);
    ui->addParameterButton->setEnabled(false);
    ui->toolButton->setEnabled(false);

    //build propertybrowser items
    variantManager->clear();
    static_cast<OptionalVariantFactory*>(variantFactory.get())->setElementName(QString::fromStdString(package->getName()));
    ui->propertyBrowser->clear();

    QtProperty* packageTopItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
                                 QLatin1String("Package"));

    packageTopItem->setEnabled(false);

    QtVariantProperty* nameItem = variantManager->addProperty(QVariant::String, QString("Name"));
    nameItem->setValue(QString::fromStdString(package->getName()));
    packageTopItem->addSubProperty(nameItem);

    QtVariantProperty* packagePathItem = variantManager->addProperty(QVariant::String, QString("Package Path"));
    packagePathItem->setValue(QString::fromStdString(package->getPath()));
    packageTopItem->addSubProperty(packagePathItem);

    ui->propertyBrowser->addProperty(packageTopItem);
    ui->propertyBrowser->setRootIsDecorated(false);
}

void DetailedApplicationView::on_startButton_clicked()
{
    emit startButtonClicked();
}

void DetailedApplicationView::on_stopButton_clicked()
{
    emit stopButtonClicked();
}

void DetailedApplicationView::on_restartButton_clicked()
{
    emit restartButtonClicked();
}

void DetailedApplicationView::on_addParameterButton_clicked()
{
    emit addParameterButtonClicked();
}

void DetailedApplicationView::on_toolButton_clicked()
{
    emit toolButtonClicked();
}



void DetailedApplicationView::updateStatus()
{
    if (statusUpdateRelevant)
    {
        QPalette stateLabelPallet;
        if (lastAppInstance.get() != nullptr)
        {
            ui->stateLabel->setText(QString::fromStdString(lastAppInstance->getStatus()));

            if (lastAppInstance->getStatus().compare("Running") == 0)
            {
                ui->stateLabel->setStyleSheet("QLabel { color : green; }");
                //stateLabelPallet.setColor(QPalette::WindowText, Qt::blue);
            }
            else if (lastAppInstance->getStatus().compare("Stopped") == 0)
            {
                ui->stateLabel->setStyleSheet("QLabel { color : red; }");
            }
            else
            {
                ui->stateLabel->setStyleSheet("QLabel { color : black; }");
            }
        }
        else if (lastScenario.get() != nullptr)
        {
            ui->stateLabel->setText(QString::fromStdString(lastScenario->getStatus()));

            if (lastScenario->getStatus().compare("Running") == 0)
            {
                ui->stateLabel->setStyleSheet("QLabel { color : green; }");
                //stateLabelPallet.setColor(QPalette::WindowText, Qt::blue);
            }
            else if (lastScenario->getStatus().compare("Stopped") == 0)
            {
                ui->stateLabel->setStyleSheet("QLabel { color : red; }");
            }
            else if (lastScenario->getStatus().compare("Mixed") == 0)
            {
                ui->stateLabel->setStyleSheet("QLabel { color : darkYellow; }");
            }
            else
            {
                ui->stateLabel->setStyleSheet("QLabel { color : black; }");
            }
        }
        ui->stateLabel->setAutoFillBackground(true);
        ui->stateLabel->setPalette(stateLabelPallet);
    }
}

void DetailedApplicationView::itemChanged(QtProperty* property, const QVariant& value)
{
    if (statusUpdateRelevant)
    {
        if (lastAppInstance.get() != nullptr)
        {
            armarx::PropertyDefinitionsPtr props = lastAppInstance->getProperties();

            Ice::PropertiesPtr properties = props->getProperties()->clone();

            if (property->propertyName().compare("Name") == 0 ||
                property->propertyName().compare("Executable Path") == 0 ||
                property->propertyName().compare("Config Path") == 0 ||
                property->propertyName().compare("Pid (Stopped = -1)") == 0 ||
                property->propertyName().compare("Scenario Path") == 0 ||
                property->propertyName().compare("Created At") == 0 ||
                property->propertyName().compare("Package Path") == 0 ||
                property->propertyName().compare("Last Change At") == 0 ||
                property->propertyName().compare("Default Properties") == 0)
            {
                return;
            }

            if (property->propertyName().compare("Instance Name") == 0 && lastAppInstance->getInstanceName().compare(value.toString().toStdString()))
            {
                lastAppInstance->setInstanceName(value.toString().toStdString());

                updateTimer.start(UPDATE_TIMER_INTERVAL);
            }
            else if (property->propertyName().compare("Instance Name") && property->propertyName() == "Enabled")
            {
                if (property->valueText() == "True")
                {
                    lastAppInstance->setEnabled(true);
                    ui->startButton->setEnabled(true);
                    ui->stopButton->setEnabled(true);
                    ui->restartButton->setEnabled(true);
                    if (lastScenarioItem != nullptr)
                    {
                        lastScenarioItem->setEnabled(true);
                    }
                }
                else
                {
                    lastAppInstance->setEnabled(false);
                    ui->startButton->setEnabled(false);
                    ui->stopButton->setEnabled(false);
                    ui->restartButton->setEnabled(false);
                    if (lastScenarioItem != nullptr)
                    {
                        lastScenarioItem->setEnabled(false);
                    }
                }
                updateTimer.start(UPDATE_TIMER_INTERVAL);
            }
            else if (property->propertyName().compare("Instance Name") && properties->getProperty(property->propertyName().toStdString()).compare(value.toString().toStdString()))
            {
                lastAppInstance->modifyProperty(property->propertyName().toStdString(), property->valueText().toStdString());
                if (value.toString().compare("<set value!>")
                    && value.toString().compare("::NOT_DEFINED::")
                    && value.toString().compare("::_NOT_SET_::"))
                {
                    lastAppInstance->setDefaultPropertyEnabled(property->propertyName().toStdString(), true);
                    variantManager->setAttribute(property, QLatin1String("enabled"), true);
                }
                updateTimer.start(UPDATE_TIMER_INTERVAL);
            }
            //showApplicationInstance(lastAppInstance);
        }
        else if (lastScenario.get() != nullptr)
        {
            armarx::PropertyDefinitionsPtr props = lastScenario->getGlobalConfig();

            Ice::PropertiesPtr properties = props->getProperties()->clone();

            if (property->propertyName().compare("Name") == 0 ||
                property->propertyName().compare("Scenario Path") == 0 ||
                property->propertyName().compare("Created At") == 0 ||
                property->propertyName().compare("Package Path") == 0 ||
                property->propertyName().compare("Last Change At") == 0 ||
                property->propertyName().compare("Default Properties") == 0)
            {
                return;
            }

            if (properties->getProperty(property->propertyName().toStdString()).compare(value.toString().toStdString()))
            {
                lastScenario->getGlobalConfig()->defineOptionalProperty<std::string>(property->propertyName().toStdString(), "::NOT_DEFINED::", "Custom Property");
                lastScenario->getGlobalConfig()->getProperties()->setProperty(property->propertyName().toStdString(), property->valueText().toStdString());

                updateTimer.start(UPDATE_TIMER_INTERVAL);
            }
        }
    }
}

void DetailedApplicationView::itemAttributeChanged(QtProperty* property, const QString& attribute, const QVariant& val)
{
    if (statusUpdateRelevant)
    {
        if (lastAppInstance.get() != nullptr && attribute.compare(QLatin1String("enabled")) == 0)
        {
            lastAppInstance->setDefaultPropertyEnabled(property->propertyName().toStdString(), val.toBool());

            if (!val.toBool())
            {
                OptionalVariantManager* internalManager = static_cast<OptionalVariantManager*>(property->propertyManager());
                armarx::PropertyDefinitionsPtr properties = lastAppInstance->getProperties();
                armarx::PropertyDefinition<std::string> definition = properties->getDefintion<std::string>(property->propertyName().toStdString());

                if (definition.isRequired())
                {
                    properties->getProperties()->setProperty(property->propertyName().toStdString(), "::_NOT_SET_::");
                    internalManager->setValue(property, "::_NOT_SET_::");
                }
                else
                {
                    properties->getProperties()->setProperty(property->propertyName().toStdString(), definition.getDefaultValue());
                    internalManager->setValue(property, QString::fromStdString(definition.getDefaultValue()));
                }
            }

            updateTimer.start(UPDATE_TIMER_INTERVAL);
        }
    }
}

void DetailedApplicationView::on_fileUpdate()
{
    if (lastAppInstance.get() != nullptr)
    {

        //DANGER delets old config
        if (lastAppInstance->isConfigWritable())
        {
            lastAppInstance->resetConfigPath();
            lastAppInstance->save();
        }

        emit saveScenario(lastAppInstance);
    }
    else if (lastScenario.get() != nullptr)
    {
        if (lastScenario->isGlobalConfigWritable())
        {
            lastScenario->save();
        }
    }
}

void DetailedApplicationView::on_reloadButton_clicked()
{
    if (statusUpdateRelevant)
    {
        if (lastAppInstance.get() != nullptr)
        {
            lastAppInstance->load();
            showApplicationInstance(lastAppInstance, nullptr);
        }
        else if (lastScenario.get() != nullptr)
        {
            lastScenario->reloadGlobalConf();
            showScenario(lastScenario);
        }
    }
}
