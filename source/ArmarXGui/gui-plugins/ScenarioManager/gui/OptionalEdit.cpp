/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OptionalEdit.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixCompleter.h>

#include <QFocusEvent>
#include <QAbstractItemView>
#include <QSettings>
#include <QApplication>

using namespace armarx;

OptionalEdit::OptionalEdit(const QString& elementName, const QString& propertyName, QWidget* parent) :
    QWidget(parent),
    elementName(elementName),
    propertyName(propertyName)
{
    ARMARX_INFO_S << elementName.toStdString() << " " << propertyName.toStdString();
    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);

    QSettings settings("KIT", "ScenarioManager");
    valueList = settings.value(elementName + "/" + propertyName).toStringList();

    edit = new QLineEdit(nullptr);
    edit->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred));

    InfixCompleter* completer = new InfixCompleter(valueList);
    completer->popup()->setFocusPolicy(Qt::NoFocus);
    completer->setCompletionList(valueList);
    edit->setCompleter(completer);

    checkbox = new QCheckBox(nullptr);
    checkbox->setToolTip("If checked, this property will be written into the config file");
    checkbox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred));

    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_InputMethodEnabled);

    connect(edit, SIGNAL(textChanged(QString)), this, SIGNAL(valueChanged(QString)));
    connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(slotEnabledChanged(int)));


    layout->addWidget(checkbox);
    layout->addWidget(edit);

    this->setLayout(layout);
}



void OptionalEdit::setPropertyEnabled(const bool& enabled)
{
    checkbox->setChecked(enabled);
    emit enabledChanged(checkbox->isChecked());
}


void OptionalEdit::setValue(const QString& value)
{
    int cursorPosition = edit->cursorPosition();
    edit->setText(value);
    static_cast<InfixCompleter*>(edit->completer())->setCompletionInfix(edit->text());
    edit->setCursorPosition(cursorPosition);

    emit valueChanged(edit->text());
}


void OptionalEdit::slotEnabledChanged(int value)
{
    emit enabledChanged(value != 0);
}

void OptionalEdit::focusInEvent(QFocusEvent* e)
{
    edit->event(e);

    static_cast<InfixCompleter*>(edit->completer())->setCompletionInfix(edit->text());
    edit->completer()->complete(edit->rect());

    if (e->reason() == Qt::MouseFocusReason
        || e->reason() == Qt::ActiveWindowFocusReason
        || e->reason() == Qt::OtherFocusReason)
    {
        edit->selectAll();
    }

    QWidget::focusInEvent(e);
}

void OptionalEdit::focusOutEvent(QFocusEvent* e)
{
    edit->event(e);

    if (e->reason() != Qt::FocusReason::PopupFocusReason)
    {
        valueList.append(edit->text());
        valueList.removeDuplicates();

        static_cast<InfixCompleter*>(edit->completer())->setCompletionList(valueList);

        QSettings settings("KIT", "ScenarioManager");
        settings.setValue(elementName + "/" + propertyName, QVariant(valueList));
    }

    QWidget::focusOutEvent(e);
}

void OptionalEdit::keyPressEvent(QKeyEvent* e)
{
    edit->event(e);

    //    if (e->key() != Qt::Key_Enter)
    //    {
    //        edit->completer()->complete(edit->rect());
    //    }
}

void OptionalEdit::keyReleaseEvent(QKeyEvent* e)
{
    edit->event(e);

    if (e->key() != Qt::Key_Enter)
    {
        edit->completer()->complete(edit->rect());
    }
}

