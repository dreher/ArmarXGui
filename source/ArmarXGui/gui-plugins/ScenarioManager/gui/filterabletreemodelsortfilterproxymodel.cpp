/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "filterabletreemodelsortfilterproxymodel.h"

#include "treeitem.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <QDate>
#include <QString>
#include <QModelIndexList>

FilterableTreeModelSortFilterProxyModel::FilterableTreeModelSortFilterProxyModel() : QSortFilterProxyModel(0)
{
    qRegisterMetaType<ScenarioItem*>();
    qRegisterMetaType<ApplicationDatabaseItem*>();
    qRegisterMetaType<OpenScenarioItem*>();
    qRegisterMetaType<SettingsItem*>();
}

QVariant FilterableTreeModelSortFilterProxyModel::data(const QModelIndex& proxy_index, int role) const
{
    switch (role)
    {
        case SCENARIOITEMSOURCE:
        case APPLICATIONITEMSOURCE:
        case OPENSCENARIOITEMSOURCE:
        case SETTINGSITEMSOURCE:
        {
            QModelIndex source_index = mapToSource(proxy_index);
            return sourceModel()->data(source_index, role);
        }
        case SCENARIOITEMPROXY:
            return QVariant::fromValue(reinterpret_cast<ScenarioItem*>(proxy_index.internalPointer()));
        case APPLICATIONITEMPROXY:
            return QVariant::fromValue(reinterpret_cast<ApplicationDatabaseItem*>(proxy_index.internalPointer()));
        case OPENSCENARIOITEMPROXY:
            return QVariant::fromValue(reinterpret_cast<OpenScenarioItem*>(proxy_index.internalPointer()));
        case SETTINGSITEMPROXY:
            return QVariant::fromValue(reinterpret_cast<SettingsItem*>(proxy_index.internalPointer()));

        default:
            return QSortFilterProxyModel::data(proxy_index, role);
    }
}

bool FilterableTreeModelSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
    QModelIndexList childrenColumn1;

    childrenColumn1 << sourceModel()->index(sourceRow, 0, sourceParent);

    //descend through the generations.
    for (int i = 0; i < childrenColumn1.size(); ++i)
    {
        for (int j = 0; j < sourceModel()->rowCount(childrenColumn1[i]); ++j)
        {
            childrenColumn1 << childrenColumn1[i].child(j, 0);
        }
    }

    bool result = false;
    for (int i = 0; i < childrenColumn1.size(); ++i)
    {
        if (sourceModel()->data(childrenColumn1[i]).toString().contains(filterRegExp()))
        {
            result = true;
            break;
        }
    }

    if (sourceModel()->columnCount() > 1)
    {
        QModelIndexList childrenColumn2;

        childrenColumn2 << sourceModel()->index(sourceRow, 1, sourceParent);

        //descend through the generations.
        for (int i = 0; i < childrenColumn2.size(); ++i)
        {
            for (int j = 0; j < sourceModel()->rowCount(childrenColumn2[i]); ++j)
            {
                childrenColumn2 << childrenColumn2[i].child(j, 1);
            }
        }

        for (int i = 0; i < childrenColumn2.size(); ++i)
        {
            if (sourceModel()->data(childrenColumn2[i]).toString().contains(filterRegExp()))
            {
                result = true;
                break;
            }
        }
    }

    return result;
}
