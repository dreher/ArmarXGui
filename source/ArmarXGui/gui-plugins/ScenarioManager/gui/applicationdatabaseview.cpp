/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "applicationdatabaseview.h"
#include "ui_applicationdatabaseview.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <qthread.h>

using namespace armarx;

ApplicationDatabaseView::ApplicationDatabaseView(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::ApplicationDatabaseView)
{
    ui->setupUi(this);

    ui->treeView->setModel(model.get());
    ui->treeView->setSortingEnabled(true);

    ui->treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->treeView->setDragEnabled(true);
    ui->treeView->setAcceptDrops(false);
    ui->treeView->setDropIndicatorShown(false);

    connect(this, SIGNAL(testSignal()), ui->treeView, SLOT(repaint()));
}

ApplicationDatabaseView::~ApplicationDatabaseView()
{
    delete ui;
}

void ApplicationDatabaseView::setModel(FilterableTreeModelSortFilterProxyModelPtr model)
{
    this->model = model;

    ui->treeView->setModel(model.get());
    ui->treeView->setColumnWidth(0, 200);
    ui->treeView->expandAll();
}



void ApplicationDatabaseView::on_lineEdit_textEdited(const QString& text)
{
    model->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
    ui->treeView->expandAll();
}

void ApplicationDatabaseView::on_treeView_clicked(const QModelIndex& index)
{
    emit itemClicked(index);
}

Mutex& ApplicationDatabaseView::getMutex()
{
    return mutex;
}

