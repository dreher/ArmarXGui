/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "scenariomodel.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <QMimeData>
#include <QStringList>
#include <QtGlobal>
#include <QColor>
#include <QIODevice>
#include <QDataStream>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace armarx;


ScenarioModel::ScenarioModel()
{
    rootItem = new ScenarioItem(QList<QVariant>({"Scenarios", "Start", "Stop", "Restart", "Status"}));
}

QVariant ScenarioModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (role == Qt::ForegroundRole)
    {
        ApplicationInstancePtr instance = static_cast<ScenarioItem*>(index.internalPointer())->getApplicationInstance();
        if (instance.get() != nullptr && !instance->getEnabled())
        {
            return QColor(Qt::lightGray);
        }
        if (index.column() == 4)
        {
            if (index.data().toString().toStdString() == "Not Found")
            {
                return QColor(Qt::lightGray);
            }
            else if (index.data().toString().toStdString() == ApplicationStatus::Running)
            {
                return QColor(Qt::green);
            }
            else if (index.data().toString().toStdString() == ApplicationStatus::Stopped)
            {
                return QColor(Qt::red);
            }
            else if (index.data().toString().toStdString() == ApplicationStatus::Mixed)
            {
                return QColor(Qt::darkYellow);
            }
            else if (index.data().toString().toStdString() == ApplicationStatus::Waiting)
            {
                return QColor(Qt::blue);
            }
            else
            {
                return QColor(Qt::black);
            }
        }
    }
    else if (role == Qt::ToolTipRole)
    {
        const QAbstractItemModel* model = index.model();
        QString appName = model->data(model->index(index.row(), 0, index.parent()), Qt::DisplayRole).toString();

        if (index.column() == 1)
        {
            return QVariant(QString("Start ") + appName);
        }
        else if (index.column() == 2)
        {
            return QVariant(QString("Stop ") + appName);
        }
        else if (index.column() == 3)
        {
            return QVariant(QString("Restart ") + appName);
        }
    }
    else if (role == SCENARIOITEMSOURCE)
    {
        return QVariant::fromValue(reinterpret_cast<ScenarioItem*>(index.internalPointer()));
    }
    else if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    ScenarioItem* item = static_cast<ScenarioItem*>(index.internalPointer());
    return item->data(index.column());
}



void ScenarioModel::clear()
{
    delete rootItem;
    rootItem = new ScenarioItem(QList<QVariant>({"Scenarios", "Start", "Stop", "Restart", "Status"}));
    reset();
}



Qt::DropActions ScenarioModel::supportedDropActions() const
{
    return Qt::CopyAction;
}

Qt::ItemFlags ScenarioModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);

    return Qt::ItemIsDropEnabled | defaultFlags;
}

QStringList ScenarioModel::mimeTypes() const
{
    QStringList types;
    types << "application/pointer";
    return types;
}

bool ScenarioModel::dropMimeData(const QMimeData* data, Qt::DropAction action,
                                 int row, int column, const QModelIndex& parent)
{
    if (action == Qt::IgnoreAction)
    {
        return true;
    }

    if (!data->hasFormat("application/pointer"))
    {
        return false;
    }

    if (column > 0)
    {
        return false;
    }

    //    int beginRow;

    //    if (row != -1)
    //        beginRow = row;
    //    else if (parent.isValid())
    //        beginRow = parent.row();
    //    else
    //        beginRow = rowCount(QModelIndex());

    QByteArray encodedData = data->data("application/pointer");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    QList<QPair<QString, ScenarioManager::Data_Structure::Application*>> result;
    int rows = 0;

    while (!stream.atEnd())
    {
        quint64 holder;
        QString instanceName;
        stream >> holder >> instanceName;

        if (holder == 0)
        {
            continue;
        }
        result << qMakePair(instanceName, reinterpret_cast<Data_Structure::Application*>(holder)); //
        ++rows;
    }

    emit applicationsDrop(result, row, parent);
    return true;
}

void ScenarioModel::update()
{
    for (int i = 0; i < rootItem->childCount(); i++)
    {
        ScenarioItem* child = reinterpret_cast<ScenarioItem*>(rootItem->child(i));
        child->update();
        if (child->childCount() > 0)
        {
            treeUpdate(child);
        }
    }

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
}

void ScenarioModel::treeUpdate(ScenarioItem* item)
{
    for (int i = 0; i < item->childCount(); i++)
    {
        ScenarioItem* child = reinterpret_cast<ScenarioItem*>(item->child(i));
        child->update();
        if (child->childCount() > 0)
        {
            treeUpdate(child);
        }
    }
}

ScenarioItem* ScenarioModel::getRootItem()
{
    return static_cast<ScenarioItem*>(rootItem);
}
