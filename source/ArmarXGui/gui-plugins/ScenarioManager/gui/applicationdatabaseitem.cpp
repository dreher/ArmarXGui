/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "applicationdatabaseitem.h"

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;
using namespace ScenarioManager;

ApplicationDatabaseItem::ApplicationDatabaseItem(QString text)
{
    m_itemData << text;
}

ApplicationDatabaseItem::ApplicationDatabaseItem(ScenarioManager::Data_Structure::PackagePtr package)
{
    this->package = package;
    m_itemData << package->getName().c_str();
}

ApplicationDatabaseItem::ApplicationDatabaseItem(ScenarioManager::Data_Structure::ApplicationPtr application)
{
    this->application = application;
    m_itemData << application->getName().c_str();
}

Data_Structure::PackagePtr ApplicationDatabaseItem::getPackage()
{
    return package;
}

Data_Structure::ApplicationPtr ApplicationDatabaseItem::getApplication()
{
    return application;
}

