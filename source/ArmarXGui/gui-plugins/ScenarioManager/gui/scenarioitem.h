/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "treeitem.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <QVector>
#include <QVariant>

/**
* @class ScenarioItem
* @brief TreeItem representing data contained in a Scenario or an Application.
*/

class ScenarioItem : public TreeItem
{
public:
    /**
    * Constructor that sets the data contained in this item.
    * @param rootData data
    */
    ScenarioItem(QList<QVariant> rootData);

    /**
    * Constructor that sets the scenario-data contained in this item.
    * @param scenario Scenario to read out the data
    */
    ScenarioItem(ScenarioManager::Data_Structure::ScenarioPtr scenario);

    /**
    * Constructor that sets the application-data contained in this item.
    * @param application Application to read out the data
    */
    ScenarioItem(ScenarioManager::Data_Structure::ApplicationInstancePtr application);

    ScenarioItem(std::string name, std::vector<ScenarioManager::Data_Structure::ApplicationInstancePtr> applications);
    std::string getPackageName();

    /**
    * Clears the item_data and parses the Scenario or Application for new data.
    */
    void update();

    /**
    * If this item represents a Scenario, it is returned.
    * @return the Scenario represented by this item
    */
    ScenarioManager::Data_Structure::ScenarioPtr getScenario();

    /**
    * If this item represents an Application, it is returned.
    * @return the Application represented by this item
    */
    ScenarioManager::Data_Structure::ApplicationInstancePtr getApplicationInstance();
    ScenarioManager::Data_Structure::ApplicationInstanceVectorPtr getApplicationInstances();

private:
    ScenarioManager::Data_Structure::ScenarioPtr scenario;
    ScenarioManager::Data_Structure::ApplicationInstancePtr application;

    std::string packageName;
    ScenarioManager::Data_Structure::ApplicationInstanceVectorPtr applications;
};

Q_DECLARE_METATYPE(ScenarioItem*)
