/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <ArmarXGui/libraries/qtpropertybrowser/src/qtpropertymanager.h>
#include "OptionalPropertyFactory.h"
#include "scenarioitem.h"
#include <boost/shared_ptr.hpp>
#include <QWidget>
#include <QTimer>


namespace Ui
{
    class DetailedApplicationView;
}

/**
* @class DetailedApplicationView
* @brief View that shows detailed information about a Scenario, Package or Application.
* Shows status and parameters of the object.
* Also allows starting, stopping and restarting of Scenarios and Applications.
*/
class DetailedApplicationView : public QWidget
{
    Q_OBJECT

public:
    /**
    * Constructor that sets up the ui.
    * @param parent parent widget
    */
    explicit DetailedApplicationView(QWidget* parent = 0);

    /**
    * Destructor.
    */
    ~DetailedApplicationView() override;

public:
    /**
    * Show an Application.
    * @param application Application to show
    */
    void showApplication(ScenarioManager::Data_Structure::ApplicationPtr application);

    /**
    * Show an ApplicationInstance.
    * @param appInstance ApplicationInstance to show
    */
    void showApplicationInstance(ScenarioManager::Data_Structure::ApplicationInstancePtr appInstance, ScenarioItem* item);

    /**
    * Show a Scenario.
    * @param scenario Scenario to show
    */
    void showScenario(ScenarioManager::Data_Structure::ScenarioPtr scenario);

    /**
    * Show a Package.
    * @param package Package to show
    */
    void showPackage(ScenarioManager::Data_Structure::PackagePtr package);


signals:
    void startButtonClicked();
    void stopButtonClicked();
    void restartButtonClicked();
    void addParameterButtonClicked();
    void toolButtonClicked();
    void saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr application);
    void applicationEnabledChange(bool enabled);

public slots:
    /**
    * Updates the status of the displayed item.
    */
    void updateStatus();

private slots:
    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_restartButton_clicked();

    void on_addParameterButton_clicked();

    void on_toolButton_clicked();

    void itemChanged(QtProperty* property, const QVariant& value);
    void itemAttributeChanged(QtProperty* property, const QString& attribute, const QVariant& val);

    void on_reloadButton_clicked();

    void on_fileUpdate();

private:
    void init();

private:
    Ui::DetailedApplicationView* ui;

    boost::shared_ptr<QtVariantPropertyManager> variantManager;
    boost::shared_ptr<QtVariantEditorFactory> variantFactory;

    bool statusUpdateRelevant;
    ScenarioManager::Data_Structure::ApplicationInstancePtr lastAppInstance;
    ScenarioManager::Data_Structure::ScenarioPtr lastScenario;
    ScenarioItem* lastScenarioItem;

    bool neadsUpdate;
    QTimer updateTimer;
};

typedef boost::shared_ptr<DetailedApplicationView> DetailedApplicationViewPtr;

