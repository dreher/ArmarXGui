/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "buttondelegate.h"

#include "treeitem.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>
#include <QObject>
#include <QApplication>
#include <QMouseEvent>


ButtonDelegate::ButtonDelegate(QObject* parent) : QStyledItemDelegate(parent),
    startPixmap(":/icons/media-playback-start.ico"),
    stopPixmap(":/icons/process-stop-7.ico"),
    restartPixmap(":/icons/view-refresh-7.png")
{
}

void ButtonDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyleOptionButton button;

    button.rect = option.rect;

    button.features = QStyleOptionButton::AutoDefaultButton;
    int size = appIconSize;
    if (!index.parent().isValid())
    {
        size = scenarioIconSize;
    }
    if (index.data().toString().compare("Start") == 0)
    {
        button.icon = startPixmap;
        button.iconSize = QSize(size, size);
    }
    else if (index.data().toString().compare("Stop") == 0)
    {
        button.icon = stopPixmap;
        button.iconSize = QSize(size, size);
    }
    else if (index.data().toString().compare("Restart") == 0)
    {
        button.icon = restartPixmap;
        button.iconSize = QSize(size, size);
    }
    else
    {
        button.text = index.data().toString();
    }

    if (index.data().toString().compare("HIDE") == 0)
    {
        const_cast<ButtonDelegate*>(this)->buttonStates[index] = 0;
        return;
    }

    if (const_cast<ButtonDelegate*>(this)->buttonStates[index] != 0)
    {
        button.state = const_cast<ButtonDelegate*>(this)->buttonStates[index];
    }
    else
    {
        const_cast<ButtonDelegate*>(this)->buttonStates[index] = QStyle::State_Raised | QStyle::State_Enabled;
        button.state = const_cast<ButtonDelegate*>(this)->buttonStates[index];
    }
    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);
}

bool ButtonDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index)
{
    //button should be hidden
    if ((buttonStates[index] & QStyle::State_Enabled) == 0)
    {
        return false;
    }

    if (event->type() == QEvent::MouseButtonPress)
    {
        buttonStates[index] = QStyle::State_Sunken | QStyle::State_Enabled;
        return true;
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        buttonStates[index] = QStyle::State_Raised | QStyle::State_Enabled;
        emit buttonClicked(index.row(), index.column(), index.parent());
        return true;
    }
    return false;
}

void ButtonDelegate::emitClicketData(int row, int column, QModelIndex parent)
{
    emit buttonClicked(row, column, parent);
}

int ButtonDelegate::getAppIconSize() const
{
    return appIconSize;
}

void ButtonDelegate::setAppIconSize(int value)
{
    appIconSize = value;
}

int ButtonDelegate::getScenarioIconSize() const
{
    return scenarioIconSize;
}

void ButtonDelegate::setScenarioIconSize(int value)
{
    scenarioIconSize = value;
}

QSize ButtonDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    int size = appIconSize;
    if (!index.parent().isValid())
    {
        size = scenarioIconSize;
    }
    return QSize(option.rect.width(), option.rect.height() + size + 6);
    //QItemDelegate::sizeHint(option,index);
}
