/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "treemodel.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <QColor>
#include <QStringList>

TreeModel::TreeModel(QObject* parent)
    : QAbstractItemModel(parent)
{
}

TreeModel::~TreeModel()
{
    if (rootItem != nullptr)
    {
        delete rootItem;
    }
}

int TreeModel::columnCount(const QModelIndex& parent) const
{
    return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    TreeItem* item = getItem(index);

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
    {
        return 0;
    }

    TreeItem* item = getItem(index);
    if (!item->isEnabled())
    {
        return QAbstractItemModel::flags(index) & (~Qt::ItemIsEnabled);
    }
    else
    {
        return QAbstractItemModel::flags(index) | (Qt::ItemIsEnabled);
    }
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return rootItem->data(section);
    }

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex& parent)
const
{
    if (!hasIndex(row, column, parent))
    {
        return QModelIndex();
    }

    TreeItem* parentItem = getItem(parent);

    TreeItem* childItem = parentItem->child(row);
    if (childItem)
    {
        QModelIndex result = createIndex(row, column, childItem);
        return result;
    }
    else
    {
        return QModelIndex();
    }
}

QModelIndex TreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
    {
        return QModelIndex();
    }

    TreeItem* childItem = getItem(index);
    TreeItem* parentItem = childItem->parent();

    if (parentItem == rootItem || parentItem == nullptr)
    {
        return QModelIndex();
    }

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex& parent) const
{
    TreeItem* parentItem = getItem(parent);

    return parentItem->childCount();
}


//TreeItem* TreeModel::getRootItem()
//{
//    return rootItem;
//}


TreeItem* TreeModel::getItem(const QModelIndex& index) const
{
    if (index.isValid())
    {
        TreeItem* item = static_cast<TreeItem*>(index.internalPointer());
        if (item)
        {
            return item;
        }
    }

    return rootItem;
}


bool TreeModel::insertColumn(int position, QVariant data, const QModelIndex& parent)
{
    bool success;

    beginInsertColumns(parent, position, position);
    success = rootItem->insertColumn(position, data);
    endInsertColumns();

    return success;
}

bool TreeModel::removeColumn(int position, const QModelIndex& parent)
{
    bool success;

    beginRemoveColumns(parent, position, position);
    success = rootItem->removeColumn(position);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
    {
        reset();
    }

    return success;
}

bool TreeModel::insertRow(int position, TreeItem* item, const QModelIndex& parent)
{
    TreeItem* parentItem = getItem(parent);
    bool success = true;

    beginInsertRows(parent, position, position);
    success = parentItem->insertChild(position, item);
    endInsertRows();

    return success;
}

bool TreeModel::removeRow(int position, const QModelIndex& parent)
{
    ARMARX_INFO_S << parent.data().toString().toStdString();
    TreeItem* parentItem = getItem(parent);
    bool success = true;

    beginRemoveRows(parent, position, position);
    success = parentItem->removeChild(position);
    endRemoveRows();

    return success;
}


