/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "filterabletreemodelsortfilterproxymodel.h"
#include "buttondelegate.h"
#include "namelocationview.h"
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <QMenu>
#include <QAction>
#include <QWidget>
#include <QAbstractItemModel>
#include <boost/shared_ptr.hpp>

namespace Ui
{
    class ScenarioListView;
}

/**
* @class ScenarioListView
* @brief View that shows a list of Scenarios.
* Allows to start, stop, restart Applications and Scenarios. Also allows to open Scenarios and remove/add items.
*/
class ScenarioListView : public QWidget
{
    Q_OBJECT

public:
    /**
    * Constructor that sets up the ui and behaviour of this view.
    * @param parent parent widget
    */
    explicit ScenarioListView(QWidget* parent = 0);

    /**
    * Destructor.
    */
    ~ScenarioListView() override;

signals:
    void startApplication(int row, int column, QModelIndex parent);
    void stopApplication(int row, int column, QModelIndex parent);
    void restartApplication(int row, int column, QModelIndex parent);
    void createScenario();
    void showOpenDialog();
    void removeItem(QModelIndex index);
    void itemClicked(const QModelIndex& index);

public slots:
    /**
    * Sets the model of this view.
    * @param model the underlying model
    */
    void setModel(FilterableTreeModelSortFilterProxyModelPtr model);

private slots:
    void on_searchBar_textEdited(const QString& text);
    void startButtonClicked(int row, int column, QModelIndex parent);
    void stopButtonClicked(int row, int column, QModelIndex parent);

    void restartButtonClicked(int row, int column, QModelIndex parent);

    void on_newButton_clicked();

    void on_openButton_clicked();
    void removeItemTriggered();

    void onCustomContextMenu(const QPoint& point);

    void on_treeView_clicked(const QModelIndex& index);

private:
    Ui::ScenarioListView* ui;
    FilterableTreeModelSortFilterProxyModelPtr model;
    ButtonDelegate startButtonDelegate;
    ButtonDelegate stopButtonDelegate;
    ButtonDelegate restartButtonDelegate;
    QMenu contextMenu;
    QAction removeAction;
};

