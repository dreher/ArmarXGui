/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <QDialog>
#include <QStringList>

namespace Ui
{



    class CreateScenarioView;
}

/**
* @class CreateScenarioView
* @brief View that allows user to create a new scenario.
*/
class CreateScenarioView : public QDialog
{
    Q_OBJECT

public:
    /**
    * Constructor that sets up the UI.
    * @param parent QT option for parent
    */
    explicit CreateScenarioView(QWidget* parent = 0);

    /**
    * Destructor
    */
    ~CreateScenarioView() override;

    /**
    * Sets the packages which are shown in the view.
    * @param packages names of the packages to show
    */
    void setPackages(QVector<QPair<QString, bool>> const& packages);

signals:

    void created(std::string name, std::string package);

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();
private:
    Ui::CreateScenarioView* ui;
};

