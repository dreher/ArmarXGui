/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/ApplicationInstance.h>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <QVector>
#include <QVariant>

enum TreeItemTypes
{
    SCENARIOITEMSOURCE = 1001,
    SCENARIOITEMPROXY = 1002,
    SETTINGSITEMSOURCE = 1003,
    SETTINGSITEMPROXY = 1004,
    APPLICATIONITEMPROXY = 1005,
    APPLICATIONITEMSOURCE = 1006,
    OPENSCENARIOITEMSOURCE = 1007,
    OPENSCENARIOITEMPROXY = 1008
};

class TreeItem
{
public:
    explicit TreeItem();
    virtual ~TreeItem();

    TreeItem* child(int row);

    int childCount() const;
    int columnCount() const;
    virtual QVariant data(int column) const;
    bool setData(int column, const QVariant& value);

    void appendChild(TreeItem* child);
    bool insertChild(int position, TreeItem* child);
    bool removeChild(int position);

    bool insertColumn(int position, QVariant data);
    bool removeColumn(int position);

    TreeItem* parent();

    int row() const;

    void setEnabled(bool enabled);
    bool isEnabled();

protected:
    QVector<QVariant> m_itemData;
    bool enabled;

private:
    TreeItem* m_parentItem;
    QList<TreeItem*> m_childItems;
};

