/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ScenarioManager::gui-plugins::ScenarioManagerWidgetController
 * @author     [Cedric Seehausen] ( [usdnr@student.kit.edu] )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ScenarioManagerWidgetController.h"

#include <QCoreApplication>
#include <QProgressDialog>
#include <QToolBar>

#include "gui/namelocationview.h"
#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <QMetaType>
#include <ArmarXGui/applications/ArmarXGui/ArmarXMainWindow.h>
#include <ArmarXCore/core/system/Synchronization.h>
#include <QToolTip>

using namespace armarx;
using namespace ScenarioManager;
using namespace Exec;
using namespace Controller;
using namespace Data_Structure;
using namespace Parser;
using namespace std;


ScenarioManagerWidgetController::ScenarioManagerWidgetController()
    :
    packages(new vector<PackagePtr>())
    , executor(new Executor(stopStrategyFactory.getStopStrategy(StopperFactory::getFactory()->getPidStopper()), StarterFactory::getFactory()->getStarter()))
    , applicationController(packages, executor)
    , detailedApplicationController(executor)
    , scenarioListController(packages, executor)
    , settingsController(packages, executor)
    , openScenarioController(packages, executor)
{
    widget.setupUi(getWidget());
    editModeAction = new QAction("Edit Mode", this);

    QSettings settings("KIT", "ScenarioManager");
    bool editModeState = settings.value("editMode", false).toBool();
    editMode(editModeState);

}


ScenarioManagerWidgetController::~ScenarioManagerWidgetController()
{

}


void ScenarioManagerWidgetController::loadSettings(QSettings* settings)
{
    QSettings autosettings("KIT", "ScenarioManager");
    QStringList scenarios = settings->value("scenarios", QStringList()).toStringList();
    if (scenarios.size() > 0)
    {
        autosettings.setValue("scenarios", scenarios);
    }
    QStringList packages = settings->value("packages", QStringList()).toStringList();
    if (packages.size() > 0)
    {
        autosettings.setValue("packages", packages);
    }
}

void ScenarioManagerWidgetController::saveSettings(QSettings* settings)
{
    QSettings autosettings("KIT", "ScenarioManager");
    settings->setValue("scenarios", autosettings.value("scenarios", QStringList()));
    settings->setValue("packages", autosettings.value("packages", QStringList()));
}


void ScenarioManagerWidgetController::onInitComponent()
{
    QMetaObject::invokeMethod(this, "init", Qt::QueuedConnection);
}

void ScenarioManagerWidgetController::init()
{
    detailedApplicationController.setDetailedApplicationView(widget.detailedApplicationView);

    QObject::connect(widget.scenarioView, SIGNAL(createScenario()),
                     &scenarioListController, SLOT(createScenario()));

    QObject::connect(widget.scenarioView, SIGNAL(createScenario()),
                     widget.applicationDatabase, SLOT(show()));

    QObject::connect(widget.scenarioView, SIGNAL(removeItem(QModelIndex)),
                     &scenarioListController, SLOT(removeItem(QModelIndex)));

    QObject::connect(widget.scenarioView, SIGNAL(itemClicked(QModelIndex)),
                     &scenarioListController, SLOT(showApplication(QModelIndex)));

    QObject::connect(widget.scenarioView, SIGNAL(startApplication(int, int, QModelIndex)),
                     &scenarioListController, SLOT(start(int, int, QModelIndex)));

    QObject::connect(widget.scenarioView, SIGNAL(stopApplication(int, int, QModelIndex)),
                     &scenarioListController, SLOT(stop(int, int, QModelIndex)));

    QObject::connect(widget.scenarioView, SIGNAL(restartApplication(int, int, QModelIndex)),
                     &scenarioListController, SLOT(restart(int, int, QModelIndex)));

    QObject::connect(&settingsController, SIGNAL(packageRemoved()),
                     this, SLOT(updateModels()));

    QObject::connect(&settingsController, SIGNAL(packageAdded(std::string)),
                     this, SLOT(reparsePackage(std::string)));

    QObject::connect(&scenarioListController, SIGNAL(updated()),
                     this, SLOT(updateModels()));

    QObject::connect(&scenarioListController, SIGNAL(applicationInstanceClicked(Data_Structure::ApplicationInstancePtr, ScenarioItem*)),
                     &detailedApplicationController, SLOT(showApplicationInstance(Data_Structure::ApplicationInstancePtr, ScenarioItem*)));

    QObject::connect(&scenarioListController, SIGNAL(scenarioClicked(Data_Structure::ScenarioPtr)),
                     &detailedApplicationController, SLOT(showScenario(Data_Structure::ScenarioPtr)));

    QObject::connect(&scenarioListController, SIGNAL(scenarioClicked(Data_Structure::ScenarioPtr)),
                     &detailedApplicationController, SLOT(showScenario(Data_Structure::ScenarioPtr)));

    QObject::connect(&scenarioListController, SIGNAL(statusUpdated()),
                     widget.detailedApplicationView, SLOT(updateStatus()));

    QObject::connect(widget.applicationDatabase, SIGNAL(openSettings()),
                     &settingsController, SLOT(showSettings()));

    QObject::connect(widget.applicationDatabase, SIGNAL(itemClicked(QModelIndex)),
                     &applicationController, SLOT(on_itemClicked(QModelIndex)));

    QObject::connect(&applicationController, SIGNAL(applicationClicked(Data_Structure::ApplicationPtr)),
                     &detailedApplicationController, SLOT(showApplication(Data_Structure::ApplicationPtr)));

    QObject::connect(&applicationController, SIGNAL(packageClicked(Data_Structure::PackagePtr)),
                     &detailedApplicationController, SLOT(showPackage(Data_Structure::PackagePtr)));

    QObject::connect(&openScenarioController, SIGNAL(updated()),
                     this, SLOT(updateModels()));

    QObject::connect(widget.scenarioView, SIGNAL(showOpenDialog()),
                     &openScenarioController, SLOT(showOpenScenarioView()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(startButtonClicked()),
                     &detailedApplicationController, SLOT(start()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(stopButtonClicked()),
                     &detailedApplicationController, SLOT(stop()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(restartButtonClicked()),
                     &detailedApplicationController, SLOT(save()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(toolButtonClicked()),
                     &detailedApplicationController, SLOT(showInStandardEditor()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(addParameterButtonClicked()),
                     &detailedApplicationController, SLOT(showPropertyAddView()));

    QObject::connect(widget.detailedApplicationView, SIGNAL(applicationEnabledChange(bool)),
                     &scenarioListController, SLOT(updateModel()));


    QObject::connect(widget.detailedApplicationView, SIGNAL(saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr)),
                     &scenarioListController, SLOT(saveScenario(ScenarioManager::Data_Structure::ApplicationInstancePtr)));

    QObject::connect(&applicationController, SIGNAL(modelUpdated(FilterableTreeModelSortFilterProxyModelPtr)),
                     widget.applicationDatabase, SLOT(setModel(FilterableTreeModelSortFilterProxyModelPtr)));

    widget.scenarioView->setModel(scenarioListController.getTreeModel());

    connect(editModeAction, SIGNAL(toggled(bool)), this, SLOT(editMode(bool)));

    editModeAction->setCheckable(true);
    editModeAction->setToolTip("If toggled the application database and the application property view will be shown.");

    QSettings settings("KIT", "ScenarioManager");
    QStringList packages = settings.value("packages").toStringList();

    if (packages.size() == 0)
    {
        //armarx::ArmarXMainWindow* mainWindow = static_cast<ArmarXMainWindow*>();
        armarx::ApplicationPtr application = Application::getInstance();
        if (application.get() != nullptr)
        {
            for (auto package : application->getDefaultPackageNames())
            {
                packages << QString::fromStdString(package);
            }
        }
        settings.setValue("packages", packages);
    }

    bool editModeState = settings.value("editMode", false).toBool();
    editModeAction->setChecked(editModeState);
    editMode(editModeState);

    settingsController.init();

    reparsePackages();
}

QPointer<QWidget> ScenarioManagerWidgetController::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        if (parent != customToolbar->parent())
        {
            customToolbar->setParent(parent);
        }

        return customToolbar.data();
    }

    customToolbar = new QToolBar(parent);
    customToolbar->setIconSize(QSize(16, 16));
    customToolbar->addAction(QIcon(":/icons/configure-3.png"), "Configure", &settingsController, SLOT(showSettings()));
    QToolTip::showText(customToolbar->mapToGlobal(QPoint(0, 0)), "Additional Packages can be opened in the settings");
    customToolbar->addAction(editModeAction);
    return customToolbar.data();
}

void ScenarioManagerWidgetController::onConnectComponent()
{

}

void ScenarioManagerWidgetController::reparsePackages()
{
    QSettings settings("KIT", "ScenarioManager");
    QStringList packages = settings.value("packages").toStringList();
    QProgressDialog progress("Loading scenarios from " + QString::number(packages.size()) + " packages ...", "", 0, packages.size(), getWidget());
    progress.setWindowModality(Qt::WindowModal);
    PackageBuilder parser;
    for (int i = 0; i < packages.size(); i++)
    {
        string name = packages.at(i).toStdString();
        progress.setLabelText(QString("Loading scenarios from package ") + name.c_str());
        progress.setValue(i + 1);
        qApp->processEvents();

        QStringList openedScenarios = settings.value("scenarios").toStringList();
        //remove duplicates
        openedScenarios = openedScenarios.toSet().toList();

        StringList openedList;

        for (auto it : openedScenarios)
        {
            openedList.push_back(it.toStdString());
        }

        PackagePtr package = parser.parsePackage(name, openedList);

        if (package.get() == nullptr)
        {
            continue;
        }

        this->packages->push_back(package);
    }

    updateModels();
}

void ScenarioManagerWidgetController::reparsePackage(string name)
{
    PackageBuilder parser;
    PackagePtr package = parser.parsePackage(name, StringList());

    this->packages->push_back(package);

    updateModels();
}

void ScenarioManagerWidgetController::updateModels()
{
    settingsController.updateModel();
    openScenarioController.updateModel();
    scenarioListController.updateModel();
    applicationController.updatePackages();
    widget.detailedApplicationView->updateStatus();

    //widget.applicationDatabase->setModel(applicationController.getModel());
    //widget.scenarioView->setModel(scenarioListController.getTreeModel());
}


void ScenarioManagerWidgetController::editMode(bool edit)
{
    QSettings settings("KIT", "ScenarioManager");
    settings.setValue("editMode", edit);
    widget.detailedApplicationView->setVisible(edit);
    widget.applicationDatabase->setVisible(edit);
}
