/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <QGraphicsGridLayout>
#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QLabel>
#include <QPushButton> //TODO: remove after testing

#include "../model/StateParameter.h"
#include "../StateScene.h"
#include "ParameterTableItem.h"
#include "StateItem.h"

armarx::ParameterTableItem::ParameterTableItem(armarx::statechartmodel::StateParameterMap parameterMap, QGraphicsItem* parent) :
    QGraphicsWidget(parent),
    paramMap(parameterMap),
    parentScene(parentItem()->scene())
{
    QGraphicsGridLayout* layout = new QGraphicsGridLayout();

    setHeadRow(layout);

    int row = 1;

    for (statechartmodel::StateParameterPtr param : paramMap)
    {
        addStateParameterToLayout(param, layout, row);
        row++;
    }

    setLayout(layout);
}

armarx::ParameterTableItem::~ParameterTableItem()
{
    //delete stateScene? rather not, as it is not owned by this class
}

void armarx::ParameterTableItem::addStateParameterToLayout(armarx::statechartmodel::StateParameterPtr param, QGraphicsGridLayout* layout, int row)
{
    QGraphicsWidget* typeLabel = parentScene->addWidget(new QLabel(param->type));
    QGraphicsWidget* defaultValueLabel = parentScene->addWidget(new QLabel(param->getDefaultValueJson())); //TODO: proper translation

    QString optionalText;

    if (param->optional)
    {
        optionalText = "yes";
    }
    else
    {
        optionalText = "no";
    }

    QGraphicsWidget* optionalLabel = parentScene->addWidget(new QLabel(optionalText));

    QGraphicsWidget* descriptionLabel = parentScene->addWidget(new QLabel(param->description));

    layout->addItem(typeLabel, row, 0);
    layout->addItem(defaultValueLabel, row, 1);
    layout->addItem(optionalLabel, row, 2);
    layout->addItem(descriptionLabel, row, 3);
}

void armarx::ParameterTableItem::setHeadRow(QGraphicsGridLayout* layout)
{
    QGraphicsWidget* typeItem = parentScene->addWidget(new QLabel("Type"));
    QGraphicsWidget* defaultValueItem = parentScene->addWidget(new QLabel("Default Value"));
    QGraphicsWidget* optionalItem = parentScene->addWidget(new QLabel("Is optional"));
    QGraphicsWidget* descriptionItem = parentScene->addWidget(new QLabel("Description"));

    layout->addItem(typeItem, 0, 0);
    layout->addItem(defaultValueItem, 0, 1);
    layout->addItem(optionalItem, 0, 2);
    layout->addItem(descriptionItem, 0, 3);
}
