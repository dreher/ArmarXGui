/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateTabWidget.h"
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StatechartView.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

namespace armarx
{
    StateTabWidget::StateTabWidget(QWidget* parent) :
        QTabWidget(parent)
    {
        connect(this, SIGNAL(tabCloseRequested(int)), this, SLOT(deleteViewerWidget(int)));
    }

    statechartmodel::StateInstancePtr StateTabWidget::currentState() const
    {
        StatechartView* viewer = qobject_cast<StatechartView*>(currentWidget());

        if (viewer)
        {
            return viewer->getStateInstance();
        }

        return statechartmodel::StateInstancePtr();
    }

    StatechartView* StateTabWidget::currentStateview() const
    {
        return dynamic_cast<StatechartView*>(currentWidget());
    }

    StatechartView* StateTabWidget::stateview(int index) const
    {
        return dynamic_cast<StatechartView*>(widget(index));
    }

    int StateTabWidget::getStateTab(statechartmodel::StatePtr state) const
    {

        for (int i = 0; i < count(); ++i)
        {
            StatechartView* view = stateview(i);

            if (view)
            {
                if (view->getStateInstance() && view->getStateInstance()->getStateClass() == state)
                {
                    return i;
                }
            }
        }

        return -1;
    }


    StatechartView*  StateTabWidget::addStateTab(statechartmodel::StatePtr state)
    {
        StatechartView* v = new StatechartView(state, false);
        addTab(v, (state) ? state->getStateName() : "NoState");
        setCurrentIndex(count() - 1);
        QSizePolicy p;
        p.setVerticalStretch(5);
        p.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        currentWidget()->setSizePolicy(p);
        v->viewAll();
        return v;
    }

    StatechartView* StateTabWidget::addEmptyStateTab()
    {
        return addStateTab();
    }

    void StateTabWidget::deleteViewerWidget(int index)
    {
        widget(index)->deleteLater();
    }

}


QSize armarx::StateTabWidget::sizeHint() const
{
    return QSize(400, 400);
}
