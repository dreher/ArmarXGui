/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/State.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/stateinstance/StateInstance.h>

#include <QTabWidget>

namespace armarx
{

    class StatechartView;
    class StateTabWidget : public QTabWidget
    {
        Q_OBJECT
    public:
        explicit StateTabWidget(QWidget* parent = 0);
        statechartmodel::StateInstancePtr currentState() const;
        StatechartView* currentStateview() const;
        StatechartView* stateview(int index) const;
        int getStateTab(statechartmodel::StatePtr state) const;
        // QWidget interface
        QSize sizeHint() const override;
    signals:

    public slots:
        StatechartView* addStateTab(statechartmodel::StatePtr state = statechartmodel::StatePtr());
        StatechartView* addEmptyStateTab();
    private slots:
        void deleteViewerWidget(int index);
    };

}


