/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <QTimer>

#include "StatechartView.h"
#include "ui_StatechartView.h"

#include "StateItem.h"
#include "GraphicsViewZoom.h"

#include "../IceStateConverter.h"
#include "../StateScene.h"
#include "../model/State.h"
#include "../model/stateinstance/StateInstance.h"
#include "../model/stateinstance/EndState.h"

#include <ArmarXCore/statechart/StatechartObjectFactories.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>



using namespace armarx;

StatechartView::StatechartView(statechartmodel::StatePtr state, bool enableLayouting, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::StatechartView),
    //    layoutController(state, enableLayouting)
    layoutThread(state, enableLayouting)
{
    //    layoutController.enableLayouting(enableLayouting);

    srand(IceUtil::Time::now().toMilliSeconds());
    ui->setupUi(this);
    stateScene = new StateScene(this);

    if (!state)
    {
        state = CreateTestStates();
    }

    setState(state);

    ui->graphicsView->setScene(stateScene);
    zoomer = new Graphics_view_zoom(ui->graphicsView);
    zoomer->set_modifiers(Qt::NoModifier);

    if (state)
    {
        layoutThread.run();
    }
    else
    {
        ARMARX_WARNING_S << "no state set!";
    }

    connect(stateScene, SIGNAL(selectionChanged()), this, SLOT(onItemChange()));
}

StatechartView::~StatechartView()
{
    delete ui;
}

statechartmodel::StateInstancePtr StatechartView::getStateInstance() const
{
    return stateScene->getStateInstance();
}

QGraphicsView* StatechartView::getGraphicsView() const
{
    return ui->graphicsView;
}

Graphics_view_zoom* StatechartView::getGraphicsViewZoomer() const
{
    return zoomer;
}

LayoutController& StatechartView::getLayoutController()
{
    return layoutThread.getController();
}

void StatechartView::setState(statechartmodel::StatePtr state)
{
    stateScene->setToplevelState(state);

    if (state)
    {
        layoutThread.setState(state);
        //        layoutController.setTopState(state);
    }
}

void StatechartView::setOriginalZoom()
{
    ui->graphicsView->setTransform(QTransform());
}

void StatechartView::deleteSelectedStates()
{
    QList<QGraphicsItem*> selection = stateScene->selectedItems();
    foreach (QGraphicsItem* item, selection)
    {
        StateItem* state = dynamic_cast<StateItem*>(item);

        if (state)
        {
            statechartmodel::StatePtr parent = state->getStateInstance()->getParent();

            if (parent)
            {
                if (parent->removeSubstate(state->getStateInstance()->getInstanceName()))
                {
                    ARMARX_INFO_S << "removed substate";
                }
                else
                {
                    ARMARX_INFO_S << "removing failed";
                }

            }
        }
    }
}

void StatechartView::viewAll()
{
    ui->graphicsView->fitInView(stateScene->itemsBoundingRect(), Qt::KeepAspectRatio);
}

void StatechartView::onItemChange()
{
    QList<QGraphicsItem*> selection = stateScene->selectedItems();

    if (selection.size() > 0)
    {
        StateItem* state = dynamic_cast<StateItem*>(selection.first());

        if (state && state->getStateInstance())
        {
            emit selectedStateChanged(state->getStateInstance());
        }
    }
}

void StatechartView::showSubSubstates(bool show)
{
    stateScene->getTopLevelStateItem()->setMaxShownSubstateLevel(show ? -1 : 1);
    stateScene->update();
}

statechartmodel::StatePtr StatechartView::CreateTestStates()
{

    statechartmodel::StatePtr topState(new statechartmodel::State());
    topState->setStateName("TopLevelState");

    statechartmodel::StatePtr sub1(new statechartmodel::State());
    sub1->setStateName("sub1");

    statechartmodel::EventList eventList;
    statechartmodel::EventPtr e1(new statechartmodel::Event());
    e1->name = "Success";
    eventList.append(e1);
    statechartmodel::EventPtr e2(new statechartmodel::Event());
    e2->name = "Failure";
    eventList.append(e2);
    statechartmodel::EventPtr e3(new statechartmodel::Event());
    e3->name = "Lolz";
    eventList.append(e3);
    sub1->setOutgoingEvents(eventList);

    statechartmodel::StatePtr sub2(new statechartmodel::State());
    sub2->setStateName("sub2");

    statechartmodel::StatePtr subsub1(new statechartmodel::State());
    subsub1->setStateName("subsub1");


    topState->addSubstate(sub1);
    topState->addSubstate(sub2);
    topState->addRemoteSubstate(sub1, "ProxyName", "remotesub1-2");
    statechartmodel::EventList eventList2;
    statechartmodel::EventPtr e21(new statechartmodel::Event());
    e21->name = "Success";
    eventList.append(e21);
    statechartmodel::EventPtr e22(new statechartmodel::Event());
    e22->name = "Failure";
    eventList.append(e22);
    subsub1->setOutgoingEvents(eventList2);
    sub1->addSubstate(subsub1);

    subsub1->addEndSubstate("EndState", "Success");

    statechartmodel::TransitionPtr t1(new statechartmodel::Transition());
    t1->eventName = "GoOn";
    t1->sourceState = topState->getSubstates()["sub1"];
    t1->destinationState = topState->getSubstates()["sub2"];
    topState->addTransition(t1);

    return topState;

}


QSize armarx::StatechartView::sizeHint() const
{
    return QSize(200, 200);
}
