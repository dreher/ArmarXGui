/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "MorphingItem.h"
#include <QStyleOptionGraphicsItem>


#include <ArmarXCore/core/logging/Logging.h>

#include <QGraphicsView>
#include <QPainter>
#include <qgraphicssceneevent.h>

namespace armarx
{

    MorphingItem::MorphingItem(QGraphicsItem* derivedItem) :
        derivedItem(derivedItem)
    {

    }

    QGraphicsView* MorphingItem::getView(QGraphicsSceneEvent* event)
    {
        QGraphicsView* view =  qobject_cast<QGraphicsView*>(event->widget()->parent());
        return view;
    }

    LevelOfDetail MorphingItem::getLevelOfDetail(QPainter* painter) const
    {
        qreal lodT = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());

        if (lodT > hideOnHighDeZoomThreshold)
        {
            //            ARMARX_IMPORTANT_S << "Hiding: " << lodT;
            return eHidden;
        }

        return getLevelOfDetail(lodT);
    }

    LevelOfDetail MorphingItem::getLevelOfDetail(float levelOfDetail) const
    {
        float width = derivedItem->boundingRect().width() * levelOfDetail;

        //        ARMARX_INFO_S << "width is " << width;
        if (width < minSizeToShowSubstates)
        {
            return eNoSubstates;
        }
        else
        {
            //            ARMARX_INFO_S << "width is " << width;
            return eFull;
        }
    }

    LevelOfDetail MorphingItem::getLevelOfDetailByParent(QPainter* painter, MorphingItem* parent) const
    {
        qreal lodT = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());
        qreal lodP = lodT;
        LevelOfDetail detail = getLevelOfDetail(painter);


        if (derivedItem)
        {
            lodP /= derivedItem->scale();

            if (parent)
                if (parent->getLevelOfDetail(lodP) >= eNoSubstates)
                {
                    detail = eHidden;
                }
        }

        return detail;
    }

} // namespace armarx
