/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "MorphingItem.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <QGraphicsPathItem>
#include <qpropertyanimation.h>
#include <boost/shared_ptr.hpp>

namespace armarx
{
    namespace statechartmodel
    {
        class Transition;
        typedef boost::shared_ptr<Transition> TransitionPtr;
        typedef boost::shared_ptr<const Transition> TransitionCPtr;
        class StateInstance;
        typedef boost::shared_ptr<StateInstance> StateInstancePtr;
    }

    class TransitionLabel;

    class TransitionItem :
        public QObject
        , public QGraphicsPathItem
        , public MorphingItem
        , public Logging
    {
        Q_OBJECT
        Q_PROPERTY(QColor color READ getColor WRITE setColor)
        Q_PROPERTY(QColor highlightColor READ getHighlightColor WRITE setHighlightColor)
    public:
        explicit TransitionItem(statechartmodel::TransitionCPtr transition, QGraphicsItem* parent = 0);

        IceUtil::Time getLastHighlightTimestamp() const;
    signals:
        void transitionEndPlaced(statechartmodel::TransitionCPtr transition, QPointF dropPosition);
        void transitionSupportPointPlaced(QPointF dropPosition);
        void transitionContextMenuRequested(statechartmodel::TransitionCPtr transition, QPoint mouseScreenPos, QPointF mouseItemPos);
    public slots:
        void setEditable(bool editable);
        void recalcPath();
        void recalcShape();
        void setSelected(bool selected);
        void attachToMouse(int supportPointIndex);
        void highlightAnimation(int duration = 5000);
        void updateLabel();
    public:
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
        QPainterPath shape() const override;
        QRectF boundingRect() const override;
        // QGraphicsItem interface
        bool isInitialTransition() const;
        QPolygonF calcHull(QPainter* painter = 0) const;

        QColor getColor() const;
        void setColor(const QColor& color);
        QColor getHighlightColor() const;
        void setHighlightColor(const QColor& color);
        statechartmodel::TransitionCPtr getTransition() const;

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
        void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
        void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
        void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;


        QPolygonF shapePolygon;
    private:
        QPointF calcIntersection(statechartmodel::StateInstancePtr state, const QPointF& supportPoint) const;
        QPointF calcIntersection(statechartmodel::StateInstancePtr state, QLineF centerLine, const QPointF& supportPoint) const;
        QPolygonF calcArrowHead(const QLineF& line);
        void recalcLabelPose();
        float calcLengthOfLine(QPointF start, QPointF end);


        statechartmodel::TransitionCPtr transition;
        QPolygonF arrowHead;
        QRectF initialStateCircle;
        TransitionLabel* transitionLabel;
        int pointAttachedToMouse;
        QColor color, highlightColor, normalColor;
        QPropertyAnimation animation;
        IceUtil::Time highlightAnimationStartTime;

        // QGraphicsItem interface
    protected:
        void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
        void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;
        void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
    };

    class TransitionLabel :
        public QObject,
        public QGraphicsSimpleTextItem,
        public MorphingItem
    {
        Q_OBJECT
        Q_PROPERTY(qreal fontPointSize READ fontPointSize WRITE setFontPointSize)
    public:

        TransitionLabel(QString labelText, QGraphicsItem* parent = 0);
        // QGraphicsItem interface
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
        qreal fontPointSize() const;
        qreal baseFontPointSize() const;
        void setBaseFontPointSize(qreal newFontSize);
        void setFontPointSize(qreal newFontSize);
        void setEnlargedFontPointSize(qreal newFontSize);
        void setHovering(bool hovering);
        bool getHovering() const;
    protected:
        QPropertyAnimation animation;
        bool hovering;
        qreal fontBasePointSize;
        qreal fontEnlargedPointSize;
    };

}


