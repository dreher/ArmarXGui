/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QGraphicsObject>
#include <QLinearGradient>
#include <QPen>

#include <ArmarXCore/core/logging/Logging.h>



class RoundRectItem :
    public QGraphicsObject,
    public armarx::Logging
{
    Q_OBJECT
public:
    RoundRectItem(const QRectF& bounds, const QColor& color,
                  QGraphicsItem* parent = 0);

    void setColor(QColor newColor);
    void setRimPen(QPen newPen);


    QRectF boundingRect() const override;
    void setBounds(QRectF newBounds);
    void setSize(const QSizeF& newSize);
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0) override;



    QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant& value) override;
    void adjustScale(float& resultScalefactor);
    virtual QPointF adjustPosition(QPointF& newPos);
    bool isLevelOfDetailLow(QGraphicsSceneEvent* event) const;
    void setEditable(bool editable);
    bool isEditable() const;
private:
    QLinearGradient fillGradient;
    bool scalingActive;
    enum
    {
        eNone,
        eTopResizing,
        eBottomResizing,
        eLeftResizing,
        eRightResizing
    } resizingMode;
    bool setCursor;
    QColor color;
    QPen rimPen;
    bool editable;

    // QGraphicsItem interface
protected:
    QRectF bounds;

    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
    virtual bool itemResizing(const QRectF& oldSize, QRectF& proposedSize)
    {
        return true;
    }
    virtual void itemResized(const QRectF& oldSize, const QRectF& newSize) {}
    virtual void itemMoved(const QPointF& oldPos, const QPointF& newPos) {}
    virtual void itemBoundingBoxChanged(float oldSize, float size) {}

    void adjustCursor(Qt::CursorShape shape);


    QRectF getBottomResizeBB() const;
    QRectF getTopResizeBB() const;
    QRectF getLeftResizeBB() const;
    QRectF getRightResizeBB() const;

    // QGraphicsItem interface
protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) override;
};


