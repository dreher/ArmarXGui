/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GraphicsViewZoom.h"

#include <QMouseEvent>
#include <QApplication>
#include <QScrollBar>
#include <cmath>
#include <iostream>
#include <QGraphicsItem>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

namespace armarx
{




    Graphics_view_zoom::Graphics_view_zoom(QGraphicsView* view)
        : QObject(view), _view(view)
    {
        _view->viewport()->installEventFilter(this);
        _view->setMouseTracking(true);
        _modifiers = Qt::ControlModifier;
        _zoom_factor_base = 1.0015;
    }

    void Graphics_view_zoom::gentle_zoom(double factor)
    {
        _view->scale(factor, factor);
        _view->centerOn(target_scene_pos);
        QPointF delta_viewport_pos = target_viewport_pos - QPointF(_view->viewport()->width() / 2.0,
                                     _view->viewport()->height() / 2.0);
        QPointF viewport_center = _view->mapFromScene(target_scene_pos) - delta_viewport_pos;
        _view->centerOn(_view->mapToScene(viewport_center.toPoint()));
        emit zoomed();
    }

    void Graphics_view_zoom::set_modifiers(Qt::KeyboardModifiers modifiers)
    {
        _modifiers = modifiers;

    }

    void Graphics_view_zoom::set_zoom_factor_base(double value)
    {
        _zoom_factor_base = value;
    }

    QGraphicsView* Graphics_view_zoom::getView() const
    {
        return _view;
    }

    bool Graphics_view_zoom::eventFilter(QObject* object, QEvent* event)
    {
        if (event->type() == QEvent::MouseMove)
        {
            QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
            QPointF delta = target_viewport_pos - mouse_event->pos();

            if (fabs(delta.x()) > 5 || fabs(delta.y()) > 5)
            {
                target_viewport_pos = mouse_event->pos();
                target_scene_pos = _view->mapToScene(mouse_event->pos());
            }

            if (mouse_event->modifiers() == Qt::AltModifier)
            {
                auto center = _view->mapToScene(_view->viewport()->rect().center());
                auto mat = _view->matrix();
                QPointF scale(mat.m11(), mat.m22());

                //            delta = _view->scale(delta.toPoint());
                delta.setX(delta.x() / scale.x());
                delta.setY(delta.y() / scale.y());
                _view->centerOn(center.x() + delta.x(), center.y() + delta.y());
                return true;

            }
        }
        else if (event->type() == QEvent::Wheel)
        {
            QWheelEvent* wheel_event = static_cast<QWheelEvent*>(event);

            if (QApplication::keyboardModifiers() == _modifiers)
            {
                if (wheel_event->orientation() == Qt::Vertical)
                {
                    double angle = wheel_event->delta() * 0.5;
                    double factor = pow(_zoom_factor_base, angle);
                    gentle_zoom(factor);
                    return true;
                }
            }
        }
        else if (event->type() == QEvent::MouseButtonDblClick)
        {
            QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
            QGraphicsItem* item = _view->itemAt(mouse_event->pos());

            if (item)
            {
                ItemZoomer* zoomer = new ItemZoomer(_view, item, 500, item);
                connect(zoomer, SIGNAL(zoomed()), this, SIGNAL(zoomed()));
                //            _view->fitInView(item->mapRectToScene(item->boundingRect().adjusted(-50,-50,50,50)), Qt::KeepAspectRatio);
            }
        }

        Q_UNUSED(object)
        return false;
    }

    ItemZoomer::ItemZoomer(QGraphicsView* view, const QGraphicsItem* item, int time, QGraphicsItem* parent)
        : QGraphicsObject(parent)
        , mView(view)
        , mItem(item)
    {
        lt = new QTimeLine(time, this);
        br = new QTimeLine(time, this);
        brFinished = false;
        ltFinished = false;

        start = mView->mapToScene(mView->rect()).boundingRect();
        QRectF end = mItem->mapToScene(mItem->boundingRect().adjusted(-50, -50, 50, 50)).boundingRect();
        curLT = start.topLeft();
        curBR = start.bottomRight();

        ltStepSize = (end.topLeft() - start.topLeft()) / 100.0;
        brStepSize = (end.bottomRight() - start.bottomRight()) / 100.0;

        lt->setFrameRange(0, 100);
        br->setFrameRange(0, 100);

        connect(lt, SIGNAL(frameChanged(int)), SLOT(setHeight(int)));
        connect(br, SIGNAL(frameChanged(int)), SLOT(setWidth(int)));

        connect(lt, SIGNAL(finished()), SLOT(setTimeLineLTFinished()));
        connect(br, SIGNAL(finished()), SLOT(setTimeLineBRFinished()));
        lt->start();
        br->start();
    }

    ItemZoomer::~ItemZoomer()
    {

    }

    void ItemZoomer::setHeight(int height)
    {
        curLT = start.topLeft() + ltStepSize * height;
        update();
    }

    void ItemZoomer::setWidth(int width)
    {
        curBR = start.bottomRight() + brStepSize * width;
        update();
    }

    void ItemZoomer::setTimeLineBRFinished()
    {
        brFinished = true;
        deleteSelf();
    }

    void ItemZoomer::setTimeLineLTFinished()
    {
        ltFinished = true;
        deleteSelf();
    }

    void ItemZoomer::deleteSelf()
    {
        if (brFinished && ltFinished)
        {
            deleteLater();
        }

    }
    void ItemZoomer::update()
    {
        QRectF rect = mItem->mapToScene(mItem->boundingRect()).boundingRect();
        rect.setTopLeft(curLT);
        rect.setBottomRight(curBR);

        mView->fitInView(rect, Qt::KeepAspectRatio);
        emit zoomed();
    }

    QRectF ItemZoomer::boundingRect() const
    {
        return QRectF(0, 0, 0, 0);
    }

    void ItemZoomer::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
    }

} // namespace armarx



