/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QObject>
#include <QGraphicsView>
#include <QTimeLine>
#include <QGraphicsObject>
namespace armarx
{


    /*!
     * This class adds ability to zoom QGraphicsView using mouse wheel. The point under cursor
     * remains motionless while it's possible.
     *
     * Note that it becomes not possible when the scene's
     * size is not large enough comparing to the viewport size. QGraphicsView centers the picture
     * when it's smaller than the view. And QGraphicsView's scrolls boundaries don't allow to
     * put any picture point at any viewport position.
     *
     * When the user starts scrolling, this class remembers original scene position and
     * keeps it until scrolling is completed. It's better than getting original scene position at
     * each scrolling step because that approach leads to position errors due to before-mentioned
     * positioning restrictions.
     *
     * When zommed using scroll, this class emits zoomed() signal.
     *
     * Usage:
     *
     *   new Graphics_view_zoom(view);
     *
     * The object will be deleted automatically when the view is deleted.
     *
     * You can set keyboard modifiers used for zooming using set_modified(). Zooming will be
     * performed only on exact match of modifiers combination. The default modifier is Ctrl.
     *
     * You can change zoom velocity by calling set_zoom_factor_base().
     * Zoom coefficient is calculated as zoom_factor_base^angle_delta
     * (see QWheelEvent::angleDelta).
     * The default zoom factor base is 1.0015.
     */
    class Graphics_view_zoom : public QObject
    {
        Q_OBJECT
    public:
        Graphics_view_zoom(QGraphicsView* view);
        void gentle_zoom(double factor);
        void set_modifiers(Qt::KeyboardModifiers modifiers);
        void set_zoom_factor_base(double value);

        QGraphicsView* getView() const;
    private:
        QGraphicsView* _view;
        Qt::KeyboardModifiers _modifiers;
        double _zoom_factor_base;
        QPointF target_scene_pos, target_viewport_pos;
        bool eventFilter(QObject* object, QEvent* event) override;

    signals:
        void zoomed();
        void moved();
    };

    class ItemZoomer : public QGraphicsObject
    {
        Q_OBJECT
    public:
        explicit ItemZoomer(QGraphicsView* view, const QGraphicsItem* item, int time, QGraphicsItem* parent = 0);
        ~ItemZoomer() override;
    private slots:
        void setHeight(int height);
        void setWidth(int width);
        void setTimeLineBRFinished();
        void setTimeLineLTFinished();
        void deleteSelf();
    signals:
        void zoomed();
    private:
        QGraphicsView* mView;
        const QGraphicsItem* mItem;
        QTimeLine* lt;
        QTimeLine* br;
        bool brFinished, ltFinished;
        QPointF curLT, curBR;
        QPointF ltStepSize;
        QPointF brStepSize;
        QRectF start;
        void update(void);

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
    };


} // namespace armarx

