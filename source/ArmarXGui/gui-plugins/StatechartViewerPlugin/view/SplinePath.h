/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <array>

#include <QPainterPath>
#include <QPointF>

#include "../layout/GraphvizConverter.h"

namespace armarx
{

    typedef std::array<QPointF, 2> QPoint2Array;
    typedef std::array<QPointF, 4> QPoint4Array;

    class SplinePath
    {
    public:
        SplinePath();

        /**
         * @brief simplePath Constructs a cubic Beziercurve with an arbitrary number of control points.
         * @param simpleControlPoints The given control points
         * @return A cubic Beziercurve in the convex hull of the simpleControlPoints.
         */
        static QPainterPath simplePath(QPointList simpleControlPoints);
        /**
         * @brief complexPath Constructs a cubic Beziercurve with a correctly specified vector of control points.
         * @param fullControlPoints The control points of the curve. Their size needs to be equal to 1 mod 3.
         * @return Beziercurve as implied by fullControlPoints.
         */
        static QPainterPath complexPath(QPointList fullControlPoints);

    private:
        /**
         * @brief relativeDistance Factor needed for calculation of smooth transitions.
         */
        static constexpr float relativeDistance = 0.5;

        /**
         * @brief line Turns the given two points into three control points, by putting one in the middle of the
         * straight line between start and end.
         * @param start
         * @param end
         * @return The last two points of the three calculated points. That is: middle, end.
         */
        static QPoint2Array line(QPointF start, QPointF end);
        /**
         * @brief smoothTransition Takes three simple control points of which one is at the transition of two
         * partial splines. Adds one point between start and middle and one between middle and end. All given
         * and calculated points are returned except start.
         * @param start
         * @param middle
         * @param end
         * @return The last four of the five calculated points. That is: new1, middle, new2, end.
         */
        static QPoint4Array smoothTransition(QPointF start, QPointF middle, QPointF end);

        static void pathToString(QPainterPath path);
    };
} //armarx

