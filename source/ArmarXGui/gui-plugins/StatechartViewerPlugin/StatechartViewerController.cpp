/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXCore/interface/statechart/RemoteStateOffererIce.h" //RemoteStateOffererInterfacePrx

#include "StatechartViewerController.h"
#include "model/State.h"
#include "model/stateinstance/StateInstance.h"
#include "model/stateinstance/LocalState.h"
#include "view/StatechartView.h"
#include "ui_StatechartViewer.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/statechart/StateUtilFunctions.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StateItem.h>

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/stateinstance/DynamicRemoteState.h>
#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/model/stateinstance/RemoteState.h>
#include <IceUtil/UUID.h>
#include <queue>

#include <QMenu>
#include <QToolBar>

namespace armarx
{

    StatechartViewerController::StatechartViewerController()
    {
        setlocale(LC_ALL, "C");

        ui = new Ui::StatechartViewer();
        connect(this, SIGNAL(componentConnected()), this, SLOT(connectToIce()));

        qRegisterMetaType<LockableGraphPtr>("LockableGraphPtr");
        qRegisterMetaType<MediatorPtr>("MediatorPtr");
        qRegisterMetaType<size_t>("size_t");
        qRegisterMetaType<IceStateConverterPtr>("IceStateConverterPtr");
        qRegisterMetaType<StateIceBasePtr>("StateIceBasePtr");

        ui->setupUi(getWidget());
        proxyFinder = new armarx::IceProxyFinder<RemoteStateOffererInterfacePrx>(getWidget());
        ui->gridLayout_2->addWidget(proxyFinder, 0, 0, 2, 1);
        proxyFinder->setSearchMask("*RemoteStateOfferer");
        follower = new ActiveStateFollower(nullptr, getWidget());
        connect(ui->btnRefreshStateInstances, SIGNAL(clicked()), this, SLOT(updateStateComboBox()));
        connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(updateStateFollower()));
        connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(removeTab(int)));
        connect(ui->btnAddViewer, SIGNAL(clicked()), this, SLOT(addNewStateView()));
        connect(proxyFinder->getUi()->cbProxyName, SIGNAL(currentIndexChanged(QString)), this, SLOT(updateStateComboBox()), Qt::UniqueConnection);

    }

    StatechartViewerController::~StatechartViewerController()
    {
        shutdown = true;
        ScopedRecursiveLock lock(mutex);// wait for network functions to finish

    }

    void StatechartViewerController::onInitComponent()
    {
        updateTask = new PeriodicTask<StatechartViewerController>(this, &StatechartViewerController::updateStatechart, 1000, false, "StatechartViewerRefetch");

    }

    void StatechartViewerController::onConnectComponent()
    {
        qRegisterMetaType<statechartmodel::TransitionCPtr>("statechartmodel::TransitionCPtr");
        qRegisterMetaType<statechartmodel::SignalType>("statechartmodel::SignalType");
        watcher = new StateWatcher;
        getArmarXManager()->addObject(watcher, false, "StateWatcher" + IceUtil::generateUUID());
        updateTask->start();
        emit componentConnected();
        proxyFinder->setIceManager(getIceManager());
    }

    void StatechartViewerController::onDisconnectComponent()
    {

        updateTask->stop();
        getArmarXManager()->removeObjectNonBlocking(watcher->getName());
    }

    void StatechartViewerController::onExitComponent()
    {
    }

    void StatechartViewerController::loadSettings(QSettings* settings)
    {
        int size = settings->beginReadArray("StatechartViewers");
        for (int i = 0; i < size; ++i)
        {
            settings->setArrayIndex(i);
            QString proxyName = settings->value("proxyName").toString();
            QString globalStateName = settings->value("globalStateName").toString();
            delayedConverterAdding.push_back(qMakePair(proxyName, globalStateName));
        }
        settings->endArray();
    }

    void StatechartViewerController::saveSettings(QSettings* settings)
    {
        settings->beginWriteArray("StatechartViewers");
        int i = 0;
        for (RemoteStateData& data : converters)
        {
            settings->setArrayIndex(i);
            settings->setValue("globalStateName", data.globalStateName);
            settings->setValue("proxyName", data.proxyName);
            i++;
        }
        settings->endArray();
    }

    void StatechartViewerController::connectToIce()
    {
        for (QPair<QString, QString>& pair : delayedConverterAdding)
        {
            addNewStateView(pair.first, pair.second);
        }
        if (!ui->tabWidget->currentStateview())
        {
            addNewStateView("RobotControlStateOfferer", "RobotStatechart->RobotControl");
        }

        ui->tabWidget->currentStateview()->viewAll();
    }

    void StatechartViewerController::updateStatechartView(StateIceBasePtr stateptr, IceStateConverterPtr converter)
    {
        ScopedRecursiveLock lock(mutex);
        auto start = IceUtil::Time::now();
        converter->convert(stateptr);
        ARMARX_DEBUG << deactivateSpam(3) << "converting instance took : " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
    }

    void StatechartViewerController::updateStatechart()
    {
        if (shutdown)
        {
            return;
        }
        QMap<QPointer<StatechartView>, RemoteStateData> tempConverters;
        {
            ScopedRecursiveLock lock(mutex);
            tempConverters = converters;
        }
        for (RemoteStateData& stateData : tempConverters)
        {
            RemoteStateOffererInterfacePrx statechartHandler;
            try
            {
                statechartHandler = getProxy<RemoteStateOffererInterfacePrx>(stateData.proxyName.toStdString(), false, "", false);
                if (!statechartHandler)
                {
                    continue;
                }
                auto start = IceUtil::Time::now();


                start = IceUtil::Time::now();

                auto asyncResult = statechartHandler->begin_getStatechartInstanceByGlobalIdStr(stateData.globalStateName.toStdString());
                while (!asyncResult->isCompleted())
                {
                    if (shutdown)
                    {
                        return;
                    }
                    usleep(100);
                    //                    qApp->processEvents();
                }
                armarx::StateIceBasePtr stateptr = statechartHandler->end_getStatechartInstanceByGlobalIdStr(asyncResult);
                if (!stateptr)
                {
                    ARMARX_WARNING_S << "Could not find state with name " << stateData.globalStateName.toStdString();
                    continue;
                }
                //            armarx::StateIceBasePtr stateptr = m_statechartHandler->getStatechartInstance(stateMap.begin()->first);
                ARMARX_DEBUG << deactivateSpam(3) << " took : " << (IceUtil::Time::now() - start).toMilliSecondsDouble() << " ms";
                QMetaObject::invokeMethod(this, "updateStatechartView", Q_ARG(StateIceBasePtr, stateptr), Q_ARG(IceStateConverterPtr, stateData.converter));
                //                stateData.converter.convert(stateptr);

            }
            catch (Ice::NotRegisteredException&)
            {
                getIceManager()->removeProxyFromCache(statechartHandler);
                ARMARX_ERROR << deactivateSpam(10) << eERROR << "No ice ID 'RobotControlStateOfferer' registered" << flush;
            }
            catch (...)
            {
                handleExceptions();
            }
        }
    }

    void StatechartViewerController::displayParameters(statechartmodel::StateInstancePtr selectedStateInstance)
    {
        try
        {
            QList<QTreeWidgetItem*> items;
            if (selectedStateInstance && watcher)
            {
                auto paramContainerTypes = {"inputParameters", "localParameters", "outputParameters"};
                ui->paramView->clear();


                for (auto& type : paramContainerTypes)
                {
                    QTreeWidgetItem* item = new QTreeWidgetItem();
                    item->setText(0, type);
                    StateParameterMap map;
                    try
                    {
                        map = watcher->getStateParameterMap(selectedStateInstance->getStateClass(), type);
                    }
                    catch (...)
                    {
                        handleExceptions();
                        continue;
                    }



                    for (auto& elem : map)
                    {
                        QTreeWidgetItem* subItem = new QTreeWidgetItem();
                        StateParameterIceBasePtr param = elem.second;
                        subItem->setText(0, elem.first.c_str());
                        subItem->setText(1, param->value->output().c_str());
                        item->addChild(subItem);

                    }
                    items.push_back(item);
                    //                    inputParamsJson->setVariant(type, var);

                }
                ui->paramView->addTopLevelItems(items);
                for (auto& item : items)
                {
                    ui->paramView->expandItem(item);
                }
            }
            else
            {
                ARMARX_INFO << "no state instance!";
            }
            if (items.size() == 0)
            {
                QTreeWidgetItem* item = new QTreeWidgetItem();
                item->setText(0, "No data available yet");
                ui->paramView->addTopLevelItem(item);
            }

        }
        catch (...)
        {
            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setText(0, "No data available yet");
            ui->paramView->addTopLevelItem(item);
            handleExceptions();
        }
    }

    void StatechartViewerController::addNewStateView(QString proxyName, QString stateName)
    {
        auto view = ui->tabWidget->addEmptyStateTab();
        if (!view)
        {
            return;
        }
        connect(view, SIGNAL(selectedStateChanged(statechartmodel::StateInstancePtr)), this, SLOT(displayParameters(statechartmodel::StateInstancePtr)));
        converters[view].converter.reset(new IceStateConverter());
        converters[view].converter->setStateWatcher(watcher);
        view->setState(converters[view].converter->getTopState()->getStateClass());
        converters[view].proxyName = proxyName;
        converters[view].globalStateName = stateName;
        QString instanceName;
        if (stateName.lastIndexOf("->") >= 0)
        {
            instanceName = stateName.right(stateName.size() - stateName.lastIndexOf("->") - 2);
        }
        else
        {
            instanceName = stateName;
        }
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), instanceName);

        view->getLayoutController().enableLayouting();
        updateStatechart();
        connectToStateTab(ui->tabWidget->currentIndex());

    }

    void StatechartViewerController::addNewStateView()
    {
        addNewStateView(proxyFinder->getSelectedProxyName(), ui->comboBoxStateInstances->currentText());
    }

    void StatechartViewerController::followActiveState()
    {
        auto view = ui->tabWidget->currentStateview();
        if (!view)
        {
            return;
        }
        follower->setStatechartView(view);
    }

    void StatechartViewerController::updateStateComboBox()
    {
        if (proxyFinder->getSelectedProxyName().isEmpty())
        {
            return;
        }
        if (getState() < eManagedIceObjectStarting)
        {
            return;
        }
        auto proxy = getProxy<RemoteStateOffererInterfacePrx>(proxyFinder->getSelectedProxyName().toStdString(), false, "", false);

        ui->comboBoxStateInstances->clear();
        if (proxy)
        {
            QStringList stateInstances;
            for (auto& elem : proxy->getAvailableStateInstances())
            {
                stateInstances << (elem.second.c_str());
            }
            stateInstances.removeDuplicates();
            ui->comboBoxStateInstances->addItems(stateInstances);
        }
        ui->btnAddViewer->setEnabled(ui->comboBoxStateInstances->count() > 0);
    }

    void StatechartViewerController::updateStateFollower()
    {
        auto view = ui->tabWidget->currentStateview();
        if (!view)
        {
            return;
        }
        follower->setStatechartView(view);
    }

    void StatechartViewerController::removeTab(int index)
    {
        QPointer<StatechartView> view = qobject_cast<StatechartView*>(ui->tabWidget->widget(index));
        if (view)
        {
            converters.remove(view);
        }
        else
        {
            ARMARX_INFO_S << "View already NULL";
        }
    }

    QPointer<QWidget> StatechartViewerController::getCustomTitlebarWidget(QWidget* parent)
    {
        if (customToolbar)
        {
            if (parent != customToolbar->parent())
            {
                customToolbar->setParent(parent);
            }

            return qobject_cast<QToolBar*>(customToolbar);
        }

        customToolbar = new QToolBar(parent);
        customToolbar->setIconSize(QSize(16, 16));
        autoFollowAction = customToolbar->addAction(QIcon(":/icons/magic-wand-icon.png"), "Auto follow active state");
        autoFollowAction->setCheckable(true);
        autoFollowAction->setChecked(true);
        follower->startFollowing();
        connect(autoFollowAction, SIGNAL(toggled(bool)), follower, SLOT(toggle(bool)));

        centerActiveStateAction = customToolbar->addAction(QIcon(":/icons/zoom-original-2.png"), "Center on active state");
        connect(centerActiveStateAction, SIGNAL(triggered(bool)), follower, SLOT(centerOnCurrentState(bool)));
        return qobject_cast<QToolBar*>(customToolbar);
    }

    void StatechartViewerController::connectToStateTab(int index)
    {
        ARMARX_INFO << "Trying to connect to " << index;
        if (index != -1 && ui->tabWidget->currentStateview() && ui->tabWidget->currentStateview()->getScene())
        {
            connect(ui->tabWidget->currentStateview()->getScene(), SIGNAL(transitionContextMenuRequested(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)),
                    this, SLOT(showTransitionContextMenu(statechartmodel::TransitionCPtr, statechartmodel::StatePtr, QPoint, QPointF)),  Qt::UniqueConnection);
        }
    }

    void StatechartViewerController::showTransitionContextMenu(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos, QPointF mouseItemPos)
    {
        ARMARX_DEBUG_S << "pos: " << mouseItemPos;
        QMenu menu;
        QAction* triggerTransition = menu.addAction("Trigger Transition " + transition->eventName);
        if (transition->sourceState != state->getActiveSubstate())
        {
            triggerTransition->setEnabled(false);
        }


        QAction* result = menu.exec(mouseScreenPos);




        if (result == triggerTransition)
        {
            ScopedRecursiveLock lock(mutex);

            for (RemoteStateData& data : converters)
            {
                std::set<std::string> proxyNames;
                std::string globalId;
                auto states = data.converter->getCompleteStateMap();
                for (auto& statepair : states)
                {
                    proxyNames.insert(data.proxyName.toStdString());
                    if (state == statepair.second.first->getStateClass())
                    {
                        globalId = statepair.first;
                        ARMARX_DEBUG << "Found state " << statepair.first;
                    }
                    StateIceBasePtr iceState = statepair.second.second;
                    RemoteStateIceBasePtr remoteState = RemoteStateIceBasePtr::dynamicCast(iceState);
                    if (remoteState)
                    {
                        proxyNames.insert(remoteState->proxyName);
                    }
                }
                if (!globalId.empty())
                {
                    RemoteStateOffererInterfacePrx correctOffererproxy;
                    ARMARX_IMPORTANT << VAROUT(Ice::StringSeq(proxyNames.begin(), proxyNames.end()));
                    std::map<RemoteStateOffererInterfacePrx, Ice::AsyncResultPtr> proxyResultPtrs;
                    for (auto proxyName : proxyNames)
                    {
                        RemoteStateOffererInterfacePrx proxy;
                        try
                        {
                            proxy = getProxy<RemoteStateOffererInterfacePrx>(proxyName, false, "", false);
                            if (proxy)
                            {
                                proxyResultPtrs[proxy] = proxy->begin_isHostOfStateByGlobalIdStr(globalId);
                            }
                        }
                        catch (...)
                        {
                            getArmarXManager()->getIceManager()->removeProxyFromCache(proxy);
                        }
                    }
                    for (auto& proxyPair : proxyResultPtrs)
                    {
                        RemoteStateOffererInterfacePrx proxy;
                        try
                        {
                            proxy = proxyPair.first;
                            if (proxy->end_isHostOfStateByGlobalIdStr(proxyPair.second))
                            {
                                correctOffererproxy = proxy;
                                break;
                            }

                        }
                        catch (...)
                        {
                            getArmarXManager()->getIceManager()->removeProxyFromCache(proxy);
                        }
                    }

                    if (correctOffererproxy)
                    {
                        ARMARX_INFO << "Sending event " << transition->eventName << " to " << transition->sourceState->getInstanceName();
                        correctOffererproxy->begin_issueEventWithGlobalIdStr(globalId, new Event(transition->sourceState->getInstanceName().toStdString(), transition->eventName.toStdString()));
                    }
                }
            }
        }

    }

}
