/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Mirko Waechter
* @author     (waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <QTimer>

#include "ArmarXCore/interface/statechart/RemoteStateOffererIce.h" //RemoteStateOffererInterfacePrx

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include "IceStateConverter.h"
#include "StateWatcher.h"
#include "view/StatechartView.h"
#include "layout/LayoutThread.h"

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/controller/ActiveStateFollower.h>

namespace Ui
{
    class StatechartViewer;
}

namespace armarx
{


    template <typename ProxyType>
    class IceProxyFinder;

    class StatechartView;
    /*!
     \class StatechartViewerController
     \ingroup ArmarXGui-ArmarXGuiPlugins ArmarXGuiPlugins
     \see StatechartViewerGuiPlugin
    */
    class StatechartViewerController :
        public ArmarXComponentWidgetControllerTemplate<StatechartViewerController>
    {
        Q_OBJECT

    public:
        StatechartViewerController();
        ~StatechartViewerController() override;
        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "Statecharts.StatechartViewer";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/statechartviewer.svg");
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        // ArmarXWidgetController interface
        //        QPointer<QWidget> getWidget();
        void updateStatechart();
    public slots:

        void connectToStateTab(int index);
        void showTransitionContextMenu(statechartmodel::TransitionCPtr transition, statechartmodel::StatePtr state, QPoint mouseScreenPos, QPointF mouseItemPos);
    signals:
        void componentConnected();

    private slots:
        void connectToIce();
        void updateStatechartView(StateIceBasePtr stateptr, IceStateConverterPtr converter);
        void displayParameters(statechartmodel::StateInstancePtr selectedStateInstance);
        void addNewStateView(QString proxyName, QString stateName);
        void addNewStateView();
        void followActiveState();
        void updateStateComboBox();
        void updateStateFollower();
        void removeTab(int index);
    private:
        Ui::StatechartViewer* ui;
        armarx::IceProxyFinder<RemoteStateOffererInterfacePrx>* proxyFinder;
        RemoteStateOffererInterfacePrx m_statechartHandler;
        StateWatcherPtr watcher;
        PeriodicTask<StatechartViewerController>::pointer_type updateTask;
        struct RemoteStateData
        {
            IceStateConverterPtr converter;
            QString proxyName;
            QString globalStateName;
        };

        QMap<QPointer<StatechartView>, RemoteStateData> converters;
        QVector<QPair<QString, QString>> delayedConverterAdding;
        ActiveStateFollower* follower;
        QPointer<QToolBar> customToolbar;
        QAction* autoFollowAction;
        QAction* centerActiveStateAction;
        RecursiveMutex mutex;
        bool shutdown = false;
        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
    };

}

