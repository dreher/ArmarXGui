/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Layout.h"
#include "LayoutController.h" //for MediatorPtr
#include "LayoutWorker.h"

#include <QObject>

#include <map>
#include <memory>

namespace armarx
{

    typedef std::unique_ptr<LayoutWorker> LayoutWorkerPtr;
    typedef std::map<size_t, LayoutWorkerPtr> WorkerMap;

    class LayoutWorkerCreator;
    typedef std::unique_ptr<LayoutWorkerCreator> LayoutWorkerCreatorPtr;

    class LayoutWorkerCreator : public QObject
    {
        Q_OBJECT

    public:
        LayoutWorkerCreator();

    signals:
        /**
         * @brief connectedWorkerAndMediator Notifies that the worker's signals and slots are now
         * connected to its mediator.
         * @param id The id of the worker and mediator.
         */
        void connectedWorkerAndMediator(size_t id);

    public slots:
        /**
         * @brief createWorker Create worker with given id and connect it to mediator via signals and slots.
         * @param mediator Manages the communication between the new worker and the associated state.
         * @param id The mediator's and worker's id.
         */
        void createWorker(MediatorPtr mediator, size_t id, QString name);

        /**
         * @brief deleteWorker Calls the destructor of the worker with the given id.
         * @param id ID of the worker that is to be deleted.
         */
        void deleteWorker(size_t id);

        /**
         * @brief deleteWorkers Calls the destructor of every worker.
         */
        void deleteWorkers();

    private:
        /**
         * @brief workers Maps the ids to the workers.
         */
        WorkerMap workers;

        /**
         * @brief graphvizContext Needed for layouting the graphs.
         */
        GvcPtr graphvizContext;
    };
} //namespace armarx

