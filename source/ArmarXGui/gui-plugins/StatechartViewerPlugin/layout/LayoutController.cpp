/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "../model/State.h"
#include "LayoutController.h"
#include "StateModelLayoutMediator.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <cassert>
#include <memory>
#include <utility>

armarx::LayoutController::LayoutController(armarx::statechartmodel::StatePtr topState, bool startEnabled)
    : QObject(),
      idCounter(0),
      layoutingDisabled(!startEnabled)
{
    qRegisterMetaType<size_t>("size_t");
    timer.setInterval(100);
    connect(&timer, SIGNAL(timeout()), this, SLOT(startNextLayouting()), Qt::QueuedConnection);

    if (topState)
    {
        createMediator(topState);
    }
    else
    {
        //        ARMARX_ERROR_S << "No state provided for constructor of LayoutController";
    }
}

void armarx::LayoutController::setTopState(armarx::statechartmodel::StatePtr topState)
{
    //here (and not at the end of the function) because signals for the layout thread are queued.
    //This garanties that the WorkerCreator first deletes all current states and then adds the
    //worker for the new topState.
    emit reset();

    mediators.clear();
    states.clear();
    idCounter = 0;

    createMediator(topState);
    timer.start();
}

void armarx::LayoutController::createAllWorkers()
{
    for (auto med : mediators)
    {
        emit createWorker(med.second, med.first, med.second->getState()->getStateName());
    }
}

size_t armarx::LayoutController::getStateId(statechartmodel::StatePtr state) const
{
    for (const auto& elem : mediators)
    {
        if (elem.second->getState().get() == state.get())
        {
            return elem.first;
        }
    }

    return -1;
}

void armarx::LayoutController::enableLayouting(bool enable)
{
    layoutingDisabled = !enable;
}

void armarx::LayoutController::scheduleMediator(size_t mediatorId, bool layoutAll)
{
    MediatorLayoutOptionPair correctPair(mediatorId, layoutAll);
    MediatorLayoutOptionPair inversePair(mediatorId, !layoutAll);

    size_t count = layoutQueue.count(correctPair) + layoutQueue.count(inversePair);
    assert(count <= 2);

    if (count == 0) //worker not yet scheduled
    {
        layoutQueue.append(correctPair);
    }
    else if (layoutAll && layoutQueue.contains(inversePair))
        //worker is scheduled, but only supposed to layout edges
        //but now it should also layout the nodes
    {
        layoutQueue.removeAll(inversePair);
        layoutQueue.removeAll(correctPair);
        layoutQueue.append(correctPair);
    }
}

bool armarx::LayoutController::layoutNow(size_t mediatorId, bool layoutAll)
{
    auto iter = mediators.find(mediatorId);

    if (iter != mediators.end())
    {
        if (iter->second->getState()->getSubstates().size() > 0)
        {
            iter->second->startLayouting(layoutAll, 1);
            return true;
        }
        return false;
    }
    else
    {
        ARMARX_ERROR_S << "Mediator with id " << mediatorId << " is scheduled but no mediator"
                       << " with this id could be found.";
        return false;
    }
}

void armarx::LayoutController::startNextLayouting()
{
    if (layoutingDisabled)
    {
        //        ARMARX_INFO_S << "Layouting disabled!";
        return;
    }

    //    ARMARX_WARNING_S << "Layouting activated";
    timer.stop();
    if (!(layoutQueue.empty()))
    {
        auto nextMediator = layoutQueue.takeFirst();
        size_t mediatorId = nextMediator.first;
        bool layoutAll = nextMediator.second;
        if (!layoutNow(mediatorId, layoutAll))
        {
            QMetaObject::invokeMethod(this, "startNextLayouting");
        }

    }
    else
    {
        timer.start();
    }
}

void armarx::LayoutController::deleteMediator(size_t mediatorId)
{
    //remove from layoutQueue
    MediatorLayoutOptionPair truePair {mediatorId, true};

    while (layoutQueue.contains(truePair))
    {
        layoutQueue.removeAt(layoutQueue.indexOf(truePair));
    }

    MediatorLayoutOptionPair falsePair {mediatorId, false};

    while (layoutQueue.contains(falsePair))
    {
        layoutQueue.removeAt(layoutQueue.indexOf(falsePair));
    }

    //remove from states
    auto iter = mediators.find(mediatorId);

    if (iter != mediators.end())
    {
        int statePos = states.indexOf(iter->second->getState());

        if (statePos != -1)
        {
            states.removeAt(statePos);
        }

        //remove from mediators, destructor called by shared_ptr if necessary
        mediators.erase(mediatorId);
    }
    else
    {
        ARMARX_INFO_S << "Mediator with id " << mediatorId << " was already deleted";
    }
}

void armarx::LayoutController::potentialStateAdded(armarx::statechartmodel::StateInstancePtr substate, armarx::statechartmodel::SignalType signalType)
{
    if (signalType == statechartmodel::eAdded)
    {
        statechartmodel::StatePtr addedState = substate->getStateClass();

        if (addedState)
        {
            if (states.contains(addedState))
            {
                return;
            }

            createMediator(addedState);
        }
    }
}

void armarx::LayoutController::createMediator(armarx::statechartmodel::StatePtr state)
{
    //    ARMARX_INFO_S << "Creating mediator for state " << state->getStateName();
    MediatorPtr mediator {new StateModelLayoutMediator(state, idCounter)};
    idCounter++;

    qRegisterMetaType<statechartmodel::TransitionCPtr>("statechartmodel::TransitionCPtr");
    qRegisterMetaType<statechartmodel::StateInstancePtr>("statechartmodel::StateInstancePtr");
    qRegisterMetaType<statechartmodel::SignalType>("statechartmodel::SignalType");
    qRegisterMetaType<SupportPoints>("SupportPoints");
    qRegisterMetaType<QPointPtr>("QPointPtr");
    qRegisterMetaType<FloatPtr>("FloatPtr");

    //state und mediator verbinden
    bool correctConnected = connect(state.get(), SIGNAL(stateDeleted()),
                                    mediator.get(), SLOT(stateDeleted()), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal stateDeleted was not connected to slot stateDeleted of mediator";
    }

    correctConnected = connect(state.get(), SIGNAL(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)),
                               mediator.get(), SLOT(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal substateChanged was not connected to slot substateChanged of mediator";
    }

    correctConnected = connect(state.get(), SIGNAL(stateChanged(statechartmodel::SignalType)),
                               mediator.get(), SLOT(stateChanged(statechartmodel::SignalType)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal stateChanged was not connected to slot stateChanged of mediator";
    }

    correctConnected = connect(state.get(), SIGNAL(transitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)),
                               mediator.get(), SLOT(transitionChanged(statechartmodel::TransitionCPtr, statechartmodel::SignalType)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal transitionChanged was not connected to slot transitionChanged of mediator";
    }

    correctConnected = connect(mediator.get(), SIGNAL(supportPointsChanged(statechartmodel::TransitionCPtr, SupportPoints, QPointPtr, FloatPtr)),
                               state.get(), SLOT(setTransitionSupportPoints(statechartmodel::TransitionCPtr, SupportPoints, QPointPtr, FloatPtr)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal supportPointsChanged of mediator"
                       << " was not connected to slot setTransitionSupportPoints";
    }

    //Controller und mediator verbinden
    correctConnected = connect(mediator.get(), SIGNAL(layoutingFinished()),
                               this, SLOT(startNextLayouting()), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal layoutingFinished of mediator"
                       << " was not connected to slot startNextLayouting of controller";
    }

    correctConnected = connect(mediator.get(), SIGNAL(deleteMe(size_t)),
                               this, SLOT(deleteMediator(size_t)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal deleteMe of mediator"
                       << " was not connected to slot deleteMediator of controller";
    }

    correctConnected = connect(mediator.get(), SIGNAL(scheduleMe(size_t, bool)),
                               this, SLOT(scheduleMediator(size_t, bool)));

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal scheduleMe of mediator"
                       << " was not connected to slot scheduleMediator of controller";
    }

    correctConnected = connect(mediator.get(), SIGNAL(substateFound(statechartmodel::StateInstancePtr, statechartmodel::SignalType)),
                               this, SLOT(potentialStateAdded(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal substateFound of mediator"
                       << " was not connected to slot potentialStateAdded of controller";
    }

    //controller und state verbinden
    correctConnected = connect(state.get(), SIGNAL(substateChanged(statechartmodel::StateInstancePtr, statechartmodel::SignalType)),
                               this, SLOT(potentialStateAdded(statechartmodel::StateInstancePtr, statechartmodel::SignalType)), Qt::QueuedConnection);

    if (!correctConnected)
    {
        ARMARX_ERROR_S << "Signal substateChanged of state was not connected to slot"
                       << " potentialStateAdded of controller";
    }

    mediators.insert(std::pair<size_t, MediatorPtr>(mediator -> getID(), mediator));
    states.push_back(state);

    emit createWorker(mediator, mediator->getID(), state->getStateName());
    QMetaObject::invokeMethod(this, "scheduleMediator", Qt::QueuedConnection, Q_ARG(size_t, mediator->getID()), Q_ARG(bool, true));
}
