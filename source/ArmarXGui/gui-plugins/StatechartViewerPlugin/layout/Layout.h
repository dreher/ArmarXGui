/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/Synchronization.h>
#include "graphviz/gvc.h"
#ifdef WITH_CGRAPH
#  include <graphviz/cgraph.h>
#else
#  include <graphviz/graph.h>
#endif

#include <memory>
#include <mutex>

namespace armarx
{

    typedef GVC_t* GvcPtr;

    typedef Agraph_t* GraphPtr;
    typedef Agnode_t* NodePtr;
    typedef Agedge_t* EdgePtr;

    /**
     * @brief The LockableGraph struct allows for easy locking of a graphviz graph and its components.
     */
    struct LockableGraph
    {
        LockableGraph(GraphPtr g = NULL)
            : graph(g),
              mutex()
        {}

        /**
         * @brief graph Graph that can be locked with mutex.
         */
        GraphPtr graph;
        /**
         * @brief mutex Mutex to lock graph
         */
        RecursiveMutex mutex;
    };
    typedef std::shared_ptr<LockableGraph> LockableGraphPtr;

}

