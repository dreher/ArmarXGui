/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Layout.h"
#include "LayoutWorkerCreator.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <utility>


armarx::LayoutWorkerCreator::LayoutWorkerCreator()
    : QObject()
{
    //    aginit();
    ARMARX_INFO_S << "Graphviz initialized";
    qRegisterMetaType<MediatorPtr>("MediatorPtr");
    qRegisterMetaType<LockableGraphPtr>("LockableGraphPtr");

    graphvizContext = gvContext();
}

void armarx::LayoutWorkerCreator::createWorker(armarx::MediatorPtr mediator, size_t id, QString name)
{
    LayoutWorkerPtr worker {new LayoutWorker(id, name, graphvizContext)};

    //auch StateInstancePtr, TransitionCPtr etc. registrieren?

    //mediator und worker verbinden
    bool correctConnect = connect(mediator.get(), SIGNAL(mediatorDeleted()),
                                  worker.get(), SLOT(stateDeleted()), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal mediatorDeleted of mediator " << mediator->getID() << " was not successfully connected "
                       << "to slot stateDeleted of worker " << id;
    }

    correctConnect = connect(mediator.get(), SIGNAL(layout(bool)),
                             worker.get(), SLOT(layout(bool)), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal layout of mediator " << mediator->getID() << " was not successfully connected "
                       << "to slot layout of worker " << id;
    }

    correctConnect = connect(worker.get(), SIGNAL(buildGraph(LockableGraphPtr)),
                             mediator.get(), SLOT(buildUpGraph(LockableGraphPtr)), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal buildGraph of worker " << id << " was not successfully connected "
                       << "to slot buildUpGraph of mediator " << mediator->getID();
    }

    correctConnect = connect(worker.get(), SIGNAL(layoutingFinished()),
                             mediator.get(), SLOT(workerFinishedLayouting()), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal layoutingFinished of worker " << id << " was not successfully connected "
                       << "to slot workerFinishedLayouting of mediator " << mediator->getID();
    }

    //worker und WorkerCreator verbinden
    correctConnect = connect(worker.get(), SIGNAL(deleteMe(size_t)),
                             this, SLOT(deleteWorker(size_t)), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal deleteMe of worker " << id << " was not successfully connected "
                       << "to slot deleteWorker of workerCreator";
    }

    correctConnect = connect(this, SIGNAL(connectedWorkerAndMediator(size_t)),
                             worker.get(), SLOT(isConnected(size_t)), Qt::QueuedConnection);

    if (!correctConnect)
    {
        ARMARX_ERROR_S << "Signal connectedWorkerAndMediator of workerCreator was not successfully connected "
                       << "to slot isConnected of worker " << id;
    }

    //causes the associated mediator to build up the graph and to let himself be scheduled
    emit connectedWorkerAndMediator(id);

    workers.insert(std::pair<size_t, LayoutWorkerPtr>(id, std::move(worker)));
}

void armarx::LayoutWorkerCreator::deleteWorker(size_t id)
{
    workers.erase(id);
}

void armarx::LayoutWorkerCreator::deleteWorkers()
{
    workers.clear();
}
