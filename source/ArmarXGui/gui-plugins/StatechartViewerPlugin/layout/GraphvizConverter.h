/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <string>
#include <vector>

#include <QPainterPath>
#include <QPointF>

#include "../model/Transition.h"

#define GV_DPI 72.0f

namespace armarx
{

    typedef std::vector<std::string> tokenVector;
    typedef std::vector<SupportPoints> splineVector;

    class GraphvizConverter
    {
    public:
        GraphvizConverter();

        static float convertToFloat(const std::string& graphvizNumber);
        static std::string convertFromFloat(float number);

        static QPointF convertToPoint(std::string graphvizPoint);
        static std::string convertFromPoint(QPointF point);

        static QRectF convertToRectangle(const std::string& graphvizPoint);


        static SupportPoints convertToSpline(const std::string& graphVizSplineType);
        static std::string convertFromSpline(SupportPoints spline);

    private:
        static SupportPoints tokenize(std::string graphVizSpline);
        static tokenVector splitBy(std::string toSplit, std::string divider);
        static SupportPoints mergeSplines(splineVector toMerge);
    };
} //namespace armarx

