/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "LayoutController.h"
#include "LayoutWorkerCreator.h"
#include "LayoutThread.h"

#include <utility>

armarx::LayoutThread::LayoutThread(armarx::statechartmodel::StatePtr state, bool startEnabled)
    : QObject(),
      controller(state, startEnabled)
{
    if (state)
    {
        setState(state);
    }
}

armarx::LayoutThread::~LayoutThread()
{
    //    exit();
    //    wait();
}

void armarx::LayoutThread::setState(armarx::statechartmodel::StatePtr state)
{
    //    quit();
    topState = state;
    controller.setTopState(state);
}

void armarx::LayoutThread::run()
{
    //construct LayoutWorkerCreator
    workerCreator.reset(new LayoutWorkerCreator());

    //connect controller and workerCreator
    connect(&controller, SIGNAL(createWorker(MediatorPtr, size_t, QString)),
            workerCreator.get(), SLOT(createWorker(MediatorPtr, size_t, QString)), Qt::QueuedConnection);

    connect(&controller, SIGNAL(reset()),
            workerCreator.get(), SLOT(deleteWorkers()), Qt::QueuedConnection);

    //für alle mediator die der controller zu diesem Zeitpunkt schon hat (das ist mindestens der
    //topState) müssen worker kreiert werden
    controller.createAllWorkers();

    //    exec();
}
