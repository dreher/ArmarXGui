/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <cassert>
#include <cmath>

#include <Eigen/Core>

#include "Beziercurve.h"
#include "BSpline.h"

armarx::BSpline::BSpline(armarx::pointVector& controlPoints, size_t degree)
{
    assert((controlPoints.size() - 1) % degree == 0);

    for (size_t i = 0; i < controlPoints.size() - 1; i += degree)
    {
        pointVector partialControlPoints;

        for (size_t j = i; j <= i + degree; j++)
        {
            partialControlPoints.push_back(controlPoints.at(j));
        }

        Beziercurve singleSpline {partialControlPoints};
        partialSplines.push_back(singleSpline);
    }
}

Eigen::VectorXf armarx::BSpline::evaluate(float t) const
{
    //t >= 0 && t <= number of partial splines
    //more complicated to account for floating point inaccuracies
    assert((t >= 0) && (t < maxEvalFloat()));

    size_t indexOfPartSpline = static_cast<size_t>(std::floor(t));
    return partialSplines.at(indexOfPartSpline).evaluate(t - indexOfPartSpline);
}

float armarx::BSpline::maxEvalFloat() const
{
    return static_cast<float>(partialSplines.size()) + std::numeric_limits<float>::epsilon();
}
