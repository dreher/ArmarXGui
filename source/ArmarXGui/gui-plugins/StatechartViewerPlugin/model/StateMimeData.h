/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "State.h"
#include <QMimeData>

namespace armarx
{
    /**
     * @brief The AbstractStateMimeData class is used to transport state data from
     * the treeview to the stateview and is implemented in the editor plugin.
     * isInSameGroup is abstract because it can only be implemented in the editor plugin,
     * because the states do not know anything about statechart groups.
     */
    class AbstractStateMimeData : public QMimeData
    {
        Q_OBJECT
    public:
        AbstractStateMimeData(statechartmodel::StatePtr state);

        // QMimeData interface

        bool hasFormat(const QString& mimetype) const override;
        QStringList formats() const override;

        statechartmodel::StatePtr getState() const;
        void setState(const statechartmodel::StatePtr& value);

        const QString& getProxyName() const;
        void setProxyName(const QString& value);
        virtual bool isInSameGroup(statechartmodel::StatePtr state) const = 0;
        virtual bool isPublic() const = 0;
    protected:
        QVariant retrieveData(const QString& mimetype, QVariant::Type preferredType) const override;
        statechartmodel::StatePtr state;
        QString proxyName;

    };

}

