/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXGui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once

#include "DynamicRemoteState.h"
#include "LocalState.h"
#include "EndState.h"
#include "RemoteState.h"

namespace armarx
{

    namespace statechartmodel
    {
        class StateInstanceFactory
        {
        public:
            static StateInstancePtr CreateFromIceType(eStateType type, StatePtr stateClass, const QString& instanceName, StatePtr parentState = StatePtr(), const QString& proxyName = "")
            {
                switch (type)
                {
                    case eNormalState:
                        return StateInstancePtr(new LocalState(stateClass, instanceName, parentState));
                    case eFinalState:
                        return StateInstancePtr(new EndState(instanceName, instanceName, parentState));
                    case eRemoteState:
                        return StateInstancePtr(new RemoteState(stateClass, proxyName, instanceName, parentState));
                    case eDynamicRemoteState:
                        return StateInstancePtr(new DynamicRemoteState(stateClass, instanceName, parentState));
                    case eUndefined:
                    default:
                        throw LocalException("No StateInstanceFactory available for this type: ") << type;
                }
            }
        };
    }
}
