/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "StateInstance.h"

namespace armarx
{
    namespace statechartmodel
    {

        class EndState : public StateInstance
        {
        public:
            EndState(QString instanceName, QString eventName, StatePtr parentState = StatePtr());

            void setInstanceName(const QString& value) override;
            QString getEventName() const;
            void setEventName(const QString& newEventName);

            void accept(Visitor& visitor) const override;
        signals:
            eStateType getType() const override;
        protected:
            QString eventName;
        };

        typedef boost::shared_ptr<EndState> EndStatePtr;
        typedef boost::shared_ptr<const EndState> EndStateCPtr;
    }
}


