/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QObject>
#include <QRectF>
#include <QString>
#include <QMetaType>

#include <boost/shared_ptr.hpp>
#include <boost/smart_ptr/enable_shared_from_this.hpp>

#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx
{

    namespace statechartmodel
    {
        class State;
        typedef boost::shared_ptr<State> StatePtr;
        typedef boost::shared_ptr<const State> StateCPtr;

        class Visitor;

        class StateInstance :
            public QObject,
            public boost::enable_shared_from_this<StateInstance>,
            public Logging
        {
            Q_OBJECT
        public:
            StateInstance(const QString& instanceName, StatePtr parentState = StatePtr());
            QString getInstanceName() const;
            virtual void setInstanceName(const QString& value);

            virtual StatePtr getStateClass() const
            {
                return StatePtr();
            }
            StatePtr getParent() const
            {
                return parentState.lock();
            }
            /**
             * @brief getTopLeft returns the topleft point of the stateinstance in the
             * coordinate system of the parent state.
             *
             */
            const QPointF& getTopLeft() const
            {
                return position;
            }
            const QPointF getCenter() const;

            /**
             * @brief getBoundingSquare return the maximum bounding box of this state instance
             * in parent coordinate system
             * @return
             * @see getBounds()
             */
            QRectF getBoundingSquare() const;

            /**
             * @brief getBounds returns the rectangle occupied by this state instance in
             * the parent coordinate system.
             * @see getBoundingBox()
             */
            QRectF getBounds() const
            {
                return QRectF(position, getClassSize() * getScale());
            }

            float getBoundingSquareSize() const
            {
                return boundingSquareSize;
            }

            /**
             * @brief getScale returns the scale of this state instance, which is the relation between
             * boundingSquareSize and classSize. Resizing is only done with this scale at the moment.
             *
             */
            float getScale() const;

            QSizeF getClassSize() const;

            virtual eStateType getType() const = 0;

            virtual void accept(Visitor& visitor) const = 0;

            static const QSizeF StateDefaultSize;

            QPointF adjustPosition(QPointF& newPos) const;

            bool isActive();

            /*!
              * @brief input values of this state instance, only used by Viewer.
              * */
            ::armarx::StateParameterMap inputParameters;
            ::armarx::StateParameterMap localParameters;
            ::armarx::StateParameterMap outputParameters;
            const int defaultBoundingSquareSize = 100;
        public slots:
            //            void setActive(bool status);
            void setPosition(QPointF newPosition);
            void setCenter(const QPointF& newStateCenter);
            void setBoundingBox(float squareSize);
            void updateScale();
            //        signals:
            //            void instanceResized();
        protected:
            QString name;
            boost::weak_ptr<State> parentState;

            QPointF position;
            float boundingSquareSize;
        };
        typedef boost::shared_ptr<StateInstance> StateInstancePtr;
    }
}
Q_DECLARE_METATYPE(armarx::statechartmodel::StateInstancePtr)
