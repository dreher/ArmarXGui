/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateInstance.h"

using namespace armarx;
using namespace statechartmodel;

#include "../State.h"

const QSizeF StateInstance::StateDefaultSize = QSizeF(800, 600);

StateInstance::StateInstance(const QString& instanceName, StatePtr parentState) :
    name(instanceName),
    parentState(parentState),
    position(QPointF(StateDefaultSize.toSize().width() * 0.1 + rand() % StateDefaultSize.toSize().width() * 0.7, StateDefaultSize.toSize().height() * 0.3 +  rand() % StateDefaultSize.toSize().height()) * 0.7),
    boundingSquareSize(defaultBoundingSquareSize)
    //    size(DEFAULTSIZE)
    //    scale(0.3f)
{

}

QString StateInstance::getInstanceName() const
{
    if (name.length() > 0)
    {
        return name;
    }
    else if (getStateClass())
    {
        return getStateClass()->getStateName();
    }
    else
    {
        return "Undefined";
    }
}

void StateInstance::setInstanceName(const QString& value)
{
    if (name == value)
    {
        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eUnchanged);
        }
    }
    else
    {
        name = value;

        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eChanged);
        }
    }
}

const QPointF StateInstance::getCenter() const
{
    QPointF newPosition = position + QPointF(boundingSquareSize * 0.5f, boundingSquareSize * 0.5f);

    return newPosition;
}

QRectF StateInstance::getBoundingSquare() const
{
    QRectF result;
    result.setTopLeft(getTopLeft());
    result.setWidth(boundingSquareSize);
    result.setHeight(boundingSquareSize);
    return result;
}



//void StateInstance::setBounds(const QRectF &newBounds)
//{
//    ARMARX_INFO_S << "new bounds of " << getInstanceName();
//    bounds = newBounds;
//    if(parentState)
//        emit parentState->substateChanged(shared_from_this(), eUpdated);
//}


QPointF StateInstance::adjustPosition(QPointF& newPos) const
{
    auto parent = getParent();
    if (!parent)
    {
        return newPos;
    }

    QRectF rect(QPointF(0, 0), parent->getSize());
    QPointF oldPos = getTopLeft();

    auto rm = parent->margin.width();
    //    auto statewidth = getBoundingSquareSize();
    auto right = rect.right();
    auto left = rect.left();
    auto lm = parent->margin.left();
    auto bw = getBounds().width();
    newPos.setX(qMin(right - bw - rm,
                     qMax(newPos.x(),
                          left + lm)));
    newPos.setY(qMin(rect.bottom() - getBounds().height() - parent->margin.height(),
                     qMax(newPos.y(),
                          rect.top() + parent->margin.top())));

    return oldPos;
}

void StateInstance::setPosition(QPointF newPosition)
{
    if (std::isnan(newPosition.x()) || std::isnan(newPosition.y()))
    {
        ARMARX_WARNING_S << "new Position for " << getInstanceName() << " contains NaN. wont set it";
        return;
    }

    adjustPosition(newPosition);
    if ((position - newPosition).manhattanLength() < 2)
    {
        //        ARMARX_INFO_S << getInstanceName() << ":UNCHANGED " << newPosition << "\n" << LogSender::CreateBackTrace();

        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eUnchanged);
        }
        else
        {
            //            ARMARX_INFO << " No parent for state " << getInstanceName();
        }
    }
    else
    {
        //        ARMARX_INFO_S << getInstanceName() << ":CHANGED " << newPosition << "\n" << LogSender::CreateBackTrace();
        position = newPosition;
        //        ARMARX_INFO_S << getInstanceName() << ": " << position << ": " << newPosition << " Distance: " << (position - newPosition).manhattanLength();
        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eChanged);
        }
        else
        {
            //            ARMARX_INFO << " No parent for state " << getInstanceName();
        }
    }
}

void StateInstance::setCenter(const QPointF& newStateCenter)
{
    if (std::isnan(newStateCenter.x()) || std::isnan(newStateCenter.y()))
    {
        ARMARX_WARNING_S << "new Position for " << getInstanceName() << " contains NaN. wont setting it";
        return;
    }

    //    ARMARX_INFO_S << getInstanceName() << ": " << position << ": " << newStateCenter;


    QPointF newPosition = newStateCenter - QPointF(boundingSquareSize * 0.5f, boundingSquareSize * 0.5f);
    setPosition(newPosition);
}

void StateInstance::setBoundingBox(float squareSize)
{
    //    ARMARX_INFO << "new bb size: " << squareSize;
    if (boundingSquareSize == squareSize)
    {
        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eUnchanged);
        }
    }
    else
    {
        boundingSquareSize = squareSize;

        if (getParent())
        {
            emit getParent()->substateChanged(shared_from_this(), eChanged);
        }
    }
}

void StateInstance::updateScale()
{
    if (getParent())
    {
        emit getParent()->substateChanged(shared_from_this(), eChanged);
    }
}

//void StateInstance::setScale(float newScale)
//{
//    if(scale == newScale)
//        return;
////    ARMARX_INFO_S << "new Scale: " << newScale;
//    scale = newScale;
//    if(parentState)
//        emit parentState->substateChanged(shared_from_this(), eUpdated);
//}

QSizeF StateInstance::getClassSize() const
{
    if (getStateClass())
    {
        return getStateClass()->getSize();
    }
    else
    {
        return StateInstance::StateDefaultSize;
    }
}


float StateInstance::getScale() const
{
    float xRatio = boundingSquareSize / getClassSize().width();
    float yRatio = boundingSquareSize / getClassSize().height();
    return std::min(xRatio, yRatio);
}




bool statechartmodel::StateInstance::isActive()
{
    //    return active;
    if (getParent() && getParent()->getActiveSubstate() == shared_from_this())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//void statechartmodel::StateInstance::setActive(bool status)
//{
//    if (active != status)
//    {
//        active = status;
//        emit getParent()->substateChanged(shared_from_this(), status ? eActivated : eDeactivated);

//        if (!status && getStateClass())
//        {
//            // this does not work with orthogonal states
//            getStateClass()->clearActiveSubstates();
//        }

//    }
//    else
//    {
//        emit getParent()->substateChanged(shared_from_this(), eUnchanged);
//    }
//}

