/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/statechart/StatechartIce.h>
#include "StateParameter.h"
#include <QString>
#include <QList>

#include <boost/shared_ptr.hpp>

namespace armarx
{
    namespace statechartmodel
    {

        class ParameterMapping
        {
        public:
            ParameterMapping();

            MappingSource source;
            QString sourceKey;
            QString destinationKey;
            QMap<QString, QString> profileValues;

        };
        typedef boost::shared_ptr<ParameterMapping> ParameterMappingPtr;
        typedef QList<ParameterMappingPtr> ParameterMappingList;

    }
}

