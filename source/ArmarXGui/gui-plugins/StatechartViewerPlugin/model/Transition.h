/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <boost/shared_ptr.hpp>

#include <QString>
#include <QPointF>
#include <QList>
#include <QMetaType>


#include "stateinstance/StateInstance.h"
#include "ParameterMapping.h"

namespace armarx
{

    typedef QList<QPointF> QPointList;
    typedef std::shared_ptr<float> FloatPtr;
    typedef std::shared_ptr<QPointF> QPointPtr;

    struct SupportPoints
    {
        SupportPoints()
            : startPoint(),
              endPoint(),
              controlPoints()
        {}

        SupportPoints(QPointList list)
            : startPoint(),
              endPoint(),
              controlPoints(list)
        {}

        QPointPtr startPoint;
        QPointPtr endPoint;
        QPointList controlPoints;

        QList<QPointF> toPointList() const;

        QList<QPointF> controlPointList() const;

        void setControlPoints(QList<QPointF> list);

        void append(const QPointF& point);
    };

    namespace statechartmodel
    {
        class Transition
        {
        public slots:
            void detachFromDestination();
            ParameterMappingPtr findMapping(const QString& destinationKey, const ParameterMappingList& mapping) const;
        public:
            Transition();
            QString eventName;
            StateInstancePtr sourceState;
            StateInstancePtr destinationState;
            ParameterMappingList mappingToNextStatesInput;
            ParameterMappingList mappingToParentStatesLocal;
            ParameterMappingList mappingToParentStatesOutput;
            SupportPoints supportPoints;
            QPointPtr labelCenterPosition;
            FloatPtr labelFontPointSize;
            IceUtil::Time lastActivationTimestamp;
            bool transitionUserCode = false;

        };
        typedef boost::shared_ptr<Transition> TransitionPtr;
        typedef boost::shared_ptr<const Transition> TransitionCPtr;
    }
}
Q_DECLARE_METATYPE(armarx::statechartmodel::TransitionPtr)
