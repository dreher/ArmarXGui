/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QObject>
#include <QMap>
#include <QMetaType>


#include <boost/shared_ptr.hpp>


#include "StateParameter.h"
#include "Transition.h"
#include "stateinstance/StateInstance.h"
#include "stateinstance/EndState.h"
#include "SignalType.h"
#include "Event.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>

namespace armarx
{

    namespace statechartmodel
    {

        class State;
        class DynamicRemoteStateClass;
        typedef boost::shared_ptr<State> StatePtr;
        typedef boost::shared_ptr<DynamicRemoteStateClass> DynamicRemoteStateClassPtr;
        typedef QList<TransitionPtr> TransitionList;
        typedef QList<TransitionCPtr> CTransitionList;
        typedef QMap<QString, StateInstancePtr> StateInstanceMap;

        class State :
            public QObject,
            public Logging,
            public boost::enable_shared_from_this<State>
        {
            Q_OBJECT
        public:
            const QRectF margin;

            explicit State(const QString& uuid = "");
            ~State() override;
            /**
             * Annotates the State object created by parseXml() before with references to other states.
             */
            void addReferences(const QMap<QString, armarx::statechartmodel::StatePtr>& uuidStateMap);

            QString getStateName() const
            {
                return stateName;
            }
            QString getUUID() const
            {
                return UUID;
            }
            const QString& getDescription() const
            {
                return description;
            }
            const QList<QString> getProxies() const
            {
                return proxies;
            }
            //            QString getPath() const { return filePath;}
            const StateParameterMap& getInputParameters() const
            {
                return inputParameters;
            }
            const StateParameterMap& getLocalParameters() const
            {
                return localParameters;
            }
            const StateParameterMap getInputAndLocalParameters() const;
            const StateParameterMap& getOutputParameters() const
            {
                return outputParameters;
            }
            CTransitionList getTransitions(bool withStartTransition = false) const;
            const StateInstanceMap& getSubstates() const
            {
                return substates;
            }
            const StateInstancePtr& getActiveSubstate() const
            {
                return activeSubstate;
            }
            StateInstancePtr getStartState() const;
            ParameterMappingList getStartStateInputMapping() const;
            const EventList& getOutgoingEvents() const
            {
                return outgoingTransitions;
            }
            EventList getAllEvents() const;

            QSizeF getSize() const
            {
                return size;
            }
            virtual eStateType getType() const
            {
                return eNormalState;
            }
            bool getDirty() const
            {
                return dirty;
            }
            TransitionCPtr getStartTransition() const;
            TransitionPtr findTransition(TransitionCPtr t) const;
            TransitionPtr findTransition(const QString& eventName, const QString& transitionSourceName, const QString& transitionDestinationName) const;
            bool hasDescendant(statechartmodel::StatePtr state) const;

            static QString StateTypeToString(eStateType type);
            void connectToSubclasses();

            bool isEditable() const;
        signals:
            /*
             * @brief stateChanged
             * name changed,
             * parameters changed,
             * promoted to endstate
             * start substate changed
             * @param state
             * @param signalType
             */
            void stateChanged(statechartmodel::SignalType signalType);

            /*
             * @brief substateChanged
             * substate added
             * substate removed
             * substate moved
             * substate resized
             * substate instance name renamed
             * substate
             * @param substate
             * @param signalType
             */
            void substateChanged(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);

            /*
             * @brief transitionChanged
             * transition added
             * transition removed
             * transition event changed
             * mapping changed,
             * target/source state changed
             * support point changed/added/removed
             * @param transition
             * @param signalType
             */
            void transitionChanged(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType);
            void outgoingTransitionChanged(const QString& eventName, statechartmodel::StatePtr stateClass, statechartmodel::SignalType signalType);

            void resized();
            void dirtyStatusChanged(bool newStatus);

            /**
             * @brief stateDeleted Signals that the destructor of this state was called.
             */
            void stateDeleted();

        public slots:
            void setStateName(const QString& newName);
            void setDescription(const QString& newDescription);
            void setSize(const QSizeF& newSize);
            void setSubstateAreaSize(const QSizeF& newSize);

            void setEditable(bool editable);

            // Substate slots
            StateInstancePtr addSubstate(StateInstancePtr stateInstance);
            StateInstancePtr addSubstate(StatePtr newSubstate, QString instanceName = "", const QPointF& pos = QPointF());
            StateInstancePtr addRemoteSubstate(StatePtr newRemoteSubstate, const QString& remoteStateOffererName, QString instanceName = "", const QPointF& pos = QPointF());
            StateInstancePtr addDynamicRemoteSubstate(StatePtr state, QString instanceName, const QPointF& pos);
            StateInstancePtr addEndSubstate(const QString& endstateName, const QString& eventName, const QPointF& pos = QPointF());

            bool renameSubstate(QString oldName, QString newName);
            bool removeSubstate(StateInstancePtr substate);
            bool removeSubstate(QString stateInstanceName);
            StateInstancePtr replaceSubstate(QString stateInstanceName, StateInstancePtr newInstance);
            void replaceSubstates(StateInstanceMap newSubstateList);

            void setActiveSubstate(StateInstancePtr newActiveState);
            void clearActiveSubstates();
            void setStartState(StateInstancePtr newStartState);
            void setStartStateInputMapping(const ParameterMappingList& newStartStateInputMapping);

            // Parameter slots
            void setInputParameters(const StateParameterMap& newInputParameters);
            void setLocalParameters(const StateParameterMap& newLocalParameters);
            void setOutputParameters(const StateParameterMap& newOutputParameters);

            // Transition slots
            void addTransition(TransitionPtr newTransition);
            void setTransitionMapping(TransitionCPtr transition, const ParameterMappingList& mappingToNextStateInput, const ParameterMappingList& mappingToParentLocal, const ParameterMappingList& mappingToParentOutput);
            void setTransitionSupportPoints(statechartmodel::TransitionCPtr transition, const SupportPoints& points, const QPointPtr& labelCenterPosition = QPointPtr(), const FloatPtr& labelFontPointSize = FloatPtr());
            void setTransitionActivated(TransitionCPtr transition);
            void setTransitionUserCodeEnabled(TransitionCPtr transition, bool enabled = true);
            void addDetachedTransition(const QString& eventName, StateInstancePtr sourceState);
            void updateTransitionDestination(TransitionCPtr transition, StateInstancePtr newDest, QPointList newSupportPoints = QPointList());
            void detachTransitionDestination(TransitionCPtr transition, QPointF floatingEndPoint);
            void detachTransitionDestination(TransitionCPtr transition);
            void bendTransition(TransitionCPtr transition, int u, int v);
            void replaceTransitions(TransitionList newTransitionList);
            void addSupportPoint(TransitionCPtr transition, QPointF supportPoint);

            bool removeTransition(TransitionPtr transition);


            void setOutgoingEvents(const EventList& outgoingEvents);
            void updateTransition(const QString& eventName, statechartmodel::StatePtr stateClass, statechartmodel::SignalType signalType);

        private slots:
            void setDirty(statechartmodel::SignalType signalType);
            void setDirty(statechartmodel::StateInstancePtr substate, statechartmodel::SignalType signalType);
            void setDirty(statechartmodel::TransitionCPtr transition, statechartmodel::SignalType signalType);
            void setDirty(bool dirty);
        protected:
            QList<StateInstancePtr> getInstances(statechartmodel::StatePtr stateClass) const;
            TransitionPtr getStartTransition();
            TransitionPtr getTransition(const QString& eventName, StateInstancePtr sourceInstance) const;
            bool checkTransition(TransitionPtr transition) const;
            bool checkSubstate(StatePtr newState) const;
            EventList outgoingTransitions;
            QString stateName;
            QString description;

            QList<QString> proxies;

            StateParameterMap inputParameters;
            StateParameterMap localParameters;
            StateParameterMap outputParameters;

            StateInstanceMap substates;
            StateInstancePtr activeSubstate;

            TransitionList transitions;  /* Transition to start state has sourceState == 0 */
            //            QString filePath;?
            QString UUID;
            QString cppClassName;

            QSizeF size;
            bool dirty;
            bool editable;
        private:
            bool active;

            void addDetachedTransitions(StateInstancePtr instance);
            QPointF calcDetachedTransitionLastControlPoint(StateInstancePtr instance) const;
            QPointF calcDetachedTransitionEndPoint(StateInstancePtr instance, const QPointF& lastControlPoint) const;
            friend class StateInstance;
        };
    }

}
Q_DECLARE_METATYPE(armarx::statechartmodel::StatePtr)

