/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Clara Scherer
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Gui::IceStateConverterTest
#define ARMARX_BOOST_TEST

//#include "../../StatechartEditorPlugin/io/XmlWriter.h"
#include "../IceStateConverter.h"
#include "../model/State.h"
#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <ArmarXGui/Test.h>

#include <iostream>

#include <ArmarXCore/core/application/Application.h>

BOOST_AUTO_TEST_CASE(testConversion)
{
    using namespace armarx;

    //construct model
    statechartmodel::StatePtr topState(new statechartmodel::State());
    topState->setStateName("TopState");

    //create substates
    statechartmodel::StatePtr sub1(new statechartmodel::State());
    sub1->setStateName("sub1");
    statechartmodel::StateInstancePtr instSub1 = topState->addSubstate(sub1);
    statechartmodel::StatePtr sub2(new statechartmodel::State());
    sub2->setStateName("sub2");
    statechartmodel::StateInstancePtr instSub2 = topState->addSubstate(sub2);
    statechartmodel::StatePtr sub3(new statechartmodel::State());
    sub3->setStateName("sub3");
    statechartmodel::StateInstancePtr instSub3 = topState->addSubstate(sub3);
    statechartmodel::StatePtr sub4(new statechartmodel::State());
    sub4->setStateName("sub4");
    statechartmodel::StateInstancePtr instSub4 = topState->addSubstate(sub4);
    statechartmodel::StatePtr sub5(new statechartmodel::State());
    sub5->setStateName("sub5");
    statechartmodel::StateInstancePtr instSub5 = topState->addSubstate(sub5);

    statechartmodel::StatePtr subsub1(new statechartmodel::State());
    subsub1->setStateName("subsub1");
    statechartmodel::StateInstancePtr instSubsub1 = sub1->addSubstate(subsub1);
    statechartmodel::StatePtr subsub2(new statechartmodel::State());
    subsub1->setStateName("subsub2");
    statechartmodel::StateInstancePtr instSubsub2 = sub1->addSubstate(subsub2);

    //set active substates
    topState->setActiveSubstate(instSub1);
    sub1->setActiveSubstate(instSubsub1);

    //set start states
    topState->setStartState(instSub5);
    sub1->setStartState(instSubsub1);

    //create transitions
    statechartmodel::TransitionPtr trans1(new statechartmodel::Transition());
    trans1->eventName = "trans1";
    trans1->sourceState = instSub5;
    trans1->destinationState = instSub4;
    topState->addTransition(trans1);
    statechartmodel::TransitionPtr trans2(new statechartmodel::Transition());
    trans2->eventName = "trans2";
    trans2->sourceState = instSub4;
    trans2->destinationState = instSub3;
    topState->addTransition(trans2);
    statechartmodel::TransitionPtr trans3(new statechartmodel::Transition());
    trans3->eventName = "trans3";
    trans3->sourceState = instSub3;
    trans3->destinationState = instSub2;
    topState->addTransition(trans3);
    statechartmodel::TransitionPtr trans4(new statechartmodel::Transition());
    trans4->eventName = "trans4";
    trans4->sourceState = instSub2;
    trans4->destinationState = instSub1;
    topState->addTransition(trans4);

    topState->addDetachedTransition("trans5", instSub1);

    statechartmodel::TransitionPtr trans1_1(new statechartmodel::Transition());
    trans1_1->eventName = "trans1_1";
    trans1_1->sourceState = instSubsub1;
    trans1_1->destinationState = instSubsub2;
    sub1->addTransition(trans1_1);

    //writer for the correct model
    //    armarx::VariantInfoPtr varInfo = armarx::VariantInfo::ReadInfoFiles(armarx::Application::createInstance<armarx::DummyApplication>()->getDefaultPackageNames());
    //    statechartio::XmlWriter correctWriter(varInfo);
    //    correctWriter.serialize(topState);
    //    QString correctResult = correctWriter.getXmlString(false);
    //    std::cout << correctResult << std::endl;

    //construct ice model
    StateIceBasePtr iceTopState(new StateIceBase());
    iceTopState->stateName = "TopState";

    //create substates of TopState
    StateList topSubstates;

    StateIceBasePtr iceSub1(new StateIceBase());
    iceSub1->stateName = "sub1";
    topSubstates.push_back(iceSub1);
    StateIceBasePtr iceSub2(new StateIceBase());
    iceSub2->stateName = "sub2";
    topSubstates.push_back(iceSub2);
    StateIceBasePtr iceSub3(new StateIceBase());
    iceSub3->stateName = "sub3";
    topSubstates.push_back(iceSub3);
    StateIceBasePtr iceSub4(new StateIceBase());
    iceSub4->stateName = "sub4";
    topSubstates.push_back(iceSub4);
    StateIceBasePtr iceSub5(new StateIceBase());
    iceSub5->stateName = "sub5";
    topSubstates.push_back(iceSub5);

    iceTopState->subStateList = topSubstates;

    //create substate of sub1
    StateList sub1Substates;

    StateIceBasePtr iceSubsub1(new StateIceBase());
    iceSubsub1->stateName = "subsub1";
    sub1Substates.push_back(iceSubsub1);
    StateIceBasePtr iceSubsub2(new StateIceBase());
    iceSubsub2->stateName = "subsub2";
    sub1Substates.push_back(iceSubsub2);

    iceSub1->subStateList = sub1Substates;

    //set active substates
    iceTopState->activeSubstate = iceSub1;
    iceSub1->activeSubstate = iceSubsub1;

    //set start states
    iceTopState->initState = iceSub5;
    iceSub1->initState = iceSubsub1;

    //create transitions
    EventBasePtr evt1(new EventBase());
    evt1->eventName = "trans1";
    TransitionIceBase iceTrans1(iceSub5, evt1, iceSub4, ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceTopState->transitions.push_back(iceTrans1);
    EventBasePtr evt2(new EventBase());
    evt2->eventName = "trans2";
    TransitionIceBase iceTrans2(iceSub4, evt2, iceSub3, ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceTopState->transitions.push_back(iceTrans2);
    EventBasePtr evt3(new EventBase());
    evt3->eventName = "trans3";
    TransitionIceBase iceTrans3(iceSub3, evt3, iceSub2, ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceTopState->transitions.push_back(iceTrans3);
    EventBasePtr evt4(new EventBase());
    evt4->eventName = "trans4";
    TransitionIceBase iceTrans4(iceSub2, evt4, iceSub1, ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceTopState->transitions.push_back(iceTrans4);

    EventBasePtr evt5(new EventBase());
    evt5->eventName = "trans5";
    TransitionIceBase iceTrans5(iceSub1, evt5, StateIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceTopState->transitions.push_back(iceTrans5);

    EventBasePtr evt1_1(new EventBase());
    evt1_1->eventName = "trans1_1";
    TransitionIceBase iceTrans1_1(iceSubsub1, evt1, iceSubsub2, ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), ParameterMappingIceBasePtr(), true);
    iceSub1->transitions.push_back(iceTrans1_1);
    //...

    //convert from ice to state model
    statechartmodel::StatePtr convertedTopState(new statechartmodel::State());
    IceStateConverter converter(convertedTopState);
    converter.convert(iceTopState);

    //    //writer for converted model
    //    statechartio::XmlWriter actualWriter(varInfo);
    //    actualWriter.serialize(topState);
    //    QString actualResult = actualWriter.getXmlString(false);
    //    std::cout << actualResult << std::endl;

    //    BOOST_CHECK_EQUAL(actualResult, correctResult);
}
/*
BOOST_AUTO_TEST_CASE(transitionConversionTest)
{
    using namespace armarx;

    //construct model
    statechartmodel::StatePtr topState(new statechartmodel::State());
    topState->setStateName("TopState");

    //create substates
    statechartmodel::StatePtr sub1(new statechartmodel::State());
    sub1->setStateName("sub1");
    statechartmodel::StateInstancePtr instSub1 = topState->addSubstate(sub1);
    statechartmodel::StatePtr sub2(new statechartmodel::State());
    sub2->setStateName("sub2");
    statechartmodel::StateInstancePtr instSub2 = topState->addSubstate(sub2);
    statechartmodel::StatePtr sub3(new statechartmodel::State());
    sub3->setStateName("sub3");
    statechartmodel::StateInstancePtr instSub3 = topState->addSubstate(sub3);
    statechartmodel::StatePtr sub4(new statechartmodel::State());
    sub4->setStateName("sub4");
    statechartmodel::StateInstancePtr instSub4 = topState->addSubstate(sub4);
    statechartmodel::StatePtr sub5(new statechartmodel::State());
    sub5->setStateName("sub5");
    statechartmodel::StateInstancePtr instSub5 = topState->addSubstate(sub5);

    statechartmodel::StatePtr subsub1(new statechartmodel::State());
    subsub1->setStateName("subsub1");
    statechartmodel::StateInstancePtr instSubsub1 = sub1->addSubstate(subsub1);
    statechartmodel::StatePtr subsub2(new statechartmodel::State());
    subsub1->setStateName("subsub2");
    statechartmodel::StateInstancePtr instSubsub2 = sub1->addSubstate(subsub2);

    //TODO: finish testcase
}
*/
