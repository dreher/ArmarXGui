/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Gui::SplineConverterTest
#define ARMARX_BOOST_TEST

#include <QApplication>
#include <QPainter>
#include <QPainterPath>
#include <QPointF>
#include <QWidget>

#include <Gui/Test.h>

#include "../SplineConverter.h"
#include "../view/SplinePath.h"

class PathWidget : public QWidget
{
public:
    PathWidget(QPainterPath spline)
        : QWidget(),
          m_spline(spline)
    {}

    void paintEvent(QPaintEvent*)
    {
        QPainter painter(this);
        painter.setPen(QColor("black"));
        painter.drawPath(m_spline);
    }

private:
    QPainterPath m_spline;
};

BOOST_AUTO_TEST_CASE(ComplexPathTest)
{
    //create QApplication
    char* args[1];
    args[0] = "SplineConverterTest";
    int argc = 1;
    QApplication app {argc, args};

    std::string graphVizSpline = "e,217.76,94.791 179.45,24.687 197.66,29.665 218.66,38.915 229.8,56 236.26,65.898 231.78,77.214 224.43,86.998";
    armarx::SplineType spline = armarx::SplineConverter::convert(graphVizSpline);
    QPainterPath splinePath = armarx::SplinePath::complexPath(spline.controlPoints);

    PathWidget* pathWidget {new PathWidget(splinePath)};
    pathWidget->show();

    armarx::QPointVector controlPoints = {QPointF(179.45, 24.687), QPointF(218.66, 38.915), QPointF(236.26, 65.898), QPointF(224.43, 86.998), QPointF(250, 65)};
    //    armarx::QPointVector controlPoints = {QPointF(100,100), QPointF(100,200), QPointF(200,200), QPointF(200,100)};
    //    armarx::QPointVector controlPoints = {QPointF(100,100), QPointF(100,200), QPointF(200,200), QPointF(400,500), QPointF(200,100), QPointF(100,100)};
    armarx::QPointVector stretchedControls;

    for (auto point : controlPoints)
    {
        point = 4 * point;
        stretchedControls.push_back(point);
    }

    controlPoints = stretchedControls;
    QPainterPath simpleSplinePath = armarx::SplinePath::simplePath(controlPoints);

    PathWidget* simplePathWidget {new PathWidget(simpleSplinePath)};
    simplePathWidget->show();

    app.exec();
}
/*
BOOST_AUTO_TEST_CASE(SimplePathWithLineTest)
{
    armarx::QPointVector controlPoints = {QPointF(0,0), QPointF(50,50), QPointF(100,100)};
    QPainterPath splinePath = armarx::SplinePath::simplePath(controlPoints);

    PathWidget* pathWidget{new PathWidget(splinePath)};
    pathWidget->show();

}
*/
