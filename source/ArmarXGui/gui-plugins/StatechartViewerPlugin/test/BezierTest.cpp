/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Gui::BezierTest
#define ARMARX_BOOST_TEST

#include <cassert>
#include <iostream>
#include <limits>
#include <vector>

#include <Eigen/Core>

#include <Gui/Test.h>

#include "../splines/Beziercurve.h"

Eigen::VectorXf cubicEval(armarx::pointVector controlPoints, float t)
{
    assert(controlPoints.size() == 4);
    Eigen::VectorXf cubicTerm = controlPoints.at(3) - 3 * controlPoints.at(2) + 3 * controlPoints.at(1) - controlPoints.at(0);
    Eigen::VectorXf squareTerm = 3 * controlPoints.at(0) - 6 * controlPoints.at(1) + 3 * controlPoints.at(2);
    Eigen::VectorXf linearTerm = 3 * controlPoints.at(1) - 3 * controlPoints.at(0);
    Eigen::VectorXf constTerm = controlPoints.at(0);

    return constTerm + t * (linearTerm + t * (squareTerm + t * cubicTerm));
}

BOOST_AUTO_TEST_CASE(lineTest)
{
    armarx::pointVector controlPoints;

    Eigen::VectorXf v1(2);
    v1 << 0, 0;
    controlPoints.push_back(v1);
    Eigen::VectorXf v2(2);
    v2 << 1, 1;
    controlPoints.push_back(v2);
    Eigen::VectorXf v3(2);
    v3 << 2, 2;
    controlPoints.push_back(v3);
    Eigen::VectorXf v4(2);
    v4 << 3, 3;
    controlPoints.push_back(v4);

    armarx::Beziercurve line {controlPoints};

    for (float t = 0; t <= 1.0 + std::numeric_limits<float>::epsilon(); t += 0.05)
    {
        Eigen::VectorXf point = line.evaluate(t);
        //        std::cout << "t = " << t << ": " << point;

        Eigen::VectorXf correctPoint = cubicEval(controlPoints, t);
        //        std::cout << "; correct point: " << correctPoint << std::endl;

        BOOST_CHECK_CLOSE(point(0), correctPoint(0), 0.00001);
        BOOST_CHECK_CLOSE(point(1), correctPoint(1), 0.00001);
    }
}

BOOST_AUTO_TEST_CASE(randomLineTest)
{
    armarx::pointVector controlPoints;

    Eigen::VectorXf v1(2);
    v1 << 0, 6;
    controlPoints.push_back(v1);
    Eigen::VectorXf v2(2);
    v2 << 1, 2;
    controlPoints.push_back(v2);
    Eigen::VectorXf v3(2);
    v3 << 2, 7;
    controlPoints.push_back(v3);
    Eigen::VectorXf v4(2);
    v4 << 3, 3;
    controlPoints.push_back(v4);

    armarx::Beziercurve line {controlPoints};

    for (float t = 0; t <= 1.0 + std::numeric_limits<float>::epsilon(); t += 0.05)
    {
        Eigen::VectorXf point = line.evaluate(t);
        //        std::cout << "t = " << t << ": " << point;

        Eigen::VectorXf correctPoint = cubicEval(controlPoints, t);
        //        std::cout << "; correct point: " << correctPoint << std::endl;

        BOOST_CHECK_CLOSE(point(0), correctPoint(0), 0.0001);
        BOOST_CHECK_CLOSE(point(1), correctPoint(1), 0.0001);
    }
}
