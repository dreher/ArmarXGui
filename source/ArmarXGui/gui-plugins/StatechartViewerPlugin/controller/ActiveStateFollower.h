/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <QObject>
#include <QVariantAnimation>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/StatechartView.h>



namespace armarx
{


    class ActiveStateFollower : public QVariantAnimation
    {
        Q_OBJECT
    public:
        ActiveStateFollower(StatechartView* statechartView, QWidget* parent = NULL);
        StatechartView* getStatechartView() const;
        void setStatechartView(StatechartView* value);

    public slots:
        void toggle(bool on);
        void startFollowing();
        void stopFollowing();
        void updatePos(QVariant value);
        void centerOnCurrentState(bool toggle);
    protected slots:
        void checkActiveState();
    signals:

        // QObject interface
    protected:

        statechartmodel::StateInstancePtr currentActiveState;
        QPointer<StateItem> currentActiveStateItem;
        QPointF currentTargetPos, startPos;
        QPointer<StatechartView> statechartView;
        QTimer activeStateCheckerTimer;
        IceUtil::Time startTime;
        int duration;

        // QVariantAnimation interface
    protected:
        void updateCurrentValue(const QVariant& value) override;
    };

}

