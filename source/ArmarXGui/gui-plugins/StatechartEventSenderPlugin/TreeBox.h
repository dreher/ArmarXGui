/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/logging/Logging.h>

#include <QApplication>
#include<QComboBox>
#include<QDirModel>
#include<QTreeView>
#include<QEvent>
#include<QMouseEvent>
#include<QModelIndex>
#include<QDir>
#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QStandardItemModel>
#include <QLineEdit>
#include "StateItemModel.h"

class QVBoxLayout;
class TreeBox;

class TreeBox :
    public QComboBox
{
    Q_OBJECT
public:
    TreeBox(QStandardItemModel* model, bool hideChildren = true, QWidget* parent = 0);



    bool eventFilter(QObject* object, QEvent* event) override;

    void showPopup() override;

    void hidePopup() override;
signals:
    void itemSelected(QModelIndex index);
public slots:
    void applyFilter(QString searchString);
private slots:
    void delayedIndexChanging();
private:
    bool applyFilter(const QStandardItem* parent, QString searchString);
    bool skipNextHide;
    QTreeView* treeView ;
    QModelIndex selectedIndex;

};


