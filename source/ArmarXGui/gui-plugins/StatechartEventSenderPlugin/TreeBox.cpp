/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TreeBox.h"


#include <QVBoxLayout>
#include <QTreeView>
#include <QLineEdit>
#include <QTextEdit>
#include <QItemEditorFactory>
#include <QTimer>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/FilterableTreeView.h>


using namespace armarx;

TreeBox::TreeBox(QStandardItemModel* model, bool hideChildren, QWidget* parent) : QComboBox(parent), skipNextHide(false) //Widget creation
{
    setModel(model);
    treeView = new FilterableTreeView(this, hideChildren);
    treeView->setHeaderHidden(true);
    treeView->setModel(model);
    treeView->setMinimumHeight(300);
    treeView->setMinimumWidth(250);
    treeView->setAnimated(true);
    treeView->setSortingEnabled(true);

    setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
    setMinimumContentsLength(10);
    setView(treeView);


    view()->viewport()->installEventFilter(this);
}

bool TreeBox::eventFilter(QObject* object, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress && object == view()->viewport())
    {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        QModelIndex index = view()->indexAt(mouseEvent->pos());

        if (!view()->visualRect(index).contains(mouseEvent->pos()))
        {
            skipNextHide = true;
        }

        //        else if(index.row() == 0 && index.parent() == qobject_cast<QStandardItemModel*>(model())->invisibleRootItem()->index() )
        //        {
        //            view()->edit(index);
        //            skipNextHide = true;
        //        }
    }
    else if ((event->type() == QEvent::KeyPress || event->type() == QEvent::KeyRelease)
             && object == view()->viewport())
    {

        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);

        if (keyEvent->key() != Qt::Key_Return && keyEvent->key() != Qt::Key_Enter)
        {
            // skipping all keyboard entries for the search but enter
            skipNextHide = true;
        }

    }

    return false;
}

void TreeBox::showPopup()
{
    QComboBox::showPopup();
}

void TreeBox::hidePopup()
{
    if (skipNextHide)
    {
        skipNextHide = false;
    }
    else
    {
        if (treeView->isVisible())
        {
            QTimer::singleShot(20, this, SLOT(delayedIndexChanging()));    // delay needed because, qt sends later an event that overwrites this
        }

        QComboBox::hidePopup();
        // TODO: better solution needed

        //        emit itemSelected(view()->currentIndex());
    }


}

void TreeBox::applyFilter(QString searchString)
{
    StateItemModel* stateModel = qobject_cast<StateItemModel*>(model());

    if (!stateModel)
    {
        throw exceptions::local::eNullPointerException("model is null");
    }

    QStandardItem* root = stateModel->invisibleRootItem();

    if (searchString == FilterableTreeView::DefaultFilterStr)
    {
        return;
    }

    applyFilter(root, searchString);
    treeView->setRowHidden(0, root->index(), false);

    if (searchString.length() > 0)
    {
        treeView->expandAll();
    }

}


void TreeBox::delayedIndexChanging()
{
    emit itemSelected(view()->currentIndex());
}





bool TreeBox::applyFilter(const QStandardItem* parent, QString searchString)
{
    bool show = false;

    for (int row = 0; row < parent->rowCount(); ++row)
    {
        QStandardItem* child = parent->child(row);

        if (!child)
        {
            continue;
        }

        bool showThisRow = applyFilter(child, searchString);
        QRegExp rex(searchString, Qt::CaseInsensitive, QRegExp::Wildcard);

        if (child->data(Qt::DisplayRole).toString().contains(rex))
        {

            showThisRow = true;
        }

        if (showThisRow)
        {
            show = showThisRow;
        }

        treeView->setRowHidden(row, parent->index(), !showThisRow);

    }

    return show;

}






