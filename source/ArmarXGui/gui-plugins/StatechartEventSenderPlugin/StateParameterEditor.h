/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QStyledItemDelegate>
#include <QTableWidget>

#include <ArmarXCore/statechart/Statechart.h>

#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

class QComboBox;

namespace armarx
{

    class StateParameterEditor : public QTableWidget
    {
        Q_OBJECT
    public:

        enum ColumnIds
        {
            eKey,
            eType,
            eOptional,
            eProvideDefaultValue,
            eValue,
            eDeleteButton
        };
        explicit StateParameterEditor(QWidget* parent = 0, const StateParameterMap& params = StateParameterMap());

        void setCommunicator(Ice::CommunicatorPtr communicator)
        {
            this->communicator = communicator;
        }
        void setVariantInfo(VariantInfoPtr variantInfo)
        {
            this->variantInfo = variantInfo;
        }
        void setCurrentProfile(StatechartProfilePtr currentProfile)
        {
            this->currentProfile = currentProfile;
        }
        void setKeyBlackList(const QSet<QString>& keyBlackList);
        QSet<QString> getKeys() const;
        StateParameterMap getStateParameters() const;
        StringVariantContainerBaseMap getStringValueMap() const;
        StateParameterMap getStateParametersWithoutValue() const;

        QString getValueAsString(int row) const;
        VariantContainerBasePtr getVariantContainer(int row) const;
        QString getJson(QString key) const;
        QString getKey(int row) const;
        QString getType(int row) const;
        QString getType(QString key) const;
        int getRow(const QString& key) const;
        bool getIsOptional(int row) const;

        void addParameterRow(QString key, QString variantIdStr, QString value, bool optional = false);
        void createValueButton(int row, const QString& jsonValue);
        void setHeaders();
        Qt::CheckState getDefaultValueState() const;
        void setDefaultValueState(const Qt::CheckState& value);

        StateParameterIceBasePtr getStateParameter(int row) const;
    signals:
        void buildRequested(const StateParameterMap& map, const std::map<QString, std::pair<QString, QString> >& jsonStringMap);
        void rowAdded(int rowId);
        void rowFilled(int rowId, const QString& key);
    public slots:
        void buildFromMap(const StateParameterMap& map, const std::map<QString, std::pair<QString, QString> >& jsonStringMap = std::map<QString, std::pair<QString, QString>>());
        int addParameterRow();
        void typeCbChanged(const QString& text);
        void deleteRow();
        void checkAndUpdateRowCount(int row, int column);
        void refreshVariantTypes();
        void editDefaultButtonClicked();
    private slots:
        void __buildFromMap(const StateParameterMap& map, const std::map<QString, std::pair<QString, QString> >& jsonStringMap = std::map<QString, std::pair<QString, QString>>());

    private:
        void connectUserEditSlots();
        void addVariantTypesToComboBox(QComboBox* combo);
        QString getHumanNameFromBaseName(QString variantBaseTypeName) const;
        QString getBaseNameFromHumanName(QString humanName) const;
        static bool compareVariantNames(const QString& a, const QString& b);

        class LineEditDelegate : public QStyledItemDelegate
        {
            QWidget* createEditor(QWidget* parent,
                                  const QStyleOptionViewItem& option,
                                  const QModelIndex& index) const override;
        };
        LineEditDelegate delegate;
        Ice::CommunicatorPtr communicator;
        VariantInfoPtr variantInfo;
        Qt::CheckState defaultValueState;
        QSet<QString> keyBlackList;
        StatechartProfilePtr currentProfile;

    };

}

