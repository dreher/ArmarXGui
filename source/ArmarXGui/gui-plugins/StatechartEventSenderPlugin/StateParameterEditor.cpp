/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateParameterEditor.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/statechart/StateParameter.h>
#include <ArmarXCore/util/json/JSONObject.h>

#include <Ice/ObjectFactory.h>

#include <QComboBox>
#include <QDialog>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QInputDialog>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QLabel>
#include <QToolButton>
#include <QMessageBox>
#include <QStyledItemDelegate>
#include <QCompleter>

using namespace armarx;

StateParameterEditor::StateParameterEditor(QWidget* parent, const StateParameterMap& params) :
    QTableWidget(parent),
    defaultValueState(Qt::Unchecked)
{
    // For jsonobject double/float serilization on german/spanish pcs....
    setlocale(LC_ALL, "C");

    setColumnCount(6);
    qRegisterMetaType<StateParameterMap>("StateParameterMap");
    qRegisterMetaType<std::map<QString, std::pair<QString, QString> > >("std::map<QString,std::pair<QString,QString> >");
    connect(this, SIGNAL(buildRequested(StateParameterMap, std::map<QString, std::pair<QString, QString> >)), this, SLOT(__buildFromMap(StateParameterMap, std::map<QString, std::pair<QString, QString> >)), Qt::QueuedConnection);
    setHeaders();

    if (params.size() == 0)
    {
        addParameterRow();
    }
    else
    {
        buildFromMap(params);
    }



    setItemDelegateForColumn(eKey, &delegate);


}

void StateParameterEditor::setKeyBlackList(const QSet<QString>& keyBlackList)
{
    this->keyBlackList = keyBlackList;
}

QSet<QString> StateParameterEditor::getKeys() const
{
    QSet<QString> result;

    for (int row = 0; row < rowCount(); row++)
    {
        if (item(row, eKey) && !item(row, eKey)->text().isEmpty())
        {
            result.insert(item(row, eKey)->text());
        }
    }

    return result;
}

StateParameterIceBasePtr StateParameterEditor::getStateParameter(int row) const
{
    if (getKey(row).isEmpty())
    {
        return NULL;
    }

    StateParameterIceBasePtr param = StateParameter::create();
    param->defaultValue = getVariantContainer(row);
    param->optionalParam = getIsOptional(row);
    param->set = false;
    return param;
}

StateParameterMap StateParameterEditor::getStateParameters() const
{
    StateParameterMap result;

    for (int row = 0; row < rowCount(); ++row)
    {
        auto param = getStateParameter(row);

        if (param)
        {
            result[getKey(row).toStdString()] = param;
        }
    }

    return result;
}

//StateParameterMap StateParameterEditor::getStateParametersWithoutValue()
//{
//    StateParameterMap result;
//    for (int row = 0; row < rowCount(); ++row) {
//        StateParameterIceBasePtr param = new StateParameterIceBase();
//if(getKey(row).length() == 0)
//    continue;

//    }
//}

StringVariantContainerBaseMap StateParameterEditor::getStringValueMap() const
{
    StringVariantContainerBaseMap result;

    for (int row = 0; row < rowCount(); ++row)
    {
        //        StateParameterIceBasePtr param = new StateParameter();
        if (getKey(row).length() == 0)
        {
            continue;
        }

        VariantContainerBasePtr con = getVariantContainer(row);
        result[getKey(row).toStdString()] = con;
    }

    return result;
}

QString StateParameterEditor::getValueAsString(int row) const
{
    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    QLineEdit* valueEdit = qobject_cast<QLineEdit*>(cellWidget(row, eValue));

    if (!valueEdit)
    {
        throw LocalException("value edit ptr is NULL");
    }

    return valueEdit->text();
}

QString StateParameterEditor::getJson(QString key) const
{
    int row = -1;

    for (int i = 0; i < rowCount(); i++)
    {
        if (getKey(i) == key)
        {
            row = i;
            break;
        }
    }

    if (row == -1)
    {
        throw LocalException("row index out of range: ") << row;
    }

    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    VariantContainerBasePtr container;

    QString type = getType(row);
    QLineEdit* valueEdit = qobject_cast<QLineEdit*>(cellWidget(row, eValue));
    QPushButton* editButton = qobject_cast<QPushButton*>(cellWidget(row, eValue));


    JSONObjectPtr jsonObject = new JSONObject(communicator);

    if (valueEdit && valueEdit->isEnabled())
    {
        int typeId = Variant::hashTypeName(type.toStdString());
        Variant variant;

        if (typeId == VariantType::Int)
        {
            variant.setInt(valueEdit->text().toInt());
        }

        else if (typeId == VariantType::Bool)
            if (valueEdit->text() == "true" || valueEdit->text() == "1")
            {
                variant.setBool(true);
            }
            else
            {
                variant.setBool(false);
            }

        else if (typeId == VariantType::Float)
        {
            variant.setFloat(valueEdit->text().toFloat());
        }
        else if (typeId == VariantType::Double)
        {
            variant.setDouble(valueEdit->text().toDouble());
        }

        else if (typeId == VariantType::String)
        {
            variant.setString(valueEdit->text().trimmed().toStdString());
        }

        container = new SingleVariant(variant);
        jsonObject->serializeIceObject(container);
        return QString::fromUtf8(jsonObject->toString().c_str());
    }
    else if (editButton && editButton->isEnabled())
    {
        QString jsonValue = editButton->property("JsonValue").toString();
        return jsonValue;
    }

    //    else
    //        throw LocalException("Neither QLineEdit nor QPushButton found!");
    return "";
}


VariantContainerBasePtr StateParameterEditor::getVariantContainer(int row) const
{
    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    VariantContainerBasePtr result;

    QString type = getType(row);
    QLineEdit* valueEdit = qobject_cast<QLineEdit*>(cellWidget(row, eValue));
    QPushButton* editButton = qobject_cast<QPushButton*>(cellWidget(row, eValue));

    if ((valueEdit && valueEdit->isEnabled()) || item(row, eValue))
    {
        int typeId = Variant::hashTypeName(type.toStdString());
        Variant variant;
        QString valueStr = valueEdit ? valueEdit->text() : item(row, eValue)->text();

        if (typeId == VariantType::Int)
        {
            variant.setInt(valueStr.toInt());
        }

        else if (typeId == VariantType::Bool)
            if (valueStr.compare("true", Qt::CaseInsensitive) == 0 || valueStr == "1")
            {
                variant.setBool(true);
            }
            else
            {
                variant.setBool(false);
            }

        else if (typeId == VariantType::Float)
        {
            variant.setFloat(valueStr.toFloat());
        }
        else if (typeId == VariantType::Double)
        {
            variant.setDouble(valueStr.toDouble());
        }

        else if (typeId == VariantType::String)
        {
            variant.setString(valueStr.trimmed().toStdString());
        }

        result = new SingleVariant(variant);
    }
    else if (editButton && editButton->isEnabled())
    {
        QString jsonValue = editButton->property("JsonValue").toString();

        if (jsonValue.isEmpty())
        {
            return result;
        }

        JSONObjectPtr jsonObject = new JSONObject(communicator);
        //        ARMARX_INFO_S << VAROUT(jsonValue.toUtf8().data());
        jsonObject->fromString(jsonValue.toUtf8().data());

        try
        {
            SerializablePtr obj = jsonObject->deserializeIceObject();
            result = VariantContainerBasePtr::dynamicCast(obj);

            if (!result)
            {
                // must be simple variant
                result = new SingleVariant(Variant(VariantDataPtr::dynamicCast(obj)));
            }
        }
        catch (std::exception& e)
        {
            ARMARX_WARNING_S << "JSON string for type " << type.toStdString()  << " could not be deserialized: " << e.what();
            return result;
        }
    }

    return result;
}

QString StateParameterEditor::getKey(int row) const
{
    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    if (!item(row, eKey))
    {
        return "";
    }

    return item(row, eKey)->text();
}

QString StateParameterEditor::getType(int row) const
{
    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    QComboBox* CBvalueType = qobject_cast<QComboBox*>(cellWidget(row, 1));

    if (!CBvalueType)
    {
        return "";
    }

    QString type = getBaseNameFromHumanName(CBvalueType->currentText()); //@@@
    return type;

}

QString StateParameterEditor::getType(QString key) const
{
    int row = getRow(key);
    return getType(row);
}

int StateParameterEditor::getRow(const QString& key) const
{
    int row = -1;

    for (int i = 0; i < rowCount(); i++)
    {
        if (getKey(i) == key)
        {
            row = i;
            break;
        }
    }

    if (row == -1)
    {
        throw LocalException("could not find key ") << key.toStdString();
    }

    return row;
}

bool StateParameterEditor::getIsOptional(int row) const
{
    if (row >= rowCount())
    {
        throw LocalException("row index out of range: ") << row;
    }

    QComboBox* cbOptional = qobject_cast<QComboBox*>(cellWidget(row, eOptional));
    return cbOptional->currentText() == "true";
}


int StateParameterEditor::addParameterRow()
{
    //    ARMARX_IMPORTANT_S << "Adding row";
    int row = rowCount();
    insertRow(row);
    setItem(row, eKey, new QTableWidgetItem());

    setItem(row, eProvideDefaultValue, new QTableWidgetItem());
    item(row, eProvideDefaultValue)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
    item(row, eProvideDefaultValue)->setCheckState(defaultValueState);

    QComboBox* valueTypebox = new QComboBox;
    valueTypebox->setEditable(true);
    //    valueTypebox->setInsertPolicy(QComboBox::InsertAlphabetically);
    //    valueTypebox->setValidator(new QIntValidator(this));
    setCellWidget(row, eType, valueTypebox);
    addVariantTypesToComboBox(valueTypebox);
    QCompleter* fullCompleter = new QCompleter(valueTypebox->model(), this);
    fullCompleter->setCompletionMode(QCompleter::PopupCompletion);
    fullCompleter->setCaseSensitivity(Qt::CaseSensitive);
    valueTypebox->setCompleter(fullCompleter);
    valueTypebox->setEditText("string");
    connect(valueTypebox, SIGNAL(editTextChanged(QString)), this, SLOT(typeCbChanged(QString)));
    //valueTypebox->model()->sort(0);
    QComboBox* cbOptional;
    cbOptional = new QComboBox();
    cbOptional->addItems(QString("true;false").split(";"));
    cbOptional->setCurrentIndex(1);
    setCellWidget(row, eOptional, cbOptional);


    QIcon icon;
    icon.addFile(QString::fromUtf8(":/icons/dialog-close.ico"), QSize(), QIcon::Normal, QIcon::Off);
    QToolButton* deleteButton = new QToolButton(this);
    deleteButton->setIcon(icon);
    deleteButton->setToolTip("Delete this row");
    setCellWidget(row, eDeleteButton, deleteButton);
    connect(deleteButton, SIGNAL(clicked()), this, SLOT(deleteRow()));
    emit rowAdded(row);
    return row;
}


void StateParameterEditor::addParameterRow(QString key, QString variantIdStr, QString value, bool optional)
{
    int row = addParameterRow();

    item(row, eKey)->setText(key);

    QComboBox* valueTypeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
    //    ARMARX_INFO_S << "type id: " << variantIdStr.toStdString() <<  " size: " << valueTypeBox->count();
    valueTypeBox->setEditText(getHumanNameFromBaseName(variantIdStr));

    QComboBox* optionalBox = qobject_cast<QComboBox*>(cellWidget(row, eOptional));

    if (optional)
    {
        optionalBox->setCurrentIndex(0);
    }
    else
    {
        optionalBox->setCurrentIndex(1);
    }

    if (!value.isEmpty())
    {
        item(row, eProvideDefaultValue)->setCheckState(Qt::Checked);
    }

    int variantId = Variant::hashTypeName(variantIdStr.toStdString());

    if (variantId == VariantType::String || variantId == VariantType::Int || variantId == VariantType::Bool || variantId == VariantType::Float || variantId == VariantType::Double)
    {
        QLineEdit* valueEdit = new QLineEdit;

        if (item(row, eProvideDefaultValue)->checkState() == Qt::Unchecked)
        {
            valueEdit->setEnabled(false);
        }

        setCellWidget(row, eValue, valueEdit);

        if (variantId == VariantType::String)
        {
            if (value.at(0) == '\"')
            {
                value.remove(0, 1);
            }

            if (value.at(value.length() - 1) == '\"')
            {
                value.remove(value.length() - 1, 1);
            }

            value = value.replace("\\\"", "\"").replace("\\\\", "\\");
        }

        valueEdit->setText(value);
    }
    else
    {
        createValueButton(row, value);
    }

    emit rowFilled(row, key);
}

void StateParameterEditor::createValueButton(int row, const QString& jsonValue)
{
    //    ARMARX_INFO_S << VAROUT(row) << VAROUT(jsonValue.toStdString());
    QComboBox* typeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));

    QString typeString = getBaseNameFromHumanName(typeBox->currentText()); //@@@
    QWidget* oldWidget = qobject_cast<QWidget*>(cellWidget(row, eValue));

    if (oldWidget)
    {
        oldWidget->hide();
    }

    QPushButton* editValueButton = new QPushButton("Edit");

    if (item(row, eProvideDefaultValue)->checkState() == Qt::Unchecked)
    {
        editValueButton->setEnabled(false);
    }

    setCellWidget(row, eValue, editValueButton);

    try
    {
        if (jsonValue.isEmpty())
        {
            auto variantContainerType = VariantContainerType::FromString(typeString.toStdString());
            JSONObjectPtr jsonObject = new JSONObject(communicator);

            //            ARMARX_INFO_S << VAROUT(variantContainerType->typeId);

            Ice::ObjectFactoryPtr factory = communicator->findObjectFactory(variantContainerType->typeId);

            if (factory)
            {
                Ice::ObjectPtr objectPtr = factory->create(variantContainerType->typeId);

                VariantDataPtr var = VariantDataPtr::dynamicCast(objectPtr);

                if (!var)
                {
                    // TODO: make better with dummy element function for all containers/variants?!
                    //                ARMARX_INFO_S << VAROUT(variantContainerType->subType->typeId);
                    Ice::ObjectFactoryPtr subfactory = communicator->findObjectFactory(variantContainerType->subType->typeId);

                    if (!subfactory)
                    {
                        //                    ARMARX_INFO_S << " no factory";
                        subfactory = IceInternal::factoryTable->getObjectFactory(variantContainerType->subType->typeId);
                    }

                    if (subfactory)
                    {
                        Ice::ObjectPtr subObj = subfactory->create(variantContainerType->subType->typeId);
                        VariantDataPtr var = VariantDataPtr::dynamicCast(subObj);

                        if (!var)
                        {

                        }

                        SingleTypeVariantListPtr list = SingleTypeVariantListPtr::dynamicCast(objectPtr);

                        if (var && list)
                        {
                            list->addVariant(Variant(var));
                        }

                        StringValueMapPtr map = StringValueMapPtr::dynamicCast(objectPtr);

                        if (var && map)
                        {
                            map->addVariant("mykey", Variant(var));
                        }
                    }
                    else
                    {
                        ARMARX_INFO_S << " no base factory";
                    }
                }

                jsonObject->serializeIceObject(SerializablePtr::dynamicCast(objectPtr));
            }

            editValueButton->setProperty("JsonValue", QString::fromStdString(jsonObject->asString(true)));
        }
        else
        {
            editValueButton->setProperty("JsonValue", jsonValue);
        }

        connect(editValueButton, SIGNAL(clicked()), this, SLOT(editDefaultButtonClicked()));
    }
    catch (...)
    {
        handleExceptions();
        editValueButton->setEnabled(false);
    }


}

void StateParameterEditor::typeCbChanged(const QString& text)
{
    QWidget* wid = qobject_cast<QWidget*>(sender());

    QModelIndex mIndex = indexAt(wid->pos()); // Some strange bug happens here: If this is used, it will deliver on table creation zero
    // If indexAt is not called, wid->pos() delivers a 0,0 Point and then rowAt() also return wrong index
    //    ARMARX_INFO_S << VAROUT(wid->pos()) << VAROUT(rowAt(wid->pos().y()));

    int row = rowAt(wid->pos().y());

    QWidget* valueWidget = cellWidget(row, eValue);

    QComboBox* typeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
    std::string typeStr = getBaseNameFromHumanName(typeBox->currentText()).toStdString(); //@@@
    int typeId = Variant::hashTypeName(typeStr);

    if (typeId == VariantType::String || typeId == VariantType::Int || typeId == VariantType::Bool || typeId == VariantType::Float || typeId == VariantType::Double)
    {
        QLineEdit* valueLineEdit = qobject_cast<QLineEdit*>(valueWidget);

        if (!valueLineEdit)
        {
            valueLineEdit = new QLineEdit;
            setCellWidget(row, eValue, valueLineEdit);
        }

        if (item(row, eProvideDefaultValue)->checkState() == Qt::Unchecked)
        {
            valueLineEdit->setEnabled(false);
        }

        if (typeId ==  VariantType::String)
        {
            valueLineEdit->setValidator(NULL);
        }
        else if (typeId ==  VariantType::Int)
        {
            valueLineEdit->setValidator(new QIntValidator(this));
        }
        else if (typeId ==  VariantType::Bool)
        {
            QRegExp rx("((1|0)|(true|false))");
            QValidator* validator = new QRegExpValidator(rx, this);
            valueLineEdit->setValidator(validator);
        }
        else if (typeId ==  VariantType::Float || typeId == VariantType::Double)
        {
            valueLineEdit->setValidator(new QDoubleValidator(this));
        }

        QString value = valueLineEdit->text();
        int pos;

        if (valueLineEdit->validator() && valueLineEdit->validator()->validate(value, pos) != QValidator::Acceptable)
        {
            valueLineEdit->setText("");
        }
    }
    else
    {
        createValueButton(row, "");
    }
}


void StateParameterEditor::deleteRow()
{
    int row;
    QWidget* wid = qobject_cast<QWidget*>(sender());
    QModelIndex mIndex = indexAt(wid->pos());
    row = mIndex.row();

    disconnect(this);
    removeRow(row);

    if (rowCount() == 0)
    {
        addParameterRow();
    }

    connectUserEditSlots();
}

void StateParameterEditor::checkAndUpdateRowCount(int row, int column)
{
    //    ARMARX_IMPORTANT_S << "checking row: " << row << ", " << column;

    if (column == eProvideDefaultValue)
    {
        if (item(row, eProvideDefaultValue)->checkState() == Qt::Unchecked)
        {
            QWidget* valueWidget = cellWidget(row, eValue);
            QWidget* valueLineEdit = qobject_cast<QWidget*>(valueWidget);

            if (valueLineEdit)
            {
                valueLineEdit->setEnabled(false);
            }
        }
        else
        {
            QWidget* valueWidget = cellWidget(row, eValue);
            QWidget* valueLineEdit = qobject_cast<QWidget*>(valueWidget);

            if (valueLineEdit)
            {
                valueLineEdit->setEnabled(true);
            }
        }
    }
    else if (column == eKey)
    {
        auto keyitem = item(row, eKey);

        if (keyitem)
        {
            if ((keyBlackList.find(keyitem->text()) != keyBlackList.end() || findItems(keyitem->text(), Qt::MatchExactly).size() > 1))
            {
                keyitem->setText(item(row, eKey)->text() + "_2");
                ARMARX_WARNING_S << "Keys must be unique (input and local parameters share the same key pool)";
            }
        }
    }

    if (row >= rowCount() - 1 && item(rowCount() - 1, 0) && !item(rowCount() - 1, 0)->text().isEmpty())
    {
        addParameterRow();
    }
}

void StateParameterEditor::refreshVariantTypes()
{
    for (int row = 0; row < rowCount(); row++)
    {
        QComboBox* valueTypeBox = qobject_cast<QComboBox*>(cellWidget(row, eType));
        addVariantTypesToComboBox(valueTypeBox);
        //        valueTypeBox->model()->sort(0);
    }

}

void StateParameterEditor::editDefaultButtonClicked()
{
    QDialog editDefaultDialog;
    editDefaultDialog.setWindowTitle("Statechart Parameter Complex Value Editor");
    editDefaultDialog.resize(QSize(600, 400));
    QTextEdit* dialogTextEdit = new QTextEdit();
    dialogTextEdit->setAcceptRichText(false);
    dialogTextEdit->setPlainText(sender()->property("JsonValue").toString());

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(dialogTextEdit);
    QDialogButtonBox* buttonBox = new QDialogButtonBox(dialogTextEdit);
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
    layout->addWidget(buttonBox);
    editDefaultDialog.setLayout(layout);

    connect(buttonBox, SIGNAL(accepted()), &editDefaultDialog, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &editDefaultDialog, SLOT(reject()));

    if (editDefaultDialog.exec() == QDialog::Accepted)
    {
        sender()->setProperty("JsonValue", dialogTextEdit->toPlainText());
    }
}

void StateParameterEditor::connectUserEditSlots()
{
    connect(this, SIGNAL(cellEntered(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
    connect(this, SIGNAL(cellPressed(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
    connect(this, SIGNAL(cellChanged(int, int)), this, SLOT(checkAndUpdateRowCount(int, int)), Qt::UniqueConnection);
}

void StateParameterEditor::addVariantTypesToComboBox(QComboBox* combo)
{
    if (!combo)
    {
        return;
    }

    combo->clear();

    QList<QString> list;

    for (std::pair<VariantTypeId, std::string> pair : Variant::getTypes())
    {
        QString typeName = tr(pair.second.c_str());
        //QString humanName = tr(Variant::getHumanName(pair->first).c_str());

        if (typeName.contains("Invalid"))
        {
            continue;
        }

        list.append(getHumanNameFromBaseName(typeName));
        /*if(combo->findText(typeName) == -1)
        {
            combo->addItem(getHumanNameFromBaseName(typeName));
        }*/
    }

    std::vector<VariantContainerType> containers;
    containers.push_back(VariantType::List);
    containers.push_back(VariantType::Map);

    for (VariantContainerType c : containers)
    {
        for (auto it : Variant::getTypes())
        {
            QString typeName = tr(it.second.c_str());


            if (typeName.contains("Invalid"))
            {
                continue;
            }

            std::string typeStr = VariantContainerType::allTypesToString(c(it.first).clone());
            typeName = QString::fromStdString(typeStr);
            list.append(getHumanNameFromBaseName(typeName));
            /*if(combo->findText(typeName) == -1)
            {
                combo->addItem(getHumanNameFromBaseName(typeName));
            }*/
        }
    }

    qSort(list.begin(), list.end(), compareVariantNames);
    combo->addItems(list);
}

QString StateParameterEditor::getHumanNameFromBaseName(QString variantBaseTypeName) const
{
    if (!variantInfo)
    {
        return variantBaseTypeName;
    }

    std::string humanName = variantInfo->getNestedHumanNameFromBaseName(variantBaseTypeName.toUtf8().data());

    if (humanName.empty())
    {
        return variantBaseTypeName;
    }

    return QString::fromUtf8(humanName.c_str());
}

QString StateParameterEditor::getBaseNameFromHumanName(QString humanName) const
{
    if (!variantInfo)
    {
        return humanName;
    }

    std::string variantBaseTypeName = variantInfo->getNestedBaseNameFromHumanName(humanName.toUtf8().data());

    if (variantBaseTypeName.empty())
    {
        return humanName;
    }

    return QString::fromUtf8(variantBaseTypeName.c_str());
}

bool StateParameterEditor::compareVariantNames(const QString& a, const QString& b)
{
    bool pa, pb;

    // list nested types after basic ones
    pa = a.contains("(");
    pb = b.contains("(");

    if (pa != pb)
    {
        return pb;
    }

    // list human types before non resolved ones
    pa = a.contains(":");
    pb = b.contains(":");

    if (pa != pb)
    {
        return pb;
    }

    // lower case (basic types) first
    pa = a.count() > 0 && a[0].isLower();
    pb = b.count() > 0 && b[0].isLower();

    if (pa != pb)
    {
        return pa;
    }

    return a.compare(b) < 0;
}
Qt::CheckState StateParameterEditor::getDefaultValueState() const
{
    return defaultValueState;
}

void StateParameterEditor::setDefaultValueState(const Qt::CheckState& value)
{
    defaultValueState = value;
}




void StateParameterEditor::setHeaders()
{
    setHorizontalHeaderLabels(QString("Key;Type;Optional;Def;Value;Del").split(";"));
    setColumnWidth(eKey, 180);
    setColumnWidth(eType, 225);
    setColumnWidth(eOptional, 70);
    setColumnWidth(eProvideDefaultValue, 29);
    setColumnWidth(eValue, 200);
    setColumnWidth(eDeleteButton, 25);
}

void StateParameterEditor::buildFromMap(const StateParameterMap& map, const std::map<QString, std::pair<QString, QString> >& jsonStringMap)
{
    emit buildRequested(map, jsonStringMap);
}

void StateParameterEditor::__buildFromMap(const StateParameterMap& map, const std::map<QString, std::pair<QString, QString> >& jsonStringMap)
{
    clearContents();
    QWidget* tempW = new QLabel("mylabel", this);
    tempW->deleteLater();
    setRowCount(0);
    //    disconnect(this);
    disconnect(this, SIGNAL(cellEntered(int, int)));
    disconnect(this, SIGNAL(cellPressed(int, int)));
    disconnect(this, SIGNAL(cellChanged(int, int)));
    StateParameterMap::const_iterator it = map.begin();

    for (; it != map.end(); it++)
    {
        StateParameterIceBasePtr p = it->second;
        JSONObjectPtr json = new JSONObject(communicator);
        QString typeStr;
        QString jsonStr;

        if (p->defaultValue)
        {
            json->serializeIceObject(p->defaultValue);
            typeStr = QString::fromStdString(VariantContainerType::allTypesToString(p->defaultValue->getContainerType()));

            //            ARMARX_INFO_S << VAROUT(typeStr.toStdString());
            //            ARMARX_INFO_S << VAROUT(json->asString(true));
            if (VariantType::IsBasicType(Variant::hashTypeName(typeStr.toStdString())))
            {
                jsonStr = QString::fromUtf8(json->getElement("variant")->getElement("value")->toString().c_str());
            }
            else
            {
                jsonStr = QString::fromUtf8(json->asString(true).c_str());
            }

            //            ARMARX_INFO_S << VAROUT(jsonStr.toStdString());
        }
        else if (jsonStringMap.find(QString::fromStdString(it->first)) != jsonStringMap.end())
        {
            typeStr = jsonStringMap.at(QString::fromStdString(it->first)).first;
            jsonStr = jsonStringMap.at(QString::fromStdString(it->first)).second;
        }

        addParameterRow(QString::fromStdString(it->first),
                        typeStr,
                        jsonStr,
                        p->optionalParam);
    }

    addParameterRow();
    connectUserEditSlots();
    //    setHeaders();

}



QWidget* StateParameterEditor::LineEditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QLineEdit* lineEdit = new QLineEdit(parent);
    QString regExpStr("(|([a-z_]{1})([a-z_0-9]*))");
    QRegExp reg(regExpStr, Qt::CaseInsensitive);
    lineEdit->setValidator(new QRegExpValidator(reg, lineEdit));
    return lineEdit;
}
