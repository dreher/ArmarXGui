/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LogViewer.h"

#include "LogTableModel.h"
#include "LogTable.h"
#include "FilterDialog.h"
#include "ui_FilterDialog.h"

// C++ includes
#include <sstream>

// Qt includes
#include <QToolBar>
#include <QScrollBar>
#include <QTimer>
#include <QInputDialog>

#include <boost/algorithm/string/regex.hpp>


#define USERROLE_BASENAME Qt::UserRole+1
#define USERROLE_LOGTABLEPTR Qt::UserRole+2
#define REGEX_COLORS "\033\\[(\\d|\\w?)[;]?(\\d+)m"
#define ALL_MESSAGES_FILTER "All Messages"

using namespace armarx;


LogViewer::LogViewer() :
    logTable(NULL),
    loggingPaused(false),
    customToolbar(0)

{
    qRegisterMetaType<Qt::Orientation>("Qt::Orientation");
    qRegisterMetaType<std::string>("std::string");
    ui.setupUi(getWidget());

    for (int i = 0; i < eLogLevelCount; i++)
    {
        ui.cbVerbosityLevel->addItem(LogSender::levelToString((MessageType)i).c_str());
    }

    ui.cbVerbosityLevel->setCurrentIndex(1);
    logTable = addEmptyFilter();
    ui.lvFilters->setCurrentRow(0);
    pendingEntriesTimer = new QTimer(getWidget());
    pendingEntriesTimer->setInterval(50);

    connect(pendingEntriesTimer, SIGNAL(timeout()), this, SLOT(insertPendingEntries()));
    connect(this, SIGNAL(componentConnected()), pendingEntriesTimer, SLOT(start()));


    QList<int> sizes;
    sizes.push_back(80);
    sizes.push_back(400);
    ui.splitter->setSizes(sizes);
    addFilter("Warning+", "", "", eWARN, "", "", ""); // add additional filters after splitter->setSizes-> otherwise they have a width of NULL



    qRegisterMetaType<LogMessage>("LogMessage");

    // SIGNALS AND SLOTS CONNECTIONS
    connect(this, SIGNAL(newEntry(LogMessage)), this, SLOT(addNewEntry(LogMessage)));
    connect(ui.edtLiveFilter, SIGNAL(textChanged(QString)), this, SLOT(performLiveFilter(QString)));
    connect(ui.edtLiveSearch, SIGNAL(textChanged(QString)), this, SLOT(performLiveSearch(QString)));
    connect(ui.btnAddFilter, SIGNAL(clicked()), this, SLOT(addFilter()));
    connect(ui.btnRemoveFilter, SIGNAL(clicked()), this, SLOT(removeSelectedFilter()));
    connect(ui.btnPause, SIGNAL(toggled(bool)), this, SLOT(pauseLogging(bool)));
    connect(ui.btnClearLog, SIGNAL(clicked()), this, SLOT(clearSelectedLog()));
    connect(ui.btnClearAllLogs, SIGNAL(clicked()), this, SLOT(clearAllLogs()));
    connect(ui.lvFilters, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(filterSelectionChanged(QListWidgetItem*, QListWidgetItem*)));
    connect(ui.lvFilters, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(editFilter(QListWidgetItem*)));
    connect(ui.cbSearchType, SIGNAL(currentIndexChanged(int)), this, SLOT(searchTypeChanged(int)));
    connect(this, SIGNAL(updateFilterListSignal()), this, SLOT(updateFilterList()));
    connect(ui.btnNextItem, SIGNAL(clicked()), this, SLOT(selectNextSearchResult()));
    connect(ui.btnPreviousItem, SIGNAL(clicked()), this, SLOT(selectPreviousSearchResult()));

    ui.edtLiveFilter->hide();
    ui.edtLiveSearch->setFocus();


}

LogViewer::~LogViewer()
{
    //    ARMARX_VERBOSE << "~LogViewer";
}



void armarx::LogViewer::loadSettings(QSettings* settings)
{
    ui.cbVerbosityLevel->setCurrentIndex(settings->value("verbosityLevel", 2).toInt());
    ui.cbAutoComponentFilters->setChecked(settings->value("autoFilterAdding", true).toBool());
}

void armarx::LogViewer:: saveSettings(QSettings* settings)
{

    settings->setValue("verbosityLevel", ui.cbVerbosityLevel->currentIndex());

    settings->setValue("autoFilterAdding", ui.cbAutoComponentFilters->isChecked());


    //    // saving filters

    //    QString filterName = ui.lvFilters->items()->data(USERROLE_BASENAME).toString();
    //    for(unsigned int i= 0; i< logTable->getModel()->getFilters().size(); i++)
    //    {
    //        std::string columnName = logTable->getModel()->getFilters()[i].first;
    //        std::string filter = logTable->getModel()->getFilters()[i].second;
    //        if(columnName == ARMARX_LOG_COMPONENTSTR)
    //            filterDialog.ui->editComponent->setText(filter.c_str());
    //        else if(columnName == ARMARX_LOG_TAGSTR)
    //            filterDialog.ui->edtTag->setText(filter.c_str());
    //        else if(columnName == ARMARX_LOG_VERBOSITYSTR){

    //            filterDialog.ui->cbVerbosity->setCurrentIndex(QString(filter.c_str()).toInt());
    //        }
    //        else if(columnName == ARMARX_LOG_MESSAGESTR)
    //            filterDialog.ui->edtMessage->setText(filter.c_str());
    //        else if(columnName == ARMARX_LOG_FILESTR)
    //            filterDialog.ui->edtFile->setText(filter.c_str());
    //        else if(columnName == ARMARX_LOG_FUNCTIONSTR)
    //            filterDialog.ui->edtFunction->setText(filter.c_str());
    //    }


}




void armarx::LogViewer::onInitComponent()
{
    usingTopic("Log");
}

void armarx::LogViewer::onConnectComponent()
{
    emit componentConnected();
}

bool LogViewer::onClose()
{
    //    QInputDialog dialog;
    //    if(dialog.exec() == QDialog::Rejected)
    //        return false;

    return ArmarXComponentWidgetController::onClose();
}

void LogViewer::onExitComponent()
{
}

void LogViewer::write(const std::string& who, Ice::Long time, const std::string& tag, MessageType severity, const std::string& message, const std::string& file, Ice::Int line, const std::string& function, const Ice::Current&)
{
    LogMessage msg;
    msg.who  = who;
    msg.time = time;
    msg.tag = tag;
    msg.type = severity;
    msg.what = message;
    msg.file = file;
    msg.line = line;
    msg.function = function;

    writeLog(msg);
}

void LogViewer::writeLog(const LogMessage& msg, const Ice::Current&)
{
    if (loggingPaused)
    {
        return;
    }

    if (ui.cbVerbosityLevel->currentIndex() <= msg.type)
    {
        ScopedLock lock(pendingEntriesMutex);
        pendingEntries.push_back(msg);
    }
}


void LogViewer::setRow(const LogMessage& msg, int rowIndex)
{
}

void LogViewer::addNewEntry(const LogMessage& msg)
{
}

void LogViewer::performLiveFilter(QString filterStr, int startRow)
{
    //    if((IceUtil::Time::now() - lastLiveSearchEditChangeTime).toSeconds() <  1)
    //        return;
    if (filterStr.length() == 0)
    {
        logTable->resetLiveFilter();
    }
    else if (filterStr.length() < 3)
    {
        return;
    }

    //    LogSearch search(logTable);
    //    search.search(searchStr);
    logTable->liveFilter(filterStr, startRow);
    lastLiveSearchEditChangeTime = IceUtil::Time::now();
}

void LogViewer::performLiveSearch(QString searchStr)
{
    if (searchStr.length() == 0)
    {
        logTable->resetLiveSearch();
        ui.btnNextItem->setEnabled(false);
        ui.btnPreviousItem->setEnabled(false);
    }
    //    else if(searchStr.length() < 3)
    //        return;
    else
    {
        if (!logTable->liveSearch(searchStr))
        {
            getMainWindow()->statusBar()->showMessage("Could not find '" + logTable->getModel()->getCurrentSearchStr() + "' in the log!", 5000);
            ui.btnNextItem->setEnabled(false);
            ui.btnPreviousItem->setEnabled(false);
        }
        else
        {
            ui.btnNextItem->setEnabled(true);
            ui.btnPreviousItem->setEnabled(true);
        }

    }
}

void LogViewer::clearSelectedLog()
{
    if (logTable)
    {
        logTable->getModel()->clearData();
    }
}

void LogViewer::clearAllLogs()
{
    std::map<QString, LogTable*>::iterator it = filterMap.begin();

    for (; it != filterMap.end() ; it++)
    {
        it->second->getModel()->clearData();
    }

    emit updateFilterListSignal();
}




void LogViewer::pauseLogging(bool pause)
{
    loggingPaused = pause;
}


LogTable* LogViewer::addEmptyFilter()
{
    LogTable* newLogTable = new LogTable();
    //    newLogTable->setColumns(standardColumns);
    QListWidgetItem* item = new QListWidgetItem();
    QString standardFilterStr = ALL_MESSAGES_FILTER;
    item->setText(standardFilterStr);
    item->setData(USERROLE_BASENAME, standardFilterStr);
    //    item->setData(USERROLE_LOGTABLEPTR, qVariantFromValue((void*)newLogTable));
    ui.lvFilters->addItem(item);
    filterMap[standardFilterStr] = newLogTable;
    ui.splitter->addWidget(newLogTable);

    return newLogTable;
}

LogTable* LogViewer::addFilter(QString filterName, QString componentFilter, QString tagFilter, MessageType minimumVerbosity, QString messageFilter, QString fileFilter, QString functionFilter)
{

    if (filterMap.find(filterName) != filterMap.end())
    {
        QString msg = "A filter with this name already exists";
        showMessageBox(msg);
        return NULL;
    }

    LogTable* newLogTable = new LogTable();
    newLogTable->hide();

    //    newLogTable->setColumns(standardColumns);
    if (componentFilter.length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_COMPONENTSTR, componentFilter.toStdString());
    }

    if (tagFilter.length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_TAGSTR, tagFilter.toStdString());
    }

    if (messageFilter.length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_MESSAGESTR, messageFilter.toStdString());
    }

    if (fileFilter.length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_FILESTR, fileFilter.toStdString());
    }

    if (functionFilter.length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_FUNCTIONSTR, functionFilter.toStdString());
    }

    newLogTable->getModel()->addFilter(ARMARX_LOG_VERBOSITYSTR, QString::number(minimumVerbosity).toStdString());

    QListWidgetItem* item = new QListWidgetItem();
    item->setText(filterName);
    //    item->setData(USERROLE_LOGTABLEPTR, qVariantFromValue((void*)newLogTable));
    item->setData(USERROLE_BASENAME, filterName);
    filterMap[filterName] = newLogTable;
    ui.lvFilters->addItem(item);

    ui.splitter->addWidget(newLogTable);
    return newLogTable;
}

bool LogViewer::checkForNewComponents(const QString& componentName)
{
    if (componentName.length() == 0)
    {
        return false;
    }

    if (!ui.cbAutoComponentFilters->isChecked())
    {
        return false;
    }

    std::map<QString, LogTable*>::iterator it = filterMap.find(componentName);

    if (it != filterMap.end())
    {
        return false;
    }

    addFilter(componentName, componentName, "", eDEBUG, "", "", "");
    return true;
}



void LogViewer::addFilter()
{
    FilterDialog filterDialog;

    if (!filterDialog.exec())
    {
        return;
    }

    QString filterName = filterDialog.ui->edtFilterName->text();

    if (filterMap.find(filterName) != filterMap.end())
    {
        QString msg = "A filter with this name already exists";
        showMessageBox(msg);
        return;
    }

    LogTable* newLogTable = new LogTable();
    newLogTable->hide();

    //    newLogTable->setColumns(standardColumns);
    if (filterDialog.ui->editComponent->text().length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_COMPONENTSTR, filterDialog.ui->editComponent->text().toStdString());
    }

    if (filterDialog.ui->edtTag->text().length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_TAGSTR, filterDialog.ui->edtTag->text().toStdString());
    }

    if (filterDialog.ui->edtMessage->text().length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_MESSAGESTR, filterDialog.ui->edtMessage->text().toStdString());
    }

    if (filterDialog.ui->edtFile->text().length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_FILESTR, filterDialog.ui->edtFile->text().toStdString());
    }

    if (filterDialog.ui->edtFunction->text().length())
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_FUNCTIONSTR, filterDialog.ui->edtFunction->text().toStdString());
    }

    if (filterDialog.ui->cbVerbosity->currentIndex() != -1)
    {
        newLogTable->getModel()->addFilter(ARMARX_LOG_VERBOSITYSTR, QString::number(filterDialog.ui->cbVerbosity->currentIndex()).toStdString());
    }

    QListWidgetItem* item = new QListWidgetItem();
    item->setText(filterName);
    //    item->setData(USERROLE_LOGTABLEPTR, qVariantFromValue((void*)newLogTable));
    item->setData(USERROLE_BASENAME, filterDialog.ui->edtFilterName->text());
    filterMap[filterName] = newLogTable;
    ui.lvFilters->addItem(item);

    ui.splitter->addWidget(newLogTable);
}

void LogViewer::editFilter(QListWidgetItem* item)
{
    QString filterName = item->data(USERROLE_BASENAME).toString();
    FilterDialog filterDialog;
    filterDialog.ui->edtFilterName->setText(filterName);

    if (filterMap.find(filterName) == filterMap.end())
    {
        ARMARX_WARNING << "filter " << filterName.toStdString() << " does not exist" << flush;
        return;
    }

    LogTable* logTable = filterMap.find(filterName)->second;

    if (!logTable)
    {
        ARMARX_WARNING << "logtable ptr is NULL " << flush;
        return;
    }

    for (unsigned int i = 0; i < logTable->getModel()->getFilters().size(); i++)
    {
        std::string columnName = logTable->getModel()->getFilters()[i].first;
        std::string filter = logTable->getModel()->getFilters()[i].second;

        if (columnName == ARMARX_LOG_COMPONENTSTR)
        {
            filterDialog.ui->editComponent->setText(filter.c_str());
        }
        else if (columnName == ARMARX_LOG_TAGSTR)
        {
            filterDialog.ui->edtTag->setText(filter.c_str());
        }
        else if (columnName == ARMARX_LOG_VERBOSITYSTR)
        {

            filterDialog.ui->cbVerbosity->setCurrentIndex(QString(filter.c_str()).toInt());
        }
        else if (columnName == ARMARX_LOG_MESSAGESTR)
        {
            filterDialog.ui->edtMessage->setText(filter.c_str());
        }
        else if (columnName == ARMARX_LOG_FILESTR)
        {
            filterDialog.ui->edtFile->setText(filter.c_str());
        }
        else if (columnName == ARMARX_LOG_FUNCTIONSTR)
        {
            filterDialog.ui->edtFunction->setText(filter.c_str());
        }
    }


    if (!filterDialog.exec())
    {
        return;
    }

    logTable->getModel()->resetFilters();

    if (filterDialog.ui->editComponent->text().length())
    {
        logTable->getModel()->addFilter(ARMARX_LOG_COMPONENTSTR, filterDialog.ui->editComponent->text().toStdString());
    }

    if (filterDialog.ui->edtTag->text().length())
    {
        logTable->getModel()->addFilter(ARMARX_LOG_TAGSTR, filterDialog.ui->edtTag->text().toStdString());
    }

    if (filterDialog.ui->edtMessage->text().length())
    {
        logTable->getModel()->addFilter(ARMARX_LOG_MESSAGESTR, filterDialog.ui->edtMessage->text().toStdString());
    }

    if (filterDialog.ui->edtFile->text().length())
    {
        logTable->getModel()->addFilter(ARMARX_LOG_FILESTR, filterDialog.ui->edtFile->text().toStdString());
    }

    if (filterDialog.ui->edtFunction->text().length())
    {
        logTable->getModel()->addFilter(ARMARX_LOG_FUNCTIONSTR, filterDialog.ui->edtFunction->text().toStdString());
    }

    if (filterDialog.ui->cbVerbosity->currentIndex() != -1)
    {
        logTable->getModel()->addFilter(ARMARX_LOG_VERBOSITYSTR, QString::number(filterDialog.ui->cbVerbosity->currentIndex()).toStdString());
    }

    logTable->getModel()->reapplyAllFilters();
    logTable->update();

}

void LogViewer::removeSelectedFilter()
{
    if (ui.lvFilters->count() == 1)
    {
        return;
    }

    std::map<QString, LogTable*>::iterator it = filterMap.find(ui.lvFilters->currentItem()->data(USERROLE_BASENAME).toString());

    if (it == filterMap.end())
    {
        return;
    }


    int row = ui.lvFilters->currentRow();
    ui.lvFilters->setCurrentRow(0);
    ui.lvFilters->takeItem(row);
    LogTable* logTable = it->second;
    delete logTable;
    filterMap.erase(it);
}


void LogViewer::filterSelectionChanged(QListWidgetItem* item, QListWidgetItem* previous)
{

    LogTable* oldLogTable = logTable;
    QString filterName = item->data(USERROLE_BASENAME).toString();
    item->setText(filterName);
    QFont font;
    font.setBold(false);
    item->setFont(font);

    if (filterMap.find(filterName) == filterMap.end())
    {
        showMessageBox("Filtername " + filterName + " not found.");
        return;
    }

    logTable = filterMap[filterName];

    if (!logTable)
    {
        ARMARX_ERROR << "logTable ptr is NULL" << flush;
        return;
    }



    if (oldLogTable)
    {
        oldLogTable->hide();
    }

    logTable->show();
    ui.edtLiveFilter->setText(logTable->getLiveFilterStr());
    ui.edtLiveSearch->setText(logTable->getModel()->getCurrentSearchStr());
}

void LogViewer::updateFilterList()
{
    static QFont font;

    // Update new message count in filter list box
    for (int i = 0; i < ui.lvFilters->count(); i++)
    {
        LogTable* curLogtable =  filterMap.find(ui.lvFilters->item(i)->data(USERROLE_BASENAME).toString())->second;
        QString newContent;

        if (curLogtable == logTable || curLogtable->getNewMessageCount() == 0)
        {
            font.setBold(false);
            ui.lvFilters->item(i)->setFont(font);
            newContent = ui.lvFilters->item(i)->data(USERROLE_BASENAME).toString();
            ui.lvFilters->item(i)->setBackgroundColor(ui.lvFilters->palette().base().color());
        }
        else
        {
            font.setBold(true);
            ui.lvFilters->item(i)->setFont(font);
            newContent = ui.lvFilters->item(i)->data(USERROLE_BASENAME).toString() + "(" + QString::number(curLogtable->getNewMessageCount()) + ")";

            if (curLogtable->getMaxNewLogLevelType() == eWARN)
            {
                ui.lvFilters->item(i)->setBackgroundColor(QColor(216, 120, 50));
            }
            else if (curLogtable->getMaxNewLogLevelType() == eERROR)
            {
                ui.lvFilters->item(i)->setBackgroundColor(QColor(255, 90, 80));
            }
            else if (curLogtable->getMaxNewLogLevelType() == eFATAL)
            {
                ui.lvFilters->item(i)->setBackgroundColor(QColor(255, 60, 50));
            }
            else
            {
                ui.lvFilters->item(i)->setBackgroundColor(ui.lvFilters->palette().base().color());
            }
        }

        ui.lvFilters->item(i)->setText(newContent);
        ui.lvFilters->item(i)->setToolTip(newContent);
    }
}

void LogViewer::OpenConfigureDialog()
{
    throw LocalException() << "Not yet implemented";
}

void LogViewer::insertPendingEntries()
{

    if (getState() >= eManagedIceObjectExiting)
    {
        return;
    }

    std::vector <LogMessage> pendingEntriesTemp;
    {
        ScopedLock lock(pendingEntriesMutex);
        pendingEntriesTemp.swap(pendingEntries);
    }



    unsigned int size = pendingEntriesTemp.size();
    boost::regex re(REGEX_COLORS);
    // check for new components, that are sending log messages and perform auto scroll
    for (unsigned int i = 0; i < size; i++)
    {
        LogMessage& msg = pendingEntriesTemp[i];
        msg.what = boost::regex_replace(msg.what, re, "");

        if (!getWidget() || ui.cbVerbosityLevel->currentIndex() > msg.type)
        {
            continue;
        }
        checkForNewComponents(msg.who.c_str());

        std::map<QString, LogTable*>::iterator it = filterMap.begin();

        for (; it != filterMap.end(); ++it)
        {
            LogTable* log = it->second;

            if (!log)
            {
                std::cerr << "log ptr is NULL" << std::endl;
                return;
            }

            //setRow(msg, currentRow);
            bool autoScroll = false;

            if (log->verticalScrollBar()->value() == log->verticalScrollBar()->maximum())
            {
                autoScroll = true;
            }

            //int row = log->addEntry(msg);

            if (autoScroll)
            {
                log->scrollToBottom();
            }

            //dynamic_cast<LogTableModel*> (logTable->model())->updateView();
        }

    }

    bool autoScroll = false;

    if (logTable->verticalScrollBar()->value() == logTable->verticalScrollBar()->maximum())
    {
        autoScroll = true;
    }

    std::map<QString, LogTable*>::iterator it = filterMap.begin();

    for (; it != filterMap.end(); ++it)
    {
        LogTable* log = it->second;
        int row = log->getModel()->rowCount();
        int rowsAdded = log->getModel()->addEntries(pendingEntriesTemp, log->getCurrentLiveFilter());
        if (rowsAdded > 0)
        {
            int count = log->getModel()->rowCount();
            int r;
            for (r = row; r < count; r++)
            {
                log->liveFilterRow(log->getCurrentLiveFilter(), r);
            }
        }
        bool autoScroll = false;

        if (log->verticalScrollBar()->value() == log->verticalScrollBar()->maximum())
        {
            autoScroll = true;
        }



        if (autoScroll)
        {
            log->scrollToBottom();
        }

    }



    if (autoScroll)
    {
        logTable->scrollToBottom();
    }

    updateFilterList();




}

void LogViewer::searchTypeChanged(int index)
{
    if (index == 0)
    {
        ui.edtLiveSearch->show();
        ui.btnPreviousItem->show();
        ui.btnNextItem->show();
        ui.edtLiveFilter->hide();
    }
    else
    {
        ui.edtLiveSearch->hide();
        ui.btnPreviousItem->hide();
        ui.btnNextItem->hide();
        ui.edtLiveFilter->show();
    }
}

void LogViewer::selectNextSearchResult()
{

    if (logTable)
    {
        if (!logTable->selectNextSearchResult(false))
        {
            getMainWindow()->statusBar()->showMessage("Could not find '" + logTable->getModel()->getCurrentSearchStr() + "' in the log!", 5000);
        }
    }
}

void LogViewer::selectPreviousSearchResult()
{
    if (logTable)
        if (!logTable->selectNextSearchResult(true))
        {
            getMainWindow()->statusBar()->showMessage("Could not find '" + logTable->getModel()->getCurrentSearchStr() + "' in the log!", 5000);
        }
}



QPointer<QWidget> LogViewer::getCustomTitlebarWidget(QWidget* parent)
{
    if (customToolbar)
    {
        if (parent != customToolbar->parent())
        {
            customToolbar->setParent(parent);
        }

        return customToolbar;
    }

    customToolbar = new QToolBar(parent);
    customToolbar->setIconSize(QSize(16, 16));
    customToolbar->addAction(QIcon(":/icons/configure-3.png"), "Configure", this, SLOT(OpenConfigureDialog()));

    return customToolbar;
}
