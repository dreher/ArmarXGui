/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QLabel>



namespace armarx
{
    class LogMessageDelegate : public QStyledItemDelegate
    {
        Q_OBJECT
    public:
        explicit LogMessageDelegate(QObject* parent = 0);
        ~LogMessageDelegate() override;

        //        void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
        //        QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
        void setEditorData(QWidget* editor, const QModelIndex& index) const override;
        void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
    signals:
        void resizeRow(int row);
    public slots:
        void commitAndCloseEditor();
    private:
        QLabel* _lineEdit;
    };
}

