/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui::SensorActorWidgetsPlugin
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LoggingPlugin.h"


// Qt
#include <QMdiSubWindow>
#include <QSettings>
#include <QListWidgetItem>

//ArmarX

#include "ArmarXLogViewer/LogViewer.h"


//Ice
#include <IceUtil/IceUtil.h>


using namespace std;

namespace armarx
{

    LoggingPlugin::LoggingPlugin()
    {
        addWidget<LogViewer>();
    }

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(armarX_gui_LoggingPlugin, LoggingPlugin)
#endif
}
