/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>

#include <QStandardItemModel>
#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>

#define CONDITION_ITEM_DELETE Qt::UserRole+1
#define CONDITION_ITEM_ID Qt::UserRole+2

namespace armarx
{
    class ConditionItemModel : public QStandardItemModel
    {
    public:
        ConditionItemModel();
        ~ConditionItemModel() override {}

        // update the model with the given registry
        void update(const ConditionRegistry& registry);

        // reset the model
        void reset();

    private:
        void extractFields(std::string in, std::string& componentName, std::string& eventName);
        void updateOrInsertCondition(int id, const ConditionRootPtr& condition);
        void markAllForDelete();
        void deleteUnusedItems();
    };
}

