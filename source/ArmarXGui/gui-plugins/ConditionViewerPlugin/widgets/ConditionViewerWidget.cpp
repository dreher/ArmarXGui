/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ConditionViewerWidget.h"

#include "../ConditionViewerWidgetController.h"
#include "../ConditionTreeFactory.h"

#include <ArmarXCore/observers/condition/Term.h>
#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/condition/ConditionRoot.h>

#include <QList>

using namespace std;
using namespace armarx;

ConditionViewerWidget::ConditionViewerWidget(ConditionViewerWidgetController* controller)
    : QWidget(0), timerId(0)
{
    this->controller = controller;

    ui.setupUi(this);

    // setup scene
    scene = new TermTreeGraphicsScene();
    ui.graphicsView->setScene(scene);

    // modes for conditions
    activeConditionsModel = new ConditionItemModel();
    pastConditionsModel = new ConditionItemModel();

    ui.activeConditionsTableView->setModel(activeConditionsModel);
    ui.pastConditionsTableView->setModel(pastConditionsModel);

    // condition checks table
    QStringList checksHeader;
    checksHeader << "datafield" << "checktype" << "parameters" << "state" << "current value";
    ui.checksTableWidget->setColumnCount(5);
    ui.checksTableWidget->setHorizontalHeaderLabels(checksHeader);
    ui.checksTableWidget->update();

    // item selection
    QItemSelectionModel* activeSelectionModel = ui.activeConditionsTableView->selectionModel();
    QItemSelectionModel* pastSelectionModel = ui.pastConditionsTableView->selectionModel();

    // connections
    connect(activeSelectionModel, SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(activeConditionItemSelected(const QItemSelection&, const QItemSelection&)));
    connect(pastSelectionModel, SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(pastConditionItemSelected(const QItemSelection&, const QItemSelection&)));
    connect(scene, SIGNAL(graphicsSceneClicked()), ui.checksTableWidget, SLOT(clearSelection()));
}

ConditionViewerWidget::~ConditionViewerWidget()
{
    if (updateTask)
    {
        updateTask->stop();
    }
}

void ConditionViewerWidget::onConnect()
{
    //clear scene and checks
    scene->clear();
    root.reset();
    ui.checksTableWidget->clearContents();
    ui.checksTableWidget->setRowCount(0);

    //reset the models
    activeConditionsModel->reset();
    pastConditionsModel->reset();

    // resize tables with active and past conditions
    ui.activeConditionsTableView->resizeColumnsToContents();
    ui.pastConditionsTableView->resizeColumnsToContents();

    // starting timer for periodic task
    while (timerId == 0)
    {
        timerId = startTimer(500);
    }

    updateTask = new PeriodicTask<ConditionViewerWidget>(this, &ConditionViewerWidget::updateLiterals, 200, false, "LiteralsUpdater");
    updateTask->start();
}

void ConditionViewerWidget::onDisconnect()
{
    killTimer(timerId);
    timerId = 0;
    if (updateTask)
    {
        updateTask->stop();
    }
}

// ****************************************************************
// slots
// ****************************************************************


void ConditionViewerWidget::activeConditionItemSelected(const QItemSelection& selected, const QItemSelection& deselected)
{
    ScopedRecursiveLock lock(dataMutex);

    if (selected.indexes().size() == 0)
    {
        return;
    }

    QModelIndexList list = selected.indexes();
    QStandardItem* i = activeConditionsModel->itemFromIndex(list.at(0));


    ConditionRootPtr condition = conditionFromItem(i, activeConditions);

    if (condition)
    {
        updateCondition(i->data(CONDITION_ITEM_ID).toInt(), condition);
    }
}

void ConditionViewerWidget::pastConditionItemSelected(const QItemSelection& selected, const QItemSelection& deselected)
{
    ScopedRecursiveLock lock(dataMutex);

    if (selected.indexes().size() == 0)
    {
        return;
    }

    QModelIndexList list = selected.indexes();
    QStandardItem* i = pastConditionsModel->itemFromIndex(list.at(0));

    ConditionRootPtr condition = conditionFromItem(i, pastConditions);

    if (condition)
    {
        updateCondition(i->data(CONDITION_ITEM_ID).toInt(), condition);
    }
}

ConditionRootPtr ConditionViewerWidget::conditionFromItem(QStandardItem* selectedItem, const ConditionRegistry& registry)
{
    ConditionRegistry::const_iterator iter = registry.find(selectedItem->data(CONDITION_ITEM_ID).toInt());

    if (iter != registry.end())
    {
        ConditionRootPtr conditionRoot = ConditionRootPtr::dynamicCast(iter->second);
        return conditionRoot;
    }

    return ConditionRootPtr();
}

void ConditionViewerWidget::updateCondition(int conditionId, ConditionRootPtr& condition)
{
    ScopedRecursiveLock lock(dataMutex);
    scene->clear();
    ui.checksTableWidget->clearContents();
    ui.checksTableWidget->setRowCount(0);
    if (controller->handler)
    {
        root = ConditionTreeFactory::createConditionTree(scene, ui.checksTableWidget, condition, conditionId, controller->getIceManager(), controller->handler);
        root->update();
    }


}

void ConditionViewerWidget::updateLiterals()
{
    if (!root)
    {
        return;
    }
    std::vector<TreeNodePtr> tempChildNodes;
    std::vector<TreeNodePtr> qtChildren;
    int conditionId = -1;
    {
        ScopedRecursiveLock lock(dataMutex);

        qtChildren.push_back(root);
        tempChildNodes = root->getChildren();
        conditionId = root->conditionId;
        qtChildren.insert(qtChildren.end(), tempChildNodes.begin(), tempChildNodes.end());
        auto result = controller->handler->getCondition(conditionId);
        if (!result)
        {
            return;
        }
        ConditionRootPtr newRoot = ConditionRootPtr::dynamicCast(result);
        TermImplSequence children;
        children.push_back(newRoot);
        auto temp2 = newRoot->getChilds();
        children.insert(children.end(), temp2.begin(), temp2.end());

        while (qtChildren.size() > 0)
        {
            TreeNodePtr node = *qtChildren.rbegin();
            TermImplPtr term = TermImplPtr::dynamicCast(*children.rbegin());
            qtChildren.pop_back();
            children.pop_back();
            auto newQtChildren =  node->getChildren();
            qtChildren.insert(qtChildren.end(), newQtChildren.begin(), newQtChildren.end());
            auto newChildren =  term->getChilds();
            children.insert(children.end(), newChildren.begin(), newChildren.end());

            if (children.size() != qtChildren.size())
            {
                ARMARX_WARNING_S << "Size discrepancy while updating term";
                return;
            }

            TermNodePtr termNode = boost::dynamic_pointer_cast<TermNode>(node);

            if (termNode && term)
            {
                emit termNode->newLiteralValue(term->getValue());
                termNode->timerEventRun();
            }
        }
    }

}

void ConditionViewerWidget::timerEvent(QTimerEvent* event)
{
    if (event->timerId() == timerId)
    {
        ScopedRecursiveLock lock(dataMutex);
        // retrieve conditions
        activeConditions = controller->getActiveConditions();
        pastConditions = controller->getPastConditions();

        // update model
        activeConditionsModel->update(activeConditions);
        pastConditionsModel->update(pastConditions);

        // resize view
        ui.activeConditionsTableView->resizeColumnsToContents();
        ui.pastConditionsTableView->resizeColumnsToContents();
    }
}
