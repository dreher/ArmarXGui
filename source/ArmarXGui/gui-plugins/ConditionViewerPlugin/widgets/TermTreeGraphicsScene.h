/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Qt
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <cstdio>

namespace armarx
{
    class TermTreeGraphicsScene : public QGraphicsScene
    {
        Q_OBJECT

    signals:
        void graphicsSceneClicked(const QPointF&);
        void graphicsSceneClicked();

    public slots:
        void resetActivation()
        {
            QPointF point(-1.0f, -1.0f);
            emit graphicsSceneClicked(point);
        }

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override
        {
            emit graphicsSceneClicked();
            emit graphicsSceneClicked(mouseEvent->scenePos());
        }
    };
}

