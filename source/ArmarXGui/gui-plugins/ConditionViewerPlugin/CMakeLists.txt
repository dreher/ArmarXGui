armarx_set_target("ConditionViewerGuiPlugin")

find_package(Eigen3 QUIET)

if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

set(SOURCES
    ConditionViewerGuiPlugin.cpp
    ConditionViewerWidgetController.cpp
    TreeNode.cpp
    TermNode.cpp
    widgets/ConditionViewerWidget.cpp
    widgets/ConditionItemModel.cpp
    widgets/TermNodeGraphicsItem.cpp
)

set(HEADERS
    ConditionTreeFactory.h
    ConditionViewerGuiPlugin.h
    ConditionViewerWidgetController.h
    TreeNode.h
    TermNode.h
    widgets/ConditionViewerWidget.h
    widgets/ConditionItemModel.h
    widgets/TermTreeGraphicsScene.h
    widgets/TermNodeGraphicsItem.h
)

set(GUI_MOC_HDRS
    ConditionViewerGuiPlugin.h
    ConditionViewerWidgetController.h
    TermNode.h
    widgets/ConditionViewerWidget.h
    widgets/TermTreeGraphicsScene.h
    widgets/TermNodeGraphicsItem.h
)

set(GUI_UIS
    widgets/ConditionViewerWidget.ui
)

set(COMPONENT_LIBS ArmarXCoreObservers)

armarx_gui_library(ConditionViewerGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}" )
