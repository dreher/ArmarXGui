/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui::TreeNode
* @author     Kai Welke ( welke at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

// boost
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

// qt
#include <QPointF>
#include <QRectF>
#include <QSize>
#include <QGraphicsScene>
#include <QGraphicsEllipseItem>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT TreeNode;

    typedef boost::shared_ptr<TreeNode> TreeNodePtr;
    typedef boost::weak_ptr<TreeNode> TreeNodeWeakPtr;

    class ARMARXCOMPONENT_IMPORT_EXPORT TreeNode : public boost::enable_shared_from_this<TreeNode>
    {
    public:
        // constants
        static const QSize DefaultNodeHorizontalSeparator;
        static const QSize DefaultNodeVerticalSeparator;
        static const QSize DefaultNodeSize;

        /**
         * Constructs a tree node as part of a Qt visualizable tree.
         * @param scene the graphics scene used for rendering
         * @param nodeSite size of the nodes boundingbox
         */
        TreeNode(QGraphicsScene* scene, QSize nodeSize = TreeNode::DefaultNodeSize);

        /**
         * Adds a child to the node in the tree structure
         * @param child the child to add
         */
        void addChild(TreeNodePtr child);

        /**
         * Updates the layout of the tree. Only applicable for the root node. Is ignored
         * for all other nodes.
         * @param positionLeftTop left top position in the graphicsscene, where the tree is drawn.
         */
        void update(QPointF positionLeftTop = QPointF(0, 0));


        /**
         * Retrieve size of the node.
         * @return node size
         */
        QSize getSize()
        {
            return size;
        }

        std::vector<TreeNodePtr> getChildren() const;

        /**
         * Retrieve boundingbox of the node. Only valid after update has been called.
         * @return boundingbox of the node in the graphicsscene
         */
        QRectF getBoundingBox()
        {
            return boundingBox;
        }

        /**
         * Retrieve size of the complete subtree where the current node is root. Only valid after
         * update has been callse;
         * @return size of the subtree represented by this node
         */
        QSize getSubTreeSize()
        {
            return subTreeSize;
        }

    protected:
        /**
         * Sets size of node. Layout has to be updated afterwards.
         * @param nodeSize size of the node
         */
        void setSize(QSize nodeSize)
        {
            size = nodeSize;
        }

        /**
         * Draws an edge to this node. Overwrite this in order provide your own visualization.
         * @param line line from parent to this node
         */
        virtual void drawEdge(QLineF line);

        /**
         * Draws the node. Overwrite this in order provide your own visualization.
         * @param boundingBox of the node to be drawn
         */
        virtual void drawNode(QRectF boundingBox);

    private:
        // tree structure
        void setParent(TreeNodeWeakPtr parent);

        // recursive layout update
        void updateLayout(QPointF center);

        // recursive size calculation
        QSize calculateSubTreeSize();

        // helpers methods
        QRectF calculateChildsBoundingBox();
        void drawEdges();

        // visualization
        QGraphicsEllipseItem* nodeItem;
        QGraphicsLineItem* edgeItem;

        // sizes
        QSize size;        // node size
        QSize subTreeSize; // including all subnodes
        QRectF boundingBox; // bounding box of subtree

        // tree structure
        TreeNodeWeakPtr parent;
        std::vector<TreeNodePtr> childs;
    };
}

