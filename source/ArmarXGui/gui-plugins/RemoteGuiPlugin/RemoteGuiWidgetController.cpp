/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXGui::gui-plugins::RemoteGuiWidgetController
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiWidgetController.h"

#include <ArmarXGui/libraries/RemoteGui/Widgets.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <boost/scope_exit.hpp>

#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <string>

using namespace armarx;

namespace
{

    const char* const REMOTE_TAB_ID = "remote.tab_id";
    const char* const REMOTE_WIDGET_NAME = "remote.widget_name";

    struct InternalUpdateGuard
    {
        InternalUpdateGuard(std::atomic<bool>* enabled)
            : enabled(enabled)
        {
            *enabled = true;
        }

        ~InternalUpdateGuard()
        {
            *enabled = false;
        }

        std::atomic<bool>* enabled;
    };

    bool operator == (RemoteGui::Value const& left, RemoteGui::Value const& right)
    {
        if (left.type != right.type)
        {
            return false;
        }

        switch (left.type)
        {
            case RemoteGui::eValue_Unknown:
                return true;

            case RemoteGui::eValue_Bool:
                return (left.valueInt != 0) == (right.valueInt != 0);

            case RemoteGui::eValue_Int:
                return left.valueInt == right.valueInt;

            case RemoteGui::eValue_Float:
                return left.valueFloat == right.valueFloat;

            case RemoteGui::eValue_String:
                return left.valueString == right.valueString;
        }

        throw std::logic_error("Remote value type not handled");
    }

    bool operator != (RemoteGui::Value const& left, RemoteGui::Value const& right)
    {
        return !(left == right);
    }

}

RemoteGuiWidgetController::RemoteGuiWidgetController()
{
    widget.setupUi(getWidget());
    widget.remoteTabWidget->clear();

    connect(widget.sendRemoteValuesButton, SIGNAL(clicked()), this, SLOT(onSendButtonClicked()));

    connect(this, SIGNAL(tabChanged(QString)), this, SLOT(onTabChanged(QString)), Qt::QueuedConnection);
    connect(this, SIGNAL(tabsChanged()), this, SLOT(onTabsChanged()), Qt::QueuedConnection);
    connect(this, SIGNAL(widgetChanged(QString)), this, SLOT(onWidgetChanged(QString)), Qt::QueuedConnection);
    connect(this, SIGNAL(stateChanged(QString)), this, SLOT(onStateChanged(QString)), Qt::QueuedConnection);
}


RemoteGuiWidgetController::~RemoteGuiWidgetController()
{

}

QPointer<QDialog> RemoteGuiWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new SimpleConfigDialog(parent);
        dialog->addProxyFinder<RemoteGuiInterfacePrx>({"RemoteGuiProvider", "", "RemoteGui*"});
    }
    return qobject_cast<SimpleConfigDialog*>(dialog);
}

void RemoteGuiWidgetController::configured()
{
    remoteGuiProviderName = dialog->getProxyName("RemoteGuiProvider");
}


void RemoteGuiWidgetController::loadSettings(QSettings* settings)
{

}

void RemoteGuiWidgetController::saveSettings(QSettings* settings)
{

}


void RemoteGuiWidgetController::onInitComponent()
{
    usingProxy(remoteGuiProviderName);
}


void RemoteGuiWidgetController::onConnectComponent()
{
    remoteGuiProvider = getProxy<RemoteGuiInterfacePrx>(remoteGuiProviderName);

    // Setup data structures
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        tabs = remoteGuiProvider->getTabs();
        tabWidgetStates = remoteGuiProvider->getTabStates();
        tabStates = remoteGuiProvider->getValuesForAllTabs();
    }

    // Load initial values into GUI
    emit tabsChanged();

    // Subscribe to updates
    usingTopic(remoteGuiProvider->getTopicName());
}

void RemoteGuiWidgetController::reportTabChanged(const std::string& tab, const Ice::Current&)
{
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        tabs = remoteGuiProvider->getTabs();
        tabWidgetStates = remoteGuiProvider->getTabStates();
        tabStates = remoteGuiProvider->getValuesForAllTabs();
    }
    QString qTabId = QString::fromStdString(tab);
    emit tabChanged(qTabId);
}


void RemoteGuiWidgetController::reportTabsRemoved(const Ice::Current&)
{
    // Setup data structures
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        tabs = remoteGuiProvider->getTabs();
        tabWidgetStates = remoteGuiProvider->getTabStates();
        tabStates = remoteGuiProvider->getValuesForAllTabs();
    }
    emit tabsChanged();
}

void RemoteGuiWidgetController::reportWidgetChanged(const std::string& tab, const RemoteGui::WidgetStateMap& widgetState, const Ice::Current&)
{
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        tabWidgetStates[tab] = widgetState;
    }
    QString qTabId = QString::fromStdString(tab);
    emit widgetChanged(qTabId);
}

void RemoteGuiWidgetController::reportStateChanged(const std::string& tab, const RemoteGui::ValueMap& newState, const Ice::Current&)
{
    {
        std::unique_lock<std::mutex> lock(stateMutex);
        tabStates[tab] = newState;
    }
    QString qTabId = QString::fromStdString(tab);
    emit stateChanged(qTabId);
}

void RemoteGuiWidgetController::onTabChanged(QString qid)
{
    // The root widget of a tab was changed, so we have to create everything from scratch
    std::string id = RemoteGui::toUtf8(qid);

    std::unique_lock<std::mutex> lock(stateMutex);

    createTab(id);
    updateWidgets(id);

    removeObsoleteTabs();
}

void RemoteGuiWidgetController::onTabsChanged()
{
    std::unique_lock<std::mutex> lock(stateMutex);

    // Add or update existing tabs
    for (auto& tabWidget : tabs)
    {
        QString qid = QString::fromStdString(tabWidget.first);
        std::string id = RemoteGui::toUtf8(qid);
        createTab(id);
        updateWidgets(id);
    }

    removeObsoleteTabs();
}

void RemoteGuiWidgetController::onWidgetChanged(QString qid)
{
    // The display options of some widgets changed (e.g. enabled, hidden)
    std::string id = RemoteGui::toUtf8(qid);

    std::unique_lock<std::mutex> lock(stateMutex);

    updateWidgets(id);
}

void RemoteGuiWidgetController::onStateChanged(QString qid)
{
    // The remotely stored values changed => update the displayed values
    std::string id = RemoteGui::toUtf8(qid);
    updateState(id);
}

void RemoteGuiWidgetController::onGuiStateChanged()
{
    if (internalUpdate)
    {
        return;
    }

    QWidget* widget = qobject_cast<QWidget*>(sender());
    if (!widget)
    {
        ARMARX_WARNING << "Expected a widget as sender of onGuiStateChanged()";
        return;
    }

    std::string id = widget->property(REMOTE_TAB_ID).toString().toStdString();
    std::string name = widget->property(REMOTE_WIDGET_NAME).toString().toStdString();

    std::unique_lock<std::mutex> lock(stateMutex);

    RemoteGui::Value& currentValue = tabStates.at(id).at(name);

    RemoteGui::WidgetPtr const& desc = guiDescriptions.at(id).at(name);
    auto& widgetHandler = RemoteGui::getWidgetHandler(desc);
    RemoteGui::Value newValue = widgetHandler.handleGuiChange(*desc, widget);

    if (newValue != currentValue)
    {
        currentValue = newValue;
        doAutoUpdate(id);
    }
}

void RemoteGuiWidgetController::onSendButtonClicked()
{
    int index = widget.remoteTabWidget->currentIndex();
    QString currentTabText = widget.remoteTabWidget->tabText(index);
    std::string tabId = RemoteGui::toUtf8(currentTabText);

    remoteGuiProvider->setValues(tabId, tabStates[tabId]);
}

void RemoteGuiWidgetController::createTab(std::string const& tabId)
{
    RemoteGui::WidgetPtr widgetP = tabs.at(tabId);
    ARMARX_CHECK_EXPRESSION(widgetP);

    ARMARX_INFO << "Updating GUI for tab '" << tabId << "'";

    // Build UI elements
    QString qTabId = QString::fromStdString(tabId);
    QWidget* tabHolder = widget.remoteTabWidget->findChild<QWidget*>(qTabId);
    QVBoxLayout* layout = nullptr;
    if (tabHolder)
    {
        // Delete all the children
        while (QWidget* w = tabHolder->findChild<QWidget*>())
        {
            delete w;
        }
        layout = qobject_cast<QVBoxLayout*>(tabHolder->layout());
    }
    else
    {
        tabHolder = new QWidget(widget.remoteTabWidget);
        tabHolder->setObjectName(qTabId);
        layout = new QVBoxLayout(tabHolder);
        layout->setContentsMargins(0, 0, 0, 0);
        tabHolder->setLayout(layout);
        widget.remoteTabWidget->addTab(tabHolder, qTabId);
    }

    guiWidgets[tabId].clear();
    QWidget* newTab = createWidgetFromDescription(tabId, widgetP);

    newTab->setParent(tabHolder);
    layout->addWidget(newTab);
}

void RemoteGuiWidgetController::removeObsoleteTabs()
{
    for (int i = 0; i < widget.remoteTabWidget->count();)
    {
        QString qTabId = widget.remoteTabWidget->tabText(i);
        std::string tabId = RemoteGui::toUtf8(qTabId);

        bool found = tabs.count(tabId) > 0;
        if (found)
        {
            ++i;
        }
        else
        {
            ARMARX_INFO << "Removing tab: " << tabId;
            widget.remoteTabWidget->widget(i)->deleteLater();
            widget.remoteTabWidget->removeTab(i);

            guiWidgets.erase(tabId);
        }
    }
}

void RemoteGuiWidgetController::updateWidgets(std::string const& tabId)
{
    // External widget state update received
    RemoteGui::WidgetStateMap const& states = tabWidgetStates.at(tabId);
    std::map<std::string, QWidget*> const& widgets = guiWidgets.at(tabId);

    for (auto& nameState : states)
    {
        std::string const& name = nameState.first;
        RemoteGui::WidgetState const& state = nameState.second;
        QWidget* widget = widgets.at(name);

        widget->setHidden(state.hidden);
        widget->setDisabled(state.disabled);
    }
}

void RemoteGuiWidgetController::updateState(std::string const& tabId)
{
    // External state update received
    std::unique_lock<std::mutex> lock(stateMutex);

    // Do not handle GUI updates triggered by this method (internal update only)
    InternalUpdateGuard guard(&internalUpdate);

    RemoteGui::ValueMap const& values = tabStates.at(tabId);
    std::map<std::string, QWidget*> const& widgets = guiWidgets.at(tabId);
    RemoteGui::WidgetMap const& widgetDesc = guiDescriptions.at(tabId);

    for (auto& nameValue : values)
    {
        std::string const& name = nameValue.first;
        RemoteGui::Value const& value = nameValue.second;
        RemoteGui::WidgetPtr const& desc = widgetDesc.at(name);
        QWidget* widget = widgets.at(name);

        auto& widgetHandler = RemoteGui::getWidgetHandler(desc);
        widgetHandler.updateGui(*desc, widget, value);
    }
}

QWidget* RemoteGuiWidgetController::createWidgetFromDescription(const std::string& tabId, const RemoteGui::WidgetPtr& desc)
{
    QWidget* widget = nullptr;

    RemoteGui::ValueMap& values = tabStates[tabId];
    bool useDefaultValue = values.count(desc->name) == 0;
    RemoteGui::Value const& initialValue = useDefaultValue ? desc->defaultValue : values[desc->name];

    RemoteGui::CreateWidgetCallback createChild = [this, tabId](RemoteGui::WidgetPtr const & desc) -> QWidget*
    {
        return this->createWidgetFromDescription(tabId, desc);
    };

    {
        // Do not trigger external updates when initial values are set
        InternalUpdateGuard guard(&internalUpdate);

        auto& widgetHandler = RemoteGui::getWidgetHandler(desc);
        widget = widgetHandler.createWidget(*desc, initialValue, createChild,
                                            this, SLOT(onGuiStateChanged()));
    }

    ARMARX_CHECK_EXPRESSION(widget != nullptr);

    widget->setProperty(REMOTE_TAB_ID, QString::fromStdString(tabId));
    widget->setProperty(REMOTE_WIDGET_NAME, QString::fromStdString(desc->name));

    // Widgets without a name cannot be referred to later
    if (!desc->name.empty())
    {
        auto result = guiWidgets[tabId].emplace(desc->name, widget);
        bool inserted = result.second;
        if (inserted)
        {
            guiDescriptions[tabId][desc->name] = desc;
            values[desc->name] = initialValue;
        }
        else
        {
            ARMARX_WARNING << "Tried to add a widget with duplicate name '" << desc->name
                           << "' to remote tab ID '" << tabId << "'";
        }
    }

    return widget;
}

void RemoteGuiWidgetController::doAutoUpdate(std::string const& id)
{
    if (widget.autoUpdateCheckBox->checkState() == Qt::Checked)
    {
        remoteGuiProvider->setValues(id, tabStates[id]);
    }
}
