/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::gui-plugins::RemoteGuiWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ui_RemoteGuiWidget.h"

#include <ArmarXGui/libraries/RemoteGui/WidgetRegister.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <boost/variant.hpp>
#include <functional>
#include <mutex>

namespace armarx
{

    /**
    \page ArmarXGui-GuiPlugins-RemoteGui RemoteGui
    \brief The RemoteGui allows visualizing remotely defined widgets for configuration.

    \image html RemoteGui.png
    The user can create customized GUIs using the RemoteGuiProvider.
    This plugin will display the created GUIs.

    API Documentation \ref RemoteGuiWidgetController

    \see RemoteGuiPlugin
    \see RemoteGuiProvider
    \see RemoteGuiExample
    */

    /**
     * \class RemoteGuiWidgetController
     * \brief RemoteGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        RemoteGuiWidgetController
        : public armarx::ArmarXComponentWidgetControllerTemplate < RemoteGuiWidgetController >
        , public armarx::RemoteGuiListenerInterface
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit RemoteGuiWidgetController();

        /**
         * Controller destructor
         */
        virtual ~RemoteGuiWidgetController();

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void configured() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Util.RemoteGui";
        }

        // ManagedIceObject interface
        void onInitComponent() override;
        void onConnectComponent() override;

        // RemoteGuiListenerInterface interface
        void reportTabChanged(const std::string& tab, const Ice::Current&) override;
        void reportTabsRemoved(const Ice::Current&) override;
        void reportWidgetChanged(const std::string& tab, const RemoteGui::WidgetStateMap& widgetState, const Ice::Current&) override;
        void reportStateChanged(const std::string& tab, const RemoteGui::ValueMap& newState, const Ice::Current&) override;

    public slots:
        /* QT slot declarations */
        void onTabChanged(QString id);
        void onTabsChanged();
        void onWidgetChanged(QString id);
        void onStateChanged(QString id);

        void onGuiStateChanged();

        void onSendButtonClicked();

    signals:
        /* QT signal declarations */
        void tabChanged(QString id);
        void tabsChanged();
        void widgetChanged(QString id);
        void stateChanged(QString id);

    private:
        void createTab(std::string const& tabId);
        void removeObsoleteTabs();
        void updateWidgets(std::string const& tabId);
        void updateState(std::string const& tabId);


        void createWidgets(std::string const& id);
        void updateValuesInWidget(std::string const& id);

        void updateValuesInState();

        void doAutoUpdate(std::string const& id);

        QWidget* createWidgetFromDescription(std::string const& tabId, RemoteGui::WidgetPtr const& desc);

    private:
        /**
         * Widget Form
         */
        Ui::RemoteGuiWidget widget;

        QPointer<SimpleConfigDialog> dialog;
        std::string remoteGuiProviderName;
        RemoteGuiInterfacePrx remoteGuiProvider;

        std::mutex stateMutex;
        RemoteGui::WidgetMap tabs;
        std::map<std::string, RemoteGui::WidgetMap> guiDescriptions;
        RemoteGui::TabValueMap tabStates;
        RemoteGui::TabWidgetStateMap tabWidgetStates;

        std::map<std::string, std::map<std::string, QWidget*>> guiWidgets;

        std::atomic<bool> internalUpdate {false};
    };
}
