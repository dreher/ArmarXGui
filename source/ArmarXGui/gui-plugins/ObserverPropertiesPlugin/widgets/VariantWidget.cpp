/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "VariantWidget.h"
#include <QLineEdit>
#include <QLabel>
#include <QTextEdit>
#include <QVBoxLayout>

using namespace armarx;

VariantWidget::VariantWidget(VariantPtr variant, QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f)
{
    this->variant = variant;
    enableEdit = true;
    showType = false;
    layout = new QVBoxLayout(this);
    variantEdit = new QTextEdit(this);
    setLayout(layout);
    setupLayout();
}

void VariantWidget::setValue(const VariantPtr& variant)
{
    VariantTypeId type = variant->getType();
    QString valueString;
    if (variant->getInitialized())
    {
        if (type == VariantType::Bool)
        {
            QString value("%1");
            value = value.arg((variant->getBool() == 1));
            valueString = value;
        }

        else if (type == VariantType::Float)
        {
            QString value("%1");
            value = value.arg(variant->getFloat(), 0, 'f', 2);
            valueString = value;
        }

        else if (type == VariantType::Double)
        {
            QString value("%1");
            value = value.arg(variant->getDouble(), 0, 'f', 2);
            valueString = value;
        }

        else if (type == VariantType::Int)
        {
            QString value("%1");
            value = value.arg(variant->getInt());
            valueString = value;
        }

        else if (type == VariantType::String)
        {
            QString value(variant->getString().c_str());
            valueString = value;
        }
        else
        {
            VariantDataClassPtr  var = variant->getClass<VariantDataClass>();
            QString value(var->output().c_str());
            valueString = value;
        }

    }
    else
    {
        valueString = "<i>not initalized</i>";
    }
    variantEdit->setEnabled(variant->getInitialized());
    variantEdit->setText(valueString);
}

void VariantWidget::setEnableEdit(bool enableEdit)
{
    this->enableEdit = enableEdit;
    setupLayout();
}

void VariantWidget::setShowType(bool showType)
{
    this->showType = showType;
    setupLayout();
}

bool VariantWidget::getEnableEdit()
{
    return enableEdit;
}

bool VariantWidget::getShowType()
{
    return showType;
}

QString VariantWidget::getValueText()
{
    return variantEdit->toPlainText();
}


void VariantWidget::setupLayout()
{
    int valueEditWidth = 120;


    // clean old lineedits
    std::vector<QWidget*>::iterator iter = widgets.begin();

    while (iter != widgets.end())
    {
        delete *iter;
        iter++;
    }
    widgets.clear();
    int x_start = 0;
    int y_start = 0;
    int x_gap = 10;



    layout->addWidget(variantEdit);
    variantEdit->setGeometry(x_start + 0 * (valueEditWidth + x_gap), y_start, valueEditWidth, 20);
    variantEdit->setReadOnly(!enableEdit);

    setValue(variant);



    if (showType)
    {
        int x = x_start + 1 * (valueEditWidth + x_gap);
        QLabel* label = new QLabel(this);
        label->setTextInteractionFlags(Qt::TextSelectableByMouse);
        label->setGeometry(x, y_start, 120, 21);
        label->setText(variant->getTypeName().c_str());
        layout->addWidget(label);

        widgets.push_back(label);
    }
}

void VariantWidget::lineEdit_editFinished()
{
    // NYI
}
