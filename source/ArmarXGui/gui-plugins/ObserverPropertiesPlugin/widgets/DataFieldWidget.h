/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Qt
#include <QWidget>
#include <string>
#include <map>

// ArmarX
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include "VariantWidget.h"
#include <QLabel>

namespace armarx
{
    class DataFieldWidget : public QWidget
    {
        Q_OBJECT
    public:
        DataFieldWidget(DataFieldIdentifierPtr dataFieldIdentifier, VariantPtr variant, QWidget* parent = 0, Qt::WindowFlags f = 0);

        void setEnableEdit(bool enableEdit);

        bool getEnableEdit();

    private:
        void setupLayout();

        DataFieldIdentifierPtr dataFieldIdentifier;
        VariantPtr variant;
        VariantWidget* variantWidget;

        QLabel* label;

        bool enableEdit;
    };
}

