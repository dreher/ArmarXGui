/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ConditionPropertiesWidget.h"
#include <ArmarXCore/interface/observers/ConditionBase.h>

using namespace armarx;


ConditionPropertiesWidget::ConditionPropertiesWidget(const ConditionCheckBasePtr condition, QWidget* parent, Qt::WindowFlags f)
    : PropertiesWidget(parent, f)
{
    createUi(condition);
}

void ConditionPropertiesWidget::createUi(const ConditionCheckBasePtr condition)
{
    // create property entries
    setPropertiesTitle("Condition");
    addProperty("Condition identifier", QString("%1").arg(condition->info.identifier.uniqueId));
    std::string text;

    if (condition->info.status.fulFilled)
    {
        text = "True";
    }
    else
    {
        text = "False";
    }

    addProperty("Fulfilled", text.c_str());

}
