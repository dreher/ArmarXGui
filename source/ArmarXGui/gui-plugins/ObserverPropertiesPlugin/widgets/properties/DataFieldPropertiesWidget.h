/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Kai Welke (welke@kit.edu)
* @copyright  2012 Humanoids Group, IAIM, IFA
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// Qt
#include <QWidget>
#include <string>
#include <map>

// ArmarX
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include "../VariantWidget.h"
#include "PropertiesWidget.h"
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include<ArmarXCore/core/services/tasks/PeriodicTask.h>
namespace armarx
{
    class PlotterController;
    class DataFieldPropertiesWidget : public PropertiesWidget
    {
        Q_OBJECT
    public:
        DataFieldPropertiesWidget(std::string description, DatafieldRefPtr dataFieldIdentifier, VariantPtr variant, IceManagerPtr iceManager, QWidget* parent = 0, Qt::WindowFlags f = 0);
        ~DataFieldPropertiesWidget() override;
        void setEnableEdit(bool enableEdit);
        bool getEnableEdit();
        DataFieldIdentifierPtr getDatafieldIdentifier() const;
        VariantPtr getVariant();
    public slots:
        void toggleAutoRefresh(bool);
        void copyValueToClipboard();
    private:
        void createUi() override;

        DatafieldRefPtr dataFieldIdentifier;
        VariantPtr variant;
        std::string description;

        VariantWidget* variantWidget;
        PlotterController* plotter;
        PeriodicTask<DataFieldPropertiesWidget>::pointer_type updateTask;

    protected:
        void updateValue();
    };
}

