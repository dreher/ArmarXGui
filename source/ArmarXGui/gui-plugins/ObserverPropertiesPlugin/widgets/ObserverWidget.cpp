/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ObserverWidget.h"
#include "../ObserverGuiPlugin.h"

#include <QList>
#include <QMenu>
#include <QAction>
#include <QItemSelectionModel>

#include "../ObserverWidgetController.h"
#include <ArmarXGui/gui-plugins/ObserverPropertiesPlugin/widgets/properties/DataFieldPropertiesWidget.h>
#include "ui_FilterPropertiesDialog.h"
#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/ButterworthFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>

using namespace std;
using namespace armarx;

ObserverWidget::ObserverWidget(ObserverWidgetController* controller) :
    QWidget(0),
    model(NULL)
{
    this->controller = controller;
    qRegisterMetaType<DatafieldFilterBasePtr>("DatafieldFilterBasePtr");
    ui.setupUi(this);
    ui.propertiesView->setLayout(new QVBoxLayout());
    ui.observerTreeView->setSortingEnabled(true);
    //ui.observerTreeView->setHideChildren(false);

    propertiesWidget = 0;

    updateTimer = new QTimer(this);

    connect(ui.observerTreeView, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(treeView_contextMenu(const QPoint&)));

    QList<int> sizes;
    sizes.append(400);
    sizes.append(800);

    ui.splitter->setSizes(sizes);

    autoUpdateAction = new QAction("Auto update", ui.observerTreeView);

    filterExpansionTimer.setSingleShot(true);
    connect(&filterExpansionTimer, SIGNAL(timeout()), this, SLOT(delayedFilterExpansion()));
}

void ObserverWidget::createNewModel()
{
    model = new ObserverItemModel(controller->getIceManager(), controller->getVariantInfo());
    proxyModel = new InfixFilterModel(this);
    proxyModel->setSourceModel(model);
    ui.observerTreeView->setModel(proxyModel);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateObservers()));
    QItemSelectionModel* selectionModel = ui.observerTreeView->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(treeView_selected(const QItemSelection&, const QItemSelection&)));
    //    updateObservers();

    connect(ui.lineEditSearch, SIGNAL(textChanged(QString)), proxyModel, SLOT(setFilterFixedString(QString)));
    connect(ui.lineEditSearch, SIGNAL(textChanged(QString)), this, SLOT(expandFilterSelection(QString)), Qt::QueuedConnection);
}

void ObserverWidget::updateObservers()
{
    if (model)
    {
        model->updateObservers();
    }

    auto selection = ui.observerTreeView->selectionModel()->selectedIndexes();

    if (selection.size() > 0)
    {
        if (propertiesWidget)
        {
            ui.propertiesView->layout()->removeWidget(propertiesWidget);
            propertiesWidget->deleteLater();
            propertiesWidget = NULL;
        }
        auto tempWidgetPtr = model->getPropertiesWidget(proxyModel->mapToSource(selection.at(0)), ui.propertiesView);
        if (tempWidgetPtr)
        {
            propertiesWidget = tempWidgetPtr;
            ui.propertiesView->layout()->addWidget(propertiesWidget);
            propertiesWidget->show();
        }
    }
}

void ObserverWidget::treeView_selected(const QItemSelection& selected, const QItemSelection& deselected)
{
    if (selected.indexes().size() == 0)
    {
        return;
    }

    if (propertiesWidget)
    {
        ui.propertiesView->layout()->removeWidget(propertiesWidget);
        propertiesWidget->deleteLater();
        propertiesWidget = NULL;
    }

    QModelIndexList list = selected.indexes();
    propertiesWidget = model->getPropertiesWidget(proxyModel->mapToSource(list.at(0)), ui.propertiesView);
    ui.propertiesView->layout()->addWidget(propertiesWidget);
    propertiesWidget->show();
}

void ObserverWidget::toggleAutoUpdate(bool checked)
{
    if (checked)
    {
        updateTimer->start(500);
    }
    else
    {
        updateTimer->stop();
    }
}

void ObserverWidget::filterSelected()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {
        //        auto widget = qobject_cast<DataFieldPropertiesWidget*>(propertiesWidget);
        DatafieldFilterBasePtr filter;
        if (!action->data().isNull())
        {
            auto filterName = action->data().toString().toStdString();
            auto objFac = controller->getIceManager()->getCommunicator()->findObjectFactory(filterName);
            if (!objFac)
            {
                ARMARX_WARNING << "Could not find obj factory of type " << filterName;
                return;
            }

            filter = DatafieldFilterBasePtr::dynamicCast(objFac->create(filterName));
            if (!filter)
            {
                ARMARX_WARNING << "Could not create filter of type " << objFac;
                return;
            }
        }
        else
        {
            ARMARX_ERROR << "Filter " << action->text().toStdString() << " is unknown";
            return;
        }
        QDialog d;
        Ui::FilterPropertiesDialog dSetup;
        dSetup.setupUi(&d);
        dSetup.tableWidget->setSortingEnabled(false);
        StringFloatDictionary props = filter->getProperties();
        ARMARX_INFO << VAROUT(props);
        dSetup.tableWidget->setRowCount(props.size());
        dSetup.tableWidget->setColumnCount(2);
        int i = 0;
        for (auto& p : props)
        {
            auto keyItem = new QTableWidgetItem(p.first.c_str());
            keyItem->setFlags(Qt::ItemIsEnabled);
            dSetup.tableWidget->setItem(i, 0, keyItem);
            auto valueItem = new QTableWidgetItem(QString::number(p.second));
            dSetup.tableWidget->setItem(i, 1, valueItem);
            i++;
        }
        dSetup.groupBox->setTitle(action->text() + " properties");
        if (d.exec() == QDialog::Accepted)
        {
            props.clear();
            for (int i = 0; i < dSetup.tableWidget->rowCount(); ++i)
            {
                bool ok;
                float value = dSetup.tableWidget->item(i, 1)->text().toFloat(&ok);
                if (ok)
                {
                    props[dSetup.tableWidget->item(i, 0)->text().toStdString()] = value;
                }
            }
            ARMARX_INFO << VAROUT(props);
            filter->setProperties(props);
            emit createFilterClicked(selectedFilterId,
                                     filter);
        }

    }
    selectedFilterId.clear();
}

void ObserverWidget::clearDetailedView()
{
    if (propertiesWidget)
    {
        ui.propertiesView->layout()->removeWidget(propertiesWidget);
        propertiesWidget->deleteLater();
        propertiesWidget = NULL;
    }
}

void ObserverWidget::treeView_contextMenu(const QPoint& point)
{
    QPoint globalPos = ui.observerTreeView->viewport()->mapToGlobal(point);
    QMenu treeViewMenu;

    // update action
    QAction* updateAction = new QAction("Update", ui.observerTreeView);

    if (autoUpdateAction->isChecked())
    {
        updateAction->setEnabled(false);
    }

    connect(updateAction, SIGNAL(triggered()), this, SLOT(updateObservers()));
    treeViewMenu.addAction(updateAction);

    //    // auto update action
    //    autoUpdateAction->setCheckable(true);
    //    connect(autoUpdateAction, SIGNAL(toggled(bool)), this, SLOT(toggleAutoUpdate(bool)));
    //    treeViewMenu.addAction(autoUpdateAction);

    DataFieldPropertiesWidget* widget = qobject_cast<DataFieldPropertiesWidget*>(propertiesWidget);
    if (widget)
    {
        auto typeId = widget->getVariant()->getType();
        selectedFilterId = widget->getDatafieldIdentifier()->getIdentifierStr();
        QMenu* filters = treeViewMenu.addMenu("Filters");
        std::map<std::string, bool> filterNames;
        for (const FactoryCollectionBasePtr& facContainer : FactoryCollectionBase::GetPreregistratedFactories())
        {
            if (!facContainer)
            {
                continue;
            }
            for (auto pair : facContainer->getFactories())
            {

                Ice::ObjectFactoryPtr fac = pair.second;
                if (!fac)
                {
                    continue;
                }
                try
                {
                    auto obj = fac->create(pair.first);
                    if (!obj)
                    {
                        continue;
                    }
                    //                    auto ids = obj->ice_ids();
                    //                    auto it = std::find(ids.begin(), ids.end(), "::armarx::DatafieldFilterBase");
                    DatafieldFilterBasePtr filter = DatafieldFilterBasePtr::dynamicCast(obj);
                    if (filter)
                    {
                        auto supportedTypes = filter->getSupportedTypes();
                        auto typeIt = std::find(supportedTypes.begin(), supportedTypes.end(), typeId);
                        filterNames.insert(std::make_pair(pair.first, typeIt != supportedTypes.end()));
                    }
                }
                catch (...)
                {

                }
            }
        }
        for (auto filter : filterNames)
        {
            const boost::regex e("::armarx::([a-zA-Z0-9_]+)Base");
            boost::match_results<std::string::const_iterator> what;
            boost::regex_search(filter.first, what, e);
            QAction* a;
            if (what.size() < 2)
            {
                a = filters->addAction(filter.first.c_str(), this, SLOT(filterSelected()));

            }
            else
            {
                std::string filteredName = what[1];
                a = filters->addAction(filteredName.c_str(), this, SLOT(filterSelected()));

            }
            a->setEnabled(filter.second);
            if (!filter.second)
            {
                a->setToolTip("This filter does not support this type");
            }
            a->setData(filter.first.c_str());
        }
    }

    // show menu
    QAction* selectedItem = treeViewMenu.exec(globalPos);



    if (selectedItem)
    {
        // something was chosen, do stuff
    }
    else
    {
        // nothing was chosen
    }
}

void ObserverWidget::expandFilterSelection(QString filterStr)
{
    //ARMARX_INFO_S << VAROUT(filterStr);
    if (filterStr.length() == 0)
    {
        ui.observerTreeView->collapseAll();
        //        ui.monitoredManagersTree->expandToDepth(1);
    }
    else
    {
        filterExpansionTimer.start(500);
    }
}

void ObserverWidget::delayedFilterExpansion()
{
    InfixFilterModel::ExpandFilterResults(ui.observerTreeView);
}
