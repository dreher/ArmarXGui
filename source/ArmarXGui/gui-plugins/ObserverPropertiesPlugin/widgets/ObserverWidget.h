/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

// Qt
#include "ui_ObserverProperties.h"

#include <QMainWindow>
#include <QStandardItemModel>
#include <QAction>
#include <QTimer>
#include <QWidget>
#include <QPointer>

#include <string>
#include <map>

// ArmarX
#include <ArmarXCore/core/application/Application.h>
#include "../ObserverItemModel.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/InfixFilterModel.h>

namespace armarx
{
    class ObserverWidgetController;
    class PlotterController;
    typedef std::vector<std::string> ObserverList;


    class ObserverWidget : public QWidget
    {
        Q_OBJECT

    public:
        ObserverWidget(ObserverWidgetController* controller);
    signals:
        void createFilterClicked(const std::string& datafieldStr, DatafieldFilterBasePtr filter);
    public slots:
        void createNewModel();
        void treeView_selected(const QItemSelection& selected, const QItemSelection& deselected);
        void treeView_contextMenu(const QPoint& point);
        void updateObservers();
        void toggleAutoUpdate(bool checked);
        void filterSelected();
        void clearDetailedView();
    private slots:
        void expandFilterSelection(QString filterStr);
        void delayedFilterExpansion();
    private:
        Ui::ObserverProperties ui;
        ObserverWidgetController* controller;
        ObserverItemModel* model;
        QPointer<QWidget> propertiesWidget;
        QTimer* updateTimer;
        QAction* autoUpdateAction;
        std::string selectedFilterId;
        InfixFilterModel* proxyModel;
        QTimer filterExpansionTimer;
    };
}

