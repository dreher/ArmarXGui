/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXGui
 * @author     Fabian Paus (fabian dot paus at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Ice/BuiltinSequences.ice>

module armarx
{
    module RemoteGui
    {
        enum ValueType
        {
            eValue_Unknown,

            eValue_Bool,
            eValue_Int,
            eValue_Float,
            eValue_String
        };

        struct Value
        {
            ValueType type = eValue_Unknown;

            int valueInt = 0;
            float valueFloat = 0.0f;
            string valueString;
        };

        dictionary <string, Value> ValueMap;
        dictionary <string, ValueMap> TabValueMap;

        struct WidgetState
        {
            bool hidden = false;
            bool disabled = false;
        };

        dictionary <string, WidgetState> WidgetStateMap;
        dictionary <string, WidgetStateMap> TabWidgetStateMap;

        // Widget definitions

        class Widget;
        sequence <Widget> WidgetSeq;

        class Widget
        {
            string name;
            Value defaultValue;
            WidgetState defaultState;
            WidgetSeq children;
        };

        // Layouts
        class HBoxLayout extends Widget
        {
        };
        class VBoxLayout extends Widget
        {
        };

        class GroupBox extends Widget
        {
            string label;
        };

        // Static elements
        class HSpacer extends Widget
        {
        };
        class VSpacer extends Widget
        {
        };
        class HLine extends Widget
        {
        };
        class VLine extends Widget
        {
        };

        // Widgets with values

        // Bool
        class CheckBox extends Widget
        {
            string label;
        };

        class ToggleButton extends Widget
        {
            string label;
        };

        // Integer
        class IntSpinBox extends Widget
        {
            int min = 0;
            int max = 0;
        };

        class IntSlider extends Widget
        {
            int min = 0;
            int max = 0;
        };

        class Button extends Widget
        {
            string label;
        };

        // Float
        class FloatSpinBox extends Widget
        {
            float min = 0;
            float max = 0;
            int steps = 100;
            int decimals = 3;
        };

        class FloatSlider extends Widget
        {
            float min = 0;
            float max = 0;
            int steps = 100;
        };

        // String
        class Label extends Widget
        {
        };
        class LineEdit extends Widget
        {
        };

        class ComboBox extends Widget
        {
            Ice::StringSeq options;
        };

        dictionary <string, Widget> WidgetMap;

    };

    interface RemoteGuiInterface
    {
        string getTopicName();

        void createTab(string tab, RemoteGui::Widget rootWidget);

        void removeTab(string tab);

        RemoteGui::WidgetMap getTabs();
        RemoteGui::TabWidgetStateMap getTabStates();
        RemoteGui::TabValueMap getValuesForAllTabs();

        void setValue(string tab, string widgetName, RemoteGui::Value value);

        RemoteGui::ValueMap getValues(string tab);
        void setValues(string tab, RemoteGui::ValueMap values);

        RemoteGui::WidgetStateMap getWidgetStates(string tab);
        void setWidgetStates(string tab, RemoteGui::WidgetStateMap widgetState);
    };

    interface RemoteGuiListenerInterface
    {
        void reportTabChanged(string tab);

        void reportTabsRemoved();

        void reportStateChanged(string tab, RemoteGui::ValueMap newValues);

        void reportWidgetChanged(string tab, RemoteGui::WidgetStateMap newState);
    };

};

