/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXGui
 * @author     Raphael Grimm ( raphael dot grimm at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>

module armarx
{
    module WidgetDescription
    {
        class Widget
        {
            bool framed = false;
        };
        sequence<Widget> WidgetSeq;
        dictionary<string,Widget> StringWidgetDictionary;

        //Layouts
        class HBoxLayout extends Widget
        {
            WidgetSeq children;
        };
        class VBoxLayout extends Widget
        {
            WidgetSeq children;
        };
        class FormLayoutElement extends Widget
        {
            string label;
            Widget labelWidget;
            Widget child;
            bool childIsSpanning = false;
        };
        sequence<FormLayoutElement> FormLayoutElementSeq;

        class FormLayout extends Widget
        {
            FormLayoutElementSeq children;
        };
        class GroupBox extends Widget
        {
            string label;
            Widget child;
        };
        //static elements
        class HSpacer extends Widget
        {
        };
        class VSpacer extends Widget
        {
        };
        class HLine extends Widget
        {
        };
        class VLine extends Widget
        {
        };
        class Label extends Widget
        {
            string text;
        };
        //dynamic content elements
        module VariantWidgetContent
        {
            class VariantWidgetContentBase
            {
            };
            class SingleVariant extends VariantWidgetContentBase
            {
                VariantBase content;
            };
            class VariantSeq extends VariantWidgetContentBase
            {
                VariantBaseSeq content;
            };
            class VariantMap extends VariantWidgetContentBase
            {
                StringVariantBaseMap content;
            };
        };
        class VariantWidget extends Widget
        {
            string name;
            VariantWidgetContent::VariantWidgetContentBase content;
        };
        //config elements
        class ConfigWidget extends Widget
        {
            string name;
        };
        class CheckBox extends ConfigWidget
        {
            string label;
            bool defaultValue = false;
        };
        class IntSpinBox extends ConfigWidget
        {
            int min=0;
            int max=0;
            int defaultValue=0;
        };
        class FloatSpinBox extends ConfigWidget
        {
            float min=0;
            float max=0;
            float defaultValue=0;
            int steps = 100;
            int decimals = 3;
        };
        class DoubleSpinBox extends ConfigWidget
        {
            double min=0;
            double max=0;
            double defaultValue=0;
            int steps = 100;
            int decimals = 3;
        };
        class IntSlider extends ConfigWidget
        {
            int min=0;
            int max=0;
            int defaultValue=0;
        };
        class FloatSlider extends ConfigWidget
        {
            float min=0;
            float max=0;
            float defaultValue=0;
        };
        class DoubleSlider extends ConfigWidget
        {
            double min=0;
            double max=0;
            double  defaultValue=0;
        };
        class StringComboBox extends ConfigWidget
        {
            Ice::StringSeq options;
            long defaultIndex = 0;
            bool multiSelect = false;
        };
        class LineEdit extends ConfigWidget
        {
            string defaultValue;
        };
        class DoubleLineEdit extends ConfigWidget
        {
            double  defaultValue=0;
        };
        class FloatLineEdit extends ConfigWidget
        {
            float  defaultValue=0;
        };
    };
};
