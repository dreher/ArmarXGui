/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Gui::application::ArmarXGui
 * @author     Manfred Kroehnert
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXGuiApp.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>

int main(int argc, char* argv[])
{
    armarx::ApplicationPtr app = armarx::Application::createInstance<armarx::ArmarXGuiApp>();
    app->setName("ArmarXGui");

    if (argc > 1)
    {
        if (boost::filesystem::exists(argv[1]))
        {
            std::string configProp = "--ArmarX.GuiConfigFile=";
            configProp += argv[1];
            argv[1] = new char[configProp.size() + 1];
            strcpy(argv[1], configProp.c_str());
        }
    }

    return app->main(argc, argv);
}
