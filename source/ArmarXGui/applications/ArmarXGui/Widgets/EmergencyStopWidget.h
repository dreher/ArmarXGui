/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::EmergencyStop
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/components/EmergencyStop/EmergencyStop.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <IceUtil/UUID.h>

#include <QWidget>
#include <QAction>
#include <QPushButton>
#include <QToolButton>

class QLabel;
class QGridLayout;
class QShortcut;

namespace armarx
{
    class ArmarXMainWindow;

    class EmergencyStopWidget :
        public ArmarXComponentWidgetControllerTemplate<EmergencyStopWidget>,
        public armarx::EmergencyStopListener
    {
        Q_OBJECT
    public:
        explicit EmergencyStopWidget(QWidget* parent = 0, ArmarXMainWindow* mainWindow = 0);
        QWidget* getButtonWidget();

    public slots:
        void clicked(bool = true);

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        std::string getDefaultName() const override;

    private slots:
        void setChecked(const EmergencyStopState);

    private:
        ArmarXMainWindow* mainWindow;
        QGridLayout* layout;
        QPixmap iconNormal;
        QPixmap iconDark;
        QToolButton* button;
        QAction* emergencyStopAction;
        QShortcut* emergencyStopShortcut;

        EmergencyStopMasterInterfacePrx emergencyStopMasterPrx;

        // ArmarXWidgetController interface
    public:
        static QString GetWidgetName()
        {
            return "EmergencyStopWidget";
        }
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        std::string iceNameUUID = IceUtil::generateUUID();
        // EmergencyStopListener interface
    public:
        void reportEmergencyStopState(EmergencyStopState, const Ice::Current&) override;
    };
    typedef IceInternal::Handle <EmergencyStopWidget> EmergencyStopWidgetPtr;
}
