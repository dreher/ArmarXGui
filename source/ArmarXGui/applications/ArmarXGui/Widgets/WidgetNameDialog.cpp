/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WidgetNameDialog.h"

#include "../ArmarXMainWindow.h"

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QGridLayout>

using namespace armarx;


WidgetNameDialog::WidgetNameDialog(QString widgetName, ArmarXMainWindow* parent) :
    QDialog(parent),
    parent(parent)
{
    setWindowTitle("Insert new widget name");
    layout = new QGridLayout(this);
    labelWidgetName = new QLabel("Instance name of widget: ", this);
    editWidgetName = new QLineEdit(widgetName, this);
    layout->addWidget(labelWidgetName, 0, 0);
    layout->addWidget(editWidgetName, 0, 1);
    buttonBox = new QDialogButtonBox(this);
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel | QDialogButtonBox::Ok);
    layout->addWidget(buttonBox, 1, 0, 2, 2);

    connect(editWidgetName, SIGNAL(textChanged(QString)), this, SLOT(checkWidgetName(QString)));
    QObject::connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    resize(350, 80);

}

QString WidgetNameDialog::getWidgetName() const
{
    return editWidgetName->text();
}

bool WidgetNameDialog::checkWidgetName(QString name)
{
    if (parent)
    {
        if (parent->listOpenWidgets.find(name) != parent->listOpenWidgets.end()
            /*|| name == ARMARX_VIEWER_NAME*/ || name.length() == 0)
        {
            QPalette p(editWidgetName->palette());
            p.setColor(QPalette::Base, QColor::fromRgb(255, 120, 120));
            editWidgetName->setPalette(p);
            buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);
            return false;
        }
        else
        {
            QPalette p(editWidgetName->palette());
            p.setColor(QPalette::Base, QColor::fromRgb(120, 255, 120));
            p.setBrush(QPalette::Base, p.light());
            editWidgetName->setPalette(p);
            buttonBox->button(QDialogButtonBox::Ok)->setDisabled(false);
            return true;
        }

    }
    return false;
}
