/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/CoinViewer.h>

#include <QWidget>

#define ARMARX_VIEWER_NAME "Visualization.3D Viewer"

class SoSeparator;

class QComboBox;
class Ui_Viewer3DWidget;
class Ui_ViewerWidgetConfigDialog;

namespace armarx
{
    struct CameraPose
    {
        float position[3];
        float orientation[4];
    };

    /**
     * \brief The Viewer3DWidget class
     */
    class Viewer3DWidget : public armarx::ArmarXWidgetController
    {
        Q_OBJECT

    public:
        explicit Viewer3DWidget(QWidget* parent = 0);
        ~Viewer3DWidget() override;

        // inherited from ArmarXWidgetController
        QString getWidgetName() const override
        {
            return GetWidgetName();
        }
        static QString GetWidgetName()
        {
            return ARMARX_VIEWER_NAME;
        }

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void setMainWindow(QMainWindow* mainWindow) override;

        /*!
         * \brief This mutex is used to protect 3d scene updates.
         * \param mutex3D
         */
        void setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D) override;

        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent = 0) override;

        CoinViewerPtr viewer;
        SoSeparator* emptyNode;
        QPointer<QComboBox> cb3DViewers;
        void setNode(int index);

    public slots:
        void select3DView(int index);
        void sceneListUpdated(QMap<QString, SoNode*> sceneMap);
        void configDialogOpen();
        void configDialogApplySettings();
        void configDialogSaveSettings();
        void configDialogLoadSettings();
        void configDialogPickColor(QColor color = QColor::Invalid);
        void toggleViewingMode();
        void viewAll();

    private slots:
        void initUI();

    private:
        Ui_Viewer3DWidget* ui;
        QMap<QString, SoNode*> sceneMap;
        QPointer<QWidget> __widget;
        std::vector<SoNode*> selectedViews;

        QToolBar* customToolbar;
        QPointer<QDialog> configDialog;
        Ui_ViewerWidgetConfigDialog* configDialogUi;
        QSettings settings;
        QAction* viewingModeAction;

        std::map<QString, CameraPose> initialCameraPoses;

        // ArmarXWidgetController interface
    public:
        QIcon getWidgetIcon() const override
        {
            return GetWidgetIcon();
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/Outline-3D.png");
        }

        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getWidget() override;
    };
    typedef IceUtil::Handle<Viewer3DWidget> Viewer3DWidgetPtr;
}

