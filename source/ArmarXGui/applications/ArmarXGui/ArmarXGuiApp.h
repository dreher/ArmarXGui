/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ArmarXGui
* @author     David Ruscheweyh (david.ruscheweyh at student.kit dot edu)
* @author     Kai Welke (welke at kit dot edu)
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/applications/ArmarXGui/ui_ExceptionDialog.h>

//SoQt
#include <Inventor/Qt/SoQt.h>

//Qt
#include <QApplication>

#include "ArmarXMainWindow.h"

#include <string>



namespace armarx
{
    typedef boost::shared_ptr<QApplication> QApplicationPtr;
    typedef boost::shared_ptr<ArmarXMainWindow> GuiWindowPtr;

    /**
     * ArmarXGuiApp property definition container.
     */
    class ArmarXGuiAppPropertyDefinitions:
        public ApplicationPropertyDefinitions
    {
    public:
        ArmarXGuiAppPropertyDefinitions(std::string prefix):
            ApplicationPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("LoadPlugins", "", "List of paths to GuiPlugin-Libs (semi-colon seperated)");
            defineOptionalProperty<std::string>("GuiConfigFile", "", "Path to config file, that should be loaded on startup");
            defineOptionalProperty<bool>("DisablePreloading", false, "Disables the preloading of widgets. Can be helpful if some preloaded widget crashes the gui.");
        }
    };

    class ArmarXQApplication :
        public  QApplication
    {
        Q_OBJECT
    public:
        ArmarXQApplication(int& argc, char** argv);
        ~ArmarXQApplication() override;
        // QCoreApplication interface
        bool notify(QObject* obj, QEvent* ev) override;
    signals:
        void exceptionCaught(QString exceptionReason);

    public slots:
        void showException(QString exceptionReason);
    protected:
        QDialog exceptionDialog;
        Ui_ExceptionDialog exceptionDialogHandler;
    };


    /**
      \class ArmarXGuiApp
      \brief The main ArmarX gui application.

      Instantiates an armarx::GuiWindow with the following functionality
      \li Gui config saving and loading.
      \li Wraps Qt-based plugin mechanism for gui plugin loading.
      \li Offers an SoQt examiner viewer for rendering 3d data. One ore multiple sources can be handled.

     */
    class ARMARXCOMPONENT_IMPORT_EXPORT ArmarXGuiApp :
        public QObject,
        virtual public armarx::Application,
        virtual public Logging
    {
        Q_OBJECT
    public:
        static int globalargc;
        /**
        * Constructs and initialized an ArmarXGuiApp
        */
        ArmarXGuiApp(int& argc = ArmarXGuiApp::globalargc, char** argv = NULL);
        ~ArmarXGuiApp() override;

        /**
        * Configures the app, sets Qt up
        */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override;

        int run(int argc, char* argv[]) override;
        /**
        * Runs the Qt Event Loop
        */
        int exec(const ArmarXManagerPtr& armarXManager) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new ArmarXGuiAppPropertyDefinitions(getDomainName()));
        }

    private slots:
        void closeRequest_sent();

    private:
        void runArmarXManager();
        int startArmarX();

        RunningTask<ArmarXGuiApp>::pointer_type armarxManagerTask;
        ArmarXQApplication* qApplication;
        ArmarXMainWindow* mainWindow;
        ManagedIceObjectRegistryInterfacePtr registry;

        int argc;
        char** argv;
    };
}

