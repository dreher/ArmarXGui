
armarx_component_set_name(ArmarXGui)

armarx_find_qt(QtCore QtGui QtDesigner)
find_package(Coin3D REQUIRED)
find_package(SoQt REQUIRED)

armarx_build_if(Coin3D_FOUND "Coin3D not available")
armarx_build_if(SOQT_FOUND "SoQt not available")

find_package(OpenMP)

include_directories(SYSTEM ${Coin3D_INCLUDE_DIRS})
include_directories(SYSTEM ${SoQt_INCLUDE_DIRS})

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS ArmarXGuiBase ArmarXCoreInterfaces ArmarXCore ${QT_LIBRARIES} ${Coin3D_LIBRARIES} ${SoQt_LIBRARIES})

set(SOURCES ArmarXGuiApp.cpp
            ArmarXMainWindow.cpp
            Widgets/ViewerWidget.cpp
            Widgets/TitlebarWidget.cpp
            Widgets/WidgetNameDialog.cpp
            Widgets/UseCaseSelectorItem.cpp
            Widgets/GuiUseCaseSelector.cpp
            Widgets/EmergencyStopWidget.cpp
            )

set(HEADERS ArmarXGuiApp.h
            ArmarXMainWindow.h
            Widgets/ViewerWidget.h
            Widgets/TitlebarWidget.h
            Widgets/WidgetNameDialog.h
            Widgets/UseCaseSelectorItem.h
            Widgets/GuiUseCaseSelector.h
            Widgets/EmergencyStopWidget.h
            )

set(GUI_MOC_HDRS ${HEADERS})

set(GUI_UIS
    ArmarXMainWindow.ui
    Widgets/ViewerWidget.ui
    Widgets/ViewerWidgetConfigDialog.ui
    Widgets/ExceptionDialog.ui
    Widgets/UseCaseSelectorItem.ui
    Widgets/GuiUseCaseSelector.ui
)

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} ")
    set(LDFLAGS "${LDFLAGS} ${OpenMP_CXX_FLAGS}")
endif()

armarx_qt_wrap_cpp(SOURCES ${GUI_MOC_HDRS})
armarx_qt_wrap_ui(UI_HEADER ${GUI_UIS})

list(APPEND HEADERS ${UI_HEADER})

include_directories(${UI_HEADER_DIR})

armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp)
armarx_add_component_executable("${EXE_SOURCES}")
