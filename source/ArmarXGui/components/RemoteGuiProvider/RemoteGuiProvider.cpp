/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiProvider
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiProvider.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXGui/libraries/RemoteGui/WidgetHandler.h>


using namespace armarx;


void RemoteGuiProvider::onInitComponent()
{
    topicName = getProperty<std::string>("TopicName").getValue();

    offeringTopic(topicName);
}


void RemoteGuiProvider::onConnectComponent()
{
    topic = getTopic<RemoteGuiListenerInterfacePrx>(topicName);
}


void RemoteGuiProvider::onDisconnectComponent()
{

}


void RemoteGuiProvider::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr RemoteGuiProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RemoteGuiProviderPropertyDefinitions(
            getConfigIdentifier()));
}

std::string RemoteGuiProvider::getTopicName(const Ice::Current&)
{
    return topicName;
}

static void fillRecursively(RemoteGui::WidgetStateMap& widgetStates,
                            RemoteGui::ValueMap& values,
                            RemoteGui::WidgetPtr const& widget)
{
    RemoteGui::WidgetHandler const& handler = RemoteGui::getWidgetHandler(widget);
    if (!handler.isValid(*widget))
    {
        auto descPtr = widget.get();
        throw LocalException() << "Widget is not valid: " << widget->name
                               << ", WidgetT: " << typeid(*descPtr).name()
                               << ", HandlerT: " << typeid(handler).name();
    }

    std::string const& name = widget->name;
    if (!name.empty())
    {
        bool nameIsNew = widgetStates.emplace(widget->name, widget->defaultState).second;
        if (!nameIsNew)
        {
            throw LocalException() << "Widget with name '" << widget->name << "' already exists";
        }
        nameIsNew = values.emplace(widget->name, widget->defaultValue).second;
        ARMARX_CHECK_EXPRESSION(nameIsNew);
    }

    for (auto& child : widget->children)
    {
        fillRecursively(widgetStates, values, child);
    }
}

void RemoteGuiProvider::createTab(const std::string& tabId, const RemoteGui::WidgetPtr& rootWidget, const Ice::Current&)
{
    ARMARX_INFO << "Creating remote tab: " << tabId;
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        tabs[tabId] = rootWidget;

        // Clear old state
        auto& widgetStates = tabWidgetStates[tabId];
        widgetStates.clear();
        auto& values = tabStates[tabId];
        values.clear();

        // Fill default values
        fillRecursively(tabWidgetStates[tabId], tabStates[tabId], rootWidget);
    }

    topic->reportTabChanged(tabId);
}

void RemoteGuiProvider::removeTab(const std::string& tabId, const Ice::Current&)
{
    ARMARX_INFO << "Removing remote tab: " << tabId;
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        tabs.erase(tabId);
        tabWidgetStates.erase(tabId);
        tabStates.erase(tabId);
    }
    topic->reportTabsRemoved();
}

RemoteGui::WidgetMap RemoteGuiProvider::getTabs(const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    return tabs;
}

RemoteGui::TabWidgetStateMap RemoteGuiProvider::getTabStates(const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    return tabWidgetStates;
}

RemoteGui::TabValueMap RemoteGuiProvider::getValuesForAllTabs(const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    return tabStates;
}

RemoteGui::ValueMap RemoteGuiProvider::getValues(const std::string& tabId, const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    return tabStates.at(tabId);
}

void RemoteGuiProvider::setValue(const std::string& tabId, const std::string& widgetName, const RemoteGui::Value& value, const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    RemoteGui::ValueMap& tabState = tabStates.at(tabId);
    tabState.at(widgetName) = value;
    topic->reportStateChanged(tabId, tabState);
}

void RemoteGuiProvider::setValues(const std::string& tabId, const RemoteGui::ValueMap& values, const Ice::Current&)
{
    {
        std::unique_lock<std::mutex> lock(tabMutex);
        tabStates.at(tabId) = values;
    }
    topic->reportStateChanged(tabId, values);
}

RemoteGui::WidgetStateMap RemoteGuiProvider::getWidgetStates(const std::string& tabId, const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    return tabWidgetStates.at(tabId);
}

void RemoteGuiProvider::setWidgetStates(const std::string& tabId, const RemoteGui::WidgetStateMap& state, const Ice::Current&)
{
    std::unique_lock<std::mutex> lock(tabMutex);
    RemoteGui::WidgetStateMap& tabWidgetState = tabWidgetStates.at(tabId);
    tabWidgetState = state;
    topic->reportWidgetChanged(tabId, tabWidgetState);
}

