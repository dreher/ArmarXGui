
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RemoteGuiProvider)
 
armarx_add_test(RemoteGuiProviderTest RemoteGuiProviderTest.cpp "${LIBS}")
