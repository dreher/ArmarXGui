/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiExample
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGuiExample.h"

#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/Storage.h>

#include <ArmarXCore/core/time/CycleUtil.h>

using namespace armarx;


RemoteGuiExample::RemoteGuiExample()
    : runningTask(new RunningTask<RemoteGuiExample>(this, &RemoteGuiExample::run))
{
}

void RemoteGuiExample::onInitComponent()
{
    remoteGuiName = getProperty<std::string>("RemoteGuiName").getValue();
    usingProxy(remoteGuiName);

    mode = getProperty<RemoteGuiExampleMode>("Mode").getValue();

    tabName = "Test";
}


void RemoteGuiExample::onConnectComponent()
{
    remoteGui = getProxy<RemoteGuiInterfacePrx>(remoteGuiName);

    switch (mode)
    {
        case RemoteGuiExampleMode::Widgets:
            createTab_Widgets();
            break;

        case RemoteGuiExampleMode::Events:
            createTab_Events();
            break;
    }

    tab = RemoteGui::TabProxy(remoteGui, tabName);

    runningTask->start();
}


void RemoteGuiExample::onDisconnectComponent()
{
    ARMARX_INFO << "Stopping task";
    if (!runningTask->isStopped())
    {
        runningTask->stop();
    }

    ARMARX_INFO << "Removing tab: " << tabName;
    remoteGui->removeTab(tabName);
}


void RemoteGuiExample::onExitComponent()
{
    onDisconnectComponent();
}

armarx::PropertyDefinitionsPtr RemoteGuiExample::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RemoteGuiExamplePropertyDefinitions(
            getConfigIdentifier()));
}

void RemoteGuiExample::run()
{
    int cycleDurationMs = 20;
    CycleUtil c(cycleDurationMs);
    while (!runningTask->isStopped())
    {
        tab.receiveUpdates();

        RemoteGui::ValueMap values = remoteGui->getValues(tabName);

        switch (mode)
        {
            case RemoteGuiExampleMode::Widgets:
                updateTab_Widgets(tab);
                break;

            case RemoteGuiExampleMode::Events:
                updateTab_Events(tab);
                break;
        }

        tab.sendUpdates();

        c.waitForCycleDuration();
    }
}

using namespace RemoteGui;

void RemoteGuiExample::createTab_Widgets()
{
    // Just add a lot of widgets...
    tabName = "Widgets";

    auto vLayout = makeVBoxLayout();

    {
        WidgetPtr label = makeTextLabel("Line: ");
        WidgetPtr lineEdit = makeLineEdit("Line")
                             .value("Hello");

        WidgetPtr line = makeHBoxLayout()
                         .children({label, lineEdit});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Combo: ");
        WidgetPtr combo = makeComboBox("Combo")
                          .options({"First", "Second", "Third", "Fourth"})
                          .value("Second");

        WidgetPtr line = makeHBoxLayout()
                         .children({label, combo});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Check: ");
        WidgetPtr checkBox = makeCheckBox("Check")
                             .value(true);

        WidgetPtr line = makeHBoxLayout()
                         .children({label, checkBox});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Toggle: ");
        WidgetPtr toggle = makeToggleButton("Toggle")
                           .label("Toggle")
                           .value(true);

        WidgetPtr line = makeHBoxLayout()
                         .children({label, toggle});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Int: ");

        WidgetPtr slider = makeIntSlider("IntSlider")
                           .min(0).max(10)
                           .value(5);

        WidgetPtr spin = makeIntSpinBox("IntSpin")
                         .min(0).max(10)
                         .value(5);

        WidgetPtr line = makeHBoxLayout()
                         .children({label, slider, spin});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Float: ");

        WidgetPtr slider = makeFloatSlider("FloatSlider")
                           .min(0.0f).max(2.0f)
                           .value(0.0f);

        WidgetPtr spin = makeFloatSpinBox("FloatSpin")
                         .min(0.0f).max(2.0f)
                         .steps(20).decimals(2)
                         .value(0.4f);

        WidgetPtr line = makeHBoxLayout()
                         .children({label, slider, spin});

        vLayout.addChild(line);
    }

    {
        WidgetPtr label = makeTextLabel("Button: ");
        WidgetPtr button = makeButton("Button")
                           .label("Button");

        WidgetPtr line = makeHBoxLayout()
                         .children({label, button});

        vLayout.addChild(line);
    }

    vLayout.addChild(new VSpacer);

    WidgetPtr groupBox = makeGroupBox("GroupBox")
                         .label("Group")
                         .child(vLayout);

    remoteGui->createTab(tabName, groupBox);
}

void RemoteGuiExample::updateTab_Widgets(TabProxy& tab)
{
    // Sync the slider and the spin box manually (slider is the master)
    ValueProxy<int> intSlider = tab.getValue<int>("IntSlider");
    ValueProxy<int> intSpin = tab.getValue<int>("IntSpin");

    int intSliderValue = intSlider.get();
    intSpin.set(intSliderValue);

    // And the other way around for the float slider/spin box
    ValueProxy<float> floatBox = tab.getValue<float>("FloatSpin");
    ValueProxy<float> floatSlider = tab.getValue<float>("FloatSlider");

    float floatBoxValue = floatBox.get();
    floatSlider.set(floatBoxValue);
}

void RemoteGuiExample::createTab_Events()
{
    tabName = "Events";

    auto vLayout = makeVBoxLayout();

    {
        WidgetPtr label = makeTextLabel("Counter: ");
        WidgetPtr spin = makeIntSpinBox("IntSpin")
                         .min(-10).max(10)
                         .value(0);

        WidgetPtr line = makeHBoxLayout()
                         .children({label, spin});

        vLayout.addChild(line);
    }

    {
        WidgetPtr buttonUp = makeButton("UpButton")
                             .label("Up");
        WidgetPtr buttonDown = makeButton("DownButton")
                               .label("Down");

        WidgetPtr line = makeHBoxLayout()
                         .children({buttonUp, buttonDown});

        vLayout.addChild(line);
    }

    vLayout.addChild(new VSpacer);

    WidgetPtr groupBox = makeGroupBox("GroupBox")
                         .label("Group")
                         .child(vLayout);

    remoteGui->createTab(tabName, groupBox);
}

void RemoteGuiExample::updateTab_Events(TabProxy& tab)
{
    // Detect button clicks and update the counter accordingly
    ValueProxy<int> intSpin = tab.getValue<int>("IntSpin");
    ButtonProxy upButton = tab.getButton("UpButton");
    ButtonProxy downButton = tab.getButton("DownButton");

    int currentValue = intSpin.get();
    bool upClicked = upButton.clicked();
    bool downClicked = downButton.clicked();

    if (upClicked)
    {
        ++currentValue;
    }
    if (downClicked)
    {
        --currentValue;
    }

    remoteGui->setValue(tabName, "IntSpin", makeValue(currentValue));
}

