/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::RemoteGuiExample
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

namespace armarx
{
    enum class RemoteGuiExampleMode
    {
        Widgets,

        Events
    };

    /**
     * @class RemoteGuiExamplePropertyDefinitions
     * @brief
     */
    class RemoteGuiExamplePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RemoteGuiExamplePropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote GUI provider");

            defineOptionalProperty<RemoteGuiExampleMode>("Mode", RemoteGuiExampleMode::Widgets, "Select which example to execute")
            .map("Widgets", RemoteGuiExampleMode::Widgets)
            .map("Events", RemoteGuiExampleMode::Events);
        }
    };

    /**
     * @defgroup Component-RemoteGuiExample RemoteGuiExample
     * @ingroup ArmarXGui-Components
     * A description of the component RemoteGuiExample.
     *
     * @class RemoteGuiExample
     * @ingroup Component-RemoteGuiExample
     * @brief Brief description of class RemoteGuiExample.
     *
     * Detailed description of class RemoteGuiExample.
     */
    class RemoteGuiExample :
        virtual public armarx::Component
    {
    public:
        RemoteGuiExample();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RemoteGuiExample";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void run();

        void createTab_Widgets();
        void updateTab_Widgets(RemoteGui::TabProxy& tab);

        void createTab_Events();
        void updateTab_Events(RemoteGui::TabProxy& tab);

    private:
        std::string remoteGuiName;
        RemoteGuiInterfacePrx remoteGui;

        RemoteGuiExampleMode mode;

        std::string tabName;
        RemoteGui::TabProxy tab;

        RunningTask<RemoteGuiExample>::pointer_type runningTask;
    };
}
